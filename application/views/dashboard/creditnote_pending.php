<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h2>Credit Note Pending</h2>
        </div>            
        <div class="col-md-6 col-sm-12 text-right">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= base_url() ?>"><i class="icon-home"></i></a></li>
              
                <li class="breadcrumb-item active">Credit Note Pending</li>
            </ul>
            
        </div>
    </div>
</div>
<div class="container-fluid">
   
    <div class="row clearfix">
       
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <h2>Credit Note Pending</h2>
                </div>
                <div class="body">
        
					<div class="table-responsive">
                        <table class="table table-bordered table-hover js-basic-example dataTable table-custom" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Invoice No.</th>
                                    <th>Company</th>
                                    <th>Reason of Return</th>
                                    <th>Return Date</th>
                                    <th>Status</th>
									
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Invoice No.</th>
                                    <th>Company</th>
                                    <th>Reason of Return</th>
                                    <th>Return Date</th>
                                    <th>Status</th>
                                   
                                </tr>
                            </tfoot>
                            <tbody>
                                <tr>
                                    <td>123456</td>
                                    <td>United Pvt Ltd</td>
                                    <td>Short Expiry</td>
                                    <td>02/04/2021</td>
                                    <td><button class="btn btn-sm round btn-outline-success">Credit Note Due</button></td>
                                </tr>
                              
                               
                               
                            </tbody>
                        </table>
					</div><br/>
	
                </div>
            </div>
        </div>
    </div>

					
</div>