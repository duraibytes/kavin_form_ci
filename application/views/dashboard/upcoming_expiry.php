<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h2>Upcoming Expiry</h2>
        </div>            
        <div class="col-md-6 col-sm-12 text-right">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= base_url() ?>"><i class="icon-home"></i></a></li>
              
                <li class="breadcrumb-item active">Upcoming Expiry</li>
            </ul>
            
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row clearfix">
       
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <h2>Upcoming Expiry</h2>
                </div>
                <div class="body">
        
					<div class="table-responsive">
                        <table class="table table-bordered table-hover js-basic-example dataTable table-custom" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Invoice No.</th>
                                    <th>Code</th>
                                    <th>Variety</th>
                                    <th>Category</th>
                                    <th>Description</th>
                                    <th>Packing 1bag=kg</th>
                                    <th>Lot</th>
                                    <th>Unit Kgs/Grams</th>
                                    <th>Net Rate</th>
                                    <th>Date of Expiry</th>
                                    <th>Net Value</th>
                                    <th>Quantity</th>
									
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                   <th>Invoice No.</th>
                                    <th>Code</th>
                                    <th>Variety</th>
                                    <th>Category</th>
                                    <th>Description</th>
                                    <th>Packing 1bag=kg</th>
                                    <th>Lot</th>
                                    <th>Unit Kgs/Grams</th>
                                    <th>Net Rate</th>
                                    <th>Date of Expiry</th>
                                    <th>Net Value</th>
                                    <th>Quantity</th>
                                   
                                </tr>
                            </tfoot>
                            <tbody>
                                <tr>
                                    <td>123456</td>
                                    <td>PAC-501</td>
                                    <td>Hybrid javar</td>
                                    <td>Field Crop</td>
                                    <td>Research Jpwar</td>
                                    <td>3kgs Bag</td>
                                    <td>068015454</td>
                                    <td>3kg</td>
                                    <td>Rs.120.00</td>
                                    <td><button class="btn btn-sm round btn-outline-success">30/12/2020</button></td>
                                    <td>Rs. 162,200,00</td>
                                    <td>100kg</td>
                                </tr>
                              
                               
                               
                            </tbody>
                        </table>
					</div><br/>
	
                </div>
            </div>
        </div>
    </div>
</div>