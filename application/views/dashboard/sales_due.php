<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h2>Sale Payment Due</h2>
        </div>            
        <div class="col-md-6 col-sm-12 text-right">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= base_url() ?>"><i class="icon-home"></i></a></li>
              
                <li class="breadcrumb-item active">Sale Payment Due</li>
            </ul>
            
        </div>
    </div>
</div>
<div class="container-fluid">
   
    <div class="row clearfix">
       
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <h2>Sale Payment Due</h2>
                </div>
                <div class="body">
        
					<div class="table-responsive">
                        <table class="table table-bordered table-hover js-basic-example dataTable table-custom" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Invoice No.</th>
                                    <th>Variety</th>
                                    <th>Category</th>
                                    <th>Description</th>
                                    <th>Packing 1 bag=kg</th>
                                    <th>Lot</th>
                                    <th>Unit kgs / Grams</th>
                                    <th>Net Rate</th>
                                    <th>Date of Expiry</th>
                                    <th>Net Value</th>
                                    <th>Status</th>
									
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                   <th>Invoice No.</th>
                                    <th>Variety</th>
                                    <th>Category</th>
                                    <th>Description</th>
                                    <th>Packing 1 bag=kg</th>
                                    <th>Lot</th>
                                    <th>Unit kgs / Grams</th>
                                    <th>Net Rate</th>
                                    <th>Date of Expiry</th>
                                    <th>Net Value</th>
                                    <th>Status</th>
                                   
                                </tr>
                            </tfoot>
                            <tbody>
                                <tr>
                                    <td>123456</td>
                                    <td>Hybrid jowar</td>
                                    <td>Field Crop</td>
                                    <td>Research jowar</td>
                                    <td>3 kgs Bag</td>
                                    <td>06843Jdf34343</td>
                                    <td>3Kg</td>
                                    <td>Rs.120.00</td>
                                    <td>30/12/2021</td>
                                    <td>Rs.61,200.00</td>
                                    <td><button class="btn btn-sm round btn-outline-success">UnPaid</button></td>
                                </tr>
                               
                            </tbody>
                        </table>
					</div><br/>
	
                </div>
            </div>
        </div>
    </div>
					
</div>