<style>
.box{
   transition: 1s;

    }
    .box:hover{
        transform: scale(1.1);
        z-index: 1;
        
    }
</style>

<div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h2>Dashboard</h2>
                </div>            
            
            </div>
        </div>
        <div class="container-fluid">

            <div class="row clearfix">
                <div class="col-lg-3 col-md-6 box">
                    <div class="card top_counter" style="box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);">
                        <div class="body"> 
                           <div class="icon">
                                <img src="<?= base_url() ?>assets/images/expired.png" style="height: 40px;padding-top:10px;">
                            </div>
                            <a href="<?= base_url()?>upcoming-expiry" class="text-dark">
                                <div class="content ">
                                    <div class="text" style="font-weight: 800;font-size: 16px;">
                                        <span class="badge badge-danger float-right" style="font-size: 25px;padding: 10px;margin-right: 0px;">08</span>
                                        Expiring<br/>Goods
                                         
                                    </div>                            
                                </div>
                            </a>
                            
                        </div>                        

                    </div>
                </div>
                <div class="col-lg-3 col-md-6 box">
                    <div class="card top_counter" style="box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);">
                        <div class="body"> 
                           <div class="icon">
                                 <img src="<?= base_url() ?>assets/images/pending.png" style="height: 40px;padding-top:10px;">
                                 
                            </div>
                            <a href="<?= base_url()?>creditnote-pending" class="text-dark">
                                <div class="content">
                                    <div class="text" style="font-weight: 800;font-size: 16px;">
                                        <span class="badge badge-danger float-right" style="    padding: 10px;font-size: 25px;margin-right: 0px;">25</span>Pending <br/>Payments
                                         
                                    </div>                            
                                </div>
                            </a>
                            
                        </div>                        

                    </div>
                </div>
                <div class="col-lg-3 col-md-6 box">
                    <div class="card top_counter" style="box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);">
                        <div class="body"> 
                           <div class="icon">
                               <img src="<?= base_url() ?>assets/images/pencreditdebit.png" style="height: 40px;padding-top:10px;">
                            </div>
                            <a href="<?= base_url()?>purchase-due" class="text-dark">
                                <div class="content">
                                    <div class="text" style="font-weight: 800;font-size: 16px;">
                                         <span class="badge badge-danger float-right" style="font-size: 25px;padding: 10px;margin-right: 0px;">15</span>
                                         Pending Credit / Debit Notes
                                        
                                    </div>                            
                                </div>
                            </a>
                            
                        </div>                        

                    </div>
                </div>
                <div class="col-lg-3 col-md-6 box">
                    <div class="card top_counter" style="box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);">
                        <div class="body"> 
                            
                           <div class="icon">
                               <img src="<?= base_url() ?>assets/images/cheque.png" style="height: 40px;padding-top:10px;">
                            </div>
                            <a href="<?= base_url()?>sales-due" class="text-dark">
                                <div class="content">
                                    <div class="text" style="font-weight: 800;font-size: 16px;">
                                        <span class="badge badge-danger float-right" style="font-size: 25px;padding: 10px;margin-right: 0px;">10</span>
                                        Cheques Issued
                                        
                                    </div>                            
                                </div>
                            </a>
                            
                        </div>                        

                    </div>
                </div>
                <div class="col-lg-3 col-md-6 ">
                    <div class="card top_counter box purchase-invoice-bg" style="font-size: 20px;">
                        <div class="body"> 
                            
                             <div class="icon text-success" style="display: flex;align-items: center;    justify-content: center;">
                                <img src="<?= base_url() ?>assets/images/bill.png" style="height: 36px;">
                            </div>
                            <a href="<?= base_url()?>purchase" class="text-white">
                                <div class="content" style="display: flex;align-items: center;">
                                    <div class="text" style="font-weight: 800;">Purchase Invoices</div>
                                    
                                </div>
                            </a>
                        </div>                        
                    </div>
                    
                    <div class="card top_counter purchase-payment-bg box" style="background: linear-gradient(#0f9f1f 0%, #0f9f1f 100%);font-size: 20px;">
                        <div class="body">
                            <a href="<?= base_url()?>purchase-payment" class="text-white">
                                <div class="icon text-success" style="display: flex;align-items: center;    justify-content: center;">
                                    <img src="<?= base_url() ?>assets/images/payment-method.png" style="height: 36px;">
                                </div>
                                <div class="content" style="display: flex;align-items: center;">
                                    <div class="text" style="font-weight: 800;">Purchase Payments</div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="card top_counter box" style="background: linear-gradient( #efa800 0%, #efa800 100%);font-size: 20px;">
                        <div class="body">
                            <a href="<?= base_url()?>purchase-return" class="text-white">
                                <div class="icon text-success" style="display: flex;align-items: center;    justify-content: center;">
                                    <img src="<?= base_url() ?>assets/images/purchase-return.png" style="height: 36px;">
                                </div>
                                <div class="content" style="display: flex;align-items: center;">
                                    <div class="text" style="font-weight: 800;">Purchase Returns</div>
                                </div>
                            </a>
                        </div>
                    </div>
                          <div class="card top_counter box" style="background: linear-gradient( #10a0b7, #10a0b7, #10a0b7);font-size: 20px;">
                        <div class="body">
                            <a href="#" class="text-white">
                                <div class="icon text-success" ><i class="fa fa-database"></i> </div>
                                <div class="content" style="display: flex;align-items: center;">
                                    <div class="text" style="font-weight: 800;">Profit & Loss</div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="card top_counter box" style="background: linear-gradient(#2b5be4 0%, #2b5be4 100%);font-size: 20px;">
                            <div class="body">
                                 <div class="icon text-success" style="display: flex;align-items: center;    justify-content: center;">
                                    <img src="<?= base_url() ?>assets/images/invoices.png" style="height: 36px;">
                                </div>
                                <a href="<?= base_url()?>sale" class="text-white">
                                    <div class="content" style="display: flex;align-items: center;">
                                        <div class="text" style="font-weight: 800;">Sales Invoices</div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="card top_counter box" style="background: linear-gradient(#06901d, #06901d, #06901d);font-size: 20px;">
                            <div class="body">
                                <a href="<?= base_url()?>sale-payment" class="text-white">
                                    <div class="icon text-success" style="display: flex;align-items: center;    justify-content: center;">
                                        <img src="<?= base_url() ?>assets/images/hand.png" style="height: 36px;">
                                    </div>
                                    <div class="content" style="display: flex;align-items: center;">
                                        <div class="text" style="font-weight: 800;">Sales Payments</div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="card top_counter box" style="background: linear-gradient(#ee2d27,#ee2d27, #ee2d27);font-size: 20px;">
                            <div class="body">
                                <a href="<?= base_url()?>sale-return" class="text-white">
                                    <div class="icon text-success" style="display: flex;align-items: center;    justify-content: center;">
                                        <img src="<?= base_url() ?>assets/images/payment.png" style="height: 36px;">
                                    </div>
                                    <div class="content" style="display: flex;align-items: center;">
                                        <div class="text" style="font-weight: 800;">Sales Returns</div>
                                    </div>
                                </a>
                            </div>
                        </div>
                       <div class="card top_counter company-settings-bg box" style="background: linear-gradient(#a41794,#a41794, #a41794);font-size: 20px;">
                            <div class="body">
                                <a href="<?= base_url() ?>company-settings" class="text-white" >
                                     <div class="icon text-success" style="display: flex;align-items: center;    justify-content: center;">
                                        <img src="<?= base_url() ?>assets/images/invoices.png" style="height: 36px;">
                                    </div>
                                    
                                    <div class="content" style="display: flex;align-items: center;">
                                        <div class="text" style="font-weight: 800;">Company Settings</div>
                                    </div>
                                    
                                </a>
                            </div>
                        </div>
                    </div>

                <div class="col-lg-3 col-md-6">
                  <div class="card top_counter box" style="background: linear-gradient( #12a5b3, #12a5b3, #12a5b3);font-size: 20px;">
                        <div class="body">
                            <a href="<?= base_url()?>inventory" class="text-white">
                                <div class="icon text-success" ><i class="fa fa-database"></i> </div>
                                <div class="content" style="display: flex;align-items: center;">
                                    <div class="text" style="font-weight: 800;">Stock</div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="card top_counter box" style="background: linear-gradient( #f09c12, #f09c12, #f09c12);font-size: 20px;">
                        <div class="body">
                            <a href="<?= base_url()?>expense" class="text-white">
                                <div class="icon text-success" style="display: flex;align-items: center;    justify-content: center;">
                                    <img src="<?= base_url() ?>assets/images/accounting.png" style="height: 36px;">
                                </div>
                                <div class="content" style="display: flex;align-items: center;">
                                    <div class="text" style="font-weight: 800;">Expenses</div>
                                </div>
                            </a>
                        </div>
                    </div>
                     <div class="card top_counter box" style="background: linear-gradient( #19c64b, #19c64b, #19c64b);font-size: 20px;">
                        <div class="body">
                            <div class="icon text-success" style="display: flex;align-items: center;    justify-content: center;">
                                    <img src="<?= base_url() ?>assets/images/lending.png" style="height: 36px;">
                                </div>
                            <a href="<?= base_url()?>transaction" class="text-white">
                                <div class="content" style="display: flex;align-items: center;">
                                    <div class="text" style="font-weight: 800">Bank Transaction</div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="card top_counter box" style="background: linear-gradient( #582bc3, #582bc3, #582bc3);font-size: 20px;">
                        <div class="body">
                            <a href="<?= base_url()?>report" class="text-white">
                                <div class="icon text-info"><i class="fa  fa-file-text-o"></i> </div>
                                <div class="content" style="display: flex;align-items: center;">
                                    <div class="text" style="font-weight: 800;">Reports</div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
               
                 <div class="col-lg-3 col-md-6">
                    <div class="card top_counter audit-bg box" style="background: linear-gradient( #fb1865, #fb1865, #fb1865);font-size: 20px;">
                        <div class="body">
                            <a href="#" class="text-white">
                                <div class="icon text-success" style="display: flex;align-items: center;    justify-content: center;">
                                    <img src="<?= base_url() ?>assets/images/accounting.png" style="height: 36px;">
                                </div>
                                <div class="content" style="display: flex;align-items: center;">
                                    <div class="text" style="font-weight: 800;">Audit Logs & Taxes</div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="card top_counter box" style="background: linear-gradient( #de4a1f, #de4a1f, #de4a1f);font-size: 20px;">
                        <div class="body">
                            <a href="#" class="text-white">
                                <div class="icon text-success" style="display: flex;align-items: center;    justify-content: center;">
                                    
                                </div>
                                <div class="content" style="display: flex;align-items: center;">
                                    <div class="text" style="font-weight: 800;"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="card top_counter box" style="background: linear-gradient(#065210, #065210, #065210);font-size: 20px;">
                        <div class="body">
                            <a href="#" class="text-white">
                                <div class="icon text-success" style="display: flex;align-items: center;    justify-content: center;">
                                </div>
                                <div class="content" style="display: flex;align-items: center;">
                                    <div class="text" style="font-weight: 800;"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                       <div class="card top_counter box" style="background: linear-gradient( #6b685e, #6b685e, #6b685e);font-size: 20px;">
                        <div class="body">
                            <a href="#" class="text-white">
                                <div class="icon text-success" style="display: flex;align-items: center;    justify-content: center;">
                                </div>
                                <div class="content" style="display: flex;align-items: center;">
                                    <div class="text" style="font-weight: 800;"> </div>
                                </div>
                            </a>
                        </div>
                    </div>
                     
                </div>
                
            </div>

        </div>