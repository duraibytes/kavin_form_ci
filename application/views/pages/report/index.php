<div class="block-header">
    <?= $this->load->view( 'layout/_nav_header', '', true )?>
</div> 
    <div class="container-fluid">            

        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                        <div class="row">
                            <div class="col-lg-3 col-md-6">
                                <div class="form-group">
                                   <select class="form-control" name="item" id="item">
                                       <option value=" " > -- select Type  -- </option>
                                       <option value="1"> Purchase </option>
                                       <option value="2s"> Purchase Return </option>
                                       <option value="2s"> Sale </option>
                                       <option value="2s"> Sale Return </option>
                                       <option value="2s"> Expense </option>
                                       <option value="2s"> Bank Transactin </option>
                                   </select>
                               </div>
                            </div>
                            <div class="col-lg-6 col-md-12">
                                <div class="input-daterange input-group" data-provide="datepicker">
                                    <input type="text" class="input-sm form-control" name="start">
                                    <span class="input-group-addon range-to">to</span>
                                    <input type="text" class="input-sm form-control" name="end">
                                </div>
                            </div>
                           
                            <div class="col-lg-3 col-md-6">
                                <div class="form-group">
                                    <a href="javascript:;" class="btn report-bg text-white"> Get Report </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="body">
                        <hr>
                       <div class="table-responsive">
                        <table class="table table-bordered table-hover js-basic-example dataTable table-custom">
                            <thead>
                                <tr class="report-bg text-white">
                                    <th> Item  </th>
                                    <th> Description </th>
                                    <th> Category  </th>
                                    <th> Invoice No </th>
                                    <th> Expiry Date </th>
                                    <th> Date Of Packing </th>
                                    <th> Date Of Tested </th>
                                    <th> Location </th>
                                    <th> Quantity </th>
                                </tr>
                            </thead>
                           
                            <tbody>
                                
                                <tr>
                                    <td>Shad Decker</td>
                                    <td>Regional Director</td>
                                    <td>Edinburgh</td>
                                    <td> 678</td>
                                    <td> 12/08/2021 </td>
                                    <td> 01/04/2021</td>
                                    <td> 05/04/2021 </td>
                                    <td> West </td>
                                    <td> 250 </td>
                                </tr>
                                <tr>
                                    <td>Shad Decker</td>
                                    <td>Regional Director</td>
                                    <td>Edinburgh</td>
                                    <td> 678</td>
                                    <td> 12/08/2021 </td>
                                    <td> 01/04/2021</td>
                                    <td> 05/04/2021 </td>
                                    <td> West </td>
                                    <td> 250 </td>
                                </tr>
                                <tr>
                                    <td>Shad Decker</td>
                                    <td>Regional Director</td>
                                    <td>Edinburgh</td>
                                    <td> 678</td>
                                    <td> 12/08/2021 </td>
                                    <td> 01/04/2021</td>
                                    <td> 05/04/2021 </td>
                                    <td> West </td>
                                    <td> 250 </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
