<div class="row clearfix">
    <div class="col-lg-6 col-md-12">
    	<div class="inline-class" >
            <div class="w-50 vcenter"> 
                Home Currency
            </div>
            <div class="w-50 vcenter"> 
                <input type="text" name="home_currency" id="home_currency" class="form-control" value="<?= $company_info->home_currency ?? '' ?>">
        	</div>
		</div>
        
	</div>
	<!-- second column  -->
	<div class="col-lg-6 col-md-12">
       <div class="inline-class" >
            <div class="w-50 vcenter"> 
                Units Length
            </div>
            <div class="w-50 vcenter"> 
                <input type="text" name="unit_length" id="unit_length" class="form-control" value="<?= $company_info->unit_length ?? '' ?>">
            </div>
        </div>
        <div class="inline-class" >
            <div class="w-50 vcenter"> 
               Units Weight
            </div>
            <div class="w-50 vcenter"> 
                <input type="text" name="unit_weight" id="unit_weight" class="form-control" value="<?= $company_info->unit_weight ?? '' ?>">
            </div>
        </div>
	</div>
		
</div>
<div class="row clearfix" style="text-align:right;">
    <div class="col-lg-12 col-md-12 m-t-20">
        <button id="addToTable" class="btn btn-sm btn-success" type="button" onclick="return update_company_setting( 'currency' );">
            Update
        </button>
    </div>
</div>