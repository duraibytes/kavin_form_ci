<div class="block-header">
    <?= $this->load->view( 'layout/_nav_header', '', true )?>
</div> 
<style type="text/css">
  .nav-link.active {
      /*background: #a1ef29 !important;*/
      background: linear-gradient(#2b5be4 0%, #00b8ff 100%);
      color:white !important;
  }
  
    .dropify-wrapper {
        height: 250px !important;
    }

</style>

<div class="container-fluid">
  <div class="row clearfix">
    <div class="col-lg-12">
      <div class="card">
          <ul class="nav nav-tabs">
              <li class="nav-item">
                <a class="nav-link active show" data-toggle="tab" href="#Home-withicon" onclick="return get_ajax_own_info( 'company' );">
                COMPANY INFO </a></li>
                <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#Bank-withicon" onclick="return get_ajax_own_info( 'bank' );">
                BANK </a></li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#Profile-withicon" onclick="return get_ajax_own_info( 'invoice' );"> INVOICE </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#currency-withicon" onclick="return get_ajax_own_info( 'currency' );"> 
                  CURRENCIES
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#payterms-withicon" onclick="return get_ajax_own_info( 'terms' );"> 
                  PAYMENT TERMS
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#price-withicon" onclick="return get_ajax_own_info( 'price' );"> 
                  TAX
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#year-withicon" onclick="return get_ajax_own_info( 'year' );"> 
                  PHYSICAL YEAR
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#CAT-withicon" onclick="return get_ajax_own_info( 'category' );"> 
                  CATEGORIES
                </a>
              </li>
          </ul>
          <?php echo form_open_multipart( '', 'id="setting_form"' ); ?>
          <div class="body" id="add_tab">
            <div class="tab-content">
                <div class="tab-pane show active" id="own_info">
                   <?= $this->load->view( 'pages/settings/_company_info', '', true ) ?>
                </div>
            </div>
          </div>
        <?= form_close()?>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  function update_company_setting( type ) {
        var setting_form           = $( '#setting_form' ).find('input, textarea, button, select');
        var data      = new FormData( $( '#setting_form' )[0]);
        data.append( 'tab', type );

        $.ajax({
            type        : 'POST',
            url         : '<?=site_url( 'company_settings/update_company_setting' );?>',
            data        : data,
            async       : false,
            contentType : false,
            processData : false,
            dataType    : 'json',
            beforeSend: function () {
                setting_form.attr( 'disabled', 'disabled' );
            },    
            success     : function( msg ) {
                if( msg.error == 1 ) {
                    toastr.error( msg.error_msg, 'Company Settings' );
                    setting_form.attr( 'disabled', false );
                } else {
                    toastr.success( msg.error_msg, 'Company Settings' );
                    setTimeout( function(){
                        window.location.href = '<?= site_url()?>company-settings';
                    }, 300 );
                }
            }
        });
        return false;
    }

    function get_ajax_own_info( type = '' ) {
          var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&type=' + type;
          $.ajax( {
              type    : "POST",
              url     : '<?= site_url( 'company_settings/get_ajax_own_info' ); ?>',
              data    : data,
              success: function( msg ) {
                  $( '#own_info' ).html( msg );
              }
          });
          return false;
      }

       var arrHead = new Array(); // array for header.
      arrHead = ['', 'Employee Name', 'Designation', 'Age'];

      // first create TABLE structure with the headers. 
      function createTable() {
          var empTable = document.createElement('table');
          empTable.setAttribute('id', 'empTable'); // table id.

          var tr = empTable.insertRow(-1);
          for (var h = 0; h < arrHead.length; h++) {
              var th = document.createElement('th'); // create table headers
              th.innerHTML = arrHead[h];
              tr.appendChild(th);
          }

          var div = document.getElementById('cont');
          div.appendChild(empTable);  // add the TABLE to the container.
      }

        // now, add a new to the TABLE.
        function addRow() {
            var empTab = document.getElementById('empTable');

            var rowCnt = empTab.rows.length;   // table row count.
            var tr = empTab.insertRow(rowCnt); // the table row.
            tr = empTab.insertRow(rowCnt);

            for (var c = 0; c < arrHead.length; c++) {
                var td = document.createElement('td'); // table definition.
                td = tr.insertCell(c);

                if (c == 0) {      // the first column.
                    // add a button in every new row in the first column.
                    var button = document.createElement('input');

                    // set input attributes.
                    button.setAttribute('type', 'button');
                    button.setAttribute('value', 'Remove');

                    // add button's 'onclick' event.
                    button.setAttribute('onclick', 'removeRow(this)');

                    td.appendChild(button);
                }
                else {
                    // 2nd, 3rd and 4th column, will have textbox.
                    var ele = document.createElement('input');
                    ele.setAttribute('type', 'text');
                    ele.setAttribute('value', '');

                    td.appendChild(ele);
                }
            }
        }

        // delete TABLE row function.
        function removeRow(oButton) {
            var empTab = document.getElementById('empTable');
            empTab.deleteRow(oButton.parentNode.parentNode.rowIndex); // button -> td -> tr.
        }

    
</script>