<style type="text/css">
	input:focus {
     box-shadow: none !important;
}
</style>
<div class="row clearfix">
	<div class="col-sm-6 offset-sm-3">
		<table class="table w-100" id="category_table">
			<tr>
				<th class="text-left w-95" colspan=""> Category Name </th>
				
				<td class="w-10"></td>
			</tr>
			<?php
			if( isset( $cat_info ) && !empty( $cat_info ) ) {
				$i = 1;
				foreach ( $cat_info as $key => $value ) {
			?>
			<tr>
				<td > 
					<div class="inline-class" >
						<input type="text" name="category[]" id="category" class="form-control w-70" value="<?= $value->name ?>" placeholder="Category" > 
					</div>
				</td>
				<td  class="text-left"> 
					<?php if( $i != 1 ) { ?>
						<a href="javascipt:;" class="btn btn-sm btn-outline-danger" onclick="deleteRow(this)">
							<i class="fa fa-close"></i>
						</a>
					<?php } ?>
				</td>
			</tr>
			<?php  $i++; } } else  { ?>
				<tr>
					<td > 
						<div class="inline-class" >
							<input type="text" name="category[]" id="category" class="form-control w-70" value="" placeholder="Category" > 
						</div>
					</td>
					
					<td  class="text-left"> 
						
					</td>
				</tr>
			<?php } ?>
		</table>
		<div id="add_new" class="btn btn-outline-success w-100">ADD NEW</div>
	</div>
</div>
<div class="row clearfix" style="text-align:right;">
    <div class="col-lg-12 col-md-12 m-t-20">
        <button id="addToTable" class="btn btn-sm btn-success" type="button" onclick="return update_company_setting( 'category' );">
            Update
        </button>
    </div>
</div>


<script type="text/javascript">

	$("#add_new").click(function(){
            
        var markup = '<tr><td><div class="inline-class" ><input type="text" name="category[]" id="category" class="form-control w-70" value="" placeholder="Category" ></div></td><td class="text-left"><a href="javascipt:;" class="btn btn-sm btn-outline-danger " onclick="deleteRow(this)"><i class="fa fa-close"></i></a></td></tr>';
        $( "table tbody" ).append( markup );

    });

    function deleteRow( button ) {
    	var row = button.parentNode.parentNode;
        var name = row.getElementsByTagName("TD")[0].innerHTML;
        //Get the reference of the Table.
        var table = document.getElementById("category_table");
        //Delete the Table row using it's Index.
        table.deleteRow(row.rowIndex);
    }
</script>