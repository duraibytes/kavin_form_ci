<style type="text/css">
	input:focus {
     box-shadow: none !important;
}
</style>
<div class="row clearfix">
	<div class="col-sm-12">
		<table class="table" id="tax_table">
			<tr>
				<td class="text-left w-70" colspan="3"> Pricing Name </td>
				<td class="text-left w-20"> Currency </td>
				<td class="w-10"></td>
			</tr>
			<?php 
			
			$count 			= 0;
			if( isset( $tax_info ) && !empty( $tax_info ) ) {
				
				foreach ( $tax_info as $key => $value ) {
					
				?>
			<tr>
				<td class=""> 
					<div class="inline-class" >
						<input type="text" name="price_name_<?= $count ?>" id="price_name_<?= $count ?>" class="form-control w-70" placeholder="Dealer Price" value="<?= $value->pricing_name ?? '' ?>" > 
					</div>
				</td>
				<td > 
					<div class="inline-class" >
						<label class="fancy-checkbox custom-color-green">
                        	<input name="is_tax_<?= $count ?>" value="yes" type="checkbox" <?php if( $value->is_tax_included ){ echo 'checked'; }?> >
                        	<span><i></i> Tax Include </span>
                    	</label>
                    </div>
				</td>
				<td > 
					<div class="inline-class" >
                    	<input type="text" name="tax_percent_<?= $count ?>" id="tax_percent_<?= $count ?>" class="form-control " style="width: 80px;" placeholder="Tax %" value="<?= $value->tax_percent ?? ''?>"> 
                    </div>
				</td>
				<td > 
					<div class="inline-class" >
						<input type="text" name="tax_currency_<?= $count ?>" id="tax_currency_<?= $count ?>" class="form-control w-50" placeholder="Indian Rupee" value="<?= $value->currency ?>">  
					</div>
				</td>
				<td class="text-left"> 
					<?php if( $count != 0) { ?>
						<a href="#" class="btn btn-sm btn-outline-danger" onclick="deleteRow(this)">
							<i class="fa fa-close"></i>
						</a>
					<?php } ?>
				</td>
			</tr>
			<?php $count++;
				}  } else { ?>
					<tr>
				
						<td class=""> 
							<div class="inline-class" >
								<input type="text" name="price_name_<?= $count ?>" id="price_name_<?= $count ?>" class="form-control w-70" placeholder="Dealer Price" > 
							</div>
						</td>
						<td > 
							<div class="inline-class" >
								<label class="fancy-checkbox custom-color-green">
			                        <input name="is_tax_<?= $count ?>" value="yes" type="checkbox">
			                        <span><i></i> Tax Include </span>
			                    </label>
			                </div>
						</td>
						<td > 
							<div class="inline-class" >
		                    	<input type="text" name="tax_percent_<?= $count ?>" id="tax_percent_<?= $count ?>" class="form-control " style="width: 80px;" placeholder="Tax %" > 
		                    </div>
						</td>
						<td > 
							<div class="inline-class" >
								<input type="text" name="tax_currency_<?= $count ?>" id="tax_currency_<?= $count ?>" class="form-control w-50" placeholder="Indian Rupee">  
							</div>
						</td>
						<td class="text-left"> 
							<!-- <a href="javascipt:;" class="btn btn-sm btn-outline-danger" >
								<i class="fa fa-close"></i>
							</a> -->
						</td>
					</tr>
			<?php }
			?>
		</table>
		<input type="hidden" name="total_items" id="total_items" value="<?= $count + 1 ?>">
		<div id="add_new" class="btn btn-outline-success w-100">ADD NEW</div>
	</div>
</div>
<div class="row clearfix" style="text-align:right;">
    <div class="col-lg-12 col-md-12 m-t-20">
        <button id="addToTable" class="btn btn-sm btn-success" type="button" onclick="return update_company_setting( 'price' );">
            Update
        </button>
    </div>
</div>

<script type="text/javascript">

	$("#add_new").click(function(){
        var total_items = $( '#total_items' ).val();
        var markup = '<tr><td><div class="inline-class" ><input type="text" name="price_name_'+total_items+'" id="price_name_'+total_items+'" class="form-control w-70" placeholder="Dealer Price" ></div></td><td><div class="inline-class" ><label class="fancy-checkbox custom-color-green"><input name="is_tax_'+total_items+'" value="yes" type="checkbox"><span><i></i> Tax Include </span></label></di></td><td><div class="inline-class" ><input type="text" name="tax_percent_'+total_items+'" id="tax_percent" class="form-control " style="width: 80px;" placeholder="Tax %" ></div></td><td><div class="inline-class" ><input type="text" name="tax_currency_'+total_items+'" id="tax_currency_'+total_items+'" class="form-control w-50" placeholder="Indian Rupee"></div></td><td class="text-left"><div class="inline-class" ><a href="#" class="btn btn-sm btn-outline-danger " onclick="deleteRow(this)"><i class="fa fa-close"></i></a></div></td></tr>';
        $( "table tbody" ).append( markup );
		var count = parseInt( total_items ) + 1;
		$( '#total_items' ).val( count );

    });

    function deleteRow( button ) {
    	var row = button.parentNode.parentNode;
        var name = row.getElementsByTagName("TD")[0].innerHTML;
        //Get the reference of the Table.
        var table = document.getElementById("tax_table");
        //Delete the Table row using it's Index.
        table.deleteRow(row.rowIndex);
    }
</script>