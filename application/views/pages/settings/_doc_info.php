<div class="row clearfix">
    <div class="col-lg-6 col-md-12">
    	<div class="inline-class" >
            <div class="w-50 vcenter"> 
                Purchase Invoice No
            </div>
            <?php
                // print_r( $doc_info );
            ?>
            <div class="w-50 vcenter"> 
                <input type="text" name="purchase_inv_no" id="purchase_inv_no" class="form-control" value="<?= $doc_info->purchase_inv_no ?? '' ?>">
        	</div>
		</div>
       
        <div class="inline-class" >
            <div class="w-50 vcenter"> 
                Purchase Payment No
            </div>
            <div class="w-50 vcenter"> 
                <input type="text" name="purchase_payment_no" id="purchase_payment_no" class="form-control" value="<?= $doc_info->purchase_payment_no ?? '' ?>">
            </div>
        </div>
        <div class="inline-class" >
            <div class="w-50 vcenter"> 
                Purchase Return No
            </div>
            <div class="w-50 vcenter"> 
                <input type="text" name="purchase_return_no" id="purchase_return_no" class="form-control" value="<?= $doc_info->purchase_return_no ?? '' ?>">
            </div>
        </div>
        <div class="inline-class" >
            <div class="w-50 vcenter"> 
                Purchase Credit Note No
            </div>
            <div class="w-50 vcenter"> 
                <input type="text" name="purchase_credit_no" id="purchase_credit_no" class="form-control" value="<?= $doc_info->purchase_credit_no ?? '' ?>">
            </div>
        </div>
        <div class="inline-class" >
            <div class="w-50 vcenter"> 
                Purchase Debit Note No
            </div>
            <div class="w-50 vcenter"> 
                <input type="text" name="purchase_debit_no" id="purchase_debit_no" class="form-control" value="<?= $doc_info->purchase_debit_no ?? '' ?>">
            </div>
        </div>
        
	</div>
	<!-- second column  -->
	<div class="col-lg-6 col-md-12">
       <div class="inline-class" >
            <div class="w-50 vcenter"> 
                Dealer Sales Invoice No
            </div>
            <div class="w-50 vcenter"> 
                <input type="text" name="dealer_sales_inv_no" id="dealer_sales_inv_no" class="form-control" value="<?= $doc_info->dealer_sales_inv_no ?? '' ?>">
            </div>
        </div>
        <div class="inline-class" >
            <div class="w-50 vcenter"> 
               Dealer Sales Payment No
            </div>
            <div class="w-50 vcenter"> 
                <input type="text" name="dealer_sales_payment_no" id="dealer_sales_payment_no" class="form-control" value="<?= $doc_info->dealer_sales_payment_no ?? '' ?>">
            </div>
        </div>
        <div class="inline-class" >
            <div class="w-50 vcenter"> 
                Dealer Sales Return No
            </div>
            <div class="w-50 vcenter"> 
                <input type="text" name="dealer_sales_return_no" id="dealer_sales_return_no" class="form-control" value="<?= $doc_info->dealer_sales_return_no ?? '' ?>">
            </div>
        </div>
        <div class="inline-class" >
            <div class="w-50 vcenter"> 
                Farmer Sales Invoice No
            </div>
            <div class="w-50 vcenter"> 
                <input type="text" name="farmer_sales_inv_no" id="farmer_sales_inv_no" class="form-control" value="<?= $doc_info->farmer_sales_inv_no ?? '' ?>">
            </div>
        </div>
        <div class="inline-class" >
            <div class="w-50 vcenter"> 
                Farmer Sales Payment No
            </div>
            <div class="w-50 vcenter"> 
                <input type="text" name="farmer_sales_payment_no" id="farmer_sales_payment_no" class="form-control" value="<?= $doc_info->farmer_sales_payment_no ?? '' ?>">
            </div>
        </div>
        <div class="inline-class" >
            <div class="w-50 vcenter"> 
                Farmer Sales Return No
            </div>
            <div class="w-50 vcenter"> 
                <input type="text" name="farmer_sales_return_no" id="farmer_sales_return_no" class="form-control" value="<?= $doc_info->farmer_sales_return_no ?? '' ?>">
            </div>
        </div>
        <div class="inline-class" >
            <div class="w-50 vcenter"> 
                Sales Credit Note No
            </div>
            <div class="w-50 vcenter"> 
                <input type="text" name="sale_credit_no" id="sale_credit_no" class="form-control" value="<?= $doc_info->sale_credit_no ?? '' ?>">
            </div>
        </div>
	    <div class="inline-class" >
            <div class="w-50 vcenter"> 
                Sales Debit Note No
            </div>
            <div class="w-50 vcenter"> 
                <input type="text" name="sale_debit_no" id="sale_debit_no" class="form-control" value="<?= $doc_info->sale_debit_no ?? '' ?>">
            </div>
        </div>

	</div>
		
</div>
<div class="row clearfix" style="text-align:right;">
    <div class="col-lg-12 col-md-12 m-t-20">
        <button id="addToTable" class="btn btn-sm btn-success" type="button" onclick="return update_company_setting( 'invoice' );">
            Update
        </button>
    </div>
</div>