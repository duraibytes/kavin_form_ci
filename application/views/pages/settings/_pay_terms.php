<div class="row clearfix">
	<div class="col-sm-12">
		<table class="table" id="maintable">
			<tr>
				<td class="text-left w-40" > Payment Terms Name </td>
				<td class="text-left w-40"> Due Days </td>
				<td class="text-left"></td>
			</tr>
			<?php
				if( isset( $term_info ) && !empty( $term_info ) ) {
					$i = 1;
					foreach ( $term_info as $key => $value ) {
			?>
						<tr >
							<td > 
								<div class="inline-class" >
									<input type="text" name="pay_terms[]" id="home_currency" class="form-control w-50" value="<?= $value->term_name ?? '' ?>" placeholder="Net 30"> 
								</div>
							</td>
							<td > 
								<div class="inline-class" >
									<input type="text" name="due_days[]" id="home_currency" class="form-control w-50" value="<?= $value->due_days ?? '' ?>" placeholder="30 Days">  
								</div>
							</td>
							<td class="text-left " > 
							<?php if( $i != 1 ) { ?>
								<a href="javascipt:;" class="btn btn-sm btn-outline-danger " onclick="deleteRow(this)">
									<i class="fa fa-close"></i>
								</a>
							<?php } ?>
							</td>
						</tr>
		<?php  $i++; } } else { ?>
			<tr >
				<td > 
					<div class="inline-class" >
						<input type="text" name="pay_terms[]" id="home_currency" class="form-control w-50" value="" placeholder="Net 30"> 
					</div>
				</td>
				<td > 
					<div class="inline-class" >
						<input type="text" name="due_days[]" id="home_currency" class="form-control w-50" value="" placeholder="30 Days">  
					</div>
				</td>
				<td class="text-left " > 
					<!-- <a href="javascipt:;" class="btn btn-sm btn-outline-danger " onclick="deleteRow(this)">
						<i class="fa fa-close"></i>
					</a> -->
				</td>
			</tr>
		<?php } ?>
			
		</table>
		
		<div id="add_new" class="btn btn-outline-success w-100">ADD NEW</div>
	</div>
</div>
<div class="row clearfix" style="text-align:right;">
    <div class="col-lg-12 col-md-12 m-t-20">
        <button id="addToTable" class="btn btn-sm btn-success" type="button" onclick="return update_company_setting( 'terms' );">
            Update
        </button>
    </div>
</div>

<script type="text/javascript">

	$("#add_new").click(function(){
            
        var markup = '<tr><td><input type="text" name="pay_terms[]" id="home_currency" class="form-control w-50" value="" placeholder="Net 30"></td><td><input type="text" name="due_days[]" id="home_currency" class="form-control w-50" value="" placeholder="30 Days"> </td><td class="text-left"><a href="javascipt:;" class="btn btn-sm btn-outline-danger " onclick="deleteRow(this)"><i class="fa fa-close"></i></a></td></tr>';
        $( "table tbody" ).append( markup );

    });

    function deleteRow( button ) {
    	var row = button.parentNode.parentNode;
        var name = row.getElementsByTagName("TD")[0].innerHTML;
        //Get the reference of the Table.
        var table = document.getElementById("maintable");
        //Delete the Table row using it's Index.
        table.deleteRow(row.rowIndex);
    }
</script>
