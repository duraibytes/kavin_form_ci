<div class="row clearfix">
    <div class="col-lg-6 col-md-12">
    	<div class="inline-class" >
            <div class="w-50 vcenter"> 
                Company Name<span class="text-danger">*</span>
            </div>
            <div class="w-50 vcenter"> 
                <input type="text" name="company_name" id="company_name" class="form-control" value="<?= $company_info->company_name ?? '' ?>">
        	</div>
		</div>
       
        <div class="inline-class" >
            <div class="w-50 vcenter"> 
                Company Email
            </div>
            <div class="w-50 vcenter"> 
                <input type="text" name="company_email" id="company_email" class="form-control" value="<?= $company_info->email ?? '' ?>">
            </div>
        </div>
        <div class="inline-class" >
            <div class="w-50 vcenter"> 
                Company Mobile Number
            </div>
            <div class="w-50 vcenter"> 
                <input type="text" name="company_mobile" id="company_mobile" class="form-control" value="<?= $company_info->mobile ?? '' ?>">
            </div>
        </div>
        <div class="inline-class" >
            <div class="w-50 vcenter"> 
                Company Phone
            </div>
            <div class="w-50 vcenter"> 
                <input type="text" name="company_phone" id="company_phone" class="form-control" value="<?= $company_info->phone_no ?? '' ?>">
            </div>
        </div>
        <div class="inline-class" >
            <div class="w-50 vcenter"> 
                Company Fax
            </div>
            <div class="w-50 vcenter"> 
                <input type="text" name="company_fax" id="company_fax" class="form-control" value="<?= $company_info->fax ?? '' ?>">
            </div>
        </div>
        <div class="inline-class" >
            <div class="w-50 vcenter"> 
                Company Address
            </div>
            <div class="w-50 vcenter"> 
                <textarea class="form-control" name="company_addres" rows="3"><?= $company_info->address ?? '' ?></textarea>
            </div>
        </div>
        
        <div class="inline-class" >
            <div class="w-50 vcenter"> 
                Company Seed License
            </div>
            <div class="w-50 vcenter"> 
                <input type="text" name="seed_license" id="seed_license" class="form-control" value="<?= $company_info->seed_license ?? '' ?>">
            </div>
        </div>
        <div class="inline-class" >
            <div class="w-50 vcenter"> 
                Company Pesticides License
            </div>
            <div class="w-50 vcenter"> 
                <input type="text" name="pesticides_license" id="pesticides_license" class="form-control" value="<?= $company_info->pesticides_license ?? '' ?>">
            </div>
        </div>
        <div class="inline-class" >
            <div class="w-50 vcenter"> 
                Company Fertilizer License
            </div>
            <div class="w-50 vcenter"> 
                <input type="text" name="fertilizer_license" id="fertilizer_license" class="form-control" value="<?= $company_info->fertilizer_license ?? '' ?>">
            </div>
        </div>

	</div>
	<!-- second column  -->
	<div class="col-lg-6 col-md-12">
       
		<div class="inline-class"  >
	        <div class="w-50 vcenter"> Company Logo 
                <?php
                if( isset( $company_info->image ) && !empty( $company_info->image ) ) {
                ?>
                <div style="position: absolute;top: 0;">
                    <img src="<?= $company_info->image ?>" style="width: 150px;">    
                </div>
                <?php
                    }
                ?>
            </div>
	        <div class="w-50 vcenter"> <input type="file" class="dropify" name="company_logo" > </div>
	    </div>
        <div class="inline-class" >
            <div class="w-50 vcenter"> 
                Sale Tax No
            </div>
            <div class="w-50 vcenter"> 
                <input type="text" name="sale_tax_no" id="sale_tax_no" class="form-control" value="<?= $company_info->sales_tax_no ?? '' ?>">
            </div>
        </div>
        <div class="inline-class" >
            <div class="w-50 vcenter"> 
                Company Website
            </div>
            <div class="w-50 vcenter"> 
                <input type="text" name="website" id="website" class="form-control" value="<?= $company_info->website ?? '' ?>">
            </div>
        </div>

	</div>
		
</div>
<div class="row clearfix" style="text-align:right;">
    <div class="col-lg-12 col-md-12 m-t-20">
        <button id="addToTable" class="btn btn-sm btn-success" type="button" onclick="return update_company_setting( 'company' );">
            Update
        </button>
    </div>
</div>

<script type="text/javascript">
    $(function() {
    
        $('.dropify').dropify();
    });
</script>