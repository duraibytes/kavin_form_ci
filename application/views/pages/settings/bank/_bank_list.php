<style type="text/css">
	.bank-card {
		background: #beecc8;
		cursor: pointer;
	}
	.bank-card:hover {
		background: #ceecbe;
	}
	.delete_bank {
		float: right;padding: 0px 11px;color: red;
	}
	.delete_bank:hover {
		color: orange;
	}
</style>

<?php
		
		if( isset( $own_bank ) && !empty( $own_bank ) ) {
			foreach ( $own_bank as $key => $value ) {
		 ?>
				<div class="col-sm-3" >

					<div class="card bank-card" style="background: ;">
						<span class="delete_bank" onClick="return are_you_sure( 'delete_own_bank', 'Delete Own Bank', 'Are you sure want to delete this bank?', '<?= $value->id?>' );"><i class="fa fa-trash"></i></span>
						<div class="card-body" onclick="return update_own_company_bank( '<?= $value->id ?>' )">
							<table class="table-hover" style="width: 100%;">
								<tr>
									<td class="text-left">Bank</td>
									<td class="text-left"><?= $value->bank_name ?></td>
								</tr>
								<tr>
									<td class="text-left">Branch</td>
									<td class="text-left"><?= $value->branch ?></td>
								</tr>
								<tr>
									<td class="text-left">IFSC</td>
									<td class="text-left"><?= $value->ifsc_code ?></td>
								</tr>
								<tr>
									<td class="text-left">AC NO</td>
									<td class="text-left"><?= $value->bank_ac_no ?></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
	<?php 
			} 
		} 
	?>