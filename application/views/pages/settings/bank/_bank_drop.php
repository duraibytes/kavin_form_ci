<select class="form-control" name="bank" id="bank" onchange="return get_bank_branch( this.value, 'bank_branch_ajax' )">
    <option value=""> -- Select Bank -- </option>
    <?php
    if( isset( $bank_info ) && !empty( $bank_info ) ) {
        foreach ( $bank_info as $key => $value ) {
            $selected       = '';
            if( isset( $own_bank_info->bank_id ) && $own_bank_info->bank_id == $value->id ) {
                $selected   = 'selected';
            }
            ?>
            <option value="<?= $value->id ?>" <?= $selected ?>> <?= $value->bank_name ?> </option>
   <?php     }
    }
    ?>
</select> 