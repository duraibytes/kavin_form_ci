<style type="text/css">
	#own-bank-form {
		width: 100% !important;
    	display: contents !important;
	}
</style>
<div class="row">
	<?=form_open( '', 'id="own-bank-form"');?>
		<?= $this->load->view( 'pages/settings/bank/form', '', true ) ?>
	<?= form_close() ?>
</div>
<hr>
<div class="row m-t-20" id="bank_list">
	<?= $this->load->view( 'pages/settings/bank/_bank_list', '', true )?>
</div>

<script type="text/javascript">
    function add_company_bank() {
        var form_name           = $( '#own-bank-form' ).find( 'input, select, button, textarea' );
        var codes_form_data     = form_name.serialize();
        codes_form_data         = codes_form_data + '&<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>';
        $.ajax({
            type        : 'POST',
            url         : '<?=site_url( 'company_settings/add_company_bank' );?>',
            data        : codes_form_data,
            async       : false,
            dataType    : 'json',
            beforeSend: function () {
                form_name.attr( 'disabled', 'disabled' );
            },    
            success     : function( msg ) {
                if( msg.error == 1 ) {
                    toastr.error( msg.error_msg, 'Bank' );
                    form_name.attr( 'disabled', false );
                } else {
                    toastr.success( msg.error_msg, 'Bank' );
                    
                    form_name.attr( 'disabled', false );
                    if( msg.form ) {
                        $( '#own-bank-form' ).html( msg.form );  
                    }
                    $( '#bank_list' ).html( msg.view );
                }
            }
        });
        return false;
    }

    function update_own_company_bank( own_bank_id ) {
        var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&own_bank_id='+own_bank_id;
          $.ajax( {
              type    : "POST",
              url     : '<?= site_url( 'company_settings/update_own_company_bank' ); ?>',
              data    : data,
              dataType: 'json',
              success: function( msg ) {
                if( msg.form ) {
                  $( '#own-bank-form' ).html( msg.form );  
                }
                
              }
          });
          return false;
    }

    function delete_own_bank( own_bank_id ) {
        var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&own_bank_id='+own_bank_id;
          $.ajax( {
              type    : "POST",
              url     : '<?= site_url( 'company_settings/delete_own_bank' ); ?>',
              data    : data,
              dataType: 'json',
              success: function( msg ) {
                if( msg.view ) {
                    $( '#bank_list' ).html( msg.view );    
                }
                if( msg.form ) {
                  $( '#own-bank-form' ).html( msg.form );  
                }
              }
          });
          return false;
    }
</script>
