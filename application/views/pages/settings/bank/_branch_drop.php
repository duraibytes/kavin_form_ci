<select class="form-control" name="bank_branch" id="bank_branch" onchange="get_ifsc( this.value )">
    <option value=""> -- Select Bank Branch -- </option>
    <?php
    if( isset( $branch_info ) && !empty( $branch_info ) ) {
        foreach ( $branch_info as $key => $value ) {
            $selected       = '';
            if( isset( $own_bank_info->branch_id ) && $own_bank_info->branch_id == $value->id ) {
                $selected   = 'selected';
            }
            ?>
            <option value="<?= $value->id ?>" <?= $selected ?>> <?= $value->branch ?> </option>
   <?php     }
    }
    ?>
</select> 