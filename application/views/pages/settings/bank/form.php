<div class="col-sm-6">
    <div class="inline-class" >
        <div class="w-50 vcenter"> Bank Name </div>
        <div class="w-50 vcenter"> 
            <div class="input-group">
                <div class="bank_info_ajax" style="width: 82%">
                    <?= $this->load->view( 'pages/settings/bank/_bank_drop', '', true ) ?>
                </div>
                <div class="input-group-prepend" onclick="return get_bank_form( 'bank_info_ajax' )">
                  <div class="input-group-text" id="btnGroupAddon">
                    <i class="fa fa-plus text-white"></i>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-sm-6">
    <div class="inline-class" >
        <div class="w-50 vcenter"> Bank AC No </div>
        <div class="w-50 vcenter"> 
            <input type="text" name="ac_no" id="ac_no" class="form-control" value="<?= $own_bank_info->bank_ac_no ?? '' ?>" > 
        </div>
    </div>
</div>
<div class="col-sm-6">
    <div class="inline-class" >
        <div class="w-50 vcenter"> Bank Branch </div>
        <div class="w-50 vcenter"> 
            <!-- <input type="text" name="bank_branch" id="bank_branch" class="form-control" value="<?= $bank->branch ?? '' ?>">  -->
            <div class="input-group">
                <div class="bank_branch_ajax" style="width: 82%">
                    <?= $this->load->view( 'pages/settings/bank/_branch_drop', '', true )?>
                </div>
                <div class="input-group-prepend" onclick="return get_bank_branch_form( 'bank_branch_ajax' )">
                  <div class="input-group-text" id="btnGroupAddon">
                    <i class="fa fa-plus text-white"></i>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-sm-6">
    <div class="inline-class" >
        <div class="w-50 vcenter"> IFSC </div>
        <div class="w-50 vcenter"> 
            <input type="text" name="ifsc" id="ifsc" class="form-control" value="<?= $own_bank_info->ifsc_code ?? '' ?>"> 
        </div>
    </div>
</div>
<input type="hidden" name="id" value="<?= $own_bank_info->id ?? '' ?>">
<div class="col-sm-12 text-right m-t-10">
    <a href="javascript:;" class="btn btnn-sm btn-success" onclick="return add_company_bank()"> Add </a>
</div>


<?php
    if( isset( $own_bank_info->bank_id ) && !empty( $own_bank_info->bank_id ) ) {

?>
    <script type="text/javascript">

        get_bank_branch( '<?= $own_bank_info->bank_id ?>', 'bank_branch_ajax', '<?= $own_bank_info->id ?>' );    
    </script>
<?php } ?>

