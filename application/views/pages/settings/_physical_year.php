<div class="row clearfix">
	<div class="col-sm-12">
		<table class="table" id="maintable">
			<tr>
				<td class="text-left w-40" > Start Year </td>
				<td class="text-left w-40"> End Year </td>
				<td class="text-left"></td>
			</tr>
			<?php
				if( isset( $year_info ) && !empty( $year_info ) ) {
					$i = 1;
					
					foreach ( $year_info as $key => $value ) {
			?>
						<tr >
							<td > 
								<div class="inline-class" >
									<input name="start_year[]" data-provide="datepicker" class="w-50 float-left form-control" data-date-autoclose="true" class="form-control" data-date-format="dd-mm-yyyy" value="<?= ( ( isset( $value->start_date ) && !empty( $value->start_date ) ) ? date( 'd-m-Y', strtotime( $value->start_date ) )  :'' ) ?>">
								</div>
							</td>
							<td > 
								<div class="inline-class" >
									<input name="end_year[]" data-provide="datepicker" class="w-50 float-left form-control" data-date-autoclose="true" class="form-control" data-date-format="dd-mm-yyyy" value="<?= ( ( isset( $value->end_date ) && !empty( $value->end_date ) ) ? date( 'd-m-Y', strtotime( $value->end_date ) )  :'' ) ?>">
								</div>
							</td>
							<td class="text-left " > 
							<?php if( $i != 1 ) { ?>
								<a href="javascipt:;" class="btn btn-sm btn-outline-danger " onclick="deleteRow(this)">
									<i class="fa fa-close"></i>
								</a>
							<?php } ?>
							</td>
						</tr>
		<?php  $i++; } } else { ?>
			<tr >
				<td > 
					<div class="inline-class" >
						<input name="start_year[]" data-provide="datepicker" class="w-50 float-left form-control" data-date-autoclose="true" class="form-control" data-date-format="dd-mm-yyyy" >
					</div>
				</td>
				<td > 
					<div class="inline-class" >
						<input name="end_year[]" data-provide="datepicker" class="w-50 float-left form-control" data-date-autoclose="true" class="form-control" data-date-format="dd-mm-yyyy" >
					</div>
				</td>
				<td class="text-left " > 
					<!-- <a href="javascipt:;" class="btn btn-sm btn-outline-danger " onclick="deleteRow(this)">
						<i class="fa fa-close"></i>
					</a> -->
				</td>
			</tr>
		<?php } ?>
			
		</table>
		
		<div id="add_new" class="btn btn-outline-success w-100">ADD NEW</div>
	</div>
</div>
<div class="row clearfix" style="text-align:right;">
    <div class="col-lg-12 col-md-12 m-t-20">
        <button id="addToTable" class="btn btn-sm btn-success" type="button" onclick="return update_company_setting( 'year' );">
            Update
        </button>
    </div>
</div>

<script type="text/javascript">

	$("#add_new").click(function(){
            
        var markup = '<tr><td><div class="inline-class" ><input name="start_year[]" data-provide="datepicker" class="w-50 float-left form-control" data-date-autoclose="true" class="form-control" data-date-format="dd-mm-yyyy" ></div></td><td><div class="inline-class" ><input name="end_year[]" data-provide="datepicker" class="w-50 float-left form-control" data-date-autoclose="true" class="form-control" data-date-format="dd-mm-yyyy" ></div></td><td class="text-left"><a href="javascipt:;" class="btn btn-sm btn-outline-danger " onclick="deleteRow(this)"><i class="fa fa-close"></i></a></td></tr>';
        $( "table tbody" ).append( markup );

    });

    function deleteRow( button ) {
    	var row = button.parentNode.parentNode;
        var name = row.getElementsByTagName("TD")[0].innerHTML;
        //Get the reference of the Table.
        var table = document.getElementById("maintable");
        //Delete the Table row using it's Index.
        table.deleteRow(row.rowIndex);
    }
</script>
