<div class="row clearfix">
    <div class="col-md-12">
        <b> User Name </b>
        <div class="input-group mb-3">
            <input type="text" class="form-control" name="user_name" id="user_name" placeholder="">
        </div>
    </div>
    <div class="col-md-12">
         <b> Email </b>
        <div class="input-group mb-3">
            <input type="text" class="form-control" name="email" id="email" placeholder="">
        </div>
    </div>
    <div class="col-md-12">
        <b> Mobile Number </b>
        <div class="input-group mb-3">
            <input type="text" class="form-control" name="mobile_no" id="mobile_no" placeholder="">
        </div>
    </div>
    <div class="col-md-12">
        <b> DOB </b>
        <div class="input-group mb-3">
            <input data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="dd/mm/yyyy">
        </div>
    </div>
    <div class="col-md-12">
        <b> Address </b>
        <div class="input-group mb-3">
            <textarea class="form-control" rows="3" name="adddress" id="address"></textarea>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card">
            <div class="header">
                <h2> Profile Image <span class="text-warning"><small>( Image will be resized 140px x 140px )</small></span></h2>
            </div>
            <div class="body">
                <input type="file" class="dropify">
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <a href="javascript:;" class="btn btn-success float-right" > Update Profile </a>
    </div>
</div>