<div class="block-header">
    <?= $this->load->view( 'layout/_nav_header', '', true )?>
</div>  
<div class="container-fluid">           
    <div class="row clearfix">
        <div class="col-lg-4 col-md-12">
            <div class="card profile-header">
                <div class="body text-center">
                    <div class="profile-image mb-3"><img src="<?= base_url() ?>assets/images/user-logo.png" class="rounded-circle" alt=""></div>
                    <div>
                        <h4 class="mb-0"><strong>Admin</strong> </h4>
                        <span>Tamil Nadu</span>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="header">
                    <h2>Info</h2>
                </div>
                <div class="body">
                    <?= $this->load->view( 'pages/myaccount/_profile_info', '', true ) ?>
                </div>
            </div>
        </div>
        <div class="col-lg-8 col-md-12">
            <div class="card">
                <div class="body">
                    <?= $this->load->view( 'pages/myaccount/_profile_form', '', true ) ?>
                </div>
            </div>
                   
        </div>
    </div>                    
</div>
