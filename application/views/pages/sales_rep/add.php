<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <?=form_open( '', 'id="ref-form"');?>
        <div class="modal-header">

            <h4 class="title" id="defaultModalLabel"><?= $btn_name ?> Sales Representative </h4>
            
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-12">
                    <?= $this->load->view( 'errors/show_error', '', true  )?>
                </div>
            </div>
            <?= $this->load->view( 'pages/sales_rep/form/_form', '', true ) ?>
        </div>
        <div class="modal-footer">
            <input type="hidden" name="id" value="<?= $id ?>">
            
            <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
            <button onClick="return add_rep()" class="btn btn-primary" type="button"> Save</button> 
          
        </div>
        <?=form_close();?>
    </div>
</div>

<script type="text/javascript">

    function add_rep() {
        var form_name           = $( '#ref-form' ).find( 'input, select, button, textarea' );
        var codes_form_data     = form_name.serialize();
        codes_form_data         = codes_form_data + '&<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>';
        $.ajax({
            type        : 'POST',
            url         : '<?=site_url( 'settings/add_rep' );?>',
            data        : codes_form_data,
            async       : false,
            dataType    : 'json',
            beforeSend: function () {
                form_name.attr( 'disabled', 'disabled' );
                $( '#form_errors' ).hide();
                $( '#form_success' ).hide();
            },    
            success     : function( msg ) {
                if( msg.error == 1 ) {
                    toastr.error( msg.error_msg, 'Sales Representation' );
                    form_name.attr( 'disabled', false );
                } else {
                    toastr.success( msg.error_msg, 'Sales Representation' );
                    setTimeout( function() {
                        window.location.href = '<?=base_url();?>sales-rep';
                    }, 300 );
                }
            }
        });
        return false;
    }

</script>