<div class="form-group">
    <label for="code"> Name </label>
    <input class="form-control" placeholder="Enter Name" type="text" name="name" id="name" value="<?= $info->name ?? '' ?>" />
</div>
<div class="form-group">
    <label for="code"> Email </label>
    <input class="form-control" placeholder="Enter Email" type="text" name="email" id="email" value="<?= $info->email ?? '' ?>" />
</div>
<div class="form-group">
    <label for="code"> Mobile Number </label>
    <input class="form-control" placeholder="Enter Mobile" type="text" name="mobile_no" id="mobile_no" value="<?= $info->mobile_no ?? '' ?>" />
</div>
<div class="form-group">
    <label for="code"> Address </label>
   	<textarea class="form-control" rows="3" cols="5" name="address"><?= $info->address ?? '' ?></textarea>
</div>