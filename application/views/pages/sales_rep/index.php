
<div class="block-header">
    <?= $this->load->view( 'layout/_nav_header', '', true )?>
</div>

<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
               
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover js-basic-example dataTable table-custom">
                            <thead>
                                <tr>
                                    <th> S.NO </th>
                                    <th> Name </th>
                                    <th> Address </th>
                                    <th class="text-center"> Action </th>
                                </tr>
                            </thead>
                           
                            <tbody>
                                
                                <?php
                                if( $all ) {
                                    $i = 1;
                                    foreach ( $all as $key => $value) {
                                            
                                    ?>
                                    <tr>
                                        <td> <?= $i?></td>
                                        <td> <?= $value->name ?></td>
                                        <td> <?= $value->address ?></td>
                                        <td class="text-center">
                                            <a href="javascript:;" class="btn btn-warning btn-sm" onclick="return open_add_sale_rep_form( '<?= $value->id ?>' )"> <i class="icon-pencil"></i> </a>
                                            <a href="javascript:;" class="btn btn-danger btn-sm" onClick="return are_you_sure( 'delete_rep', 'Delete Rep', 'Are you sure want to delete this rep?', '<?= $value->id?>' );"> <i class="icon-trash"></i> </a>
                                        </td>
                                    </tr>
                                <?php 
                                     $i++;   } 
                                    } 

                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
    </div>
    
</div>



<script type="text/javascript">
    $(document).ready(function() {
        $('#module_table').DataTable();
    } );
   
    function open_add_sale_rep_form( id='' ) {
          var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&id=' + id;
          $.ajax( {
              type    : "POST",
              url     : '<?= site_url( 'settings/open_add_sale_rep_form' ); ?>',
              data    : data,
              success: function( msg ) {
                  $( '#defaultModal' ).modal();
                  $( '#defaultModal' ).html( msg );
              }
          });
          return false;
      }

      function delete_rep( id ) {
        var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&id=' + id;
        $.ajax( {
            type    : "POST",
            url     : '<?= site_url( 'settings/delete_rep' ); ?>',
            data    : data,
            dataType: 'json',
            success: function( msg ) {
                toastr.success( 'Sales Rep deleted successfully', 'Sales Representation' );
                setTimeout( function() {
                    window.location.href = '<?= site_url()?>sales-rep';
                }, 300 );
                
            }
        });
        return false;
      }
    
</script> 