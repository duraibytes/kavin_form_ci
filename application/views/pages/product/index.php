<div class="block-header">
      <?= $this->load->view( 'layout/_nav_header', '', true )?>
</div>

<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                
                <div class="body">
                    <div class="table-responsive">
                        <div class="text-center hd-font m-b-10 m-t-10"> 
                            <span class="btn btn-sm inventory-bg text-white">
                                Product - Management
                            </span> 
                        </div>
                        <table id="product_table" class="table table-bordered table-hover js-basic-example dataTable table-custom">
                            <thead>
                                <tr class="inventory-bg text-white">
                                    <th> Product Name </th>
                                    <th> Product Manufacturer </th>
                                    <th> Product Code </th>
                                    <th> Product Category </th>
                                    <th> Product Description </th>
                                    <th> Product UOM </th>
                                    <th> Action </th>
                                </tr>
                            </thead>
                            
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
    </div>
    
</div>
<script type="text/javascript">
    $(document).ready(function() {
        var dataTable = $( '#product_table' ).DataTable( {
            'processing' : true,
            'serverSide' : true,
            'order'      : [],
            'ajax'       : {
                url      : '<?= site_url( 'product/get_product' ); ?>',
                type     : 'POST',
                data     : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>' }
            },
            'emptyTable' : 'No data available in table',
            'columnDefs' : [
                {  
                    'targets'   : [ 6 ],
                    'orderable' : false,
                }
            ],
            "lengthMenu": [
                [5, 15, 20, 50, 100, -1],
                [5, 15, 20, 50, 100, "All"] 
            ],
            "pageLength": '<?=NO_OF_DT_ROWS;?>',
        });
    });

    function delete_product( id ) {
        var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&id=' + id;
        $.ajax( {
            type    : "POST",
            url     : '<?=site_url( 'product/delete_product' );?>',
            data    : data,
            success: function( msg ) {
                toastr.success( 'product deleted successfully', 'Product' );
                setTimeout( function() {
                    window.location.href = '<?= site_url()?>product';
                }, 300 );
            }
        });
        return false;
    }
</script>