<div class="block-header">
      <?= $this->load->view( 'layout/_nav_header', '', true )?>
</div>
<style type="text/css">
    .dropify-wrapper {
        height: 150px !important;
    }
    .head-card {

        -webkit-transition: all 0.3s ease-in-out;
        -moz-transition: all 0.3s ease-in-out;
        -ms-transition: all 0.3s ease-in-out;
        -o-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
        background: #fff;
        border-color: #eee;
         margin-bottom: 3px; 
        position: relative;
        width: 100%;
        padding: 3px;
    }
    .head-card span {
            width: 300px;
            text-align: center;
            padding: 6px 50px;
            border: 1px solid;
            font-size: 16px;
            font-weight: 900;
            text-transform: uppercase;
    }
</style>
<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            
            <div class="card">
                <div class="body">
                    <?php echo form_open_multipart( '', 'id="product_form"' ); ?>
                        <div class="demo-masked-input">
                            <div class="row clearfix">
                                 <div class="col-lg-12">
                                     <div class="text-center hd-font m-b-20"> 
                                        <span class="btn btn-sm inventory-bg text-white">
                                            ADD PRODUCT
                                        </span> 
                                    </div>
                                </div>
                                <?= $this->load->view( 'pages/product/form/_left-form-field', '', true ) ?>
                                <?= $this->load->view( 'pages/product/form/_right-form-field', '', true ) ?>
                                <?= $this->load->view( 'pages/product/form/_product-price-form', '', true ) ?>
                                
                            </div>
                            
                           <div class="row clearfix">
                                <div class="col-sm-12">
                                    <hr>
                                </div>
                                <input type="hidden" name="id" value="<?= $product_info->id ?? '' ?>">
                                <div class="col-sm-12 text-right">
                                    <a href="<?= base_url()?>product" class="btn btn-sm btn-outline-danger"> Cancel </a>
                                     <?php 
                                        if( ! $slug ) {
                                    ?>
                                    <a href="javascript:;" class="btn btn-sm btn-outline-dark" onclick="return clear_form( 'product_form' )"> Clear </a>
                                <?php } ?>
                                    <a href="javascript:;" class="btn btn-sm btn-success" onclick="return insert_product()"> <?= $title ?></a>
                                </div>
                            </div>
                        </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    function insert_product() {
        var product_form           = $( '#product_form' ).find('input, textarea, button, select');
        var data      = new FormData( $( '#product_form' )[0]);
        $.ajax({
            type        : 'POST',
            url         : '<?=site_url( 'product/insert' );?>',
            data        : data,
            async       : false,
            contentType : false,
            processData : false,
            dataType    : 'json',
            beforeSend: function () {
                product_form.attr( 'disabled', 'disabled' );
            },    
            success     : function( msg ) {
                if( msg.error == 1 ) {
                    toastr.error( msg.error_msg, 'Product' );
                    product_form.attr( 'disabled', false );
                } else {
                    toastr.success( msg.error_msg, 'Product' );
                    setTimeout( function(){
                        window.location.href = '<?= site_url()?>product';
                    }, 300 );
                }
            }
        });
        return false;
    }
</script>