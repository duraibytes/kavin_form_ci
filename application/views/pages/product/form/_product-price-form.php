<div class="col-lg-6 col-md-6">
    <div class="inline-class-head m-t-30" style="background: aliceblue;">
        <div class="w-100">
            <span class="Hfont text-muted"> Product Pricing Scheme </span>
        </div>
    </div>
    <div class="inline-class" >
        <div class="w-95 vcenter"> 
            Net Purchase Price 
        </div>
        <div class="w-5 vcenter"> 
            <input type="text" name="net_purchase_price" id="net_purchase_price" class="form-control text-right" value="<?= $product_info->net_purchase_price ?? '' ?>"> 
        </div>
    </div>
    <div class="inline-class" >
        <div class="w-95 vcenter"> 
            Dealer Sale Price 
        </div>
        <div class="w-5 vcenter"> 
            <input type="text" name="dealer_sale_price" id="dealer_sale_price" class="form-control text-right" value="<?= $product_info->dealer_sale_price ?? '' ?>"> 
        </div>
    </div>
    <div class="inline-class" >
        <div class="w-95 vcenter"> 
            Farmer Sale Price 
        </div>
        <div class="w-5 vcenter"> 
            <input type="text" name="farmer_sale_price" id="farmer_sale_price" class="form-control text-right" value="<?= $product_info->farmer_sale_price ?? '' ?>"> 
        </div>
    </div>
    <div class="inline-class" >
        <div class="w-95 vcenter"> 
            Retail Sale Price 
        </div>
        <div class="w-5 vcenter"> 
            <input type="text" name="retail_sale_price" id="retail_sale_price" class="form-control text-right" value="<?= $product_info->retail_sale_price ?? '' ?>"> 
        </div>
    </div>
</div>