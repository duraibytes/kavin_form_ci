<div class="col-lg-6 col-md-6">
    <div class="inline-class" >
        <div class="w-50 vcenter"> 
            Product Name <span class="text-danger">*</span>
        </div>
        <div class="w-50 vcenter"> 
            <input type="text" name="product_name" id="product_name" class="form-control" value="<?= $product_info->product_name ?? '' ?>"> 
        </div>
    </div>
    <div class="inline-class" >
        <div class="w-50 vcenter"> 
            Product Manufacturer <span class="text-danger">*</span>
        </div>
        <div class="w-50 vcenter"> 
            <input type="text" name="product_manufacturer" id="product_manufacturer" class="form-control" value="<?= $product_info->product_company ?? '' ?>"> 
        </div>
    </div>
    <div class="inline-class" >
        <div class="w-50 vcenter"> 
            Product Code <span class="text-danger">*</span>
        </div>
        <div class="w-50 vcenter"> 
            <input type="text" name="product_code" id="product_code" class="form-control" value="<?= $product_info->product_code ?? '' ?>"> 
        </div>
    </div>
    <div class="inline-class" >
        <div class="w-50 vcenter"> 
            Product Category <span class="text-danger">*</span>
        </div>
        <div class="w-50 vcenter"> 
            <input type="text" name="product_category" id="product_category" class="form-control" value="<?= $product_info->product_category ?? '' ?>"> 
        </div>
    </div>
    <div class="inline-class" >
        <div class="w-50 vcenter"> 
            Product description <span class="text-danger">*</span>
        </div>
        <div class="w-50 vcenter"> 
            <input type="text" name="product_description" id="product_description" class="form-control" value="<?= $product_info->description ?? '' ?>"> 
        </div>
    </div>
    <div class="inline-class" >
        <div class="w-50 vcenter"> 
            Product UOM <span class="text-danger">*</span>
        </div>
        <div class="w-50 vcenter"> 
            <input type="text" name="product_uom" id="product_uom" class="form-control" value="<?= $product_info->packaging ?? '' ?>"> 
        </div>
    </div>
</div>