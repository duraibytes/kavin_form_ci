<style type="text/css">
	.print_table thead {
      background: #395870;
      color: #fff;
    }
    .purchase_table td, .own_table td, .company_table td {
    	font-size: 10px;
    }
    .print_img {
    	width: 80px !important;
    	height: 40px !important;
    }
    .purchase_item_table thead {
    	background: #ddd;
    	font-size: 12px;
    }
    .purchase_item_table td {
    	font-size: 11px;
    }
    .purchase_item {
    	display: table;
	    border-collapse: separate;
	    box-sizing: border-box;
	    text-indent: initial;
	    border-spacing: 2px;
	    border-color: grey;
	    margin-top: 10px;
	    border: 1px solid #dee2e6;
    }

    .purchase_item_table td {
	    padding: .25rem;
	    vertical-align: top;
	    border-bottom: 1px solid #dee2e6;
	}

	.purchase_item_table th {
		padding: .25rem;
	    vertical-align: top;
	    border-bottom: 1px solid #dee2e6;
	    border-top: 1px solid #dee2e6;
	}

	.remark_table {
		display: table;
	    border-collapse: separate;
	    box-sizing: border-box;
	    text-indent: initial;
	    border-spacing: 2px;
	    border-color: grey;
	    border: 1px solid #dee2e6;
	}

	
</style>
<table style="width: 100%">
	<tr>
		<td style="text-align: center;">
			<h6><center><strong class="text-primary">PURCHASE PAYMENT INVOICE</strong></center></h6>
		</td>
	</tr>
</table>
<table style="width: 100%;" class="print_table">
	<tr>
		<td style="width: 45%;font-size: 9px;">
			<table>
				<tr>
					<td>
						<?= $this->load->view( 'pages/purchase/invoice/_payment_company_info', '', true )?>
					</td>
				</tr>
			</table>
		</td>
		<td style="width: 10%"></td>
		<td style="width: 45%">
			<?= $this->load->view( 'pages/purchase/invoice/_payment_own_info', '', true )?>
		</td>
	</tr>
</table>
<hr>
<table style="margin-top:20px;width: 100%;">
	<tr>
		<td>Payment Date</td>
		<td><?= date( 'd-M-Y', strtotime( $payment_info->payment_date ) ) ?></td>
	</tr>
	<tr>
		<td>Payment Number</td>
		<td><?= $payment_info->receipt_no ?></td>
	</tr>
	<tr>
		<td>Bank</td>
		<td><?= $payment_info->bank_name?></td>
	</tr>
	<tr>
		<td>Branch</td>
		<td><?= $payment_info->bank_branch?></td>
	</tr>
	<tr>
		<td>Bank IFSC CODE</td>
		<td><?= $payment_info->ifsc?></td>
	</tr>
	<tr>
		<td>Bank Account Number</td>
		<td><?= $payment_info->bank_ac_no?></td>
	</tr>

	<tr>
		<td>Is Advance Booking</td>
		<td><?= ucfirst( $payment_info->is_advance_booking ) ?></td>
	</tr>
	 <?php 
    	if( $payment_info->is_advance_booking == 'yes' ) {
    ?>
	<tr>
		<td>Advance Description </td>
		<td><?= $payment_info->advance_description ?></td>
	</tr>
	<?php } ?>
	<tr>
		<td>Verified By</td>
		<td><?= $payment_info->fname ?></td>
	</tr>
	<tr>
		<td>Amount</td>
		<td>Rs <?= number_format( $payment_info->amount, 2 ) ?></td>
	</tr>
	
</table>
<hr>
<table style="width: 100%;margin-top: 20px;">
	<tr>
		<td style="font-size: 18px;color: red;">
			<span>Amount in words </span><span style="font-weight:normal;">: <?= number_to_word( $payment_info->amount ) ?>/-</span> 
		</td>
	</tr>
</table>