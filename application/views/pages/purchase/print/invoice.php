<style type="text/css">
	.print_table thead {
      background: #395870;
      color: #fff;
    }
    .purchase_table td, .own_table td, .company_table td {
    	font-size: 10px;
    }
    .print_img {
    	width: 80px !important;
    	height: 40px !important;
    }
    .purchase_item_table thead {
    	background: #ddd;
    	font-size: 12px;
    }
    .purchase_item_table td {
    	font-size: 11px;
    }
    .purchase_item {
    	display: table;
	    border-collapse: separate;
	    box-sizing: border-box;
	    text-indent: initial;
	    border-spacing: 2px;
	    border-color: grey;
	    margin-top: 10px;
	    border: 1px solid #dee2e6;
    }

    .purchase_item_table td {
	    padding: .25rem;
	    vertical-align: top;
	    border-bottom: 1px solid #dee2e6;
	}

	.purchase_item_table th {
		padding: .25rem;
	    vertical-align: top;
	    border-bottom: 1px solid #dee2e6;
	    border-top: 1px solid #dee2e6;
	}

	.remark_table {
		display: table;
	    border-collapse: separate;
	    box-sizing: border-box;
	    text-indent: initial;
	    border-spacing: 2px;
	    border-color: grey;
	    border: 1px solid #dee2e6;
	}

	
</style>
<table style="width: 100%">
	<tr>
		<td style="text-align: center;">
			<h6><center><strong class="text-primary">PURCHASE INVOICE</strong></center></h6>
		</td>
	</tr>
</table>
<table style="width: 100%;" class="print_table">
	<tr>
		<td style="width: 45%;font-size: 9px;">
			<table>
				<tr>
					<td>
						<div style="border: 1px solid #343a4080;border-radius: 5%;">
							<?= $this->load->view( 'pages/purchase/invoice/_company_info', '', true )?>
							<hr>
							<?= $this->load->view( 'pages/purchase/invoice/_own_info', '', true )?>
						</div>
					</td>
				</tr>
				
			</table>
		</td>
		<td style="width: 10%"></td>
		<td style="width: 45%">
			<?= $this->load->view( 'pages/purchase/invoice/_purchase_info', '', true )?>
		</td>
	</tr>
</table>
<table class="purchase_item_table" style="margin-top: 30px;">
	
    <thead class="thead-dark">
        <tr>
            <th>NO</th>	
            <th>Product Code</th>
            <th><div>Category</div><div>Variety</div></th>
            <th>Product Desc</th>
            <th>Lot Number</th>
            <th>Dt.Of Packing</th>
            <th>Dt.of Expiry</th>
            <th>Qty</th>
            <th>UOM</th>
            <th>Net Rate(Rs)</th>
            <th>Discounts</th>
            <th>Amount(Rs)</th>
        </tr>
    </thead>
    <tbody>
	<?php
		
		if( isset( $items ) && !empty( $items ) ) {
			$i = 1;
			foreach ( $items as $key => $value ) {
	?>
        <tr>
            <td><?= $i ?></td>
            <td><?= $value->product_code ?></td>
            <td><?= $value->product_category ?></td>
            <td><?= $value->description ?></td>
            <td><?= $value->lot_no ?></td>
            <td> <?= date( 'd-M-Y', strtotime( $value->date_of_packed ) ) ?> </td>
            <td> <?= date( 'd-M-Y', strtotime( $value->date_of_expiry ) ) ?> </td>
            
            <td> <?= $value->qty ?> </td>
            <td> <?= $value->packaging ?></td>
            <td> <?= number_format( $value->net_rate, 2 ) ?></td>
            <td> <?= number_format( $value->discount, 2 ) ?></td>
            <td> <?= number_format( $value->total_amount , 2 ) ?></td>
        </tr>
       <?php $i++; } } else {  ?>
       	<tr>
       		<td colspan="12"> No Products found </td>
       	</tr>
       <?php } ?>
    </tbody>
    
</table>
<table style="width: 100%;font-size: 11px;" class="remark_table">
	<tr>
		<td width="75%" style="">
			<b>Remarks:</b>
	        <div style="padding: 14px 5px;background: #f7f7f7;">
	        	<?= $purchase_info->remarks ?? ''?>	
	        </div>
		</td>
		<td width="25%" style="">
			<table style="width: 100%;">
        		<tr style="border-bottom: 1px solid">
        			<th style="font-size: 11px;text-align: right;"> Gst </th>
        			<td style="font-size: 11px;padding-right: 20px;text-align: right;"> 0 % </td>
        		</tr>
        		<tr>
        			<th style="font-size: 11px;text-align: right;border-bottom: 1px solid #dee2e6;">Transport Charge </th>
        			<td style="font-size: 11px;text-align:right;padding-right: 20px;border-bottom: 1px solid #dee2e6;" >
        				
        				<?= $purchase_info->transport_charge ?? '0.00'?> 
        			</td>
        		</tr>
        		<tr>
        			<th style="text-align:right;"><h6>Total</h6></th>
        			<td style="padding-right: 20px;text-align: right;">
        				<h6> 
        					<?= number_format( $purchase_info->total_amount, 2 )?> 
        				</h6>
        			</td>
        		</tr>
        	</table>
		</td>
	</tr>
</table>
<table style="width: 100%;font-size: 12px;">
	<tr>
		<td>
			<span><b><u>Amount in words </u></b></span><span style="font-weight:normal;">: <?= number_to_word( $purchase_info->total_amount ) ?>/-</span> 
		</td>
	</tr>
</table>