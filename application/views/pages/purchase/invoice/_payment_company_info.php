
<div class="col-lg-12 col-md-12">
	<div>
		<?php if( isset( $company_address->company_logo ) && !empty( $company_address->company_logo ) ) { ?>
			<img src="<?= $company_address->company_logo?>" width="75" height="80" class="align-top mr-2 print_img" alt="" style="float: left;">
		<?php } else { ?>
			<img src="<?=  base_url() ?>assets/images/default-logo.png" width="75" height="80" class="align-top mr-2 print_img" alt="" style="float: left;">
		<?php }?>
	</div>
	
	<h3>
		<?= $company_address->company_name ?>
	</h3>
		<div>
			<?= $company_address->address ?? '' ?>
			<?= $company_address->city ?? '' ?>
			<?= $company_address->state ?? '' ?>
			<?= $company_address->country ?? '' ?>
			<?= $company_address->zip_code ?? '' ?>
		</div>
		
		<div >
			STATE: <?= $company_address->state ?? '' ?> 
		</div>
		<div >
			GSTIN: <?= $company_address->gstin_no ?? '' ?>, 
			PAN: <?= $company_address->pan_no ?? 'N/A' ?>
		</div>
		<div >
			SEED License: <?= $company_address->seed_license ?? '' ?> 
		</div>
	</div>

