<div style="padding: 10px;">
	<table class="own_table">
		<tr>
			<td style="width: 30%">
				<div>
					<?php if( isset( $own_info->image ) && !empty( $own_info->image ) ) { ?>
						<img src="<?= $own_info->image?>" width="160" height="80" class="align-top mr-2 print_img" alt="">
					<?php } else { ?>
						<img src="<?=  base_url() ?>assets/images/default-logo.png" width="160" height="80" class="align-top mr-2 print_img" alt="">
					<?php }?>
				</div>
			</td>
			<td style="width: 70%">
				<div>
					<div style="font-weight: 700;font-size: 15px;text-decoration: underline;">
						<?= $own_info->company_name ?>
					</div>
					<div>
						<?= $own_info->address ?? '' ?>
					</div>
					<div>
						<abbr title="Phone">Ph:</abbr> 
						<?= $own_info->phone_no ?? "(123) 456-34636" ?>, 
						Mobile: <?= $own_info->mobile ?? "+91 9*********" ?>
					</div>
					<div>
						<?= $own_info->website ?? "www.12334455.com" ?>, Email : <?= $own_info->email ?? "info@xxxxxxx.com" ?>
					</div>
					<div>
						Seeds Licenses # <?= $own_info->seed_license ?? 'xxxxxxxxxxx' ?>	
					</div>
				</div>
			</td>
		</tr>
	</table>
</div>