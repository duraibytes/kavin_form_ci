
	
	<div class="col-lg-12 col-md-12">
		<div>
			<?php if( isset( $own_info->image ) && !empty( $own_info->image ) ) { ?>
				<img src="<?= $own_info->image?>" width="75" height="80" class="align-top mr-2 print_img" alt="" style="float: left;">
			<?php } else { ?>
				<img src="<?=  base_url() ?>assets/images/default-logo.png" width="75" height="80" class="align-top mr-2 print_img" alt="">
			<?php }?>
		</div>
		<h3><?= $own_info->company_name ?></h3>
			<div><?= $own_info->address ?? '' ?></div>
			<div>Ph:
			<?= $own_info->phone_no ?? "(123) 456-34636" ?>, 
			Mobile: <?= $own_info->mobile ?? "+91 9*********" ?>
			</div>
			Email : <?= $own_info->email ?? "info@xxxxxxx.com" ?>
		<p>Seeds Licenses # <?= $own_info->seed_license ?? 'xxxxxxxxxxx' ?>	</p>
	</div>