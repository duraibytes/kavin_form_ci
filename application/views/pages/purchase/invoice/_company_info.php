<div style="padding: 10px;">
	<table class="company_table">
		<tr>
			<td style="width: 30%">
				<div>
					<?php if( isset( $company_address->company_logo ) && !empty( $company_address->company_logo ) ) { ?>
						<img src="<?= $company_address->company_logo?>" width="160" height="80" class="align-top mr-2 print_img" alt="">
					<?php } else { ?>
						<img src="<?=  base_url() ?>assets/images/default-logo.png" width="160" height="80" class="align-top mr-2 print_img" alt="">
					<?php }?>
				</div>
			</td>
			<td style="width: 70%">
				<div>
					<div style="font-weight: 700;font-size: 15px;text-decoration: underline;">
						<?= $company_address->company_name ?>
					</div>
					<div>
						<?= $company_address->address ?? '' ?>
						<?= $company_address->city ?? '' ?>
						<?= $company_address->state ?? '' ?>
						<?= $company_address->country ?? '' ?>
						<?= $company_address->zip_code ?? '' ?>
					</div>
					<div style="display: inline-flex;width: 100%">
						<div style="width: 50%">
							STATE: <?= $company_address->state ?? '' ?> 
						</div>
						<div style="width: 50%">
							GSTIN: <?= $company_address->gstin_no ?? '' ?>
						</div>
					</div>
					<div style="display: inline-flex;width: 100%">
						<div style="width: 50%">
							SEED License: <?= $company_address->seed_license ?? '' ?> 
						</div>
						<div style="width: 50%">
							PAN: <?= $company_address->pan_no ?? 'N/A' ?>
						</div>
					</div>
				</div>
			</td>
		</tr>
	</table>
</div>