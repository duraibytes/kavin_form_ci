<div class="block-header m-t-20">
    <?= $this->load->view( 'layout/_nav_header', '', true )?>
</div> 
<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                    <div class="table-responsive">
                        <div class="text-center hd-font"> 
                            <span class="btn btn-sm purchase-payment-bg text-white">
                                PURCHASE PAYMENTS
                            </span> 
                        </div>
                        <table class="table table-bordered table-hover w-100" id="purchase-payment-table">
                            <thead>
                                <tr class="purchase-payment-bg text-white">
                                    <th> 
                                        <div>Payment </div>
                                        <div> Date </div>
                                    </th>
                                    <th> 
                                        <div> Payment </div>
                                        <div> Receipt No </div>
                                    </th>
                                    <th> 
                                        <div> Purchase </div>
                                        <div> Inv Number</div>
                                    </th>
                                    <!-- <th> 
                                        <div> Payment  </div>
                                        <div> DueDate</div>
                                    </th> -->
                                    <th> 
                                        <div> Company </div>
                                        <div> Name </div>
                                    </th>
                                    <!-- <th> 
                                        <div> Total </div>
                                        <div> Current Bal </div>
                                    </th> -->
                                    <th> 
                                        <div> Paid </div>
                                        <div> Amount </div>
                                    </th>
                                    <th> 
                                        <div> Payment </div>
                                        <div> Method </div>
                                    </th>
                                    <th> 
                                        <div> Cheque </div>
                                        <div> IssueDate </div>
                                    </th>
                                    <th> 
                                        Status
                                    </th>
                                    <th style="width: 10px;"> Action </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        var dataTable = $( '#purchase-payment-table' ).DataTable( {
            'processing' : true,
            'serverSide' : true,
            'order'      : [],
            'ajax'       : {
                url      : '<?= site_url( 'purchase/get_purchase_payment' ); ?>',
                type     : 'POST',
                data     : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>' }
            },
            'emptyTable' : 'No data available in table',
            'columnDefs' : [
                {  
                    'targets'   : [ 6 ],
                    'orderable' : false,
                }
            ],
            "lengthMenu": [
                [5, 15, 20, 50, 100, -1],
                [5, 15, 20, 50, 100, "All"] 
            ],
            "pageLength": '<?=NO_OF_DT_ROWS;?>',
        });
    });

    function delete_purchase_payment( id ) {
        var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&id=' + id;
        $.ajax( {
            type    : "POST",
            url     : '<?=site_url( 'purchase/delete_purchase_payment' );?>',
            data    : data,
            success: function( msg ) {
                toastr.success( 'purchase payment deleted successfully', 'Purchase payment' );
                setTimeout( function() {
                    window.location.href = '<?= site_url()?>purchase-payment';
                }, 300 );
            }
        });
        return false;
    }
</script>