<div class="block-header">
    <?= $this->load->view( 'layout/_nav_header', '', true )?>
</div> 
<div class="container-fluid">
   
    <div class="row clearfix">
       
        <div class="col-lg-12">
            <div class="card">
                <?php echo form_open_multipart( '', 'id="purchase_payment_form"' ); ?> 
                    <div class="body demo-masked-input">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 m-b-10">
                                <div class="text-center hd-font"> 
                                    <span class="btn btn-sm purchase-payment-bg text-white">
                                      PURCHASE PAYMENTS
                                    </span> 
                                </div>
                            </div>
                            
                           <div class="col-lg-6 col-md-6">
                                <div class="inline-class" >
                                    <div class="w-50 vcenter"> 
                                        Pay to Company <span class="text-danger">*</span>
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <select class="custom-select" id="to_company" name="to_company" onchange="return get_current_balance( this.value )">
                                           <option value="" selected>Select</option>
                                             <?php
                                              if( isset( $company_all ) && !empty( $company_all ) ) {
                                                foreach ( $company_all as $key => $value ) {
                                                  $selected         = '';
                                                  if( isset( $payment_info->from_company ) && $payment_info->from_company == $value->id ) {
                                                    $selected       = 'selected';
                                                  }
                                                    if( isset( $purchase_info->from_company ) && $purchase_info->from_company == $value->id ) {
                                                        $selected       = 'selected';
                                                    }
                                              ?>
                                                <option value="<?= $value->id ?>" <?= $selected ?>> <?= $value->company_name ?></option>
                                            <?php } } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="inline-class alert alert-warning m-t-20" >
                                    <div class="w-50 vcenter"> 
                                        Total Current Balance <span class="text-danger">*</span>
                                    </div>
                                    <div class="w-50 vcenter" > 
                                        RS. <span id="current_balance"> <?= $current_balance ?></span>
                                        <input type="hidden" id="total_current_balanace" name="total_current_balanace" value="<?= $current_balance ?>">
                                    </div>
                                </div>
                                <div class="inline-class" >
                                    <div class="w-50 vcenter"> 
                                        Payment Method <span class="text-danger">*</span>
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <select class="custom-select" id="payment_method" name="payment_method">
                                            <option value="" selected>-- select method -- </option>
                                            <?php
                                      if( isset( $payment ) && !empty( $payment ) ) {
                                        foreach ( $payment as $pay_key => $pay_value ) {
                                          $selected         = '';
                                          if( isset( $payment_info->payment_method_id ) && $payment_info->payment_method_id == $pay_value->id ) {
                                            $selected       = 'selected';
                                          }
                                      ?>
                                        <option value="<?= $pay_value->id ?>" <?= $selected ?>> <?= $pay_value->name ?></option>
                                      <?php } } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="inline-class not-cash" >
                                    <div class="w-50 vcenter"> 
                                        Bank <span class="text-danger">*</span>
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <select class="custom-select" id="bank_id" name="bank_id" onchange="get_bank_branch_info( this.value )">
                                            <option value="" selected>-- select Bank -- </option>
                                            <?php
                                                if( isset( $bank_info ) && !empty( $bank_info ) ) {
                                                    foreach ( $bank_info as $key => $value ) {
                                                        $selected       = '';
                                                        if( isset( $payment_info->bank_id ) && $payment_info->bank_id == $value->bank_id ) {
                                                            $selected   = 'selected';
                                                        }
                                                        ?>
                                                        <option value="<?= $value->bank_id ?>" <?= $selected ?>> <?= $value->bank_name ?> </option>
                                            <?php   }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="inline-class not-cash" >
                                    <div class="w-50 vcenter"> 
                                        Bank Branch <span class="text-danger">*</span>
                                    </div>
                                    <div class="w-50 vcenter" id="bank_branch_dropdown"> 
                                        <input type="text" name="bank_branch" id="bank_branch" class="form-control" autocomplete="off" value="<?= $payment_info->bank_branch ?? '' ?>" readonly>
                                        <input type="hidden" name="bank_branch_id" id="bank_branch_id" value="<?= $payment_info->bank_branch_id ?? '' ?>">
                                    </div>
                                </div>
                                 <div class="inline-class not-cash" >
                                    <div class="w-50 vcenter"> 
                                        IFSC code
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <input type="text" name="ifsc_code" id="ifsc_code" class="form-control" autocomplete="off" value="<?= $payment_info->ifsc ?? '' ?>" readonly>
                                    </div>
                                </div>
                                <div class="inline-class not-cash" >
                                    <div class="w-50 vcenter"> 
                                        Bank Account Number <span class="text-danger">*</span>
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <input type="text" name="bank_ac_no" id="bank_ac_no" value="<?= $payment_info->bank_ac_no ?? ''  ?>" class="form-control" readonly> 
                                    </div>
                                </div>
                                <div class="inline-class cheque_type dd_type not-cash" style="display: none;">
                                    <div class="w-50 vcenter"> 
                                        Cheque / DD / UTR Ref No <span class="text-danger">*</span>
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <input type="text" name="cheque_dd_no" id="cheque_dd_no" class="form-control" value="<?= $payment_info->cheque_no ?? '' ?>"> 
                                    </div>
                                </div>
                                <div class="inline-class cheque_type not-cash" style="display: none;">
                                    <div class="w-50 vcenter"> 
                                        Cheque Issued Date <span class="text-danger">*</span>
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <input data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="dd/mm/yyyy" name="cheque_issued_date" value="<?= isset( $payment_info->issue_date ) && !empty( $payment_info->issue_date ) ? date( 'd/m/Y', strtotime( $payment_info->issue_date )): '' ?>"> 
                                    </div>
                                </div>
                                <div class="inline-class cheque_type not-cash" style="display: none;">
                                    <div class="w-50 vcenter"> 
                                        Cheque Issued Amount <span class="text-danger">*</span>
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <input type="text" name="cheque_issued_amount" id="cheque_issued_amount" class="form-control" value="<?= $payment_info->cheque_issue_amount ?? '' ?>"> 
                                    </div>
                                </div>
                                <div class="inline-class not-cash" >
                                    <div class="w-50 vcenter"> 
                                        Bank Additional Charge 
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <input type="text" name="bank_add_charge" class="form-control" value="<?= $payment_info->bank_charges ?? '' ?>" >
                                    </div>
                                </div>
                                <div class="inline-class" >
                                    <div class="w-50 vcenter"> 
                                        Total Amount <span class="text-danger">*</span>
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <input type="text" name="total_amount" id="total_amount" class="form-control" onchange="return check_valid_amount( this.value )" value="<?= $payment_info->amount ?? '' ?>" >
                                    </div>
                                </div>
                                
        		            </div>

    	                   <div class="col-lg-6 col-md-6">
                             <div class="inline-class" >
                                    <div class="w-50 vcenter"> 
                                        Payment Receipt Number <span class="text-danger">*</span>
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <input type="text" name="payment_receipt_no" id="payment_receipt_no" value="<?= $purchase_pay_no ?? '' ?>" class="form-control" readonly> 
                                    </div>
                                </div>
                                <div class="inline-class" >
                                    <div class="w-50 vcenter"> 
                                        Payment Date <span class="text-danger">*</span>
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <input data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="dd/mm/yyyy" name="payment_date" autocomplete="off" value="<?= isset( $payment_info->payment_date ) && !empty( $payment_info->payment_date ) ? date( 'd/m/Y', strtotime( $payment_info->payment_date )): date( 'd/m/Y' ) ?>" readonly>
                                    </div>
                                </div>
                                <div class="inline-class" >
                                    <div class="w-50 vcenter"> 
                                      Payment towards Advance Booking ? :
                                    </div>
                                    <div class="w-50 vcenter"> 
                                      <label class="fancy-radio custom-color-green">
                                        <input name="bookin" value="yes" type="radio" <?php if( isset( $payment_info->is_advance_booking ) && $payment_info->is_advance_booking == 'yes' ) { ?>checked <?php } ?> onclick="return check_advance_booking( this.value )">
                                        <span><i></i> Yes</span>
                                    </label>
                                      <label class="fancy-radio custom-color-green">
                                        <input name="bookin" value="no" type="radio" <?php if( isset( $payment_info->is_advance_booking ) && $payment_info->is_advance_booking == 'no' ) { ?>checked <?php } ?> onclick="return check_advance_booking( this.value )">
                                        <span><i></i> 
                                      No</span></label>
                                    </div>
                                </div>
                                <?php
                                $display        = 'none';
                                if( isset( $payment_info->is_advance_booking ) && $payment_info->is_advance_booking == 'no' ) {
                                    ?>
                                    <div class="inline-class show_purchase_invoice" >
                                <?php } else { ?>
                                    <div class="inline-class show_purchase_invoice" style="display: none;">
                                <?php } ?>
                                
                                    <div class="w-50 vcenter"> 
                                        Purchase Invoice 
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <input type="text" name="purchase_invoice_number" id="purchase_invoice_number" class="form-control purchase_typeahead" autocomplete="off" value="<?= $payment_info->invoice_no ?? ( $purchase_info->invoice_no ?? '' ) ?>" <?php if( isset( $payment_info->invoice_no ) && !empty( $payment_info->invoice_no ) ) { echo 'readonly'; } ?> <?php if( isset( $purchase_info->invoice_no ) && !empty( $purchase_info->invoice_no ) ) { echo 'readonly'; } ?>> 
                                        <input type="hidden" name="purchase_id" id="purchase_id" value="<?= $payment_info->purchase_id ?? ( $purchase_info->id ?? '' ) ?>" >
                                        <input type="hidden" name="invoice_topay" id="invoice_topay">
                                    </div>
                                </div>
                                <?php
                                $display        = 'none';
                                if( isset( $payment_info->is_advance_booking ) && $payment_info->is_advance_booking == 'yes' ) {
                                    ?>
                                    <div class="inline-class show_description" >
                                <?php } else { ?>
                                    <div class="inline-class show_description" style="display:none">
                                <?php }
                                ?>
                                    <div class="w-50 vcenter"> 
                                        Description
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <input type="text" name="advance_description" id="advance_description" class="form-control" value="<?= $payment_info->advance_description ?? '' ?>">
                                    </div>
                                </div>

                                <div class="inline-class alert alert-warning m-t-20" id="invoice_amount"<?php if( isset( $topay_invoice_amount ) && !empty( $topay_invoice_amount ) ) { } else { ?> style="display: none;"<?php } ?>>
                                    <div class="w-50 vcenter"> 
                                        Purchase To Pay Amount <span class="text-danger">*</span>
                                    </div>
                                    <div class="w-50 vcenter" > 
                                        RS. <span id="to_pay"> <?= $topay_invoice_amount ?? '0.00' ?></span>
                                        <input type="hidden" id="topay_invoice_amount" name="topay_invoice_amount" value="<?= $topay_invoice_amount ?? '' ?>">
                                    </div>
                                </div>

                                <div class="inline-class" style="border-bottom: none;" >
                                    <div class="w-50 vcenter"> Attach Payment info here </div>
                                    <div class="w-50 vcenter"> 
                                        <input type="file" class="" name="attachment"> 
                                        <?php
                                        if( isset( $payment_info->attachment ) && !empty( $payment_info->attachment ) ) {
                                        ?>
                                        <span> 
                                            <a href="<?= $payment_info->attachment ?>" target="_blank">
                                                View File
                                            </a>
                                        </span>
                                    <?php } ?>
                                    </div>
                                </div>
                                
                            </div>
    		
    		            </div>
    		              <input type="hidden" name="id" value="<?= $payment_info->id ?? '' ?>">
    		            <div class="row clearfix" style="text-align:right;">
    		                <div class="col-lg-12 col-md-12">
    		                    <a href="<?= base_url() ?>purchase-payment" class="btn btn-outline-danger" >
                                    Cancel
                                </a>
                                <?php if( isset( $payment_info->id ) && !empty( $payment_info->id ) ) { } else { ?>
                                <a  class="btn btn-outline-dark" href="javascript:;" onclick="return clear_form( 'purchase_payment_form' )">Clear</a>
                                <?php } ?>
    		                    <button class="btn btn-outline-info" onclick="return insert_purchase_payment()">
                                Submit
                                <span id="submit-loading" class="spinner-border spinner-border-sm" style="display: none;"></span>
                            </button>
    		                    <button  class="btn btn-success" type="button" onclick="return insert_purchase_payment( 'print' )">
                                   Submit & Print
                                   <span id="submit-loading-print" class="spinner-border spinner-border-sm" style="display: none;"></span>
                                </button>
                		    </div>
                		</div>
                    </div>
                    <?= form_close() ?>
            </div>
        </div>
    </div>
					
</div>
<div id="printableArea" style="display: none;"></div>
<script type="text/javascript">

    $(document).ready( function(){

    
        $('input.bank_typeahead' ).typeahead({ 
            source:  function (query, process) {
                var bank_id = $( '#bank_id' ).val();
                if( bank_id == '' ) {
                    toastr.error( 'Please select To Company first', 'Purchase Payment' );
                    $( '#bank_branch' ).val( '' );
                    $( '#bank_id' ).focus();
                    return false;
                }     
                return $.get( '<?=site_url( 'purchase/get_branch_typeahead' );?>', { query: query, bank_id:bank_id }, function (data) {
                    data = $.parseJSON( data );
                    return process(data);
                });
            },
            updater:function ( item ) {
                if( item.id ) {
                    $( '#bank_branch_id' ).val( item.id );
                }
                if( item.ifsc ) {
                    $( '#ifsc_code' ).val( item.ifsc );
                }
                return item;
            }
        });


        $('input.purchase_typeahead' ).typeahead({ 
            source:  function (query, process) {
                var return_company = $( '#to_company' ).val();
                if( return_company == '' ) {
                    toastr.error( 'Please select To Company first', 'Purchase Payemnt' );
                    $( '#purchase_invoice_number' ).val( '' );
                    $( '#to_company' ).focus();
                    return false;
                }     
                return $.get( '<?=site_url( 'purchase/get_purchase_typeahead' );?>', { query: query, return_company:return_company }, function (data) {
                    data = $.parseJSON( data );
                    return process(data);
                });
            },
            updater:function ( item ) {
                if( item.id ) {
                    $( '#purchase_id' ).val( item.id );
                }
                if( item.topay_amount ) {
                    $( '#to_pay' ).html( item.topay_amount );
                    $( '#topay_invoice_amount' ).val( item.topay_amount );
                    $( '#invoice_amount' ).show();
                }
                return item;
            }
        });
    });

    $(function () {

        $( '.custom-select' ).select2();

        $("#payment_method").change(function () {

            var selectedText    = $(this).find("option:selected").text();
            selectedText        = selectedText.trim();
            selectedText        = selectedText.toUpperCase();
            
            if( selectedText == 'CHEQUE' ) {
                $( '.cheque_type' ).show();
                $( '.not-cash' ).show();
            } else if( selectedText == 'DD' ) {
                $( '.cheque_type' ).hide();
                $( '.dd_type' ).show();
                $( '.not-cash' ).show();
            } else if( selectedText == 'CASH' ) {
                $( '.not-cash' ).hide();
                $( '.cheque_type' ).hide();
                $( '.dd_type' ).hide();
            } else {
                $( '.not-cash' ).show();
                $( '.cheque_type' ).hide();
                $( '.dd_type' ).hide();
            }
        });       

    });

    function get_current_balance( company_id ) {
        var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&company_id=' + company_id;
        $.ajax( {
            type    : "POST",
            url     : '<?=site_url( 'purchase/get_current_balance' );?>',
            data    : data,
            dataType: 'json',
            success: function( msg ) {
                if( msg.amount ) {
                    $( '#current_balance' ).html( msg.amount );
                    $( '#total_current_balanace' ).val( msg.amount );
                } else {
                    $( '#current_balance' ).html( '0.00' );
                }
               
            }
        });
        return false;
    }

    function insert_purchase_payment( print = '' ) {
      var purchase_payment_form     = $( '#purchase_payment_form' ).find('input, textarea, button, select');
      var data                      = new FormData( $( '#purchase_payment_form' )[0] );
      $.ajax({
        type        : 'POST',
        url         : '<?=site_url( 'purchase/insert_purchase_payment' );?>',
        data        : data,
        async       : false,
        contentType : false,
        processData : false,
        dataType    : 'json',
        beforeSend: function () {
            
          purchase_payment_form.attr( 'disabled', true );
        },    
        success     : function( msg ) {
          if( msg.error == 1 ) {
            toastr.error( msg.error_msg, 'Purchase payment' );
            purchase_payment_form.attr( 'disabled', false );
           
          } else {
            toastr.success( msg.error_msg, 'Purchase payment' );
            if( print ) {
                if( msg.slug ) {
                  print_purchase_payment( msg.slug );
                }
            }
            setTimeout( function(){
                window.location.href = '<?= site_url()?>purchase-payment';
            }, 1200 );
          }

        }
      });
      return false;
    }

    function check_valid_amount( amount ) {
        var total_current_balanace  = $( '#total_current_balanace' ).val();
        total_current_balanace      = parseFloat( total_current_balanace );
        var total_amount            = parseFloat( amount );
        if( total_amount > total_current_balanace ) {
            toastr.error( 'Entered amount greater than total current balance', 'Purchase Payment' );
            $( '#total_amount' ).val( '' );
            $( '#total_amount' ).focus();
        }
        
    }

    function get_bank_branch_info( bank_id ) {
        var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&bank_id=' + bank_id;
        $.ajax( {
            type    : "POST",
            url     : '<?=site_url( 'purchase/get_bank_branch_info' );?>',
            data    : data,
            beforeSend: function(){
                $( '#ifsc_code' ).val( "" );
                $( '#bank_ac_no' ).val( "" );
            },
            success: function( msg ) {
                $( '#bank_branch_dropdown' ).html( msg );
            }
        });
        return false;
    }

    function get_branch_ifsc_info( branch_id ) {
        var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&branch_id=' + branch_id;
        $.ajax( {
            type    : "POST",
            url     : '<?=site_url( 'purchase/get_branch_ifsc_info' );?>',
            data    : data,
            dataType: 'json',
            beforeSend: function(){
                $( '#ifsc_code' ).val( "" );
                $( '#bank_ac_no' ).val( "" );
            },
            success: function( msg ) {
                
                if( msg.ifsc_code ) {
                    $( '#ifsc_code' ).val( msg.ifsc_code );
                }
                if( msg.bank_ac_no ) {
                    $( '#bank_ac_no' ).val( msg.bank_ac_no );
                }
            }
        });
        return false;
    }

    function check_advance_booking( value ) {

        if( value == 'no' ) {
            $( '.show_purchase_invoice' ).show();
            $( '.show_description' ).hide();
        } else {
            $( '.show_purchase_invoice' ).hide();
            $( '.show_description' ).show();
        }
    }

</script>