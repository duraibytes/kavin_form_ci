<div class="block-header m-t-20">
    <?= $this->load->view( 'layout/_nav_header', '', true )?>
</div> 
<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body" id="printarea">
                    <h6><center><strong class="text-primary">PURCHASE PAYMENT INVOICE</strong></center></h6>
       				
			        <div class="tab-content mt-3">
			            <div role="tabpanel" class="tab-pane in active" id="details" aria-expanded="true">
			                <?php 
			                // print_r( $payment_info );
			                ?>
							<div class="row">
			                    <div class="col-md-5 col-sm-12">
			                    	<div class="row text-right">
				                    	<?= $this->load->view( 'pages/purchase/invoice/_payment_company_info', '', true )?>
			                    	</div>
				                </div>
								<div class="col-sm-12 col-md-1 col-lg-1"></div>
			                    <div class="col-md-5 col-sm-12 text-right" >	
									<div class="row">
										<?= $this->load->view( 'pages/purchase/invoice/_payment_own_info', '', true )?>	
				                     </div>
			                    </div>
		                	</div>
			                	
			               
			                <hr>
			                <div class="row">
			                	<div class="col-sm-6 ">
			                		Payment Date
			                	</div>
			                	<div class="col-sm-6">
			                		<?= date( 'd-M-Y', strtotime( $payment_info->payment_date ) ) ?>
			                	</div>
			                </div>
			                <div class="row">
			                	<div class="col-sm-6 ">
			                		Payment Number
			                	</div>
			                	<div class="col-sm-6">
			                		<?= $payment_info->receipt_no ?>
			                	</div>
			                </div>
			                <div class="row">
			                	<div class="col-sm-6 ">
			                		Bank
			                	</div>
			                	<div class="col-sm-6">
			                		<?= $payment_info->bank_name?>
			                	</div>
			                </div>
			                <div class="row">
			                	<div class="col-sm-6 ">
			                		Branch
			                	</div>
			                	<div class="col-sm-6">
			                		<?= $payment_info->bank_branch?>
			                	</div>
			                </div>
			                <div class="row">
			                	<div class="col-sm-6 ">
			                		Bank IFSC CODE
			                	</div>
			                	<div class="col-sm-6">
			                		<?= $payment_info->ifsc?>
			                	</div>
			                </div>
			                <div class="row">
			                	<div class="col-sm-6 ">
			                		Bank Account Number
			                	</div>
			                	<div class="col-sm-6">
			                		<?= $payment_info->bank_ac_no?>
			                	</div>
			                </div>
			                <div class="row">
			                	<div class="col-sm-6 ">
			                		Is Advance Booking
			                	</div>
			                	<div class="col-sm-6">
			                		<?= ucfirst( $payment_info->is_advance_booking ) ?>
			                	</div>
			                </div>
			                <?php 
			                	if( $payment_info->is_advance_booking == 'yes' ) {
			                ?>
			                <div class="row">
			                	<div class="col-sm-6 ">
			                		Advance Description 
			                	</div>
			                	<div class="col-sm-6">
			                		<?= $payment_info->advance_description ?>
			                	</div>
			                </div>
			            <?php } ?>
			                <div class="row">
			                	<div class="col-sm-6 ">
			                		Verified By
			                	</div>
			                	<div class="col-sm-6">
			                		<?= $payment_info->fname ?>
			                	</div>
			                </div>
			                <div class="row">
			                	<div class="col-sm-6 ">
			                		Amount
			                	</div>
			                	<div class="col-sm-6">
			                		Rs <?= number_format( $payment_info->amount, 2 ) ?>
			                	</div>
			                </div>
		                    <div class="col-md-12"style="padding:10px;border:1px solid #e0dddd;border-radius:20px;margin-top:10px; ">
		                        <span><b><u>Amount in words </u></b></span><span style="font-weight:normal;">: <?= number_to_word( $payment_info->amount ) ?>/-</span> 
		                    </div>									
			            </div>                        
			        </div>
    			</div>
            </div>
        </div>
    </div>
</div>
<div id="printableArea" style="display: none;"></div>

<script type="text/javascript">

	
	
</script>