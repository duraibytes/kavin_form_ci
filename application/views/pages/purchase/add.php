<style type="text/css">
  #purchase_item_table:hover {
    background: #ddd;
  }

 
</style>
<div class="block-header ">
    <?= $this->load->view( 'layout/_nav_header', '', true )?>
</div> 
<div class="container-fluid">
  <div class="row clearfix">
    <div class="col-lg-12">
      <div class="card">
          <?php echo form_open_multipart( '', 'id="purchase_form"' ); ?> 
          <div class="body">
            
                  <div class="demo-masked-input">
                    <div class="row clearfix">
                      <div class="col-lg-12 col-md-12 m-b-10">
                        <div class="text-center hd-font"> 
                            <span class="btn btn-sm purchase-invoice-bg text-white">
                              PURCHASE INVOICES
                            </span> 
                          </div>
                      </div>
                      <div class="col-lg-6 col-md-6">
                       
                        <div class="inline-class" >
                            <div class="w-50 vcenter"> 
                                Purchase Invoice Number <span class="text-danger">*</span>
                            </div>
                            <div class="w-50 vcenter"> 
                                <input type="text" name="company_invoice_no" id="company_invoice_no" class="form-control" value="<?= $purchase_info->company_invoice_no ?? '' ?>"> 
                            </div>
                        </div>
                        
                        <div class="inline-class" >
                            <div class="w-50 vcenter"> 
                                From Company<span class="text-danger">*</span>
                            </div>
                            <div class="w-50 vcenter"> 
                                 <select class="custom-select" id="" name="from_company">
                                  <option value="" selected>Select</option>
                                  <?php
                                  if( isset( $company_all ) && !empty( $company_all ) ) {
                                    foreach ( $company_all as $key => $value ) {
                                      $selected         = '';
                                      if( isset( $purchase_info->from_company ) && $purchase_info->from_company == $value->id ) {
                                        $selected       = 'selected';
                                      }
                                  ?>
                                    <option value="<?= $value->id ?>" <?= $selected ?>> <?= $value->company_name ?></option>
                                  <?php } } ?>
                                  
                                </select>
                            </div>
                        </div>
                        <div class="inline-class mode_of_payment" >
                            <div class="w-50 vcenter"> 
                                Mode of Payment <span class="text-danger">*</span>
                            </div>
                            <div class="w-50 vcenter"> 
                              <div class="input-group">
                                <div class="mode_of_payment_ajax" style="width: 82%">
                                  <select class="custom-select" id="mode_of_payment" name="mode_of_payment" onchange="return get_due_days( this.value )">
                                    <option value="" selected>Select</option>
                                   <?php
                                    if( isset( $payment ) && !empty( $payment ) ) {
                                      foreach ( $payment as $pay_key => $pay_value ) {
                                        $selected         = '';
                                        if( isset( $purchase_info->mode_of_payment ) && $purchase_info->mode_of_payment == $pay_value->id ) {
                                          $selected       = 'selected';
                                        }
                                    ?>
                                      <option value="<?= $pay_value->id ?>" <?= $selected ?>> <?= $pay_value->name ?></option>
                                    <?php } } ?>
                                  </select>
                                </div>
                                <div class="input-group-prepend" onclick="return add_form( 'mode_of_payment_ajax' )">
                                  <div class="input-group-text" id="btnGroupAddon">
                                    <i class="fa fa-plus text-white"></i>
                                  </div>
                                </div>
                              </div>
                            </div> 
                        </div>
                        <div class="inline-class" >
                            <div class="w-50 vcenter"> 
                                Mode of Transport <span class="text-danger">*</span>
                            </div>
                            <div class="w-50 vcenter"> 
                              <div class="input-group">
                                <div class="mode_of_transport_ajax" style="width: 82%">
                                  <select class="custom-select" id="" name="mode_of_transport">
                                    <option value="" selected>Select</option>
                                   <?php
                                    if( isset( $transport_all ) && !empty( $transport_all ) ) {
                                      foreach ( $transport_all as $tran_key => $tran_value ) {
                                        $selected         = '';
                                        if( isset( $purchase_info->transport_id ) && $purchase_info->transport_id == $tran_value->id ) {
                                          $selected       = 'selected';
                                        }
                                    ?>
                                       <option value="<?= $tran_value->id ?>" <?= $selected ?>> <?= $tran_value->name ?></option>
                                    <?php } } ?>
                                  </select>
                                </div>
                                <div class="input-group-prepend" onclick="return open_add_transport_form( '', 'mode_of_transport_ajax' );">
                                  <div class="input-group-text" id="btnGroupAddon">
                                    <i class="fa fa-plus text-white"></i>
                                  </div>
                                </div>
                              </div>
                            </div>
                        </div>
                        <div class="inline-class" >
                            <div class="w-50 vcenter"> 
                                Name of Transport <span class="text-danger">*</span>
                            </div>
                            <div class="w-50 vcenter"> 
                                <input type="text" name="name_of_transport" id="name_of_transport" class="form-control" value="<?= $purchase_info->name_of_transport ?? '' ?>"> 
                            </div>
                        </div>
                        <div class="inline-class" >
                            <div class="w-50 vcenter"> 
                              Transportation Charge <span class="text-danger">*</span>
                            </div>
                            <div class="w-50 vcenter"> 
                                <select class="custom-select" id="tranport_payment" name="tranport_payment">
                                  <option value="" selected>Select</option>
                                   <option value="Paid" <?php if( isset( $purchase_info->tranport_payment ) && $purchase_info->tranport_payment == 'Paid' ) { echo 'selected'; } ?>>Paid</option>
                                    <option value="ToPay" <?php if( isset( $purchase_info->tranport_payment ) && $purchase_info->tranport_payment == 'ToPay' ) { echo 'selected'; } ?> >ToPay</option>
                                </select>
                            </div>
                        </div>
                        <div class="inline-class topay_transport" style="display:none;">
                            <div class="w-50 vcenter"> 
                                Transport Charge <span class="text-danger">*</span>
                            </div>
                            <div class="w-50 vcenter"> 
                                <input type="text" name="transport_charge" id="transport_charge" class="form-control" value="<?= $purchase_info->transport_charge ?? '' ?>" onchange="return get_total_amount( this.value )"> 
                            </div>
                        </div>
                        
                      </div>
                      <!--  -->
                      <div class="col-lg-6 col-md-6 m-t-5">
                        <div class="inline-class" >
                            <div class="w-50 vcenter"> 
                                Purchase Ref Number <span class="text-danger">*</span>
                            </div>
                            <div class="w-50 vcenter"> 
                                <input type="text" name="purchase_ref_no" id="purchase_ref_no" class="form-control" readonly value="<?= $purchase_ref_no ?? '' ?>"> 
                            </div>
                        </div>
                        <div class="inline-class" >
                            <div class="w-50 vcenter"> 
                                Purchase Date <span class="text-danger">*</span>
                            </div>
                            <div class="w-50 vcenter"> 
                                <input id="purchase_date" data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="dd/mm/yyyy" name="purchase_date" value="<?= isset( $purchase_info->purchase_date ) ? date( 'd/m/Y', strtotime( $purchase_info->purchase_date ) ) : '' ?>" autocomplete="off" > 
                            </div>
                        </div>
                        <div class="inline-class show_duedate" >
                            <div class="w-50 vcenter"> 
                                Invoice Due Date <span class="text-danger">*</span>
                            </div>
                            <div class="w-50 vcenter"> 
                              <input data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="dd/mm/yyyy" id="invoice_date" name="invoice_due_date" value="<?= isset( $purchase_info->invoice_due_date ) ? date( 'd/m/Y', strtotime( $purchase_info->invoice_due_date ) ) : '' ?>" autocomplete="off">
                            </div>
                        </div>
                        <div class="inline-class">
                            <div class="w-50 vcenter"> 
                              Attach Original Invoice here 
                            </div>
                            <div class="w-50 vcenter"> 
                              <input type="file" name="upload">
                              <?php
                                if( isset( $purchase_info->attachment ) && !empty( $purchase_info->attachment ) ) {
                              ?>
                                  <span>
                                    <a href="<?= $purchase_info->attachment ?>" target="_blank">Click to View File</a>
                                  </span>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="inline-class" >
                            <div class="w-50 vcenter"> 
                                Invoice VerifiedBy <span class="text-danger">*</span>
                            </div>
                            <div class="w-50 vcenter"> 
                              <?php 

                                if( isset( $purchase_info->fname ) && !empty( $purchase_info->fname ) ) {
                                    echo $purchase_info->fname .' ' . date( 'H:i A', strtotime( $purchase_info->created_at ) );
                                } else {
                                    date_default_timezone_set('Asia/Kolkata');
                                ?>
                                <?= $this->session->userdata( 'user_name' ) ?> <?= date( 'H:i A' ) ?>
                              <?php } ?>
                            </div>
                        </div>
                        
                      </div>
                      <div class="col-lg-12 col-md-12 m-t-20">
                        
                        <div class="">
                          <table class="table table-bordered table-striped" cellspacing="0" id="purchase_item_table">
                              <thead>
                                  <tr class="purchase-invoice-bg text-white">
                                      
                                      <th>
                                        <div> Product </div>
                                        <div> Code </div>
                                      </th>
                                      
                                      <th>
                                        <div> Product </div>
                                        <div> LOT </div>
                                      </th>
                                      <th>
                                        <div> Date of </div>
                                        <div> Packed </div>
                                      </th>
                                      <th>
                                        <div> Date of </div>
                                        <div> Tested </div>
                                      </th>
                                      <th >
                                        <div> Date of </div>
                                        <div> Expiry </div>
                                      </th>
                                      <th>
                                        <div> Net </div>
                                        <div> Rate </div>
                                      </th>
                                      <th>Qty</th>
                                      <th>
                                        <div> UOM </div>
                                       
                                      </th>
                                      <th>
                                        <div>Discount</div>
                                        <div>(%)</div>
                                      </th>
                                      <th>
                                        Net Value
                                      </th>
                                      <th></th>                                              
                                      
                                  </tr>
                              </thead>
                              <tbody>
                                <?= $this->load->view( 'pages/purchase/_purchase_items', '', true ) ?>
                              </tbody>
                          </table>
                          <input type="hidden" name="total_item" id="total_item" value="<?= $total_item?>">
                          <div class="btn btn-outline-success w-100" id="add_new">
                            <i class="fa fa-plus" ></i> Add Product
                          </div>
                          <table class="table" style="font-size: 12px;" id="remarks_table" > 
                            <tr>
                              <td style="width: 60%;padding: 10px;text-align: left;">
                                <div>
                                  <label> Remarks:</label>
                                </div>
                                <div>
                                  <div class="inline-class" >
                                    <textarea rows="3" cols="3" class="form-control" name="remarks"><?= $purchase_info->remarks ?? '' ?></textarea>
                                  </div>
                                </div>
                              </td>
                              <td style="width: 40%">
                                <table style="width:100%;">
                                  <tr>
                                    <td>
                                      <div style="display: flex;padding: 10px">
                                        <div >
                                          GST ( % ) :  0.00% 
                                        </div>
                                        <div style="padding-left: 10px;">
                                          CST ( % ) :  0.00% 
                                        </div>
                                        <div style="padding-left: 10px;">
                                          VAT ( % ) :  0.00% 
                                        </div>
                                      </div>
                                    </td>

                                  </tr>
                                  <tr>
                                    <td>
                                      <div class="inline-class" >
                                        <div class="w-70 vcenter" style="width: 70%"> 
                                            Transport Charge <span class="text-danger">*</span>
                                        </div>
                                        <div class="w-30 vcenter text-right" style="width: 30%;"> 
                                            <div id="total_transport_charge" class="" style="    width: 100%;margin-right: 15px;"></div>
                                        </div>
                                      </div>
                                  </td>
                                </tr>
                                <!-- <tr>
                                    <td>
                                      <div class="inline-class" >
                                        <div class="w-70 vcenter" style="width: 70%"> 
                                            Discount (%)<span class="text-danger">*</span>
                                            <input type="text" name="purchase_discount" id="discount" class="form-control text-right" style="width: 45px;" placeholder="%">
                                        </div>
                                        <div class="w-30 vcenter text-right" style="width: 30%;"> 
                                            <div id="total_transport_charge" class="" style="    width: 100%;">
                                              <input type="text" name="purchase_discount_amount" id="purchase_discount_amount" class="form-control text-right" placeholder="0.00" >
                                            </div>
                                        </div>
                                      </div>
                                  </td>
                                </tr> -->
                                <tr>
                                  <td>
                                      <div class="inline-class" >
                                        <div class="w-70 vcenter" style="width: 70%"> 
                                            Total Amount <span class="text-danger">*</span>
                                        </div>
                                        <div class="w-30 vcenter"> 
                                            <input type="text" name="total_amount" id="total_amount" class="form-control text-right" value="<?= $purchase_info->total_amount ?? '' ?>"> 
                                            <input type="hidden" name="total_item_amount" id="total_item_amount">
                                             <input type="hidden" name="hidden_total_amount" id="hidden_total_amount" value="<?= $purchase_info->total_amount ?? 0 ?>">
                                        </div>
                                      </div>
                                  </td>
                                </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                          <div>

                            Amount In Words : 
                            <span id="amount_in_word" class="text-danger">
                              <?php
                                if( isset( $purchase_info->total_amount ) && !empty( $purchase_info->total_amount ) ) {
                              ?>
                                  <?= number_to_word( $purchase_info->total_amount ) ?>
                                <?php } ?>
                            </span>
                            
                          </div>
                      </div>
                      </div>
                    </div>
                  </div>
                <input type="hidden" name="purchase_id" id="purchase_id" value="<?= $purchase_info->id ?? '' ?>">
            <div class="row clearfix" style="text-align:right;">
              <div class="col-lg-12 col-md-12 m-t-10">
                  <a  class="btn btn-outline-danger" href="<?= base_url()?>purchase">
                    Cancel
                  </a>
                  <?php 
                  if( isset( $purchase_info->id ) && !empty( $purchase_info->id ) ) {

                  } else {



                  ?>
                  <a  class="btn btn-outline-dark" href="javascript:;" onclick="return clear_form( 'purchase_form' )"> Clear </a>
                <?php } ?>
                  <button id="addToTable" class="btn btn-outline-info" type="button" onclick="return insert_purchase();"> Submit </button>
                  <button id="addToTable" class="btn btn-success" type="button" onclick="return insert_purchase( 'print' )"> Submit & print </button>
              </div>
            </div>
          <?= form_close() ?>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="printableArea" style="display: none;"></div>
<script type="text/javascript">
  //Autocomplete
  $(function () {
    // $( '.custom-select' ).select2();
  });
   function deleteRow( button ) {
      var row = button.parentNode.parentNode;
      var name = row.getElementsByTagName("TD")[0].innerHTML;
      //Get the reference of the Table.
      var table = document.getElementById("purchase_item_table");
      //Delete the Table row using it's Index.
      table.deleteRow(row.rowIndex);
    }

    $("#add_new").click(function(){
      var total_item    = $( '#total_item' ).val();
      var count         = parseInt( total_item ) + 1;
      $( '#total_item' ).val( count );
       var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&total_item=' + count;
      $.ajax( {
          type    : "POST",
          url     : '<?= site_url( 'purchase/get_ajax_items' ); ?>',
          data    : data,
          success: function( msg ) {
            $( "table#purchase_item_table tbody" ).append( msg );
          }
      });
      return false;
    });

    

    function insert_purchase( print = '' ) {
      var purchase_form       = $( '#purchase_form' ).find('input, textarea, button, select');
      var data                = new FormData( $( '#purchase_form' )[0] );
      $.ajax({
        type        : 'POST',
        url         : '<?=site_url( 'purchase/insert_purchase' );?>',
        data        : data,
        async       : false,
        contentType : false,
        processData : false,
        dataType    : 'json',
        beforeSend: function () {
          purchase_form.attr( 'disabled', 'disabled' );
        },    
        success     : function( msg ) {
          if( msg.error == 1 ) {
            toastr.error( msg.error_msg, 'Purchase' );
            purchase_form.attr( 'disabled', false );
          } else {
            toastr.success( msg.error_msg, 'Purchase' );
            if( print ) {
              if( msg.slug ) {
                print_purchase( msg.slug );
              }
            }
            setTimeout( function(){
               window.location.href = '<?= site_url()?>purchase';
            }, 1200 );
          }
        }
      });
      return false;
    }
    
    $( '#tranport_payment' ).change( function() {
     
         var transport_payment  = $(this).val();
         
         if( transport_payment == 'ToPay' ) {
           $( '.topay_transport' ).show();
         } else {
            $( '.topay_transport' ).hide();
         }
      
    });
    $( '#invoice_date' ).click( function() {
     
        var payment     = $( '#mode_of_payment' ).val();
        
        if( payment == '' || payment == 'undefined' )  {
          toastr.error( 'Mode of Payment is required', 'Purchase' );
            $( '#mode_of_payment' ).focus();
            $( '#invoice_date' ).val( '' );
        }
      
    });

    function get_due_days( payment_id ) {
      var purchase_date   = $( '#purchase_date' ).val();
      if( purchase_date == '' || purchase_date == 'undefined' )  {
          toastr.error( 'Purchase Date is required', 'Purchase' );
          $( '#purchase_date' ).focus();
          $( '#mode_of_payment' ).val( '' );

          return false;
        }
      var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&payment_id=' + payment_id + '&purchase_date=' +purchase_date;
      $.ajax( {
          type    : "POST",
          url     : '<?= site_url( 'purchase/get_due_days' ); ?>',
          data    : data,
          dataType:'json',
          success: function( msg ) {
            $( "#invoice_date" ).val( msg.due_day );
            if( msg.is_advance ) {
              $( '.show_duedate' ).hide();
            } else{
              $( '.show_duedate' ).show();
            }
          }
      });
      return false;
    }

    function get_total_amount( amount ) {

      var tr_amount         = parseFloat( amount );
      var total_amount      = $( '#total_item_amount' ).val();

      $( '#total_transport_charge' ).html( tr_amount );
      console.log( tr_amount );
      if( total_amount == '' || total_amount == undefined ) {
          
         total_amount       = 0;
         total_amount       = parseFloat( total_amount );
         total_amount       = total_amount + tr_amount;
      } else {
          total_amount      = parseFloat( total_amount );
          
          total_amount      = total_amount + tr_amount;
          
      }
      $( '#total_amount' ).val( total_amount );
      $( '#hidden_total_amount' ).val( total_amount );
    }
        
    $( '#purchase_discount_amount' ).change(function(){
        var dis_amount    = $(this).val();
        var total_amount  = $( '#hidden_total_amount' ).val();

        total_amount      = parseFloat( total_amount );
        dis_amount        = parseFloat( dis_amount );

        total_amount      = total_amount - dis_amount;

        $( '#total_amount' ).val( total_amount );
    });
</script>