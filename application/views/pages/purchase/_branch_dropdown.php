

<select class="form-control" name="bank_branch_id" id="bank_branch_id" onchange="return get_branch_ifsc_info( this.value )">
	<option value=""> -- select -- </option>
	<?php
	if( isset( $branch_info ) && !empty( $branch_info ) ) {
		foreach ($branch_info as $key => $value) {
			?>
			<option value="<?= $value->branch_id?>"><?= $value->branch ?></option>
	<?php	}
	}
	?>
</select>