<style type="text/css">
  #purchase_return_table:hover {
    background: #ddd;
  }
</style>
<div class="block-header">
    <?= $this->load->view( 'layout/_nav_header', '', true )?>
</div> 
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12">
                  <?php echo form_open_multipart( '', 'id="purchase_return_form"' ); ?> 
                    <div class="card">
                       
                        <div class="body">
                          <div class="demo-masked-input">
                            <div class="row clearfix">
                              <div class="col-lg-12 col-md-12 m-b-10">
                                  <div class="text-center hd-font"> 
                                      <span class="btn btn-sm purchase-return-bg text-white">
                                        PURCHASE RETURNS
                                      </span> 
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6">
                                <div class="inline-class" >
                                    <div class="w-50 vcenter"> 
                                        Return to Company<span class="text-danger">*</span>
                                    </div>
                                    <div class="w-50 vcenter"> 
                                         <select class="custom-select" id="return_company" name="return_company">
                                          <option value="" selected>Select</option>
                                         <?php
                                          if( isset( $company_all ) && !empty( $company_all ) ) {
                                            foreach ( $company_all as $key => $value ) {
                                              $selected         = '';
                                              if( isset( $return_info->return_to ) && $return_info->return_to == $value->id ) {
                                                $selected       = 'selected';
                                              }
                                          ?>
                                            <option value="<?= $value->id ?>" <?= $selected ?>> <?= $value->company_name ?></option>
                                        <?php } } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="inline-class" >
                                    <div class="w-50 vcenter"> 
                                        Purchase Invoice Number <span class="text-danger">*</span>
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <input type="text" name="purchase_invoice_no" id="purchase_invoice_no" class="form-control purchase_invoice_no" autocomplete="off" value="<?= $return_info->purchase_invoice_no ?? ''  ?>"> 
                                        <input type="hidden" name="purchase_id" id="purchase_id" value="<?= $return_info->purchase_id ?? '' ?>">
                                    </div>
                                </div>
                                <div class="inline-class" >
                                    <div class="w-50 vcenter"> 
                                        Reason for Return <span class="text-danger">*</span>
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <input type="text" name="reason" id="reason" class="form-control" value="<?= $return_info->reason ?? ''  ?>">
                                    </div>
                                </div>
                                <div class="inline-class" >
                                    <div class="w-50 vcenter"> 
                                        Mode of Transport <span class="text-danger">*</span>
                                    </div>
                                    <?php 
                                      // print_r( $purchase_info );
                                    ?>
                                    <div class="w-50 vcenter"> 
                                      <div class="input-group">
                                        <div class="mode_of_transport_ajax" style="width: 82%">
                                          <select class="custom-select" id="" name="mode_of_transport">
                                            <option value="" selected>Select</option>
                                           <?php
                                            if( isset( $transport_all ) && !empty( $transport_all ) ) {
                                              foreach ( $transport_all as $tran_key => $tran_value ) {
                                                $selected         = '';
                                                if( isset( $return_info->transport_id ) && $return_info->transport_id == $tran_value->id ) {
                                                  $selected       = 'selected';
                                                }
                                            ?>
                                               <option value="<?= $tran_value->id ?>" <?= $selected ?>> <?= $tran_value->name ?></option>
                                            <?php } } ?>
                                          </select>
                                        </div>
                                        <div class="input-group-prepend" onclick="return open_add_transport_form( '', 'mode_of_transport_ajax' );">
                                          <div class="input-group-text" id="btnGroupAddon">
                                            <i class="fa fa-plus text-white"></i>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                </div>
                                <div class="inline-class" >
                                    <div class="w-50 vcenter"> 
                                        Name of Transport <span class="text-danger">*</span>
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <input type="text" name="name_of_transport" id="name_of_transport" class="form-control" value="<?= $return_info->transport_name ?? '' ?>"> 
                                    </div>
                                </div>
                              </div>

                              <!--  -->
                              <div class="col-lg-6 col-md-6">
                                <div class="inline-class" >
                                    <div class="w-50 vcenter"> 
                                        Purchase Return Number <span class="text-danger">*</span>
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <input type="text" name="purchase_return_no" id="purchase_return_no" class="form-control" readonly value="<?= $purchase_return_ref_no ?? '' ?>"> 
                                    </div>
                                </div>
                                <div class="inline-class" >
                                    <div class="w-50 vcenter"> 
                                        Return Date <span class="text-danger">*</span>
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <input type="text" name="return_date" readonly class="form-control" value="<?= isset( $return_info->return_date ) ? date( 'd-m-Y', strtotime( $return_info->return_date ) ) :  date( 'd-m-Y' ) ?>">
                                    </div>
                                </div>
                                <div class="inline-class"  >
                                    <!-- <div class="w-50 vcenter"> Upload </div>
                                    <div class="w-50 vcenter"> <input type="file" class="dropify"> </div> -->
                                    <div class="w-50 vcenter"> 
                                        Upload Final Credit Note
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <input type="file" name="upload">
                                        <?php
                                          if( isset( $return_info->attachment ) && !empty( $return_info->attachment ) ) {
                                            ?>
                                            <span>
                                              <a href="<?= $return_info->attachment ?>" target="_blank">
                                                 View File
                                              </a>
                                            </span>
                                         <?php }
                                        ?>
                                    </div>
                                </div>
                                
                                <div class="inline-class" >
                                    <div class="w-50 vcenter"> 
                                      Transportation Payment <span class="text-danger">*</span>
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <select class="form-control" id="transport_payment" name="transport_payment">
                                          <option value="" selected>Select</option>
                                           <option value="Paid" <?php if( isset( $return_info->transport_payment ) && $return_info->transport_payment == 'Paid' ) { echo 'selected';} ?>>Paid</option>
                                            <option value="ToPay" <?php if( isset( $return_info->transport_payment ) && $return_info->transport_payment == 'ToPay' ) { echo 'selected';} ?> >ToPay</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="inline-class topay_transport" style="display:none;">
                                  <div class="w-50 vcenter"> 
                                    Transport Charge <span class="text-danger">*</span>
                                  </div>
                                  <div class="w-50 vcenter"> 
                                    <input type="text" name="transport_charge" id="transport_charge" class="form-control" value="<?= $purchase_info->transport_charge ?? '' ?>" onchange="return get_total_amount( this.value )"> 
                                  </div>
                                </div>
                                
                              </div>
                              <div class="col-lg-12 col-md-12 m-t-20">
                               
                                <div class="">
                                  <table class="table table-bordered table-striped" cellspacing="0" id="purchase_return_table">
                                      <thead>
                                        <tr class="purchase-invoice-bg text-white">
                                            <th>
                                              <div> Product </div>
                                              <div> Code </div>
                                            </th>
                                            <th>
                                              <div> Product </div>
                                              <div> Variety </div>
                                            </th>
                                            <th>
                                              <div> Product </div>
                                              <div> Desc </div>
                                            </th>
                                            
                                            <th>
                                              <div> Product </div>
                                              <div> LOT </div>
                                            </th>
                                            <th>
                                              <div> UOM </div>
                                             
                                            </th>
                                            <th>
                                              <div> Net </div>
                                              <div> Rate </div>
                                            </th>

                                            <th>
                                              <div> Date of </div>
                                              <div> Packed </div>
                                            </th>
                                            <th>
                                              <div> Date of </div>
                                              <div> Tested </div>
                                            </th>
                                            <th >
                                              <div> Date of </div>
                                              <div> Expiry </div>
                                            </th>
                                            <th>Qty</th>
                                            <th>
                                              <div>Discount</div>
                                              <div>(%)</div>
                                            </th>
                                            <th>
                                              Net Value
                                            </th>
                                            <th></th>                                              
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <?= $this->load->view( 'pages/purchase/return/_return_items', '', true ) ?>
                                    </tbody>     
                                  </table>
                                  <input type="hidden" name="total_item" id="total_item" value="<?= $total_item?>">
                                  <div id="return_view"></div>
                                  <!-- <div class="btn btn-outline-success w-100" id="add_new">
                                    <i class="fa fa-plus" ></i> Add Product
                                  </div> -->
                                  <table class="table" style="font-size: 12px;" id="remarks_table">
                                    <tr>
                                      <td style="width: 60%;padding: 10px;text-align: left;">
                                        <div>
                                          <label> Remarks:</label>
                                        </div>
                                        <div>
                                          <div class="inline-class" >
                                            <textarea rows="3" cols="3" class="form-control" name="remarks"><?= $return_info->remarks ?? '' ?></textarea>
                                          </div>
                                        </div>
                                      </td>
                                      <td style="width: 40%">
                                        
                                        <table style="width: 100%;">
                                          <tr>
                                            <td>
                                              <div style="display: flex;padding: 10px">
                                                <div >
                                                  GST ( % ) :  0.00% 
                                                </div>
                                                <div style="padding-left: 10px;">
                                                  CST ( % ) :  0.00% 
                                                </div>
                                                <div style="padding-left: 10px;">
                                                  VAT ( % ) :  0.00% 
                                                </div>
                                              </div>
                                            </td>

                                          </tr>
                                          <tr>
                                            <td>
                                              <div class="inline-class" >
                                                <div class="w-70 vcenter" style="width: 70%"> 
                                                    Transport Charge <span class="text-danger">*</span>
                                                </div>
                                                <div class="w-30 vcenter text-right" style="width: 30%;"> 
                                                    <div id="total_transport_charge" class="" style="    width: 100%;margin-right: 15px;"></div>
                                                </div>
                                              </div>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td>
                                            <div class="inline-class" >
                                              <div class="w-70 vcenter" style="width: 70%"> 
                                                  Total Amount <span class="text-danger">*</span>
                                              </div>
                                              <div class="w-30 vcenter"> 
                                                  <input type="text" name="total_amount" id="total_amount" class="form-control text-right" value="<?= $return_info->total_amount ?? '' ?>"> 
                                                  <input type="hidden" name="total_item_amount" id="total_item_amount">
                                              </div>
                                            </div>
                                          </td>
                                        </tr>
                                      </table>

                                      </td>
                                    </tr>
                                  </table>
                                  <div>
                                    Amount In Words : 
                                    <span id="amount_in_word" class="text-danger">
                                       <?php
                                          if( isset( $return_info->total_amount ) && !empty( $return_info->total_amount ) ) {
                                        ?>
                                            <?= number_to_word( $return_info->total_amount ?? '' ) ?>
                                      <?php } ?>
                                    </span>
                                  </div>
                              </div>
                            </div>
                          </div>
                        </div>
				                <input type="hidden" name="id" id="id" value="<?= $return_info->id ?? '' ?>">
				                <div class="row clearfix" style="text-align:right;">
				                  <div class="col-lg-12 col-md-12">
			
				                      <a  class="btn btn-outline-danger" href="<?= base_url()?>purchase-return">
                                Cancel
                              </a>
                              <a  class="btn btn-outline-dark" href="javascript:;" onclick="return clear_form( 'purchase_return_form' )">
                                Clear
                              </a>
				                      <button id="addToTable" class="btn btn-outline-info" type="button" onclick="return insert_purchase_return()">
                                Submit
                              </button>
				                      <button id="addToTable" class="btn btn-success" type="button" onclick="return insert_purchase_return( 'print' )">
                                Submit & print
                              </button>
				                  </div>
				                </div>
				
                      </div>
                  </div>
                  <?= form_close() ?>
              </div>
          </div>

						
      </div>
<div id="printableArea" style="display: none;"></div>

<script type="text/javascript">
  $(function () {
    $( '.custom-select' ).select2();
  });
  $(document).ready( function(){

    
    $('input.purchase_invoice_no' ).typeahead({ 
        source:  function (query, process) {
          var return_company = $( '#return_company' ).val();
          if( return_company == '' ) {
            toastr.error( 'Please select Return Company first', 'Purchase Return' );
            $( '#purchase_invoice_no' ).val( '' );
            $( '#return_company' ).focus();
            return false;
          }     
        return $.get( '<?=site_url( 'purchase/get_purchase_typeahead' );?>', { query: query, return_company:return_company }, function (data) {
          data = $.parseJSON( data );
          return process(data);
        });
      },
      updater:function ( item ) {
          if( item.total_amount ) {
            $( '#total_amount' ).val( item.total_amount );
            var words = toWords( item.total_amount );
            
            $( '#amount_in_word' ).html( words );
            $( '#amount_in_word' ).css( 'text-transform', 'uppercase' );
          } else {
            $( '#total_amount' ).val( '' );
            $( '#amount_in_word' ).html( '' );
          }
          if( item.id ) {
            $( '#purchase_id' ).val( item.id );
            get_purchase_items( item.id );
          }
          if( item.total_item ) {
            $( '#total_item' ).val( item.total_item );
          }
          
          return item;
      }
    });
  });


  function get_purchase_items( purchase_id ) {
    var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&purchase_id=' + purchase_id;
    $.ajax( {
        type    : "POST",
        url     : '<?= site_url( 'purchase/get_purchase_items' ); ?>',
        data    : data,
        dataType: 'json',
        success: function( msg ) {
          if( msg.item_view ) {
            $( "table#purchase_return_table tbody" ).html( msg.item_view );  
          }
          if( msg.return_view ) {
            $( "#return_view" ).html( msg.return_view );  
          }
        }
    });
    return false;
  }

  function deleteRow( button ) {
      var row = button.parentNode.parentNode;
      var name = row.getElementsByTagName("TD")[0].innerHTML;
      //Get the reference of the Table.
      var table = document.getElementById("purchase_return_table");
      //Delete the Table row using it's Index.
      table.deleteRow(row.rowIndex);
    }


  function get_item_rate( value, count ) {

      var rate            = $( '#rate_'+count ).val();
      var qty             = $( '#qty_'+count ).val();
      var discount        = $( '#discount_'+count ).val();

      if( isNaN( qty ) || qty == '' ) {
        qty               = 1;
      }
      rate                = parseFloat( rate );
      qty                 = parseInt( qty );
      discount            = parseInt( discount );
     
      var net_rate        = rate * qty;
      net_rate            = parseFloat( net_rate );
      if( isNaN( discount ) || discount == '' ) {
        total_net_price   = net_rate;
      } else {
        total_net_price   = net_rate - (net_rate * (discount / 100));
      }
      
      if( isNaN( total_net_price ) ) {
        total_net_price   = 0;
      }
      $( '#net_value_'+count ).val( total_net_price );
      get_total_rate( 'item_rate' );
    }

    function get_total_rate( class_name ) {
      var tot = 0;
      $( "."+class_name ).each(function() {
        
        let price = parseFloat( $(this).val());
        if( isNaN(price) ) {
          price   = 0
        }
        tot += price;
      });
      
      
      $( '#total_amount' ).val( tot );
      if( tot ) {
        var words = inWords( tot );
      }
      
      $( '#amount_in_word' ).html( words );
      $( '#amount_in_word' ).css( 'text-transform', 'uppercase' );
    }

    function insert_purchase_return( print = '' ) {
      var purchase_return_form       = $( '#purchase_return_form' ).find('input, textarea, button, select');
      var data                = new FormData( $( '#purchase_return_form' )[0] );
      $.ajax({
        type        : 'POST',
        url         : '<?=site_url( 'purchase/insert_purchase_return' );?>',
        data        : data,
        async       : false,
        contentType : false,
        processData : false,
        dataType    : 'json',
        beforeSend: function () {
          purchase_return_form.attr( 'disabled', 'disabled' );
        },    
        success     : function( msg ) {
          if( msg.error == 1 ) {
            toastr.error( msg.error_msg, 'Purchase Return' );
            purchase_return_form.attr( 'disabled', false );
          } else {
            toastr.success( msg.error_msg, 'Purchase Return' );
            if( print ) {
              if( msg.slug ) {
              print_purchase_return( msg.slug );
            }
            }
            
            setTimeout( function(){
                window.location.href = '<?= site_url()?>purchase-return';
            }, 1200 );
          }
        }
      });
      return false;
    }

     $( '#transport_payment' ).change( function() {
     
         var transport_payment  = $(this).val();
         
         if( transport_payment == 'ToPay' ) {
           $( '.topay_transport' ).show();
         } else {
            $( '.topay_transport' ).hide();
         }
      
    });

     function get_total_amount( amount ) {

      var tr_amount         = parseFloat( amount );
      var total_amount      = $( '#total_item_amount' ).val();

      $( '#total_transport_charge' ).html( tr_amount );
      console.log( tr_amount );
      if( total_amount == '' || total_amount == undefined ) {
          
         total_amount       = 0;
         total_amount       = parseFloat( total_amount );
         total_amount       = total_amount + tr_amount;
      } else {
          total_amount      = parseFloat( total_amount );
          
          total_amount      = total_amount + tr_amount;
          
      }
      $( '#total_amount' ).val( total_amount );
    }
    
</script>