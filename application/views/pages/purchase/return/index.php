
<div class="block-header m-t-20">
    <?= $this->load->view( 'layout/_nav_header', '', true )?>
</div> 
<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                    <div class="table-responsive">
                        <div class="text-center hd-font"> 
                            <span class="btn btn-sm purchase-return-bg text-white">
                                PURCHASE RETURNS
                            </span> 
                        </div>
                        <table id="purchase_return_table" class="table table-bordered table-hover w-100">
                            <thead>
                                <tr class="purchase-return-bg text-white">
                                    <th> 
                                        <div> Return  </div>
                                        <div> Inv No </div>
                                    </th>
                                    <th> 
                                        <div> Return </div>
                                        <div> Date </div>
                                    </th>
                                    <th> 
                                        <div> Company </div>
                                        <div> Name </div>
                                    </th>
                                    <th> 
                                        <div> Purchase </div>
                                        <div> Inv No </div>
                                    </th>
                                    <th> 
                                        <div> Purchase  </div>
                                        <div> Inv Date </div>
                                    </th>
                                    <th> 
                                       
                                        <div> Return </div>
                                         <div> Reason</div>
                                    </th>
                                    <th> 
                                        <div> Total </div>
                                        <div> Qty </div>
                                    </th>
                                    <th> 
                                        <div> Total </div>
                                        <div> Amount </div>
                                    </th>
                                    <th> Status </th>
                                    <th> Action </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
    </div>
    
</div>
<script type="text/javascript">
    $(document).ready(function() {
        var dataTable = $( '#purchase_return_table' ).DataTable( {
            'processing' : true,
            'serverSide' : true,
            'order'      : [],
            'ajax'       : {
                url      : '<?= site_url( 'purchase/get_purchase_return' ); ?>',
                type     : 'POST',
                data     : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>' }
            },
            'emptyTable' : 'No data available in table',
            'columnDefs' : [
                {  
                    'targets'   : [ 6 ],
                    'orderable' : false,
                }
            ],
            "lengthMenu": [
                [5, 15, 20, 50, 100, -1],
                [5, 15, 20, 50, 100, "All"] 
            ],
            "pageLength": '<?=NO_OF_DT_ROWS;?>',
        });
    });

    function delete_purchase_return( id ) {
        var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&id=' + id;
        $.ajax( {
            type    : "POST",
            url     : '<?=site_url( 'purchase/delete_purchase_return' );?>',
            data    : data,
            success: function( msg ) {
                toastr.success( 'purchase return deleted successfully', 'Purchase Return' );
                setTimeout( function() {
                    window.location.href = '<?= site_url()?>purchase-return';
                }, 300 );
            }
        });
        return false;
    }
</script>