<div class="block-header m-t-20">
    <?= $this->load->view( 'layout/_nav_header', '', true )?>
</div> 
<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body" id="printarea">
                    <h6><center><strong class="text-primary">PURCHASE RETURN INVOICE</strong></center></h6>
			        <div class="tab-content mt-3">
			            <div role="tabpanel" class="tab-pane in active" id="details" aria-expanded="true">
							<div class="row clearfix">
			                    <div class="col-md-6 col-sm-6">
			                    	<div class="row">
			                    		<div class="col-lg-12 col-sm-12">
			                    			<div style="border: 1px solid #343a4080;border-radius: 5%;">
				                    			<?= $this->load->view( 'pages/purchase/invoice/_company_info', '', true )?>
				                    			<hr>
				                    			<?= $this->load->view( 'pages/purchase/invoice/_own_info', '', true )?>
				                    		</div>
			                    		</div>
			                    	</div>
				                </div>
								<div class="col-sm-1"></div>
			                    <div class="col-md-5 col-sm-5 text-right" >	
									<div class="row clearfix">
										<div class="col-md-12 col-sm-12" >
											<?= $this->load->view( 'pages/purchase/invoice/_purchase_return_info', '', true )?>											
										</div>
				                     </div>
				                    </div>
			                	</div>
			                	<br/>
			                <div class="row clearfix">
			                    <div class="col-md-12">
			                        <div class="table-responsive">
			                            <table class="table table-hover">
			                                <thead class="thead-dark">
			                                    <tr>
			                                        <th>SL.NO</th>	
			                                        <th>Product Code</th>
			                                        <th><div>Category</div><div>Variety</div>
			                                        </th>
			                                        <th>Product Description</th>
			                                        <th>Lot Number</th>
			                                        <th>Dt.Of Packing</th>
			                                        <th>Dt.of Expiry</th>
			                                        <th>Qty</th>
			                                        <th>UOM</th>
			                                        <th>Net Rate( \Rs)</th>
			                                        <th>Discounts</th>
			                                        <th>Amount(Rs)</th>
			                                    </tr>
			                                </thead>
			                                <tbody>
		                                	<?php
		                                		
		                                		if( isset( $items ) && !empty( $items ) ) {
		                                			$i = 1;
		                                			foreach ( $items as $key => $value ) {
		                                	?>
			                                    <tr>
			                                        <td><?= $i ?></td>
			                                        <td><?= $value->product_code ?></td>
			                                        <td><?= $value->product_category ?></td>
			                                        <td><?= $value->description ?></td>
			                                        <td><?= $value->lot_no ?></td>
			                                        <td> <?= date( 'd-M-Y', strtotime( $value->date_of_packed ) ) ?> </td>
			                                        <td> <?= date( 'd-M-Y', strtotime( $value->date_of_expiry ) ) ?> </td>
			                                        
			                                        <td> <?= $value->qty ?> </td>
			                                        <td> <?= $value->packaging ?></td>
			                                        <td> <?= number_format( $value->net_rate, 2 ) ?></td>
			                                        <td> <?= number_format( $value->discount, 2 ) ?></td>
			                                        <td> <?= number_format( $value->total_amount , 2 ) ?></td>
			                                    </tr>
			                                   <?php $i++; } } else {  ?>
			                                   	<tr>
			                                   		<td colspan="12"> No Products found </td>
			                                   	</tr>
			                                   <?php } ?>
			                                </tbody>
			                            </table>
			                        </div>
			                    </div>
			                </div>
			                <hr>
			                <div class="row clearfix">
			                    <div class="col-md-8">
			                        <b>Remarks:</b>
			                        <div style="padding: 14px 5px;background: #f7f7f7;">
			                        	<?= $purchase_info->remarks ?? ''?>	
			                        </div>
			                        
			                    </div>
			                    <div class="col-md-4 text-right">
			                    	<table style="width: 100%">
			                    		<tr>
			                    			<th> Gst </th>
			                    			<td style="padding-right: 20px;"> 0 % </td>
			                    		</tr>
			                    		<tr>
			                    			<th>Transport Charge </th>
			                    			<td style="padding-right: 20px;">
			                    				
			                    				<?= $purchase_info->transport_charge ?> 
			                    			</td>
			                    		</tr>
			                    		<tr>
			                    			<th><h6>Total</h6></th>
			                    			<td style="padding-right: 20px;">
			                    				<h6> 
			                    					<?= number_format( $purchase_info->total_amount, 2 )?> 
			                    				</h6>
			                    			</td>
			                    		</tr>
			                    	</table>
			                    </div>                                    
			                </div>   
		                    <div class="col-md-12"style="padding:10px;border:1px solid #e0dddd;border-radius:20px;margin-top:10px; ">
		                        <span><b><u>Amount in words </u></b></span><span style="font-weight:normal;">: <?= number_to_word( $purchase_info->total_amount ) ?>/-</span> 
		                    </div>									
			            </div>                        
			        </div>
    			</div>
            </div>
        </div>
    </div>
</div>
<div id="printableArea" style="display: none;"></div>

<script type="text/javascript">

	
	
</script>