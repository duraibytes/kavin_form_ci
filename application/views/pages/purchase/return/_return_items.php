<?php
  
  if( isset( $items )&& !empty( $items ) ) {
    $i = 1;
    foreach ( $items as $key => $value ) { ?>
      <tr>
        <td>
          <input type="text" name="code_<?= $i ?>" id="code_<?= $i ?>" value="<?= $value->product_code ?? '' ?>" class="form-control   <?= $i ?>" autocomplete="off">
        </td>
        <td> 
          <input type="text" name="variety_<?= $i ?>" id="variety_<?= $i ?>" value="<?= $value->product_category ?? '' ?>" class="form-control">
        </td>
        <td> 
          <input type="text" name="desc_<?= $i ?>" id="desc_<?= $i ?>" value="<?= $value->description ?>" class="form-control"> 
        </td>
        <td> 
          <input type="text" name="lot_<?= $i ?>" id="lot_<?= $i ?>" value="<?= $value->lot_no ?>" class="form-control" required> 
        </td>
        <td> 
          <input type="text" name="uom_<?= $i ?>" id="uom_<?= $i ?>" value="<?= $value->packaging ?>" class="form-control"> 
        </td>
        <td> 
          <input type="text" name="rate_<?= $i ?>" id="rate_<?= $i ?>" value="<?= $value->net_rate ?>" class="form-control" onchange="return get_item_rate( this.value, '<?= $i ?>' )"> 
        </td>
        <td> 
          <input name="packed_date_<?= $i ?>" id="packed_date_<?= $i ?>" data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="dd/mm/yyyy" value="<?= isset( $value->date_of_packed ) ? date( 'd/m/Y', strtotime( $value->date_of_packed ) ) : '' ?>">
        </td>
        <td> 
          <input name="tested_date_<?= $i ?>" id="tested_date_<?= $i ?>" data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="dd/mm/yyyy" value="<?= isset( $value->date_of_tested ) ? date( 'd/m/Y', strtotime( $value->date_of_tested ) ) : '' ?>">
        </td>
        <td>
           <input name="expiry_date_<?= $i ?>" id="expiry_date_<?= $i ?>" data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="dd/mm/yyyy" value="<?= isset( $value->date_of_expiry ) ? date( 'd/m/Y', strtotime( $value->date_of_expiry ) ) : '' ?>">
        </td>
       
        <td>
           <input type="text" name="qty_<?= $i ?>" id="qty_<?= $i ?>" value="<?= $value->qty ?? '' ?>"  class="form-control number-only" onchange="return get_item_rate( this.value, '<?= $i ?>' )"> 
        </td>
         <td>
           <input type="text" name="discount_<?= $i ?>" id="discount_<?= $i ?>" value="<?= $value->discount ?? '' ?>"  class="form-control" onchange="return get_item_rate( this.value, '<?= $i ?>' )">
        </td>
        <td>
          <input type="text" name="net_value_<?= $i ?>" id="net_value_<?= $i ?>" value="<?= $value->total_amount  ?? '' ?>"  class="form-control item_rate" required >
          <input type="hidden" name="product_id_<?= $i ?>" id="product_id_<?= $i ?>"  value="<?= $value->product_id?>" >
          <input type="hidden" name="item_id_<?= $i ?>" id="item_id_<?= $i ?>" value="<?= $value->id ?>">
        </td>
        <td>
          <a href="#" class="btn btn-sm btn-outline-danger" onclick="deleteRow(this)">
            <i class="fa fa-trash"></i>
          </a>
        </td>
      </tr>
      
  <?php $i++; }

  } else {   ?>
    <tr>
      <td>
        <input type="text" name="code_<?= $total_item ?>" id="code_<?= $total_item ?>" value="" class="form-control product_typeahead_<?= $total_item ?>" autocomplete="off">
      </td>
      <td> 
        <input type="text" name="variety_<?= $total_item ?>" id="variety_<?= $total_item ?>" value="" class="form-control">
      </td>
      <td> 
        <input type="text" name="desc_<?= $total_item ?>" id="desc_<?= $total_item ?>" value="" class="form-control"> 
      </td>
      <td> 
        <input type="text" name="lot_<?= $total_item ?>" id="lot_<?= $total_item ?>" value="" class="form-control" required> 
      </td>
      <td> 
        <input type="text" name="uom_<?= $total_item ?>" id="uom_<?= $total_item ?>" value="" class="form-control"> 
      </td>
      <td> 
        <input type="text" name="rate_<?= $total_item ?>" id="rate_<?= $total_item ?>" value="" class="form-control" onchange="return get_item_rate( this.value, '<?= $total_item ?>' )"> 
      </td>
      <td> 
        <input name="packed_date_<?= $total_item ?>" id="packed_date_<?= $total_item ?>" data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="dd/mm/yyyy">
      </td>
      <td> 
        <input name="tested_date_<?= $total_item ?>" id="tested_date_<?= $total_item ?>" data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="dd/mm/yyyy">
      </td>
      <td>
         <input name="expiry_date_<?= $total_item ?>" id="expiry_date_<?= $total_item ?>" data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="dd/mm/yyyy">
      </td>
     
      <td>
         <input type="text" name="qty_<?= $total_item ?>" id="qty_<?= $total_item ?>" value=""  class="form-control number-only" onchange="return get_item_rate( this.value, '<?= $total_item ?>' )"> 
      </td>
       <td>
         <input type="text" name="discount_<?= $total_item ?>" id="discount_<?= $total_item ?>" value=""  class="form-control" onchange="return get_item_rate( this.value, '<?= $total_item ?>' )">
      </td>
      <td>
        <input type="text" name="net_value_<?= $total_item ?>" id="net_value_<?= $total_item ?>" value=""  class="form-control item_rate" required >
        <input type="hidden" name="product_id_<?= $total_item ?>" id="product_id_<?= $total_item ?>" >
      </td>
      <td>
        <a href="#" class="btn btn-sm btn-outline-danger" onclick="deleteRow(this)">
          <i class="fa fa-trash"></i>
        </a>
      </td>
    </tr>

<?php  }
?>



<script type="text/javascript">
  
  $(document).ready( function(){
    var total_item 				= '1';
    $('input.product_typeahead_'+ total_item ).typeahead({ 
        source:  function (query, process) {
                    
        return $.get( '<?=site_url( 'product/get_product_typeahead' );?>', { query: query, total_item:total_item }, function (data) {
          data = $.parseJSON( data );
          return process(data);
        });
      },
      updater:function ( item ) {
        console.log( item );
          if( item.product_category ) {
            $( '#variety_' + total_item ).val( item.product_category );
          }
          if( item.description ) {
            $( '#desc_' + total_item ).val( item.description );
          }
          if( item.net_purchase_price ) {
            $( '#rate_' + total_item ).val( item.net_purchase_price );
            $( '#qty_'+ total_item ).val(1);
            get_item_rate( '', total_item );
          }
          if( item.packaging ) {
            $( '#uom_' + total_item ).val( item.packaging );
          }
          if( item.id ) {
            $( '#product_id_'+total_item ).val( item.id );
          }
          
          return item;
      }
    });
  });
    
</script>