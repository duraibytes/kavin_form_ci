<?php 

if( isset( $return_items )&& !empty( $return_items ) ) {
?>
<table class="table table-bordered table-striped" cellspacing="0" id="">
  
    <thead>
      <tr>
        <td colspan="12"> Purchase Returned Items </td>
      </tr>
      <tr class="purchase-invoice-bg text-white">
          <th>
            <div> Product </div>
            <div> Code </div>
          </th>
          <th>
            <div> Product </div>
            <div> Variety </div>
          </th>
          <th>
            <div> Product </div>
            <div> Desc </div>
          </th>
          
          <th>
            <div> Product </div>
            <div> LOT </div>
          </th>
          <th>
            <div> UOM </div>
           
          </th>
          <th>
            <div> Net </div>
            <div> Rate </div>
          </th>

          <th>
            <div> Date of </div>
            <div> Packed </div>
          </th>
          <th>
            <div> Date of </div>
            <div> Tested </div>
          </th>
          <th >
            <div> Date of </div>
            <div> Expiry </div>
          </th>
          <th>Qty</th>
          <th>
            <div>Discount</div>
            <div>(%)</div>
          </th>
          <th>
            Net Value
          </th>
      </tr>
  </thead>
  <tbody>
    <?php
        foreach ( $return_items as $key => $value ) { 
          // print_r( $value );
    ?>
      <tr>
        <td>
          <?= $value->product_code ?>
        </td>
        <td>
          <?= $value->product_category ?>
        </td>
        <td>
          <?= $value->description ?>
        </td>
        <td>
          <?= $value->lot_no ?>
        </td>
        <td>
          <?= $value->packaging ?>
        </td>
        <td>
          Rs<?= number_format( $value->net_rate, 2 ) ?>
        </td>

        <td>
          <?= date( 'd-M-Y', strtotime( $value->date_of_packed ) ) ?>
        </td>
        <td>
          <?= date( 'd-M-Y', strtotime( $value->date_of_tested ) ) ?>
        </td>
        <td>
          <?= date( 'd-M-Y', strtotime( $value->date_of_expiry ) ) ?>
        </td>
        <td>
          <?= $value->qty ?>
        </td>
        <td>
          <?= $value->discount ?>
        </td>
        <td>
          Rs<?= number_format( $value->total_amount, 2 ) ?>
        </td>
      </tr>
    <?php 
        } 
    ?>
  </tbody>     
</table>
<?php } ?>