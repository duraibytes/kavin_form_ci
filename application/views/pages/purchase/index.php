
<div class="block-header m-t-20">
    <?= $this->load->view( 'layout/_nav_header', '', true )?>
</div> 

<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                    <div class="table-responsive">
                        <div class="text-center hd-font"> 
                            <span class="btn btn-sm purchase-invoice-bg text-white">
                                PURCHASE INVOICES
                            </span> 
                        </div>
                        <table id="purchase_table" class="table table-bordered table-hover w-100">
                            <thead>
                                <tr class="purchase-invoice-bg" style="color:white">
                                    <th> 
                                        <div> Company </div>
                                        <div> Name </div>
                                    </th>
                                    <th> 
                                        <div> Purchase </div>
                                        <div> Inv Date </div>
                                    </th>
                                    <th> 
                                        <div> Purchase Due Date </div>
                                        <div></div>
                                    </th>
                                    <th> 
                                        <div> Purchase </div>
                                        <div> Invoice No </div>
                                    </th>
                                     <th> 
                                        <div> Company </div>
                                        <div> Invoice No </div>
                                    </th>
                                    <th> 
                                        Transport
                                    </th>
                                    <th> Purchase Amount </th>
                                    <th> Paid Amount </th>
                                    <th> 
                                        <div> Payment </div>
                                        <div> Status </div>
                                    </th>
                                    <th style="width: 10px;"> Action </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        var dataTable = $( '#purchase_table' ).DataTable( {
            'processing' : true,
            'serverSide' : true,
            'order'      : [],
            'ajax'       : {
                url      : '<?= site_url( 'purchase/get_purchase' ); ?>',
                type     : 'POST',
                data     : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>' }
            },
            'emptyTable' : 'No data available in table',
            'columnDefs' : [
                {  
                    'targets'   : [ 6 ],
                    'orderable' : false,
                }
            ],
            "lengthMenu": [
                [5, 15, 20, 50, 100, -1],
                [5, 15, 20, 50, 100, "All"] 
            ],
            "pageLength": '<?=NO_OF_DT_ROWS;?>',
        });
    });

    function delete_purchase( id ) {
        var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&id=' + id;
        $.ajax( {
            type    : "POST",
            url     : '<?=site_url( 'purchase/delete_purchase' );?>',
            data    : data,
            success: function( msg ) {
                toastr.success( 'purchase deleted successfully', 'Purchase' );
                setTimeout( function() {
                    window.location.href = '<?= site_url()?>purchase';
                }, 300 );
            }
        });
        return false;
    }
</script>