<style type="text/css">
    #transaction-table td {
        text-align: left;
        vertical-align: middle;
    }
    #bank_filter_form {
        background: #ddd;
        padding: 20px;
    }
</style>

<div class="block-header m-t-20">
    <?= $this->load->view( 'layout/_nav_header', '', true )?>
</div>
    <div class="container-fluid">            

        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                        <div class="text-center hd-font m-b-20"> 
                            <span class="btn btn-sm transaction-bg text-white">
                               BANK / CASH MANAGEMENT
                            </span> 
                        </div>
                        
                        <?= $this->load->view( 'pages/transaction/filter/form', '', true ) ?>
                       
                    </div>
                    <div class="body">
                        <!-- <ul class="nav nav-tabs">
                            <li class="nav-item"><a class="nav-link active show" data-toggle="tab" href="#Home-withicon"><i class="fa fa-home"></i> Deposit </a></li>
                            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Profile-withicon"><i class="fa fa-user"></i> Widthraw </a></li>
                           
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane show active" id="Home-withicon">
                               <?= $this->load->view( 'pages/transaction/_deposit', '', true ) ?>
                            </div>
                            <div class="tab-pane" id="Profile-withicon">
                                <?= $this->load->view( 'pages/transaction/_widthdraw', '', true ) ?>
                            </div>
                           
                        </div> -->
                       
                        <table class="table table-bordered table-hover" id="transaction-table" style="width: 100%;"> 
                            <thead>
                                <tr class="transaction-bg text-white">
                                    <th> 
                                        <div>Transaction </div>
                                        <div> Date </div>
                                    </th>
                                    <th> 
                                        <div> Transaction </div>
                                        <div> Type </div>
                                    </th>
                                    <th>
                                        Company
                                    </th>
                                    <th> 
                                        <div> Description / </div>
                                        <div> Reference </div>
                                    </th>
                                    <th> 
                                        <div> Transaction </div>
                                        <div> Amount </div>
                                    </th>
                                    <th> 
                                        <div> Transaction </div>
                                        <div> Method </div>
                                    </th>
                                    <th> 
                                        <div> Cheque Info  </div>
                                    </th>
                                    <th> 
                                        <div>Bank A/c</div>
                                        <div>Number</div>
                                    </th>
                                    <th> 
                                        <div>Bank</div>
                                        <div>Name</div>
                                    </th>
                                    <th> 
                                        <div>Bank  </div>
                                        <div> Branch</div>
                                    </th>
                                     <th> 
                                        <div> BANK  </div>
                                        <div> IFSC </div>
                                    </th>
                                </tr>
                            </thead>
                            
                            <tbody>
                               
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script type="text/javascript">
    function get_deposit_form( id='' ) {
          var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&id=' + id;
          $.ajax( {
              type    : "POST",
              url     : '<?= site_url( 'transaction/get_deposit_form' ); ?>',
              data    : data,
              success: function( msg ) {
                  $( '#defaultModal' ).modal();
                  $( '#defaultModal' ).html( msg );
              }
          });
          return false;
      }

      


      $(document).ready(function() {
        $('.datepicker').datepicker({
                 format: 'dd-mm-yyyy' 

        });
        
        var dataTable = $( '#transaction-table' ).DataTable( {
            'processing' : true,
            'serverSide' : true,
            'order'      : [],
            'ajax'       : {
                url      : '<?= site_url( 'transaction/get_bank_transaction' ); ?>',
                type     : 'POST',
                data     : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>' }
            },
            'emptyTable' : 'No data available in table',
            'columnDefs' : [
                {  
                    'targets'   : [ 3 ],
                    'orderable' : false,
                }
            ],
            "lengthMenu": [
                [5, 15, 20, 50, 100, -1],
                [5, 15, 20, 50, 100, "All"] 
            ],
            "pageLength": '<?=NO_OF_DT_ROWS;?>',
        });
    });

    function transaction_filter() {
      var stock_filter       = $( '#bank_filter_form' ).find('input, textarea, button, select');
      var data                = new FormData( $( '#bank_filter_form' )[0] );
      $.ajax({
        type        : 'POST',
        url         : '<?=site_url( 'transaction/transaction_filter' );?>',
        data        : data,
        async       : false,
        contentType : false,
        processData : false,
        dataType    : 'json',
        success     : function( msg ) {
            setTimeout( function(){
               window.location.href = '<?= site_url()?>transaction';
            }, 200 );
        }
      });
      return false;
    }

    function clear_transaction_filter() {
        var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>';
        $.ajax( {
            type    : "POST",
            url     : '<?= site_url( 'transaction/clear_transaction_filter' ); ?>',
            data    : data,
            success: function( msg ) {
                setTimeout( function(){
                    window.location.href = '<?= site_url()?>transaction';
                }, 300 );
            }
        });
        return false;
      }
</script>