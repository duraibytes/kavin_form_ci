<?= form_open( '', "id='bank_filter_form'")?>
<?php
    if( $this->session->has_userdata( 'transaction_filter' ) ) {
        $sess_filter            = $this->session->userdata( 'transaction_filter' );
        // print_r( $this->session->userdata( 'transaction_filter' ) );
    }
?>
<div class="row">
    <div class="col-sm-12 col-md-3 col-lg-2">
        <div class="">
           <select class="form-control" name="bank" id="bank">
               <option value=" " > -- select Bank -- </option>
               <?php
                    if( isset( $bank_info ) && !empty( $bank_info ) ) {
                        foreach ( $bank_info as $key => $value ) {
                            $selected       = '';
                            if( isset( $sess_filter[ 'bank' ] ) && $sess_filter[ 'bank' ] == $value->bank_id ) {
                                $selected   = 'selected';
                            }
                            ?>
                            <option value="<?= $value->bank_id ?>" <?= $selected ?>> <?= $value->bank_name ?> </option>
                <?php   }
                    }
                ?>
           </select>
       </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="input-daterange input-group" >
            <input type="text" class="input-sm form-control datepicker" name="start" value="<?= $sess_filter['start'] ?? '' ?>">
            <span class="input-group-addon range-to ">to</span>
            <input type="text" class="input-sm form-control datepicker" name="end" value="<?= $sess_filter['end'] ?? '' ?>">
        </div>
    </div>
    <div class="col-sm-12 col-md-3 col-lg-4">
        <div class="">
            <a href="javascript:;" class="btn btn-sm transaction-bg text-white" onclick="return transaction_filter()"> Filter </a>
            <a href="javascript:;" class="btn btn-sm btn-warning text-white" onclick="return clear_transaction_filter()"> Clear Filter </a>
        </div>
    </div>
</div>
    <?= form_close() ?>