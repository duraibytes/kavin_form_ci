<div class="row">
    <div class="col-sm-12">
    	<div class="inline-class" >
            <div class="w-50 vcenter"> 
              Payment Type:
            </div>
            <div class="w-50 vcenter"> 
              <label class="fancy-radio custom-color-green"><input name="gender3" value="NEFT60" type="radio" checked=""><span><i></i> Deposit </span></label>
              <label class="fancy-radio custom-color-green"><input name="gender3" value="NEFT60" type="radio" checked=""><span><i></i> 
              Withdraw </span></label>
            </div>
        </div>
		<div class="inline-class" >
		    <div class="w-50 vcenter"> 
		        Payment Method <span class="text-danger">*</span>
		    </div>
		    <div class="w-50 vcenter"> 
		        <select class="custom-select" id="inputGroupSelect02">
		            <option selected>-- select method -- </option>
		            <option value="1"> Cash </option>
		            <option value="2"> RTGS </option>
		            <option value="3"> IMPS </option>
		        </select>
		    </div>
		</div>

		<div class="inline-class" >
		    <div class="w-50 vcenter"> 
		        Bank 
		    </div>
		    <div class="w-50 vcenter"> 
		        <select class="custom-select" id="inputGroupSelect02">
		            <option selected>-- select Bank -- </option>
		            <option value="1">State Bank of India</option>
		            <option value="2">Indian Bank</option>
		        </select>
		    </div>
		</div>
		<div class="inline-class" >
		    <div class="w-50 vcenter"> 
		        Bank Branch / IFSC CODE 
		    </div>
		    <div class="w-50 vcenter"> 
		        <input type="text" name="product_name" id="product_name" class="form-control"> 
		    </div>
		</div>
		<div class="inline-class" >
		    <div class="w-50 vcenter"> 
		        Bank Account Number
		    </div>
		    <div class="w-50 vcenter"> 
		        <input type="text" name="product_name" id="product_name" class="form-control"> 
		    </div>
		</div>
		<div id="cheque_field">
			<div class="inline-class" >
			    <div class="w-50 vcenter"> 
			        Cheque / DD / UTR Ref No 
			    </div>
			    <div class="w-50 vcenter"> 
			        <input type="text" name="product_name" id="product_name" class="form-control"> 
			    </div>
			</div>
			<div class="inline-class" >
			    <div class="w-50 vcenter"> 
			        Cheque Issued Date 
			    </div>
			    <div class="w-50 vcenter"> 
			        <input data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="dd/mm/yyyy">
			    </div>
			</div>
			<div class="inline-class" >
			    <div class="w-50 vcenter"> 
			        Cheque Issued Amount 
			    </div>
			    <div class="w-50 vcenter"> 
			        <input type="text" name="product_name" id="product_name" class="form-control"> 
			    </div>
			</div>
		</div>
		<div class="inline-class" >
		    <div class="w-50 vcenter"> 
		        Total Amount <span class="text-danger">*</span>
		    </div>
		    <div class="w-50 vcenter"> 
		        <input type="text" name="total_amount" class="form-control">
		    </div>
		</div>
		<div class="inline-class" >
		    <div class="w-50 vcenter"> 
		        Deposited By <span class="text-danger">*</span>
		    </div>
		    <div class="w-50 vcenter"> 
		        <input type="text" name="total_amount" class="form-control">
		    </div>
		</div>
		<div class="inline-class" >
		    <div class="w-50 vcenter"> 
		        Description <span class="text-danger">*</span>
		    </div>
		    <div class="w-50 vcenter"> 
		        <textarea class="form-control" rows="3" name="description"></textarea>
		    </div>
		</div>
	</div>
</div>