

<div class="block-header">
    <?= $this->load->view( 'layout/_nav_header', '', true )?>
</div>
<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
               
                <div class="body">
        			<div class="table-responsive">
                        <table id="bank_branch_table" class="table table-bordered table-hover js-basic-example dataTable table-custom">
        					<thead>
        						<tr class="inventory-bg text-white">
        							<th> S.NO </th>
        							<th> Bank Name </th>
        							<th> Branch Name </th>
                                    <th> IFSC CODE </th>
                                    <th> Address </th>
        							<th style="width: 10%;"> Action </th>
        						</tr>
        					</thead>
        					<tbody>
        						<tr>
        							<td></td>
        							<td></td>
        							<td></td>
        							<td></td>
                                    <td></td>
                                    <td></td>
        						</tr>
        					</tbody>
        				</table>
        			</div>
		        </div>

	        </div>	
        </div>
    </div>
</div>


<script type="text/javascript">
$(document).ready(function() {
	var dataTable = $( '#bank_branch_table' ).DataTable( {
        'processing' : true,
        'serverSide' : true,
        'order'      : [],
        'ajax'       : {
            url      : '<?= site_url( 'bank/get_bank_branch' ); ?>',
            type     : 'POST',
            data     : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>', 'slug':'<?= $slug ?>' }
        },
        'emptyTable' : 'No data available in table',
        'columnDefs' : [
            {  
                'targets'   : [ 0, 5 ],
                'orderable' : false,
            }
        ],
        
        
        "lengthMenu": [
            [5, 15, 20, 50, 100, -1],
            [5, 15, 20, 50, 100, "All"] 
        ],
        "pageLength": '<?=NO_OF_DT_ROWS;?>',
                    
    });
});

    function delete_bank_branch( id, slug ) {
        var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&id=' + id;
        $.ajax( {
            type    : "POST",
            url     : '<?=site_url( 'bank/delete_bank_branch' );?>',
            data    : data,
            success: function( msg ) {
                toastr.success( 'Bank Branch deleted successfully', 'Bank' );
                setTimeout(function(){
                    window.location.href = '<?= site_url()?>bank-list/'+slug;
                });
                    
            }
        });
        return false;
    }
</script>