<div class="block-header">
    <?= $this->load->view( 'layout/_nav_header', '', true )?>
</div>
<div class="container-fluid">
	<div class="row clearfix">
        <div class="col-lg-12">
			<div class="card">
				<div class="row">
					<div class="col-sm-12">
						<div id="form_errors" class="alert alert-danger" style="display: none;"></div>
					</div>
				</div>
				<?= form_open( '', 'id="bank_branch_form"' )?>
				<?= $this->load->view( 'pages/bank/branch/_form', '', true ) ?>

			
				<div class="body">
					<div class="row clearfix">
						<input type="hidden" name="slug" value="<?= $slug ?? '' ?>">
						<input type="hidden" name="bank_slug" value="<?= $bank_slug ?? '' ?>">
						<div class="col-sm-12 text-right">
							<a href="<?= base_url()?>bank-list/<?= $bank_slug ?>" class="btn btn-danger"> Cancel </a>
							<a href="javascript:" class="btn btn-primary" onclick="return add_bank_branch( '<?= $bank_slug ?>' )"> <?= $btn_name ?> </a>
						</div>
					</div>
				</div>
				<?= form_close() ?>
			
			</div>		
		</div>
	</div>
</div>

	<script type="text/javascript">
		function add_bank_branch( bank_slug ) {
	       	var bank_form   		= $( '#bank_branch_form' ).find('input, textarea, button, select');
	        var data                = bank_form.serialize();
	        data                    = data + '&<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>';
        
        	bank_form.attr( 'disabled','disabled' );
	       $.ajax({
	           type    : 'POST',
	           url     : '<?= site_url()?>bank/add_bank_branch',
	           data    : data,
	           dataType: 'json',
	           success : function( msg ) {
	               if( msg.error == 1 ) {
	                  	toastr.error( msg.error_msg, 'Bank Branch' );
	                  	bank_form.attr( 'disabled', false );
	               } else {
	               		toastr.success( msg.error_msg, 'Bank Branch' );
	                   window.location.href = '<?= site_url()?>bank-list/'+bank_slug;
	               }
	           }
	       });
	       return false;
	   }
	</script>