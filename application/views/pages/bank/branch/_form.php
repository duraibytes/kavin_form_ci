<div class="body">
	<div class="row clearfix">
		<div class="col-sm-4">
			<div class="form-group">
				<label>
					Branch Name <span class="text-danger">*</span>
				</label>
				<input type="text" name="branch" id="branch" class="form-control" value="<?= $bank_info->branch ?? '' ?>">
			</div>
		</div>
		<div class="col-sm-4">
			<div class="form-group">
				<label>
					Email
				</label>
				<input type="text" name="email" id="email" class="form-control" value="<?= $bank_info->email ?? '' ?>">
			</div>
		</div>
		<div class="col-sm-4">
			<div class="form-group">
				<label>
					Phone
				</label>
				<input type="text" name="phone" id="phone" class="form-control" value="<?= $bank_info->phone ?? '' ?>">
			</div>
		</div>
		<div class="col-sm-4">
			<div class="form-group">
				<label>
					IFSC CODE
				</label>
				<input type="text" name="ifsc" id="ifsc" class="form-control" value="<?= $bank_info->ifsc ?? '' ?>">
			</div>
		</div>
		<div class="col-sm-4">
			<div class="form-group">
				<label>
					MICR CODE
				</label>
				<input type="text" name="micr_code" id="micr_code" class="form-control"  value="<?= $bank_info->micr_code ?? '' ?>">
			</div>
		</div>
		<div class="col-sm-12">
			<div class="form-group">
				<label>
					Address
				</label>
				<textarea class="form-control" rows="3" name="address" id="address"><?= $bank_info->address ?? '' ?></textarea>
			</div>
		</div>
	</div>
</div>