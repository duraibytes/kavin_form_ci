
<div class="block-header">
    <?= $this->load->view( 'layout/_nav_header', '', true )?>
</div>
<div class="container-fluid">
	<div class="row clearfix">
        <div class="col-lg-12">
			<div class="card">
				<div class="row">
					<div class="col-sm-12">
						<div id="form_errors" class="alert alert-danger" style="display: none;"></div>
					</div>
				</div>
				<div class="text-center hd-font m-b-10 m-t-20"> 
                    <span class="btn btn-sm inventory-bg text-white">
                       ADD BANK
                    </span> 
                </div>
				<?= form_open( '', 'id="bank_form"' )?>
				<?= $this->load->view( 'pages/bank/form/_form', '', true ) ?>

				<div class="body">
					<div class="row clearfix">
						<input type="hidden" name="slug" value="<?= $slug ?? '' ?>">
						<div class="col-sm-12 text-right">
							<a href="<?= base_url()?>bank" class="btn btn-danger"> Cancel </a>
							<a href="javascript:" class="btn btn-primary" onclick="return add_bank()"> <?= $btn_name ?> </a>
						</div>
					</div>
				</div>
				<?= form_close() ?>
				
			</div>
		</div>
	</div>		
</div>

	<script type="text/javascript">
		function add_bank() {
	       	var bank_form   		= $( '#bank_form' ).find('input, textarea, button, select');
	        var data                = bank_form.serialize();
	        data                    = data + '&<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>';
        
        	bank_form.attr( 'disabled','disabled' );
	       $.ajax({
	           type    : 'POST',
	           url     : '<?= site_url()?>bank/add_bank',
	           data    : data,
	           dataType: 'json',
	           success : function( msg ) {
	               if( msg.error == 1 ) {
	                  	
	                  	toastr.error( msg.error_msg, 'Bank' );
	                  	bank_form.attr( 'disabled', false );
	               } else {
	               		
	               		toastr.success( msg.error_msg, 'Bank' );
	               		setTimeout(function() {
	               			window.location.href = '<?= site_url()?>bank';
	               		}, 300 );
	                  	
	               }
	           }
	       });
	       return false;
	   }
	</script>