
<div class="block-header">
    <?= $this->load->view( 'layout/_nav_header', '', true )?>
</div>
<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
               
                <div class="body">
                    <div class="table-responsive">
                        <div class="text-center hd-font m-b-20"> 
                            <span class="btn btn-sm inventory-bg text-white">
                               BANK MANAGEMENT
                            </span> 
                        </div>
                        <table id="bank_table" class="table table-bordered table-hover js-basic-example dataTable table-custom">
                            <thead>
                                <tr class="inventory-bg text-white">
                                    <th style="width: "> S.no </th>
                                    <th> Bank Name </th>
                                    <th> No of Branches </th>
                                    <th style="width: 10%;text-align: center;"> Action </th>
                                </tr>
                            </thead>
                            
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
    </div>
    
</div>

<script type="text/javascript">
    
    $(document).ready(function() {
        var dataTable = $( '#bank_table' ).DataTable( {
            'processing' : true,
            'serverSide' : true,
            'order'      : [],
            'ajax'       : {
                url      : '<?= site_url( 'bank/get_bank' ); ?>',
                type     : 'POST',
                data     : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>' }
            },
            'emptyTable' : 'No data available in table',
            'columnDefs' : [
                {  
                    'targets'   : [ 0, 2, 3 ],
                    'orderable' : false,
                }
            ],
            
            
            "lengthMenu": [
                [5, 15, 20, 50, 100, -1],
                [5, 15, 20, 50, 100, "All"] 
            ],
            "pageLength": '<?=NO_OF_DT_ROWS;?>',
                        
        });
    });

    function delete_bank( id ) {
        var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&id=' + id;
        $.ajax( {
            type    : "POST",
            url     : '<?=site_url( 'bank/delete_bank' );?>',
            data    : data,
            success: function( msg ) {
                toastr.success( 'Bank deleted successfully', 'Bank' );
                setTimeout( function() {
                    window.location.href = '<?= site_url()?>bank';
                }, 300 );
            }
        });
        return false;
    }
</script>