<div class="body">
	<div class="row clearfix">
		<div class="col-sm-4">
			<div class="form-group">
				<label>
					Bank Name <span class="text-danger">*</span>
				</label>
				<input type="text" name="bank_name" id="bank_name" class="form-control" placeholder="bank name" value="<?= $bank_info->bank_name ?? '' ?>">
			</div>
		</div>
		<div class="col-sm-4">
			<div class="form-group">
				<label>
					Email
				</label>
				<input type="text" name="email" id="email" class="form-control" placeholder="bank email" value="<?= $bank_info->email ?? '' ?>">
			</div>
		</div>
		<div class="col-sm-4">
			<div class="form-group">
				<label>
					Phone
				</label>
				<input type="text" name="phone" id="phone" class="form-control" placeholder="bank phone" value="<?= $bank_info->phone ?? '' ?>">
			</div>
		</div>
		<div class="col-sm-12">
			<div class="form-group">
				<label>
					Address
				</label>
				<textarea class="form-control" rows="3" name="address" id="address"><?= $bank_info->address ?? '' ?></textarea>
			</div>
		</div>
	</div>
</div>