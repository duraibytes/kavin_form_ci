<div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
        <?=form_open( '', 'id="bank-branch-form"');?>
        <div class="modal-header">
            <h4 class="title" id="defaultModalLabel"><?= $btn_name ?> Bank</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-12">
                    <?= $this->load->view( 'errors/show_error', '', true  )?>
                </div>
            </div>
            <div class="form-group">
                <label for="code"> Branch Name </label>
                <input class="form-control" placeholder="Enter Branch" type="text" name="branch" id="branch" value="" />
            </div>
            <div class="form-group">
                <label for="code"> IFSC code </label>
                <input class="form-control" placeholder="Enter ifsc" type="text" name="ifsc_code" id="ifsc_code" value="" />
            </div>
        </div>
        <div class="modal-footer">
            <input type="hidden" name="bank_id" id="bank_id" value="<?= $bank_id ?>">
            <input type="hidden" name="class_name" id="class_name" value="<?= $class_name ?>" >
            <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
            <button onClick="return add_bank_branch()" class="btn btn-primary" type="button"> Save</button> 
        </div>
        <?=form_close();?>
    </div>
</div>

<script type="text/javascript">

    function add_bank_branch() {
        var form_name           = $( '#bank-branch-form' ).find( 'input, select, button, textarea' );
        var codes_form_data     = form_name.serialize();
        codes_form_data         = codes_form_data + '&<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>';
        $.ajax({
            type        : 'POST',
            url         : '<?=site_url( 'settings/add_bank_branch' );?>',
            data        : codes_form_data,
            async       : false,
            dataType    : 'json',
            beforeSend: function () {
                form_name.attr( 'disabled', 'disabled' );
               
            },    
            success     : function( msg ) {
                if( msg.error == 1 ) {
                    toastr.error( msg.error_msg, 'Bank' );
                    form_name.attr( 'disabled', false );
                } else {
                    toastr.success( msg.error_msg, 'Bank' );
                    $( '#defaultModal' ).modal( 'hide' );

                    if( msg.class_name ) {
                        
                        $( '.'+msg.class_name ).html( msg.view );
                    } else {
                        $( '#bank_info' ).html( msg.view );    
                    }
                    
                }
            }
        });
        return false;
    }

</script>