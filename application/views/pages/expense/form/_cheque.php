 <div class="inline-class cheque_type dd_field" >
    <div class="w-50 vcenter"> 
        Cheque / DD no 
    </div>
    <div class="w-50 vcenter"> 
        <input type="text" name="cheque_no" id="cheque_no"  class="form-control"> 
    </div>
</div>
<div class="inline-class cheque_type" >
    <div class="w-50 vcenter"> 
        Cheque Date 
    </div>
    <div class="w-50 vcenter"> 
        <input data-provide="datepicker" data-date-autoclose="true" class="form-control" name="issue_date" id="issue_date"  data-date-format="dd/mm/yyyy">
    </div>
</div>
<div class="inline-class cheque_type" >
    <div class="w-50 vcenter"> 
        Cheque Issued Amount 
    </div>
    <div class="w-50 vcenter"> 
        <input type="text" name="cheque_issued_amount" id="cheque_issued_amount"  class="form-control"> 
    </div>
</div>
