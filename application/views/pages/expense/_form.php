
<div class="inline-class" >
    <div class="w-50 vcenter"> 
        Expense <span class="text-danger">*</span>
    </div>
    <div class="w-50 vcenter"> 
    <input type="text" name="expense_name" id="expense_name"  value="<?= $info->expense_name ?? '' ?>" class="form-control"> 
       
    </div>
</div>
<?php
    // print_r( $info );
?>
<div class="inline-class" >
    <div class="w-50 vcenter"> 
        Payment Method <span class="text-danger">*</span>
    </div>
    <div class="w-50 vcenter"> 
       <select class="custom-select" name="payment_method_id" id="payment_method_id"  >
            <option value=""> -- select type -- </option>
            <?php
              if( isset( $payment ) && !empty( $payment ) ) {
                foreach ( $payment as $pay_key => $pay_value ) {
                  $selected         = '';
                  if( isset( $info->payment_method_id ) && $info->payment_method_id == $pay_value->id ) {
                    $selected       = 'selected';
                  }
              ?>
                <option value="<?= $pay_value->id ?>" <?= $selected ?>> <?= $pay_value->name ?></option>
              <?php } } ?>
        </select> 
    </div>
</div>

<div class="inline-class" >
    <div class="w-50 vcenter"> 
        Bank <span class="text-danger">*</span>
    </div>
    <div class="w-50 vcenter"> 

        <select class="custom-select" name="bank_id" id="bank_id" >
         <option value=""> -- Select Bank -- </option>
          <?php
            if( isset( $bank_info ) && !empty( $bank_info ) ) {
                foreach ( $bank_info as $key => $value ) {
                    $selected       = '';
                    if( isset( $info->bank_id ) && $info->bank_id == $value->id ) {
                        $selected   = 'selected';
                    }
                    ?>
                    <option value="<?= $value->id ?>" <?= $selected ?>> <?= $value->bank_name ?> </option>
           <?php     }
            }
            ?>
        </select>
    </div>
</div>
<div class="inline-class" >
    <div class="w-50 vcenter"> 
        Bank Branch <span class="text-danger">*</span>
    </div>
    <div class="w-50 vcenter"> 
        <input type="text" name="bank_branch" id="bank_branch" class="form-control" value="<?= $info->bank_branch ?? ''?>">
    </div>
</div>
<div class="inline-class" >
    <div class="w-50 vcenter"> 
        Bank Ac No <span class="text-danger">*</span>
    </div>
    <div class="w-50 vcenter"> 
        <input type="text" name="bank_ac_no" id="bank_ac_no"  value="<?= $info->bank_ac_no ?? '' ?>" class="form-control"> 
    </div>
</div>
<?= $this->load->view( 'pages/expense/form/_cheque', '', true ) ?>
 
<div class="inline-class" >
    <div class="w-50 vcenter"> 
        Amount <span class="text-danger">*</span>
    </div>
    <div class="w-50 vcenter"> 
        <input type="text" name="amount" id="amount"  value="<?= $info->amount ?? '' ?>"class="form-control"> 
    </div>
</div>
<div class="inline-class" >
    <div class="w-50 vcenter"> 
        Description
    </div>
    <div class="w-50 vcenter"> 
       <textarea class="form-control" rows="3" cols="5" name="description" id="description" ><?= $info->description ?? '' ?></textarea>
    </div>
</div>
<script type="text/javascript">
    $( '.cheque_type' ).hide();
  
     $(function () {
        $("#payment_method_id").change(function () {

            var selectedText    = $(this).find("option:selected").text();
            selectedText        = selectedText.trim();
            selectedText        = selectedText.toUpperCase();
            
            if( selectedText == 'CHEQUE' ) {
                $( '.cheque_type' ).show();
                $( '.farmer_type' ).hide();

            } else if( selectedText == 'DD' ) {
                $( '.cheque_type' ).hide();
                $( '.dd_field' ).show();
            } else {
                $( '.cheque_type' ).hide();
                
            }
        });
    });
</script>