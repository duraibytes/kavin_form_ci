<div class="block-header m-t-20">
    <?= $this->load->view( 'layout/_nav_header', '', true )?>
</div> 

<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
               
                <div class="body">
                    <div class="table-responsive">
                        <div class="text-center hd-font"> 
                            <span class="btn btn-sm expense-bg text-white">
                                EXPENSES
                            </span> 
                        </div>
                        <table id="expense_table" class="table table-bordered table-hover w-100">
                            <thead>
                                <tr class="expense-bg text-white">
                                    <th> Date </th>
                                    <th> Expense Type </th>
                                    <th> Description </th>
                                    <th> Amount </th>
                                    <th> Action </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
    </div>
    
</div>



<script type="text/javascript">
      
    function open_add_expense_form( id='' ) {
          var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&id=' + id;
          $.ajax( {
              type    : "POST",
              url     : '<?= site_url( 'expense/open_add_expense_form' ); ?>',
              data    : data,
              success: function( msg ) {
                  $( '#defaultModal' ).modal();
                  $( '#defaultModal' ).html( msg );
              }
          });
          return false;
      }

     $(document).ready(function() {
        var dataTable = $( '#expense_table' ).DataTable( {
            'processing' : true,
            'serverSide' : true,
            'order'      : [],
            'ajax'       : {
                url      : '<?= site_url( 'expense/get_expense' ); ?>',
                type     : 'POST',
                data     : { '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>' }
            },
            'emptyTable' : 'No data available in table',
            'columnDefs' : [
                {  
                    'targets'   : [ 4 ],
                    'orderable' : false,
                }
            ],
            "lengthMenu": [
                [5, 15, 20, 50, 100, -1],
                [5, 15, 20, 50, 100, "All"] 
            ],
            "pageLength": '<?=NO_OF_DT_ROWS;?>',
        });
    });

    function delete_expense( id ) {
        var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&id=' + id;
        $.ajax( {
            type    : "POST",
            url     : '<?=site_url( 'expense/delete_expense' );?>',
            data    : data,
            success: function( msg ) {
                toastr.success( 'Expense deleted successfully', 'Expense' );
                setTimeout( function() {
                    window.location.href = '<?= site_url()?>expense';
                }, 300 );
            }
        });
        return false;
    }

    
</script> 