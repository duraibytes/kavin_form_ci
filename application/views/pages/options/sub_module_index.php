
<div class="block-header">
    <?= $this->load->view( 'layout/_nav_header', '', true )?>
</div>
<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
               
                <div class="body">
                    <div class="table-responsive">
                        <table id="module_table" class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr  class="inventory-bg text-white">
                                    <th> S.NO </th>
                                    <th> Module </th>
                                    <th> Sub Module </th>
                                    <th> Description </th>
                                    <th class="text-center"> Action </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    if( $optons ) {
                                        $i = 1;
                                        foreach ( $optons as $key => $value) {
                                            
                                        
                                ?>
                                <tr>
                                    <td> <?= $i?></td>
                                    <td> <?= $value->code_name ?> </td>
                                    <td> <?= $value->name ?></td>
                                    <td> <?= $value->decription ?></td>
                                    <td class="text-center">
                                        <a href="javascript:;" class="btn btn-warning btn-sm" onclick="return open_add_module_form( '<?= $value->id ?>', 'sub', '<?= $value->slug ?>' )"> <i class="icon-pencil"></i> </a>
                                        <a href="javascript:;" class="btn btn-danger btn-sm" onClick="return are_you_sure( 'delete_module', 'Delete Module', 'Are you sure want to delete this module?', '<?= $value->id?>', '<?= $value->slug ?>' );"> <i class="icon-trash"></i> </a>
                                    </td>
                                </tr>
                            <?php 
                                 $i++;   } 
                                } else {


                            ?>
                            <tr>
                                <td colspan="5"> NO RECORDS FOUND </td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
		</div>

	</div>	
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#module_table').DataTable();
    } );
    

   
    
</script>