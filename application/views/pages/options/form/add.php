<div class="modal-dialog module-form modal-lg" role="document">
    <div class="modal-content">
        <?=form_open( '', 'id="module-form"');?>
        <div class="modal-header">

            <h4 class="modal-title"><?= $btn_name ?> Modlue</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-12">
                    <?= $this->load->view( 'errors/show_error', '', true )?>
                </div>
            </div>
            <?= $this->load->view( 'pages/options/form/_form', '', true ) ?>
        </div>
        <div class="modal-footer">
            <input type="hidden" name="id" value="<?= $id ?? '' ?>">
            <input type="hidden" name="value" value="<?= $value ?? '' ?>">
            <input type="hidden" name="slug" value="<?= $slug ?? '' ?>">
            <button class="btn btn-secondary" data-dismiss="modal" type="button"> Close</button>
          
            <button onClick="return add_module()" class="btn btn-primary" type="button"> Save</button>
            
        </div>
        <?=form_close();?>
    </div>
</div>

<script type="text/javascript">

    function add_module() {
        var form_name           = $( '#module-form' ).find( 'input, select, button, textarea' );
        var codes_form_data     = form_name.serialize();
        codes_form_data         = codes_form_data + '&<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>';
        $.ajax({
            type        : 'POST',
            url         : '<?=site_url( 'settings/add_module' );?>',
            data        : codes_form_data,
            async       : false,
            dataType    : 'json',
            beforeSend: function () {
                form_name.attr( 'disabled', 'disabled' );
                $( '#form_errors' ).hide();
                $( '#form_success' ).hide();
            },    
            success     : function( msg ) {
                if( msg.error == 1 ) {
                    toastr.error( msg.error_msg, 'Options' );
                    form_name.attr( 'disabled', false );
                } else {
                    toastr.success( msg.error_msg, 'Options' );
                    setTimeout( function(){
                        if( msg.slug ) {
                            window.location.href = '<?=base_url();?>options/'+msg.slug;
                        } else {
                            window.location.href = '<?=base_url();?>options';
                        }
                    }, 300 );
                    
                }
            }
        });
        return false;
    }

</script>