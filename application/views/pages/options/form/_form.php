<div class="form-group">
    <label for="code"> Module </label>
    <input class="form-control" placeholder="Enter module" type="text" name="module" id="module" value="<?= $info->name ?? '' ?>" />
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <label for="code_description"> Description</label>
            <textarea placeholder="Enter description" id="code_description" name="code_description" class="form-control"><?= $info->decription ?? '' ?></textarea>
        </div>
    </div>
</div>