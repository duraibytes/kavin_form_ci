
<div class="block-header">
    <?= $this->load->view( 'layout/_nav_header', '', true )?>
</div>

<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
               
                <div class="body">
                    <div class="table-responsive">
                        <div class="text-center hd-font m-b-10 m-t-10"> 
                            <span class="btn btn-sm inventory-bg text-white">
                                Module - Management
                            </span> 
                        </div>
                        <table class="table table-bordered table-hover js-basic-example dataTable table-custom">
                            <thead>
                                <tr  class="inventory-bg text-white">
                                    <th> S.NO </th>
                                    <th> Module </th>
                                    <th> Description </th>
                                    <th> Sub Module </th>
                                    <th class="text-center"> Action </th>
                                </tr>
                            </thead>
                           
                            <tbody>
                                
                            <?php
                                if( $module ) {
                                    $i = 1;
                                    foreach ( $module as $key => $value) {
                                        
                                ?>
                                    <tr>
                                        <td> <?= $i?></td>
                                        <td> <?= $value->name ?></td>
                                        <td> <?= $value->decription ?></td>
                                        <td> <a href="<?= base_url()?>options/<?= $value->slug ?>" class="btn btn-sm btn-primary" > Sub Module List </a></td>
                                        <td style="text-align: center;">
                                            <a href="javascript:;" class="btn btn-warning btn-sm" onclick="return open_add_module_form( '<?= $value->id ?>' )"> <i class="icon-pencil"></i> </a>
                                            <a href="javascript:;" class="btn btn-danger btn-sm" onClick="return are_you_sure( 'delete_module', 'Delete Module', 'Are you sure want to delete this module?', '<?= $value->id?>' );"> <i class="icon-trash"></i> </a>
                                        </td>
                                    </tr>
                                <?php 
                                     $i++;   } 
                                    } else {
                                ?>
                                <tr>
                                    <td colspan="4"> NO RECORDS FOUND </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
    </div>
    
</div>