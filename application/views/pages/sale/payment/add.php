<div class="block-header">
    <?= $this->load->view( 'layout/_nav_header', '', true )?>
</div> 
<div class="container-fluid">
   
    <div class="row clearfix">
       
        <div class="col-lg-12">
            <div class="card">
               
                <div class="body demo-masked-input">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 m-b-10">
                            <div class="text-center hd-font m-t-10"> 
                              <span class="btn btn-sm sales-invoice-bg text-white">
                                  SALES PAYMENTS
                              </span> 
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                           
                            
                            <div class="inline-class" >
                                <div class="w-50 vcenter"> 
                                    Payment From  <span class="text-danger">*</span>
                                </div>
                                <div class="w-50 vcenter"> 
                                    <label class="fancy-radio custom-color-green"><input name="gender3" value="NEFT60" type="radio" checked=""><span><i></i>Dealer</span></label>
                                    <label class="fancy-radio custom-color-green"><input name="gender3" value="NEFT60" type="radio" checked=""><span><i></i>Farmer</span></label>
                                </div>
                            </div>
                            <div class="inline-class" >
                                <div class="w-50 vcenter"> 
                                    Dealer / Farmer Name  <span class="text-danger">*</span>
                                </div>
                                <div class="w-50 vcenter"> 
                                    <input type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
                                </div>
                            </div>
                            <div class="inline-class alert alert-warning" >
                                <div class="w-50 vcenter"> 
                                    Total Outstanding Balance<span class="text-danger">*</span>
                                </div>
                                <div class="w-50 vcenter"> 
                                    RS. 22300.00
                                </div>
                            </div>
                            
                            <div class="inline-class" >
                                <div class="w-50 vcenter"> 
                                    Payment Method  <span class="text-danger">*</span>
                                </div>
                                <div class="w-50 vcenter"> 
                                    <label class="fancy-radio custom-color-green"><input name="gender3" value="NEFT60" type="radio" checked=""><span><i></i>Cash</span></label>
                                    <label class="fancy-radio custom-color-green"><input name="gender3" value="NEFT60" type="radio" checked=""><span><i></i>Cheque</span></label>
                                    <label class="fancy-radio custom-color-green"><input name="gender3" value="NEFT60" type="radio" checked=""><span><i></i>NEFT</span></label>
                                    <label class="fancy-radio custom-color-green"><input name="gender3" value="NEFT60" type="radio" checked=""><span><i></i>IMPS</span></label>
                                    <label class="fancy-radio custom-color-green"><input name="gender3" value="NEFT60" type="radio" checked=""><span><i></i>RTGS</span></label>
                                    <label class="fancy-radio custom-color-green"><input name="gender3" value="NEFT60" type="radio" checked=""><span><i></i>DD</span></label>
                                </div>
                            </div>
                            <div class="inline-class" >
                                <div class="w-50 vcenter"> 
                                    Bank Name <span class="text-danger">*</span>
                                </div>
                                <div class="w-50 vcenter"> 
                                    <select class="custom-select" id="inputGroupSelect02">
                                        <option selected>Select Bank</option>
                                        <option value="1">State Bank of India</option>
                                        <option value="2">Indian Bank</option>
                                    </select>
                                </div>
                            </div>
                            <div class="inline-class" >
                                <div class="w-50 vcenter"> 
                                    Cheque/DD Number<span class="text-danger">*</span>
                                </div>
                                <div class="w-50 vcenter"> 
                                    <input type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
                                </div>
                            </div>
                            <div class="inline-class" >
                                <div class="w-50 vcenter"> 
                                    Cheque Issued Amount <span class="text-danger">*</span>
                                </div>
                                <div class="w-50 vcenter"> 
                                    <input type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
                                </div>
                            </div>
                            <div class="inline-class" >
                                <div class="w-50 vcenter"> 
                                    Cheque Issued Date<span class="text-danger">*</span>
                                </div>
                                <div class="w-50 vcenter"> 
                                    <input data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="dd/mm/yyyy">
                                </div>
                            </div>
                            
                            
                            
                    		
    		            </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="inline-class" >
                                <div class="w-50 vcenter"> 
                                    Payment Receipt Number  <span class="text-danger">*</span>
                                </div>
                                <div class="w-50 vcenter"> 
                                    <input type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" readonly>
                                </div>
                            </div>
                             <div class="inline-class" >
                                <div class="w-50 vcenter"> 
                                    Payment Date <span class="text-danger">*</span>
                                </div>
                                <div class="w-50 vcenter"> 
                                    <input type="text" class="form-control" name="payment_date" value="<?= date( 'd-m-Y' ) ?>" readonly>
                                </div>
                            </div>
                            <div class="inline-class" >
                                <div class="w-50 vcenter"> 
                                    Pay Against Sales/Retail Invoice<span class="text-danger">*</span>
                                </div>
                                <div class="w-50 vcenter"> 
                                   <select class="form-control" name="invoice_no">
                                       <option value=""> -- select -- </option>
                                   </select>
                                </div>
                            </div>
                            <div class="inline-class" >
                                <div class="w-50 vcenter"> 
                                    Payment Amount <span class="text-danger">*</span>
                                </div>
                                <div class="w-50 vcenter"> 
                                    <input type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
                                </div>
                            </div>
                            <div class="inline-class" style="border-bottom: none;" >
                                <div class="w-50 vcenter"> Upload Cheque/DD image here  </div>
                                <div class="w-50 vcenter"> <input type="file" class=""> </div>
                            </div>
                            
                            
                        </div>
		            </div>
		
		            <div class="row clearfix" style="text-align:right;">
		 
		                <div class="col-lg-12 col-md-12 m-t-10">
		             
		                    <a href="<?= base_url() ?>sale-payment" class="btn btn-outline-danger" >
                                Cancel
                            </a>
		                  <a href="javascript:;" class="btn btn-outline-dark" >
                                Clear
                            </a>
		                    <button id="addToTable" class="btn btn-outline-info" type="button">
                                Submit
                            </button>
		                    <button id="addToTable" class="btn btn-success " type="button">
                               Submit & Print
                            </button>
            		    </div>
            		</div>
		
                </div>
            </div>
        </div>
    </div>
					
</div>