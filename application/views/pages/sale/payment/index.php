<div class="block-header">
    <?= $this->load->view( 'layout/_nav_header', '', true )?>
</div> 
<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                    <div class="table-responsive">
                        <div class="text-center hd-font"> 
                            <span class="btn btn-sm sale-payment-bg text-white">
                                SALES PAYMENTS
                            </span> 
                        </div>
                        <table class="table table-bordered table-hover js-basic-example dataTable table-custom">
                            <thead>
                                <tr class="sale-payment-bg text-white">
                                    <th>
                                        <div>Payment</div>
                                        <div>Receipt Date</div>
                                    </th>
                                    <th>
                                        <div>Payment</div>
                                        <div>Receipt No</div>
                                    </th>
                                    <th>
                                        <div>Sale/Retail</div>
                                        <div>Invoice No</div>
                                    </th>
                                    <th>
                                        <div>Dealer/Farmer</div>
                                        <div> Name </div>
                                    </th>

                                    <th>
                                        <div> Payment </div>
                                        <div> Method </div>
                                    </th>
                                    <th>
                                        <div> Cheque Issued</div>
                                        <div> Date </div>
                                    </th>

                                    <th>
                                        <div>Total Outstanding</div>
                                        <div>Amount</div>
                                    </th>
                                    <th>
                                        <div>Paid</div>
                                        <div> Amount </div>
                                    </th>
                                    <th>
                                        <div>Balance</div>
                                        <div>Amount</div>
                                    </th>
                                    <th> Status </th>
                                    <th> Action </th>
                                    
                                </tr>
                            </thead>
                            
                            <tbody>
                                
                                <tr>
                                    <td> 14-03-2021 </td>
                                    <td>
                                       PAY/20-21/9871206                                        
                                    </td>
                                    
                                    <td> 45005454 </td>
                                    <td> PREM Kumar </td>
                                    <td>Cheque</td>
                                    <td>12-05-2021</td>
                                    <td> INR 2100.00 </td>
                                    <td> INR 458.00 </td>
                                    <td> INR 5208.00 </td>
                                    <td> <span class="badge badge-danger">Payment Pending</span></td>
                                    <td>
                                        <a href="<?= base_url() ?>sale-payment/view" class="btn btn-outline-success">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        <a href="javascript:;" class="btn btn-outline-warning">
                                            <i class="icon-pencil"></i>
                                        </a>
                                         <a href="javascript:;" class="btn btn-outline-primary">
                                            <i class="fa fa-money"></i>
                                        </a>   
                                       
                                      
                                       <a href="javascript:;" class="btn btn-outline-danger">
                                            <i class="icon-trash"></i>
                                        </a>
                                    </td>
                                    
                                </tr>
                                <tr>
                                    <td> 25-05-2021 </td>
                                    <td>
                                       PAY/20-21/9876                                        
                                    </td>
                                    
                                    <td> 45005454 </td>
                                    <td> Aruk Kumar </td>
                                    <td>Cheque</td>
                                    <td>12-05-2021</td>
                                    <td> INR 2300.00 </td>
                                    <td> INR 678.00 </td>
                                    <td> INR 908.00 </td>
                                    <td> <span class="badge badge-danger">Payment Pending</span></td>
                                    <td>
                                        <a href="<?= base_url() ?>sale-payment/view" class="btn btn-outline-success">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        <a href="javascript:;" class="btn btn-outline-warning">
                                            <i class="icon-pencil"></i>
                                        </a>
                                         <a href="javascript:;" class="btn btn-outline-primary">
                                            <i class="fa fa-money"></i>
                                        </a>   
                                       
                                      
                                       <a href="javascript:;" class="btn btn-outline-danger">
                                            <i class="icon-trash"></i>
                                        </a>
                                    </td>
                                    
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
    </div>
    
</div>