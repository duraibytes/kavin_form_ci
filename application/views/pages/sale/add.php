<div class="block-header">
    <?= $this->load->view( 'layout/_nav_header', '', true )?>
</div> 
<style type="text/css">
  .nav-link.active {
      /*background: #a1ef29 !important;*/
      background: linear-gradient(#2b5be4 0%, #00b8ff 100%);
      color:white !important;
  }
</style>
<div class="container-fluid">
 <?php echo form_open_multipart( '', 'id="sale_form"' ); ?> 
  <div class="row clearfix">
    <div class="col-lg-12">
        <div class="card">
          <div class="text-center hd-font m-t-10"> 
                <span class="btn btn-sm sales-invoice-bg text-white">
                    SALES INVOICES
                </span> 
            </div>
            <ul class="nav nav-tabs">
                <li class="nav-item" >
                  <a class="nav-link active show" data-toggle="tab" href="#Home-withicon" onclick="return get_ajax_sales_tab( 'dealer' );">
                    <img src="<?= base_url()?>assets/images/dealer.png" style="width:23px">  Dealer Sales 
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#Profile-withicon" onclick="return get_ajax_sales_tab( 'farmer' );">
                    <img src="<?= base_url()?>assets/images/farmer.png" style="width:23px"> Farmer / Retail Sales 
                  </a>
                </li>
            </ul>

            <div class="body" id="add_tab">
              <div class="tab-content">
                <div class="tab-pane show active" id="sale-invoice-tab-pane">
                   <?= $this->load->view( 'pages/sale/form/_dealer_form', '', true ) ?>
                </div>
              </div>
              <div class="col-lg-12 col-md-12 m-t-20">
                <div class="">
                  <table class="table table-bordered table-striped" cellspacing="0" id="sale_table">
                      <thead>
                        <tr class="purchase-invoice-bg text-white">
                            <th>
                              <div> Product </div>
                              <div> Code </div>
                            </th>
                            <th>
                              <div> Product </div>
                              <div> LOT </div>
                            </th>
                            <th>
                              <div> Date of </div>
                              <div> Packed </div>
                            </th>
                            <th>
                              <div> Date of </div>
                              <div> Tested </div>
                            </th>
                            <th >
                              <div> Date of </div>
                              <div> Expiry </div>
                            </th>
                            <th>
                              <div> Net </div>
                              <div> Rate </div>
                            </th>
                            <th>Qty</th>
                            <th>
                              <div> UOM </div>
                            </th>
                            <th>
                              <div>Discount</div>
                              <div>(%)</div>
                            </th>
                            <th>
                              Net Value
                            </th>
                            <th></th>                                              
                        </tr>
                      </thead>
                    <tbody>
                        <?= $this->load->view( 'pages/sale/_sale_items', '', true ) ?>
                      </tbody>
                  </table>
                  <input type="hidden" name="total_item" id="total_item" value="<?= $total_item ?? '' ?>">
                  <div class="btn btn-outline-success w-100" id="add_new">
                    <i class="fa fa-plus" ></i> Add Product
                  </div>
                  <table class="table" style="font-size: 12px;" id="remarks_table" > 
                    <tr>
                      <td style="width: 60%;padding: 10px;text-align: left;">
                        <div>
                          <label> Remarks:</label>
                        </div>
                        <div>
                          <div class="inline-class" >
                            <textarea rows="3" cols="3" class="form-control" name="remarks"><?= $sale_info->remarks ?? '' ?></textarea>
                          </div>
                        </div>
                      </td>
                      <td style="width: 40%">
                        <table style="width:100%;">
                          <tr>
                            <td>
                              <div class="inline-class" >
                                <div class="w-70 vcenter" style="width: 70%"> 
                                  Total Items Amount 
                                  <span class="text-danger">*</span>
                                </div>
                                <div class="w-30 vcenter"> 
                                  <input type="text" name="total_item_amount" id="total_item_amount" class="form-control text-right" value="<?= $sale_info->total_item_amount ?? '' ?>"> 
                                
                                  <input type="hidden" name="hidden_total_item_amount" id="hidden_total_item_amount" value="<?= $sale_info->total_item_amount ?? 0 ?>">
                                </div>
                              </div>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <div style="display: flex;padding: 10px">
                                <div >
                                  GST ( % ) :  0.00% 
                                </div>
                                <div style="padding-left: 10px;">
                                  CST ( % ) :  0.00% 
                                </div>
                                <div style="padding-left: 10px;">
                                  VAT ( % ) :  0.00% 
                                </div>
                              </div>
                            </td>
                          </tr>
                          <tr id="hide-transport-dealer">
                            <td>
                              <div class="inline-class" >
                                <div class="w-70 vcenter" style="width: 70%"> 
                                    Transport Charge <span class="text-danger">*</span>
                                </div>
                                <div class="w-30 vcenter text-right" style="width: 30%;"> 
                                    <div id="total_transport_charge" class="" style="    width: 100%;margin-right: 15px;">0.00</div>
                                </div>
                              </div>
                          </td>
                        </tr>
                        <tr>
                            <td>
                              <div class="inline-class" >
                                <div class="w-70 vcenter" style="width: 70%"> 
                                    Discount (%)<span class="text-danger">*</span>
                                    <input type="text" name="sale_discount" id="discount" class="form-control text-right" style="width: 45px;" placeholder="%">
                                </div>
                                <div class="w-30 vcenter text-right" style="width: 30%;"> 
                                    <div id="total_transport_charge" class="" style="    width: 100%;">
                                      <input type="text" name="total_item_amount_discount_amount" id="total_item_amount_discount_amount" class="form-control text-right" placeholder="0.00" readonly>
                                    </div>
                                </div>
                              </div>
                          </td>
                        </tr>
                        <tr>
                          <td>
                              <div class="inline-class" >
                                <div class="w-70 vcenter" style="width: 70%"> 
                                    Total Amount <span class="text-danger">*</span>
                                </div>
                                <div class="w-30 vcenter"> 
                                    <input type="text" name="total_amount" id="total_amount" class="form-control text-right" value="<?= $sale_info->total_amount ?? '' ?>"> 
                                     <input type="hidden" name="hidden_total_amount" id="hidden_total_amount" value="<?= $sale_info->total_amount ?? 0 ?>">
                                </div>
                              </div>
                          </td>
                        </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                  <div>
                    Amount In Words : 
                  </div>
                </div>
              </div>
              <div class="row clearfix" style="text-align:right;">
                <div class="col-lg-12 col-md-12">
                  <button id="addToTable" class="btn btn-outline-danger" type="button">
                    Cancel
                  </button>
                  <a  class="btn btn-outline-dark" href="javascript:;" onclick="return clear_form( 'sale_form' )">
                    Clear
                  </a>
                  <button id="addToTable" class="btn btn-outline-info" type="button"  onclick="return insert_purchase()">
                    Submit
                  </button>
                  <button id="addToTable" class="btn btn-success " type="button" onclick="return insert_purchase( 'print' )">
                    Submit & Print
                  </button>
                </div>
              </div>
            </div>
        </div>
    </div>
  </div>
  <?= form_close() ?>
</div>
<div id="printableArea" style="display: none;"></div>

<script type="text/javascript">

  function deleteRow( button ) {
      var row = button.parentNode.parentNode;
      var name = row.getElementsByTagName("TD")[0].innerHTML;
      //Get the reference of the Table.
      var table = document.getElementById("sale_table");
      //Delete the Table row using it's Index.
      table.deleteRow(row.rowIndex);
    }

    $("#add_new").click(function(){
      var total_item    = $( '#total_item' ).val();
      var count         = parseInt( total_item ) + 1;
      $( '#total_item' ).val( count );
      var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&total_item=' + count;
      $.ajax( {
          type    : "POST",
          url     : '<?= site_url( 'sale/get_ajax_items' ); ?>',
          data    : data,
          success: function( msg ) {
            $( "table#sale_table tbody" ).append( msg );
          }
      });
      return false;
    });

  function get_ajax_sales_tab( tab_type ) {
     
      var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&tab_type=' + tab_type;
      $.ajax( {
          type    : "POST",
          url     : '<?= site_url( 'sale/get_ajax_sales_tab' ); ?>',
          data    : data,
          dataType:'json',
          beforeSend: function() {
            $( '#total_amount' ).val( '' );
            $( '#total_transport_charge' ).val( '0.00' );
            $( '#transport_charge' ).val('');
          },
          success: function( msg ) {
            if( tab_type == 'farmer' ) {
              $( '#hide-transport-dealer' ).hide();
            } else {
              $( '#hide-transport-dealer' ).show();
            }
            if( msg.view ) {
              $( '#sale-invoice-tab-pane' ).html( msg.view );
            }
          }
      });
      return false;
    }

    function get_total_amount( amount ) {

      var tr_amount         = parseFloat( amount );
      var total_amount      = $( '#total_item_amount' ).val();

      $( '#total_transport_charge' ).html( tr_amount );
      console.log( tr_amount );
      if( total_amount == '' || total_amount == undefined ) {
          
         total_amount       = 0;
         total_amount       = parseFloat( total_amount );
         total_amount       = total_amount + tr_amount;
      } else {
          total_amount      = parseFloat( total_amount );
          
          total_amount      = total_amount + tr_amount;
          
      }
      $( '#total_amount' ).val( total_amount );
      $( '#hidden_total_amount' ).val( total_amount );
    }

    $( '#discount' ).change( function() {
        var total_item_amount       = $( '#total_item_amount' ).val();
        if( total_item_amount == '' || total_item_amount == undefined || total_item_amount == null ) {
          toastr.error( 'Please add sale items', 'Sale Error' );
          $( this ).val( '' );
          return false;
        }
        var transport_charge        = $( '#transport_charge' ).val();
        if( transport_charge == '' || transport_charge == undefined || transport_charge == null ) {
          transport_charge          = 0;
        }
        transport_charge            = parseFloat( transport_charge );
        var discount                = parseFloat( $(this).val() );

        total_item_amount           = parseFloat( total_item_amount );
        discount                    = total_item_amount * (discount / 100);
        $( '#total_item_amount_discount_amount' ).val( discount.toFixed(2) );
        total_net_price             = total_item_amount - ( discount );
        total_net_price             = total_net_price + transport_charge;
        $( '#total_amount' ).val( total_net_price.toFixed(2) );
        console.log( total_net_price );
    });

    function insert_purchase( print = '' ) {
      var sale_form       = $( '#sale_form' ).find('input, textarea, button, select');
      var data                = new FormData( $( '#sale_form' )[0] );
      $.ajax({
        type        : 'POST',
        url         : '<?=site_url( 'sale/insert_sale' );?>',
        data        : data,
        async       : false,
        contentType : false,
        processData : false,
        dataType    : 'json',
        beforeSend: function () {
          sale_form.attr( 'disabled', 'disabled' );
        },    
        success     : function( msg ) {
          if( msg.error == 1 ) {
            toastr.error( msg.error_msg, 'sale' );
            sale_form.attr( 'disabled', false );
          } else {
            toastr.success( msg.error_msg, 'sale' );
            if( print ) {
              if( msg.slug ) {
                print_sale( msg.slug );
              }
            }
            setTimeout( function(){
               window.location.href = '<?= site_url()?>sale';
            }, 1200 );
          }
        }
      });
      return false;
    }

</script>