<div class="block-header">
    <?= $this->load->view( 'layout/_nav_header', '', true )?>
</div> 
<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body" id="printarea">
                    <h6><center><strong class="text-primary">Sales Invoice</strong></center></h6>
       
			        <div class="tab-content mt-3">
			            <div role="tabpanel" class="tab-pane in active" id="details" aria-expanded="true">
			                <div class="row clearfix">
			                    <div class="col-md-8 col-sm-4">
									 <address>
				                            <strong>SRI PACKIA SEEDS</strong><br>
				                            4, East Market Street, Theni - 625531. Theni District. TN.<br>
				                            <abbr title="Phone">Ph:</abbr> (123) 456-34636, Mobile: +91 986524586<br/>
											www.sripackiaseeds.com, Email : info@sripackiaseeds.com<br/>
											Seeds Licenses # MDU/2001/123456
				                        </address>
				               </div>
							    <div class="col-md-4 col-sm-4">
									<img src="../assets/images/logo.png" width="160" height="80" class="align-top mr-2" alt="">
								</div>
			               	</div>
							<div class="row clearfix">
			                    <div class="col-md-6 col-sm-6">
									<div class="table-responsive">
				                        <table style="border:1px solid #e0dddd;">
											<tr>
												<td style="background:#e0dddd;"><strong>Dealers/Customer:</strong></td>
										
											</tr>
											<tr>
												<td style="padding-left:10px;padding-right:10px;"><strong>Muthu Seeds Agencies</strong><br>34 Main Street, Theni - 625531. TN<br/><abbr title="Phone">Ph:</abbr> (123) 456-34636, Mobile: +91 986524586<br/></td>
											</tr>
											<tr>
												<td style="background:#e0dddd;">Seed License # MDU/2001-2005/ 345 6432 34<br></td>
											</tr>
											<tr>
												<td>Payment Terms : NET 30 Days from Invoiced </td>
											</tr>
											<tr>
												<td style="background:#e0dddd;">Previous Due/Outstanding Amount : 10,000.00</td>
											</tr>

										</table>
									</div>
				                       
				                </div>
									
			                    <div class="col-md-6 col-sm-6 text-right" >	
								<div class="row clearfix" style="margin-right:10px;">
								<div class="col-md-1 col-sm-1">
								</div>
								<div class="col-md-5 col-sm-5" style="border-left:1px solid #e0dddd;border-top:1px solid #e0dddd;border-bottom:1px solid #e0dddd;text-align:left;">
								      <p class="m-b-0"><strong>Sale / Invoice Date </strong></p>
								      <p class="m-b-0"><strong>Sale / Invoice Number </strong></p>
								      <p class="m-b-0"><strong>Sale / Invoice Type </strong></p>
								      <p class="m-b-0"><strong>Transport Expenses </strong></p>
								      <p class="m-b-0"><strong>Mode of Order </strong></p>
								      <p class="m-b-0"><strong>Mode of Transport </strong></p>
			                  
								</div>
								<div class="col-md-1 col-sm-1" style="border-top:1px solid #e0dddd;border-bottom:1px solid #e0dddd;text-align:left;">
								 <p class="m-b-0"><strong>:</strong></p>
								 <p class="m-b-0"><strong>:</strong></p>
								 <p class="m-b-0"><strong>:</strong></p>
								 <p class="m-b-0"><strong>:</strong></p>
								 <p class="m-b-0"><strong>:</strong></p>
								 <p class="m-b-0"><strong>:</strong></p>
								</div>
								<div class="col-md-5 col-sm-5" style="border-right:1px solid #e0dddd;border-top:1px solid #e0dddd;border-bottom:1px solid #e0dddd;text-align:left;">
								        <p class="m-b-0">Today Sys Date</p>
								        <p class="m-b-0">SPS/DLR/SLS/12-13/001</p>
								        <p class="m-b-0">CREDIT INVOICE</p>
								        <p class="m-b-0">TO PAY(DUE)</p>
								        <p class="m-b-0">PHONE ORDER</p>
								        <p class="m-b-0">MGRAJA LORRY SERVICE</p>
			                 
								</div>
			                     </div>
								
			                    </div>
			                </div><br/>
			                <div class="row clearfix">
			                    <div class="col-md-12">
			                        <div class="table-responsive">
			                            <table class="table table-hover">
			                                <thead class="thead-dark">
			                                    <tr>
			                                        <th>SL.NO</th>                                                        
			                                        <th>Company</th>
			                                        <th class="hidden-sm-down">Product</th>
			                                        <th>PRD ID</th>
			                                        <th class="hidden-sm-down">Description</th>
			                                        <th>Lot No</th>
			                                        <th>Expiry</th>
			                                        <th>Qnty</th>
			                                        <th>Units </th>
			                                        <th>Price</th>
			                                        <th>Discount</th>
			                                        <th>Total (Rs)</th>
			                                    </tr>
			                                </thead>
			                                <tbody>
			                                    <tr>
			                                        <td>1</td>
			                                        <td>iPhone 7</td>
			                                        <td class="hidden-sm-down">Lorem ipsum dolor sit amet.</td>
			                                        <td>SPS</td>
			                                        <td class="hidden-sm-down">Hybrid</td>
			                                        <td>Ax0389798</td>
			                                        <td>03/08/14</td>
			                                        <td>10</td>
			                                        <td>Kgs</td>
			                                        <td>500.00</td>
			                                        <td>5%</td>
			                                        <td>4700.00</td>
			                                    </tr>
			                                   
			                                </tbody>
			                            </table>
									
			                        </div>
			                    </div>
			                </div>
			                <hr>
			                <div class="row clearfix">
			                    <div class="col-md-6">
			                        <h6>Amount in words <span style="font-weight:normal;">: Rs. Fouty thousand rupees only /-</span> </h6>
			                    </div>
			                    <div class="col-md-6 text-right">
			                        <p class="m-b-0"><b>Total:</b> 4700.00</p>
			                       
			                    </div>                                    
			                    
			                </div>   
			                    <div class="col-md-12"style="padding-top:10px;padding-top:20px;border:1px solid #e0dddd;border-radius:20px; ">
			                        <h6><u>Remarks/Disclaimers:</u></h6>
			                        <p>1. Goods once SOLD are not taken back at any cost
			                        <br/>2. Additional interest charges made when the invoiced payments not made within the above said due date.<br/>
			                        3. All Discounts are subject to apply only if all your pending payments are cleared on or before the due date.<br/>
			                        4. Must cross check printed invoice info against the goods delivered to you and let us know immediately if corrections needed</p>
			                    </div>									
			            </div>                        
			                             
			        </div>
    			</div>
            </div>
        </div>
    </div>
    
</div>

<script type="text/javascript">
	function printpage() {
        var printContents = document.getElementById( 'printarea' ).innerHTML;
	     var originalContents = document.body.innerHTML;

	     document.body.innerHTML = printContents;

	     window.print();

	     document.body.innerHTML = originalContents;
	}
</script>