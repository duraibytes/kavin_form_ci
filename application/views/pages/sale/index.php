<div class="block-header m-t-20">
    <?= $this->load->view( 'layout/_nav_header', '', true )?>
</div> 
<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                    <div class="table-responsive">
                        <div class="text-center hd-font"> 
                            <span class="btn btn-sm sales-invoice-bg text-white">
                                SALES INVOICES
                            </span> 
                        </div>
                        <table class="table table-bordered table-hover js-basic-example dataTable table-custom">
                            <thead>
                                <tr class="text-white sales-invoice-bg">
                                    <th> 
                                        <div> Sales  </div>
                                        <div> Inv No </div>
                                    </th>
                                    <th> 
                                        <div> Sale  </div>
                                        <div> Inv Date</div>
                                    </th>
                                    <th> 
                                        <div> Dealer/Farmer  </div>
                                        <div> Name </div>
                                    </th>
                                    <th> 
                                        <div> Payment   </div>
                                        <div> Terms </div>
                                    </th>
                                    
                                    <th> 
                                        <div> Product  </div>
                                        <div> Code </div>
                                    </th>
                                    <th> 
                                        <div> Product  </div>
                                        <div> Description </div>
                                    </th>
                                    <th> 
                                        <div> Product  </div>
                                        <div> Lot </div>
                                    </th>
                                    <th> 
                                        <div> Date Of  </div>
                                        <div> Expiry </div>
                                    </th>
                                    <th> 
                                        <div> Total  </div>
                                        <div> QTY </div>
                                    </th>
                                    <th> 
                                        <div> Total  </div>
                                        <div> Amount Due </div>
                                    </th>
                                    <th> 
                                        <div> Payment  </div>
                                        <div> Status </div>
                                    </th>
                                    <th> Action </th>
                                    
                                </tr>
                            </thead>
                            
                            <tbody>
                                
                                <tr>
                                    <td> SAL/20-21/898989 </td>
                                    <td>
                                       15-02-2021                                        
                                    </td>
                                    
                                    <td> Aravind
                                        <div class="badge badge-success"> Dealer </div>
                                     </td>
                                    <td> Cash </td>
                                    <td> KL908K </td>
                                    <td> DESCRIPTIO n  </td>
                                    <td> LOT:987678</td>
                                    <td> 16-03-2021 </td>
                                    <td> 23 </td>
                                    <td> INR 125.00 </td>
                                    <td> <span class="badge badge-danger">Payment Pending</span></td>
                                    <td>
                                        <a href="<?= base_url() ?>sale/view" class="btn btn-outline-success">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                       <a href="javascript:;" class="btn btn-outline-warning">
                                            <i class="icon-pencil"></i>
                                        </a>
                                        <a href="javascript:;" class="btn btn-outline-primary">
                                            <i class="fa fa-money"></i>
                                        </a>   
                                        <a href="javascript:;" class="btn btn-outline-danger">
                                            <i class="icon-trash"></i>
                                        </a>
                                    </td>
                                    
                                </tr>
                                <tr>
                                    <td> SAL/20-21/1200021 </td>
                                    <td>
                                       15-02-2021                                        
                                    </td>
                                    
                                    <td> Vimal 
                                        <div class="badge badge-warning"> Farmer </div>
                                     </td>
                                    <td> Cash </td>
                                    <td> KL908K </td>
                                    <td> DESCRIPTIO n  </td>
                                    <td> LOT:987678</td>
                                    <td> 16-03-2021 </td>
                                    <td> 23 </td>
                                    <td> INR 450.00 </td>
                                    <td> <span class="badge badge-danger">Payment Pending</span></td>
                                    <td>
                                        <a href="<?= base_url() ?>sale/view" class="btn btn-outline-success">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                       <a href="javascript:;" class="btn btn-outline-warning">
                                            <i class="icon-pencil"></i>
                                        </a>
                                        <a href="javascript:;" class="btn btn-outline-primary">
                                            <i class="fa fa-money"></i>
                                        </a>   
                                        <a href="javascript:;" class="btn btn-outline-danger">
                                            <i class="icon-trash"></i>
                                        </a>
                                    </td>
                                    
                                </tr>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
    </div>
    
</div>