<div class="block-header">
    <?= $this->load->view( 'layout/_nav_header', '', true )?>
</div> 
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card ">
                        
                        <div class="body">
                            <div class="row clearfix demo-masked-input">
                              <div class="col-lg-12 col-md-12 m-b-10">
                                 <div class="text-center hd-font m-t-10"> 
                                    <span class="btn btn-sm sales-invoice-bg text-white">
                                        SALES INVOICES
                                    </span> 
                                </div>
                              </div>
                              <div class="col-lg-6 col-md-12">
                                <div class="inline-class" >
                                    <div class="w-50 vcenter"> 
                                      Sales Return From  <span class="text-danger">*</span>
                                    </div>
                                    <div class="w-50 vcenter"> 
                                      <label class="fancy-radio custom-color-green"><input name="gender3" value="NEFT60" type="radio" checked=""><span><i></i> Dealer</span></label>
                                      <label class="fancy-radio custom-color-green"><input name="gender3" value="NEFT60" type="radio" checked=""><span><i></i> 
                                      Farmer</span></label>
                                    </div>
                                </div>
                                <div class="inline-class alert alert-warning" >
                                    <div class="w-50 vcenter"> 
                                        Total Outstanding Balance<span class="text-danger">*</span>
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        RS. 22300.00
                                    </div>
                                </div>
                                <div class="inline-class" >
                                    <div class="w-50 vcenter"> 
                                        Return Against Invoice  <span class="text-danger">*</span>
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <input type="text" name="product_name" id="product_name" class="form-control"> 
                                    </div>
                                </div>
                                <div class="inline-class" >
                                    <div class="w-50 vcenter"> 
                                        Mode of Transport <span class="text-danger">*</span>
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <select class="custom-select" id="inputGroupSelect02">
                                          <option selected>Select</option>
                                          <option value="1">One</option>
                                          <option value="2">Two</option>
                                          <option value="3">Three</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="inline-class" >
                                    <div class="w-50 vcenter"> 
                                        Name of Transport <span class="text-danger">*</span>
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <input type="text" name="product_name" id="product_name" class="form-control"> 
                                    </div>
                                </div>

                               
              				          
              				        </div>

  				                    <div class="col-lg-6 col-md-12">
                                <div class="inline-class" >
                                    <div class="w-50 vcenter"> 
                                        Sale Return Number <span class="text-danger">*</span>
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <input type="text" name="product_name" id="product_name" class="form-control" readonly=""> 
                                    </div>
                                </div>
                                <div class="inline-class" >
                                    <div class="w-50 vcenter"> 
                                        Return Date <span class="text-danger">*</span>
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <input type="text" class="form-control" value="<?= date( 'd-m-Y H:i A ' ) ?>" readonly>
                                    </div>
                                </div>
                                 <div class="inline-class" >
                                    <div class="w-50 vcenter"> 
                                        Reason for Return <span class="text-danger">*</span>
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <select class="custom-select" id="inputGroupSelect02">
                                          <option selected>Select</option>
                                          <option value="1">One</option>
                                          <option value="2">Two</option>
                                          <option value="3">Three</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="inline-class" >
                                    <div class="w-50 vcenter"> 
                                        PAID / TOPAY <span class="text-danger">*</span>
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <select class="custom-select" id="inputGroupSelect02">
                                          <option selected>Select</option>
                                           <option value="Paid">Paid</option>
                                            <option value="ToPay">ToPay</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="inline-class" style="border-bottom: none;" >
                                    <div class="w-50 vcenter"> Attachment Document here </div>
                                    <div class="w-50 vcenter"> <input type="file" class=""> </div>
                                </div>
  				                    </div>
				                  </div>
				                <div class="col-lg-12 col-md-12 m-t-20">
                          <div class="">
                                  <table class="table table-bordered table-striped" cellspacing="0" id="addrowExample">
                                      <thead>
                                          <tr class="sale-return-bg text-white">
                                              
                                              <th>
                                                <div> Product </div>
                                                <div> Code </div>
                                              </th>
                                              <th>
                                                <div> Product </div>
                                                <div> Variety </div>
                                              </th>
                                              <th>
                                                <div> Product </div>
                                                <div> Desc </div>
                                              </th>
                                              <th>
                                                <div> Product </div>
                                                <div> Packeging </div>
                                              </th>
                                              <th>
                                                <div> Product </div>
                                                <div> LOT </div>
                                              </th>
                                              <th>
                                                <div> UOM </div>
                                               
                                              </th>
                                              <th>
                                                <div> Net </div>
                                                <div> Rate </div>
                                              </th>

                                              <th>
                                                <div> Date of </div>
                                                <div> Packed </div>
                                              </th>
                                              <th>
                                                <div> Date of </div>
                                                <div> Tested </div>
                                              </th>
                                              <th >
                                                <div> Date of </div>
                                                <div> Expiry </div>
                                              </th>
                                              <th>Qty</th>
                                              <th>
                                                <div>Discount</div>
                                                <div>(%)</div>
                                              </th>
                                              <th>
                                                Net Value
                                              </th>
                                              <th></th>                                              
                                              
                                          </tr>
                                      </thead>
                                      <tbody>
                                        <tr>
                                          <td>
                                            <input type="text" name="code" id="code" value="" class="form-control" placeholder="code">
                                          </td>
                                          <td> 
                                            <input type="text" name="code" id="code" value="" placeholder="variety" class="form-control">
                                          </td>
                                          <td> 
                                            <input type="text" name="code" id="code" value="" placeholder="desc" class="form-control"> 
                                          </td>
                                          <td> 
                                            <input type="text" name="code" id="code" value="" placeholder="packaging" class="form-control"> 
                                          </td>
                                          <td> 
                                            <input type="text" name="code" id="code" value="" placeholder="lot" class="form-control"> 
                                          </td>
                                          <td> 
                                            <input type="text" name="code" id="code" value="" placeholder="UOM" class="form-control"> 
                                          </td>
                                          <td> 
                                            <input type="text" name="code" id="code" value="" placeholder="rate" class="form-control"> 
                                          </td>
                                          <td> 
                                            <input data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="dd/mm/yyyy">
                                          </td>
                                          <td> 
                                            <input data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="dd/mm/yyyy">
                                          </td>
                                          <td>
                                             <input data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="dd/mm/yyyy">
                                          </td>
                                         
                                          <td>
                                             <input type="text" name="code" id="code" value="" placeholder="" class="form-control"> 
                                          </td>
                                           <td>
                                             <input type="text" name="code" id="code" value="" placeholder="" class="form-control">
                                          </td>
                                          <td>
                                            <input type="text" name="code" id="code" value="" placeholder="" class="form-control">
                                          </td>
                                          <td>
                                            <i class="fa fa-trash"></i>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td colspan="14" class="text-center">
                                             <div class="btn btn-outline-success w-100">
                                               <i class="fa fa-plus" ></i> Add Product
                                             </div>
                                          </td>
                                        </tr>
                                      </tbody>
                                      
                                     
                                  </table>
                                  <table class="table" style="font-size: 12px;">
                                    <tr>
                                      <td style="width: 60%;padding: 10px;text-align: left;">
                                        <div>
                                          <label> Remarks:</label>
                                        </div>
                                        <div>
                                          <textarea rows="3" cols="3" class="form-control"></textarea>
                                        </div>
                                      </td>
                                      <td style="width: 40%">
                                        
                                        <div style="display: flex;padding: 10px">
                                            <div >
                                              GST ( % ) :  0.00% 
                                            </div>
                                            <div style="padding-left: 10px;">
                                              CST ( % ) :  0.00% 
                                            </div>
                                            <div style="padding-left: 10px;">
                                              VAT ( % ) :  0.00% 
                                            </div>
                                        </div>
                                        <div class="inline-class" >
                                          <div class="w-70 vcenter" style="width: 70%"> 
                                              Total Amount <span class="text-danger">*</span>
                                          </div>
                                          <div class="w-30 vcenter"> 
                                              <input type="text" name="product_name" id="product_name" class="form-control"> 
                                          </div>
                                        </div>
                                      </td>
                                    </tr>
                                  </table>
                                  <div>
                                    Amount In Words : 
                                  </div>
                              </div>       
                                
                        </div>
							           
							          
				
				                <div class="row clearfix" style="text-align:right;">
				 
				                  <div class="col-lg-12 col-md-12">
			
				                      <a  class="btn btn-outline-danger" href="<?= base_url()?>sale-return">
                                Cancel
                              </a>
                              <a  class="btn btn-outline-dark" href="javascript:;">
                                Clear
                              </a>
				                      <button id="addToTable" class="btn btn-outline-info" type="button">
                                Submit
                              </button>
				                      <button id="addToTable" class="btn btn-success" type="button">
                                Submit & print
                              </button>
				                  </div>
				                </div>
				
                      </div>
                  </div>
              </div>
          </div>
						
      </div>