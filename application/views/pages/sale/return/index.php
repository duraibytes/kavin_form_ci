<div class="block-header">
    <?= $this->load->view( 'layout/_nav_header', '', true )?>
</div> 
<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                    <div class="table-responsive">
                        <div class="text-center hd-font"> 
                            <span class="btn btn-sm sale-return-bg text-white">
                                SALES RETURNS
                            </span> 
                        </div>
                        <table class="table table-bordered table-hover js-basic-example dataTable table-custom">
                            <thead>
                                <tr class="sale-return-bg text-white">
                                    <th>
                                        <div>Sales</div>
                                        <div>Return No</div>
                                    </th>
                                    <th>
                                        <div>Sales Return</div>
                                        <div>Invoice Date</div>
                                    </th>
                                    <th>
                                        <div>Dealer</div>
                                        <div>Name</div>
                                    </th>
                                    <th>
                                        <div>Sales</div>
                                        <div>Inv No</div>
                                    </th>
                                    <th>
                                        <div>Sales</div>
                                        <div>Inv Date</div>
                                    </th>

                                    <th>
                                        <div>Return </div>
                                        <div> Reason </div>
                                    </th>
                                    <th>
                                        <div>Product</div>
                                        <div> Code </div>
                                    </th>

                                    <th>
                                        <div> Product </div>
                                        <div> Description </div>
                                    </th>
                                    <th>
                                        <div> Product </div>
                                        <div> Lot </div>
                                    </th>
                                    <th>
                                        <div> Total</div>
                                        <div> Qty </div>
                                    </th>
                                    <th>
                                        <div>Date Of</div>
                                        <div>Expiry</div>
                                    </th>
                                    <th>
                                        <div>Net</div>
                                        <div>Rate</div>
                                    </th>
                                    <th>
                                        <div>Discounts</div>
                                        <div> ( % )</div>
                                    </th>
                                     <th>
                                        <div> Total </div>
                                        <div> Amount </div>
                                    </th>
                                    <th> Status </th>
                                    <th> Action </th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                <tr>
                                    <td> SRUT/20-21/12345678</td>
                                    <td>
                                       12-05-2021
                                    </td>
                                    <td> Baau </td>
                                    <td> 23456789 </td>
                                    <td> 15-04-2021 </td>
                                    <td> Damaged </td>
                                    <td> MK90L </td>
                                    <td> description </td>
                                    <td> LOT:9876567 </td>
                                    <td> 12 </td>
                                    <td> 15-06-2021 </td>
                                    <td> INR 123.00 </td>
                                    <td> 12 </td>
                                    <td> INR 1500.00 </td>
                                    <td> <span class="badge badge-danger">Payment Pending</span></td>
                                    <td>
                                        <a href="<?= base_url() ?>sale-return/view" class="btn btn-outline-success">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                         <a href="javascript:;" class="btn btn-outline-warning">
                                            <i class="icon-pencil"></i>
                                        </a>
                                         <a href="javascript:;" class="btn btn-outline-primary">
                                            <i class="fa fa-money"></i>
                                        </a>   
                                       
                                      
                                       <a href="javascript:;" class="btn btn-outline-danger">
                                            <i class="icon-trash"></i>
                                        </a>
                                    </td>
                                    
                                </tr>
                                <tr>
                                    <td> SRUT/20-21/12345678</td>
                                    <td>
                                       12-05-2021
                                    </td>
                                    <td> Baau </td>
                                    <td> 23456789 </td>
                                    <td> 15-04-2021 </td>
                                    <td> Damaged </td>
                                    <td> MK90L </td>
                                    <td> description </td>
                                    <td> LOT:9876567 </td>
                                    <td> 12 </td>
                                    <td> 15-06-2021 </td>
                                    <td> INR 123.00 </td>
                                    <td> 12 </td>
                                    <td> INR 1500.00 </td>
                                    <td> <span class="badge badge-danger">Payment Pending</span></td>
                                    <td>
                                        <a href="<?= base_url() ?>sale-return/view" class="btn btn-outline-success">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                         <a href="javascript:;" class="btn btn-outline-warning">
                                            <i class="icon-pencil"></i>
                                        </a>
                                         <a href="javascript:;" class="btn btn-outline-primary">
                                            <i class="fa fa-money"></i>
                                        </a>   
                                       
                                      
                                       <a href="javascript:;" class="btn btn-outline-danger">
                                            <i class="icon-trash"></i>
                                        </a>
                                    </td>
                                    
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
    </div>
    
</div>