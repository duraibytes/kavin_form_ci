<div class="row clearfix">
    <div class="col-lg-6 col-md-12">
    	<div class="inline-class" >
            <div class="w-50 vcenter"> 
                Dealer Name<span class="text-danger">*</span>
            </div>
            <div class="w-50 vcenter"> 
                <select class="custom-select" name="dealer" id="dealer" onchange="return get_outstanding( this.value )"> ;
	                <option value="" selected>Select</option>
	                <?php 
                    if( isset( $customer_all ) && !empty( $customer_all ) ) {
                        foreach ( $customer_all as $key => $cus ) {
                    ?>
                    <option value="<?= $cus->id ?>"> <?= $cus->fname ?></option>
                    <?php  }  }
                    ?>
	            </select>
        	</div>
		</div>
        <div class="inline-class alert alert-warning m-t-20" >
            <div class="w-50 vcenter"> 
                 Dealer's Current Outstanding Amount:<span class="text-danger">*</span>
            </div>
            <div class="w-50 vcenter"> 
                RS. <span id="outstanding"> 0.00 </span>
                <input type="hidden" name="dealer_outstanding" id="dealer_outstanding">
            </div>
        </div>
		<input type="hidden" name="customer_type" id="customer_type" value="<?= $customer_type ?>">
		<div class="inline-class" >
            <div class="w-50 vcenter"> 
                Transportation Charge <span class="text-danger">*</span>
            </div>
            <div class="w-50 vcenter"> 
               <select class="custom-select" id="tranport_payment" name="tranport_payment">
                  <option value="" selected>Select</option>
                   <option value="Paid" <?php if( isset( $purchase_info->tranport_payment ) && $purchase_info->tranport_payment == 'Paid' ) { echo 'selected'; } ?>>Paid</option>
                    <option value="ToPay" <?php if( isset( $purchase_info->tranport_payment ) && $purchase_info->tranport_payment == 'ToPay' ) { echo 'selected'; } ?> >ToPay</option>
                </select>
            </div>
        </div>
        <div class="inline-class topay_transport" style="display:none;">
            <div class="w-50 vcenter"> 
                Transport Charge <span class="text-danger">*</span>
            </div>
            <div class="w-50 vcenter"> 
                <input type="text" name="transport_charge" id="transport_charge" class="form-control" value="<?= $purchase_info->transport_charge ?? '' ?>" onchange="return get_total_amount( this.value )"> 
            </div>
        </div>
        <div class="inline-class" >
            <div class="w-50 vcenter"> 
                Mode of Transport <span class="text-danger">*</span>
            </div>
            <div class="w-50 vcenter"> 
               <div class="input-group">
                    <div class="mode_of_transport_ajax" style="width: 82%">
                      <select class="custom-select" id="mode_of_transport" name="mode_of_transport">
                        <option value="" selected>Select</option>
                       <?php
                        if( isset( $transport_all ) && !empty( $transport_all ) ) {
                          foreach ( $transport_all as $tran_key => $tran_value ) {
                            $selected         = '';
                            if( isset( $purchase_info->transport_id ) && $purchase_info->transport_id == $tran_value->id ) {
                              $selected       = 'selected';
                            }
                        ?>
                           <option value="<?= $tran_value->id ?>" <?= $selected ?>> <?= $tran_value->name ?></option>
                        <?php } } ?>
                      </select>
                    </div>
                    <div class="input-group-prepend" onclick="return open_add_transport_form( '', 'mode_of_transport_ajax' );">
                      <div class="input-group-text" id="btnGroupAddon">
                        <i class="fa fa-plus text-white"></i>
                      </div>
                    </div>
                  </div>
            </div>
        </div>
        <div class="inline-class" >
            <div class="w-50 vcenter"> 
                Name of Transport <span class="text-danger">*</span>
            </div>
            <div class="w-50 vcenter"> 
                <input type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" name="name_of_transport">
            </div>
        </div>
	</div>
	<!-- second column  -->
	<div class="col-lg-6 col-md-12">
        <div class="inline-class" >
            <div class="w-50 vcenter"> 
                Sales Invoice No<span class="text-danger">*</span>
            </div>
            <div class="w-50 vcenter"> 
                <input type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" readonly name="sale_invoice_no" value="<?= $invoice_no ?? ''?>">
            </div>
        </div>
        <div class="inline-class" >
            <div class="w-50 vcenter"> 
                Current date<span class="text-danger">*</span>
            </div>
            <div class="w-50 vcenter"> 
                <input type="text" name="current_date" value="<?= date( 'd-m-Y' )?>" class="form-control" readonly>
            </div>
        </div>
        <div class="inline-class" >
            <div class="w-50 vcenter"> 
                Payment Terms <span class="text-danger">*</span>
            </div>
            <div class="w-50 vcenter"> 
                <div class="input-group">
                    <div class="mode_of_payment_ajax" style="width: 82%">
                      <select class="custom-select" id="mode_of_payment" name="mode_of_payment" onchange="return get_due_days( this.value )">
                        <option value="" selected>Select</option>
                       <?php
                        if( isset( $payment ) && !empty( $payment ) ) {
                          foreach ( $payment as $pay_key => $pay_value ) {
                            $selected         = '';
                            // if( isset( $purchase_info->mode_of_payment ) && $purchase_info->mode_of_payment == $pay_value->id ) {
                            //   $selected       = 'selected';
                            // }
                        ?>
                          <option value="<?= $pay_value->id ?>" <?= $selected ?>> <?= $pay_value->name ?></option>
                        <?php } } ?>
                      </select>
                    </div>
                    <div class="input-group-prepend" onclick="return add_form( 'mode_of_payment_ajax', 'sale' )">
                      <div class="input-group-text" id="btnGroupAddon">
                        <i class="fa fa-plus text-white"></i>
                      </div>
                    </div>
                  </div>
            </div>
        </div>
        <div class="inline-class" >
            <div class="w-50 vcenter"> 
                Sales Invoice DUE<span class="text-danger">*</span>
            </div>
            <div class="w-50 vcenter"> 
                <input type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" placeholder="dd-mm-yyyy" readonly name="sale_due_date" id="sale_due_date">
            </div>
        </div>
		<div class="inline-class"  >
	        <div class="w-50 vcenter"> Upload Image </div>
	        <div class="w-50 vcenter"> <input type="file" class="" name="sale_image" id="sale_image"> </div>
	    </div>
	</div>
		
</div>

<script type="text/javascript">
    $( '#tranport_payment' ).change( function() {
     
         var transport_payment  = $(this).val();
         
         if( transport_payment == 'ToPay' ) {
           $( '.topay_transport' ).show();
         } else {
            $( '.topay_transport' ).hide();
         }
      
    });

    function get_due_days( payment_id ) {
      var dealer   = $( '#dealer' ).val();
      if( dealer == '' || dealer == 'undefined' )  {
          toastr.error( 'Dealer Name is required', 'Sale' );
          $( '#dealer' ).focus();
          $( '#mode_of_payment' ).val( '' );
          return false;
        }
      var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&payment_id=' + payment_id;
      $.ajax( {
          type    : "POST",
          url     : '<?= site_url( 'sale/get_due_days' ); ?>',
          data    : data,
          dataType:'json',
          success: function( msg ) {
            $( "#sale_due_date" ).val( msg.due_day );
          }
      });
      return false;
    }

    function get_outstanding( dealer_id ) {
        
      var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&dealer_id=' + dealer_id;
      $.ajax( {
          type    : "POST",
          url     : '<?= site_url( 'Calculation/get_outstanding_sale' ); ?>',
          data    : data,
          dataType:'json',
          success: function( msg ) {
            $( "#dealer_outstanding" ).val( msg.out_standing );
            $( "#outstanding" ).html( msg.out_standing );
          }
      });
      return false;
    }
</script>