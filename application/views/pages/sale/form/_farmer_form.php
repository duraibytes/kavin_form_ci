<div class="row clearfix">
    <div class="col-lg-6 col-md-12">

		<div class="inline-class" >
            <div class="w-50 vcenter"> 
                Farmer Name<span class="text-danger">*</span>
            </div>
            <div class="w-50 vcenter"> 
                <div class="input-group">
                    <div class="farmer_form_ajax" style="width: 82%">
                      <select class="custom-select" id="farmer" name="farmer">
                        <option value="" selected>Select</option>
                        <?php 
                            if( isset( $customer_all ) && !empty( $customer_all ) ) {
                                foreach ( $customer_all as $key => $cus ) {
                        ?>
                            <option value="<?= $cus->id ?>"> <?= $cus->fname ?></option>
                        <?php 
                                }  
                            }
                        ?>
                      </select>
                    </div>
                    <div class="input-group-prepend" onclick="return add_farmer( 'farmer_form_ajax' )">
                      <div class="input-group-text" id="btnGroupAddon">
                        <i class="fa fa-plus text-white"></i>
                      </div>
                    </div>
                </div>
        	</div>
		</div>
		<div class="inline-class" >
            <div class="w-50 vcenter"> 
                Contact Number<span class="text-danger">*</span>
            </div>
            <div class="w-50 vcenter"> 
                <input type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" name="contact_number" id="contact_number">
        	</div>
		</div>
		<div class="inline-class" >
            <div class="w-50 vcenter"> 
                Address <span class="text-danger">*</span>
            </div>
            <div class="w-50 vcenter"> 
                <input type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" name="address" id="address">
        	</div>
		</div>
		<div class="inline-class m-t-10" >
            <div class="w-50 vcenter"> 
                Payment Terms  <span class="text-danger">*</span>
            </div>
            <div class="w-50 vcenter"> 
               <label class="fancy-radio custom-color-green">
                    <input name="pay_term_farmer" value="cash" type="radio" checked><span><i></i>
                    CASH</span>
                </label>
                <label class="fancy-radio custom-color-green">
                    <input name="pay_term_farmer" value="credit" type="radio" ><span><i></i>CREDIT</span>
                </label>
            </div>
        </div>
        <div class="inline-class  m-t-10" style="border-bottom: none;" >
            <div class="w-50 vcenter"> Attach Credit Document </div>
            <div class="w-50 vcenter"> 
                <input type="file" class="" name="sale_image" id="sale_image"> 
            </div>
        </div>
	</div>
	<div class="col-lg-6 col-md-12">
        <div class="inline-class" >
            <div class="w-50 vcenter"> 
                Retail Sales Invoice No<span class="text-danger">*</span>
            </div>
            <div class="w-50 vcenter"> 
                <input type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" readonly value="<?= $invoice_no ?? '' ?>">
            </div>
        </div>
        <div class="inline-class" >
            <div class="w-50 vcenter"> 
                Date/Time<span class="text-danger">*</span>
            </div>
            <div class="w-50 vcenter"> 
                <input data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="dd/mm/yyyy" value="<?= date( 'd-m-Y' )?>" readonly>
            </div>
        </div>
        
        <div class="inline-class" >
            <div class="w-50 vcenter"> 
                Commission Shop Reference <span class="text-danger">*</span>
            </div>
            <div class="w-50 vcenter"> 
                <div class="input-group">
                    <div class="commission_shop_ajax" style="width: 82%">
                      <select class="custom-select" id="commission_shop" name="commission_shop" onchange="return get_due_days( this.value )">
                        <option value="" selected>Select</option>
                       <?php
                        if( isset( $commission ) && !empty( $commission ) ) {
                          foreach ( $commission as $pay_key => $pay_value ) {
                            $selected         = '';
                            // if( isset( $purchase_info->mode_of_payment ) && $purchase_info->mode_of_payment == $pay_value->id ) {
                            //   $selected       = 'selected';
                            // }
                        ?>
                          <option value="<?= $pay_value->id ?>" <?= $selected ?>> <?= $pay_value->name ?></option>
                        <?php } } ?>
                      </select>
                    </div>
                    <div class="input-group-prepend" onclick="return add_form( 'commission_shop_ajax', 'sale' )">
                      <div class="input-group-text" id="btnGroupAddon">
                        <i class="fa fa-plus text-white"></i>
                      </div>
                    </div>
                  </div>
            </div>
        </div>
        <input type="hidden" name="customer_type" id="customer_type" value="<?= $customer_type ?>">
        <div class="inline-class pay-credit" >
            <div class="w-50 vcenter"> 
                Payment Due Date <span class="text-danger">*</span>
            </div>
            <div class="w-50 vcenter"> 
                <input data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="dd/mm/yyyy">
            </div>
        </div>

	</div>
</div>
	
<script type="text/javascript">
    $( '.pay-credit' ).hide();   
    $('input[type=radio][name=pay_term_farmer]').change(function() {
        if (this.value == 'credit') {
            $( '.pay-credit' ).show();
        }
        else if (this.value == 'cash') {
            $( '.pay-credit' ).hide();   
        }
    });
</script>