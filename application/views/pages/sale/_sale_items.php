<?php
  
  if( isset( $items )&& !empty( $items ) ) {
    $i = 1;
    foreach ( $items as $key => $value ) { ?>
      <tr>
        <td>
          <div class="input-group">
            <input type="text" name="code_<?= $total_item ?>" id="code_<?= $total_item ?>" value="<?= $value->product_code ?? '' ?>" class="form-control product_typeahead_<?= $total_item ?>" autocomplete="off">
            <div class="input-group-prepend" onclick="return open_add_product_form( '<?= $total_item ?>' );">
              <div class="input-group-text" id="btnGroupAddon">
                <i class="fa fa-plus text-white"></i>
              </div>
            </div>
          </div>
          <input type="hidden" name="variety_<?= $total_item ?>" id="variety_<?= $total_item ?>" value="<?= $value->product_category ?? '' ?>" class="form-control">
          <input type="hidden" name="desc_<?= $total_item ?>" id="desc_<?= $total_item ?>" value="  <?= $value->description ?>" class="form-control"> 

          <div class="variety_<?= $total_item ?> text-left">
            Category: <?= $value->product_category ?? '' ?>
          </div>
          <div class="desc_<?= $total_item ?> text-left">
            Description: <?= $value->product_category ?? '' ?>
          </div>
        </td>
        <td> 
          <input type="text" name="lot_<?= $i ?>" id="lot_<?= $i ?>" value="<?= $value->lot_no ?>" class="form-control" required> 
        </td>
        
        
        <td> 
          <input name="packed_date_<?= $i ?>" id="packed_date_<?= $i ?>" data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="dd/mm/yyyy" value="<?= isset( $value->date_of_packed ) ? date( 'd/m/Y', strtotime( $value->date_of_packed ) ) : '' ?>">
        </td>
        <td> 
          <input name="tested_date_<?= $i ?>" id="tested_date_<?= $i ?>" data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="dd/mm/yyyy" value="<?= isset( $value->date_of_tested ) ? date( 'd/m/Y', strtotime( $value->date_of_tested ) ) : '' ?>">
        </td>
        <td>
           <input name="expiry_date_<?= $i ?>" id="expiry_date_<?= $i ?>" data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="dd/mm/yyyy" value="<?= isset( $value->date_of_expiry ) ? date( 'd/m/Y', strtotime( $value->date_of_expiry ) ) : '' ?>">
        </td>
        <td> 
          <input type="text" name="rate_<?= $i ?>" id="rate_<?= $i ?>" value="<?= $value->net_rate ?>" class="form-control" onchange="return get_item_rate( this.value, '<?= $i ?>' )"> 
        </td>
        <td>
           <input type="text" name="qty_<?= $i ?>" id="qty_<?= $i ?>" value="<?= $value->qty ?? '' ?>"  class="form-control number-only" onchange="return get_item_rate( this.value, '<?= $i ?>' )" required> 
        </td>
        <td> 
          <input type="text" name="uom_<?= $i ?>" id="uom_<?= $i ?>" value="<?= $value->packaging ?>" class="form-control"> 
        </td>
         <td>
           <input type="text" name="discount_<?= $i ?>" id="discount_<?= $i ?>" value="<?= $value->discount ?? '' ?>"  class="form-control" onchange="return get_item_rate( this.value, '<?= $i ?>' )">
        </td>
        <td>
          <input type="text" name="net_value_<?= $i ?>" id="net_value_<?= $i ?>" value="<?= $value->total_amount  ?? '' ?>"  class="form-control item_rate" required >
          <input type="hidden" name="product_id_<?= $i ?>" id="product_id_<?= $i ?>"  value="<?= $value->product_id?>" >
          <input type="hidden" name="item_id" id="item_id" value="<?= $value->id ?>">
        </td>
        <td>
          <a href="#" class="btn btn-sm btn-outline-danger" onclick="deleteRow(this)">
            <i class="fa fa-trash"></i>
          </a>
        </td>
      </tr>
      
  <?php $i++; }

  } else {   ?>
    <tr>
      <td>
        <div class="input-group">
          <input type="text" name="code_<?= $total_item ?>" id="code_<?= $total_item ?>" value="" class="form-control product_typeahead_<?= $total_item ?>" autocomplete="off">
          <div class="input-group-prepend" onclick="return open_add_product_form( '<?= $total_item ?>' );">
            <div class="input-group-text" id="btnGroupAddon">
              <i class="fa fa-plus text-white"></i>
            </div>
          </div>
        </div>
        <input type="hidden" name="variety_<?= $total_item ?>" id="variety_<?= $total_item ?>" value="" class="form-control">
      
        <input type="hidden" name="desc_<?= $total_item ?>" id="desc_<?= $total_item ?>" value="" class="form-control"> 

        <div class="variety_<?= $total_item ?> text-left">
        </div>
        <div class="desc_<?= $total_item ?> text-left">
        </div>

      </td>
      <td> 
        <input type="text" name="lot_<?= $total_item ?>" id="lot_<?= $total_item ?>" value="" class="form-control" required> 
      </td>
      
      
      <td> 
        <input name="packed_date_<?= $total_item ?>" id="packed_date_<?= $total_item ?>" data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="dd/mm/yyyy">
      </td>
      <td> 
        <input name="tested_date_<?= $total_item ?>" id="tested_date_<?= $total_item ?>" data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="dd/mm/yyyy">
      </td>
      <td>
         <input name="expiry_date_<?= $total_item ?>" id="expiry_date_<?= $total_item ?>" data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="dd/mm/yyyy">
      </td>
      <td> 
        <input type="text" name="rate_<?= $total_item ?>" id="rate_<?= $total_item ?>" value="" class="form-control" onchange="return get_item_rate( this.value, '<?= $total_item ?>' )"> 
      </td>
      <td>
         <input type="text" name="qty_<?= $total_item ?>" id="qty_<?= $total_item ?>" value=""  class="form-control number-only" onchange="return get_item_rate( this.value, '<?= $total_item ?>', 'sale' )" required> 
      </td>
      <td> 
        <input type="text" name="uom_<?= $total_item ?>" id="uom_<?= $total_item ?>" value="" class="form-control"> 
      </td>
       <td>
         <input type="text" name="discount_<?= $total_item ?>" id="discount_<?= $total_item ?>" value="0"  class="form-control" onchange="return get_item_rate( this.value, '<?= $total_item ?>' )">
      </td>
      <td>
        <input type="text" name="net_value_<?= $total_item ?>" id="net_value_<?= $total_item ?>" value=""  class="form-control item_rate" required >
        <input type="hidden" name="product_id_<?= $total_item ?>" id="product_id_<?= $total_item ?>" >
      </td>
      <td>
        <a href="#" class="btn btn-sm btn-outline-danger" onclick="deleteRow(this)">
          <i class="fa fa-trash"></i>
        </a>
      </td>
    </tr>

<?php  }
?>



<script type="text/javascript">
  
  $(document).ready( function(){
    var total_item = '<?= $total_item ?>';
    $('input.product_typeahead_'+ total_item ).typeahead({ 
        source:  function (query, process) {
          var customer_type     = $( '#customer_type' ).val();
        return $.get( '<?=site_url( 'product/get_product_typeahead' );?>', { query: query, total_item:total_item, customer_type: customer_type }, function (data) {
          data = $.parseJSON( data );
          return process(data);
        });
      },
      updater:function ( item ) {
        
        if( item.product_category ) {
          $( '#variety_' + total_item ).val( item.product_category );
          $( '.variety_' + total_item ).html( 'Category : ' + item.product_category );
        }
        if( item.description ) {
          $( '#desc_' + total_item ).val( item.description );
          $( '.desc_' + total_item ).html( 'Desc : ' + item.description );
        }
        if( item.customer_type == 'dealer' ) {
          if( item.dealer_sale_price ) {
            $( '#rate_' + total_item ).val( item.dealer_sale_price );
            $( '#qty_'+ total_item ).val(1);
            get_item_rate( '', total_item );
          }
        } else {
          if( item.farmer_sale_price ) {
            $( '#rate_' + total_item ).val( item.farmer_sale_price );
            $( '#qty_'+ total_item ).val(1);
            get_item_rate( '', total_item );
          }
        }
        
        if( item.packaging ) {
          $( '#uom_' + total_item ).val( item.packaging );
        }
        if( item.id ) {
          $( '#product_id_'+total_item ).val( item.id );
          get_product_instock( item.id, total_item );
        }
        return item;
      }
    });
  });

  function get_product_instock( product_id, count, qty_check = '' ) {
  var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&product_id=' + product_id + '&count=' +count;
    $.ajax( {
        type    : "POST",
        url     : '<?= site_url( 'product/get_product_instock' ); ?>',
        data    : data,
        dataType: 'json',
        success: function( msg ) {
          
          if( msg.balance_qty == 0 ) {
            toastr.error( 'product has no stock, please purchase to sale', 'Sale' );
            $( '#qty_'+count ).val( '' );
            return false;
          }

          if( msg.balance_qty ) {
            $( '#qty_'+count ).val( msg.balance_qty );
          }
        }
    });
    return false;
  }
    
</script>