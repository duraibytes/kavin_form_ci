<select class="custom-select" id="commission_shop" name="commission_shop" onchange="return get_due_days( this.value )">
  <option value="" selected>Select</option>
  <?php
    if( isset( $commission ) && !empty( $commission ) ) {
      foreach ( $commission as $pay_key => $pay_value ) {
        $selected         = '';
        // if( isset( $purchase_info->mode_of_payment ) && $purchase_info->mode_of_payment == $pay_value->id ) {
        //   $selected       = 'selected';
        // }
  ?>
    <option value="<?= $pay_value->id ?>" <?= $selected ?>> <?= $pay_value->name ?></option>
  <?php } } ?>
</select>