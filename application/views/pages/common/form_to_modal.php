<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
       <?php echo form_open_multipart( '', 'id="product_form"' ); ?>
        <div class="modal-header">

            <h4 class="title" id="defaultModalLabel"><?= $title ?> </h4>
            
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-12">
                    <?= $this->load->view( 'errors/show_error', '', true  )?>
                </div>
            </div>
            <div class="row">
                <?= $this->load->view( 'pages/product/form/_left-form-field', '', true ) ?>
                <!-- <?= $this->load->view( 'pages/product/form/_right-form-field', '', true ) ?> -->
                <?= $this->load->view( 'pages/product/form/_product-price-form', '', true ) ?>
            </div>
            
        </div>
        <div class="modal-footer">
            <input type="hidden" name="count" id="count" value="<?= $count ?>">
            <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
            <button onClick="return insert_product()" class="btn btn-primary" type="button"> Save</button> 
        </div>
        <?=form_close();?>
    </div>
</div>

<script type="text/javascript">

    function insert_product() {
        var count                   = '<?= $count ?>';
        var product_form            = $( '#product_form' ).find('input, textarea, button, select');
        var data      = new FormData( $( '#product_form' )[0]);
        $.ajax({
            type        : 'POST',
            url         : '<?=site_url( 'product/insert' );?>',
            data        : data,
            async       : false,
            contentType : false,
            processData : false,
            dataType    : 'json',
            beforeSend: function () {
                product_form.attr( 'disabled', 'disabled' );
            },    
            success     : function( msg ) {
                if( msg.error == 1 ) {
                    toastr.error( msg.error_msg, 'Product' );
                    product_form.attr( 'disabled', false );
                } else {
                    toastr.success( msg.error_msg, 'Product' );
                    $( '#defaultModal' ).modal( 'hide' );
                    if( msg.product_category ) {
                        $( '#variety_' + count ).val( msg.product_category );
                        $( '.variety_' + count ).html( 'Category : ' + msg.product_category );
                    }
                  if( msg.description ) {
                    $( '#desc_' + count ).val( msg.description );
                    $( '.desc_' + count ).html( 'Desc : ' + msg.description );
                  }
                  if( msg.net_purchase_price ) {
                    $( '#rate_' + count ).val( msg.net_purchase_price );
                    $( '#qty_'+ count ).val(1);
                    get_item_rate( '', count );
                  }
                  if( msg.packaging ) {
                    $( '#uom_' + count ).val( msg.packaging );
                  }
                  if( msg.id ) {
                    $( '#product_id_'+count ).val( msg.id );
                  }
                  if( msg.product_code ) {
                    $( '#code_'+ count ).val( msg.product_code );
                  }
                }
            }
        });
        return false;
    }

</script>