<div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
        <?=form_open( '', 'id="common-form"');?>
        <div class="modal-header">
            <h4 class="title" id="defaultModalLabel"><?= $title ?? '' ?> </h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-12">
                    <?= $this->load->view( 'errors/show_error', '', true  )?>
                </div>
            </div>
            <?= $this->load->view( 'pages/dealer/form/_farmer_form', '', true  )?>
        </div>
        <div class="modal-footer">
            <input type="hidden" name="ajax_type" id="ajax_type" value="<?= $ajax_type ?? '' ?>">

            <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
            <button onClick="return insert_form()" class="btn btn-primary" type="button"> Save</button> 
        </div>
        <?=form_close();?>
    </div>
</div>

<script type="text/javascript">

    function insert_form() {
        var form_name           = $( '#common-form' ).find( 'input, select, button, textarea' );
        var codes_form_data     = form_name.serialize();
        codes_form_data         = codes_form_data + '&<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>';
        $.ajax({
            type        : 'POST',
            url         : '<?=site_url( 'customer/insert_customer' );?>',
            data        : codes_form_data,
            async       : false,
            dataType    : 'json',
            beforeSend: function () {
                form_name.attr( 'disabled', 'disabled' );
            },    
            success     : function( msg ) {
                if( msg.error == 1 ) {
                    toastr.error( msg.error_msg, 'Form' );
                    form_name.attr( 'disabled', false );
                } else {
                    toastr.success( msg.error_msg, 'Form' );
                    $( '#defaultModal' ).modal( 'hide' );
                    if( msg.view ) {
                        $( '.farmer_form_ajax' ).html( msg.view );    
                    }
                }
            }
        });
        return false;
    }

</script>