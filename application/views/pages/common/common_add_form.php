<div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
        <?=form_open( '', 'id="common-form"');?>
        <div class="modal-header">

            <h4 class="title" id="defaultModalLabel"><?= $title ?> </h4>
            
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-12">
                    <?= $this->load->view( 'errors/show_error', '', true  )?>
                </div>
            </div>
            <div class="form-group">
                <label for="code"> <?= $label ?> </label>
                <input class="form-control" placeholder="Enter value.." type="text" name="form_value" id="form_value" value="" />
            </div>
        </div>
        <div class="modal-footer">
            <input type="hidden" name="module" id="module" value="<?= $module ?? '' ?>">
            <input type="hidden" name="parent_id" id="parent_id" value="<?= $parent_id ?>">
            <input type="hidden" name="id" id="id" value="<?= $id ?>">
            <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
            <button onClick="return insert_form()" class="btn btn-primary" type="button"> Save</button> 
        </div>
        <?=form_close();?>
    </div>
</div>

<script type="text/javascript">

    function insert_form() {
        var form_name           = $( '#common-form' ).find( 'input, select, button, textarea' );
        var codes_form_data     = form_name.serialize();
        codes_form_data         = codes_form_data + '&<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>';
        $.ajax({
            type        : 'POST',
            url         : '<?=site_url( 'settings/insert_form' );?>',
            data        : codes_form_data,
            async       : false,
            dataType    : 'json',
            beforeSend: function () {
                form_name.attr( 'disabled', 'disabled' );
               
            },    
            success     : function( msg ) {
                if( msg.error == 1 ) {
                    toastr.error( msg.error_msg, 'Form' );
                    form_name.attr( 'disabled', false );
                } else {
                    toastr.success( msg.error_msg, 'Form' );
                    $( '#defaultModal' ).modal( 'hide' );
                    if( msg.id && msg.view ) {
                        $( '.'+msg.id ).html( msg.view );    
                    }
                    }
            }
        });
        return false;
    }

</script>