<select class="custom-select" id="" name="mode_of_transport">
  <option selected>Select</option>
 <?php
  if( isset( $transport_all ) && !empty( $transport_all ) ) {
    foreach ( $transport_all as $tran_key => $tran_value ) {
      $selected         = '';
      if( isset( $purchase_info->transport_id ) && $purchase_info->transport_id == $tran_value->id ) {
        $selected       = 'selected';
      }
  ?>
     <option value="<?= $tran_value->id ?>" <?= $selected ?>> <?= $tran_value->name ?></option>
  <?php } } ?>
</select>
<script type="text/javascript">
  $(function () {
    $( '.custom-select' ).select2();
  });
</script>