<select class="custom-select" id="mode_of_payment" name="mode_of_payment" onchange="return get_due_days( this.value )">
    <option selected>Select</option>
   <?php
    if( isset( $payment ) && !empty( $payment ) ) {
      foreach ( $payment as $pay_key => $pay_value ) {
        $selected = '';
    ?>
        <option value="<?= $pay_value->id ?>" <?= $selected ?>> 
          <?= $pay_value->name ?>
        </option>
    <?php } } ?>
  </select>

<script type="text/javascript">
  $(function () {
    $( '.custom-select' ).select2();
  });
</script>