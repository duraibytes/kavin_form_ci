
 <div class="block-header">
    <?= $this->load->view( 'layout/_nav_header', '', true )?>
</div>
<?php
    
    if( isset( $customer_info->type ) && !empty( $customer_info->type ) ) {

?>
<script type="text/javascript">
    setTimeout(function(){
        get_type_field( '<?= $customer_info->type ?>' );
        address_list( '<?= $id ?>' );
    }, 200);
    
</script>
<?php } ?>
<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                                                                                                        
                    <?php echo form_open_multipart( '', 'id="customer_form"' ); ?>
                    <div class="demo-masked-input">
                        <div class="row clearfix">
                             <div class="col-lg-12 col-md-12">
                                <div class="text-center hd-font m-b-10 m-t-10"> 
                                    <span class="btn btn-sm inventory-bg text-white">
                                       ADD DEALER / FARMER
                                    </span> 
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                
                                <div class="inline-class" >
                                    <div class="w-50 vcenter"> 
                                        Customer Type
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <select class="form-control" name="customer_type" id="customer_type" onchange="return get_type_field( this.value )" >
                                            <option value=""> -- select type -- </option>
                                            <option value="dealer" <?= ( isset( $customer_info->type ) && $customer_info->type == 'dealer' ) ? 'selected' : '' ?>> Dealer </option>
                                            <option value="farmer" <?= ( isset( $customer_info->type ) && $customer_info->type == 'farmer' ) ? 'selected' : '' ?>> Farmer </option>
                                        </select> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="inline-class" style="border-bottom: none;" >
                                    <div class="w-50 vcenter"> Upload Image 
                                        <?php 
                                        if( isset( $customer_info->image ) && !empty( $customer_info->image ) ) {
                                        ?>
                                            <img src="<?= $customer_info->image?>" style="width: 75px;margin-left: 10px;"><
                                        <?php } ?>
                                    </div>
                                    <div class="w-50 vcenter"> <input type="file" name="customer_image" class="dropify"> </div>
                                </div>
                                
                            </div>
                            <?= $this->load->view( 'pages/dealer/form/_dealer_form', '', true ) ?>
                            <?= $this->load->view( 'pages/dealer/form/_farmer_form', '', true ) ?>
                            <!-- address and bank info -->
                            <div class="col-lg-6 col-md-6 dealer_type"> 
                                <div class="inline-class-head m-t-30" style="background: aliceblue;">
                                    <div class="w-100" >
                                        <span class="Hfont text-muted"> Address</span>
                                        <span class="float-right"><a href="javascript:;" onclick="return get_address_form( '', 'customer_address' )"> + add new </a></span>
                                    </div>
                                </div>
                                <div id="customer_address">
                                    <?= $this->load->view( 'pages/company/form/_form_address_info', '', true ) ?>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 dealer_type">
                                <div class="inline-class-head m-t-30" style="background: aliceblue;">
                                    <div class="w-100" >
                                        <span class="Hfont text-muted"> Bank Info </span>
                                        <span class="float-right"><a href="javascript:;" onclick="return get_bank_form()"> + add new </a></span>
                                    </div>
                                </div>
                                <div id="bank_info">
                                    <?= $this->load->view( 'pages/dealer/form/_form_bank', '', true ) ?>    
                                </div>
                            </div>
                        </div>
                        
                       <div class="row clearfix">
                            <div class="col-sm-12">
                                <hr>
                            </div>
                            <input type="hidden" name="id" value="<?= $id ?>">
                            <div class="col-sm-12 text-right">
                                <a href="<?= base_url()?>customer" class="btn btn-sm btn-outline-danger"> Cancel </a>
                                <?php
                                    if( !$customer_info ) {

                                ?>
                                <a href="javascript:;" class="btn btn-sm btn-outline-dark" onclick="return clear_form( 'customer_form' )"> Clear </a>
                            <?php } ?>
                                <a href="javascript:;" class="btn btn-sm btn-success" onclick="return insert_customer()"> <?= $title ?> </a>
                            </div>
                        </div>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php 
    if( isset( $customer_info->type ) && $customer_info->type == 'farmer' ) { ?>
        <script type="text/javascript">
            setTimeout( function(){
                get_type_field( 'farmer' );
            }, 300 );
        </script>
    <?php } else if( isset( $customer_info->type ) && $customer_info->type == 'dealer' ) { ?>
        <script type="text/javascript">
            setTimeout( function(){
                get_type_field( 'dealer' );
            }, 300 );
        </script>
    <?php }

?>

<script type="text/javascript">
    $( '.dealer_type' ).hide();
    $( '.farmer_type' ).hide();

    function get_type_field( type ) {
        if( type == 'dealer' ) {
            $( '.dealer_type' ).show();
            $( '.farmer_type' ).hide();
        } else if( type == 'farmer' ) {
            $( '.dealer_type' ).hide();
            $( '.farmer_type' ).show();
        } else {
            $( '.dealer_type' ).hide();
            $( '.farmer_type' ).hide();
        }
    }

    function insert_customer() {
        var customer_form           = $( '#customer_form' ).find('input, textarea, button, select');
        var data      = new FormData( $( '#customer_form' )[0]);
        $.ajax({
            type        : 'POST',
            url         : '<?=site_url( 'customer/insert_customer' );?>',
            data        : data,
            async       : false,
            contentType : false,
            processData : false,
            dataType    : 'json',
            beforeSend: function () {
                customer_form.attr( 'disabled', 'disabled' );
            },    
            success     : function( msg ) {
                if( msg.error == 1 ) {
                    toastr.error( msg.error_msg, 'Customer' );
                    customer_form.attr( 'disabled', false );
                } else {
                    toastr.success( msg.error_msg, 'Customer' );
                    setTimeout( function(){
                        window.location.href = '<?= site_url()?>customer';
                    }, 300 );
                }
            }
        });
        return false;
    }

    function address_list( customer_id = '' ) {
        var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&customer_id=' + customer_id;
        $.ajax( {
            type    : "POST",
            url     : '<?= site_url( 'customer/address_list' ); ?>',
            data    : data,
            success: function( msg ) {
                $( '#customer_address' ).html( msg );
            }
        });
        return false;
    }

     function delete_address( key, session_name ) {

          var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&key=' + key +'&session_name=' + session_name;
          $.ajax( {
              type    : "POST",
              url     : '<?= site_url( 'settings/delete_address' ); ?>',
              data    : data,
              success: function( msg ) {
                  toastr.success( 'Address deleted Successfully', 'Address' );
                  address_list();
              }
          });
          return false;
      }
</script>