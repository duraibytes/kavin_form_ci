<?php 
    if( isset( $ajax_type ) && !empty( $ajax_type ) ) { ?>
<div class="col-lg-12 col-md-12 farmer_type">
    <input type="hidden" name="customer_type" id="customer_type" value="farmer">
<?php } else { ?>
<div class="col-lg-6 col-md-6 farmer_type">
<?php    }
?>

    
    <div class="inline-class" >
        <div class="w-50 vcenter"> 
            Farmer Name <span class="text-danger">*</span>
        </div>
        <div class="w-50 vcenter"> 
            <input type="text" name="farmer_name" class="form-control" value="<?= $customer_info->fname ?? ''?>"> 
        </div>
    </div>
    <div class="inline-class" >
        <div class="w-50 vcenter"> 
           Farmer Mobile <span class="text-danger">*</span>
        </div>
        <div class="w-50 vcenter"> 
            <input type="text" name="farmer_mobile" id="farmer_mobile" class="form-control" value="<?= $customer_info->mobile_no ?? ''?>"> 
        </div>
    </div>
    <div class="inline-class" >
        <div class="w-50 vcenter"> 
           Altermate Mobile
        </div>
        <div class="w-50 vcenter"> 
            <input type="text" name="alter_mobile" id="alter_mobile" class="form-control" value="<?= $customer_info->alter_mobile_no ?? ''?>"> 
        </div>
    </div>
    <div class="inline-class" >
        <div class="w-50 vcenter"> Farmer Email </div>
        <div class="w-50 vcenter"> 
            <input type="text" name="farmer_email" id="farmer_email" class="form-control" value="<?= $customer_info->email ?? ''?>"> 
        </div>
    </div>
    <div class="inline-class" >
        <div class="w-50 vcenter"> Address </div>
        <div class="w-50 vcenter"> 
            <textarea class="form-control" name="farmer_address" id="farmer_address"><?= $customer_info->address ?? ''?></textarea>
        </div>
    </div>
    <div class="inline-class" >
        <div class="w-50 vcenter"> City / State </div>
        <div class="w-50 vcenter"> 
            <input type="text" name="farmer_state" id="farmer_state" class="form-control" value="<?= $customer_info->state ?? ''?>"> 
        </div>
    </div>
    <div class="inline-class" >
        <div class="w-50 vcenter"> ZipCode </div>
        <div class="w-50 vcenter"> 
            <input type="text" name="zip_code" id="zip_code" class="form-control" value="<?= $customer_info->zipcode ?? ''?>"> 
        </div>
    </div>
</div>
