<div class="col-lg-6 col-md-6 dealer_type">
    <div class="inline-class" >
        <div class="w-50 vcenter"> 
            Dealer Name <span class="text-danger">*</span>
        </div>
        <div class="w-50 vcenter"> 
            <input type="text" name="dealer_name" class="form-control" value="<?= $customer_info->fname ?? '' ?>"> 
        </div>
    </div>
    <div class="inline-class-head" style="background: aliceblue;">
        <div class="w-100" >
            <span class="Hfont text-muted"> Dealer POC </span>
        </div>
    </div>
    <div class="inline-class" >
        <div class="w-50 vcenter"> 
            Proprietor Name  <span class="text-danger">*</span>
        </div>
        <div class="w-50 vcenter"> 
            <input type="text" name="proprietor_name" class="form-control" value="<?= $customer_info->proprietor_name ?? '' ?>"> 
        </div>
    </div>
    <div class="inline-class" >
        <div class="w-50 vcenter"> 
           Proprietor Mobile <span class="text-danger">*</span>
        </div>
        <div class="w-50 vcenter"> 
            <input type="text" name="proprietor_mobile" id="proprietor_mobile" class="form-control" value="<?= $customer_info->proprietor_mobile ?? '' ?>"> 
        </div>
    </div>
    <div class="inline-class" >
        <div class="w-50 vcenter"> Proprietor Email </div>
        <div class="w-50 vcenter"> 
            <input type="text" name="proprietor_email" id="proprietor_email" class="form-control" value="<?= $customer_info->proprietor_email ?? '' ?>"> 
        </div>
    </div>
</div>
<div class="col-lg-6 col-md-6 dealer_type">
    <div class="inline-class" >
        <div class="w-50 vcenter"> 
            Dealer Seed License  
        </div>
        <div class="w-50 vcenter"> 
            <input type="text" name="license" class="form-control" value="<?= $customer_info->seed_license ?? '' ?>"> 
        </div>
    </div>
    <div class="inline-class" >
        <div class="w-50 vcenter"> 
           Dealer Pesticides License
        </div>
        <div class="w-50 vcenter"> 
            <input type="text" name="pesticides_license" id="pesticides_license" class="form-control" value="<?= $customer_info->pesticides_license ?? '' ?>"> 
        </div>
    </div>
    <div class="inline-class" >
        <div class="w-50 vcenter"> Dealer Fertilizer License </div>
        <div class="w-50 vcenter"> 
            <input type="text" name="fertilizer_license" id="fertilizer_license" class="form-control" value="<?= $customer_info->fertilizer_license ?? '' ?>"> 
        </div>
    </div>
    <div class="inline-class" >
        <div class="w-50 vcenter"> Dealer GST </div>
        <div class="w-50 vcenter"> 
            <input type="text" name="gst" id="gst" class="form-control" value="<?= $customer_info->gst ?? '' ?>"> 
        </div>
    </div>
    <div class="inline-class" >
        <div class="w-50 vcenter"> Dealer CST </div>
        <div class="w-50 vcenter"> 
            <input type="text" name="cst" id="cst" class="form-control" value="<?= $customer_info->cst ?? '' ?>"> 
        </div>
    </div>
    <div class="inline-class" >
        <div class="w-50 vcenter"> Dealer VAT </div>
        <div class="w-50 vcenter"> 
            <input type="text" name="vat" id="vat" class="form-control" value="<?= $customer_info->vat ?? '' ?>"> 
        </div>
    </div>
</div>