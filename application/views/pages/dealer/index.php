
<div class="block-header m-t-20">
    <?= $this->load->view( 'layout/_nav_header', '', true )?>
</div>
<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
               
                <div class="body">
                    <div class="table-responsive">
                        <div class="text-center hd-font m-b-10 m-t-10"> 
                            <span class="btn btn-sm inventory-bg text-white">
                                Dealer / Farmer - Management
                            </span> 
                        </div>
                        <table class="table table-bordered table-hover js-basic-example dataTable table-custom">
                            <thead>
                                <tr class="inventory-bg text-white">
                                    <th> Customer Info </th>
                                    <th> Email </th>
                                    <th> Mobile </th>
                                    <th> Address </th>
                                    <th> Action </th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                <?php
                                if( isset( $customer_info ) && !empty( $customer_info ) ) {
                                    foreach ( $customer_info as $key => $value ) {
                                       $address    = $this->Common_model->get_common_address( $value->id, 'customer' );
                                    
                                ?>
                                
                                <tr>
                                    
                                    <td>
                                       
                                        <?= strtoupper( $value->fname ) ?>
                                        <span class="badge badge-warning"> <?= ucfirst( $value->type ) ?> </span>
                                    </td>
                                    
                                    <td><?= $value->email ?? '' ?><?= $value->proprietor_email ?? '' ?></td>
                                    <td><?= $value->mobile_no ?? ( $value->proprietor_mobile ?? '' ) ?></td>
                                    <td><?= $value->address ?? ( $address->address ?? '' )?></td>
                                    <td>
                                       <a href="<?= base_url() ?>customer/add/<?= $value->id ?>" class="btn btn-outline-warning ">
                                            <i class="icon-pencil"></i>
                                        </a>
                                        <a href="javascript:;" class="btn btn-danger btn-sm" onClick="return are_you_sure( 'delete_customer', 'Delete customer', 'Are you sure want to delete this customer?', '<?= $value->id?>' );"> 
                                            <i class="icon-trash"></i> 
                                        </a>
                                    </td>
                                    
                                </tr>
                                <?php 
                                    } 
                                } 
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
    </div>
    
</div>

<script type="text/javascript">
    function delete_customer( id ) {
        var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&id=' + id;
        $.ajax( {
            type    : "POST",
            url     : '<?=site_url( 'customer/delete_customer' );?>',
            data    : data,
            success: function( msg ) {
                toastr.success( 'Customer deleted successfully', 'Customer' );
                setTimeout( function() {
                    window.location.href = '<?= site_url()?>customer';
                }, 300 );
            }
        });
        return false;
    }
</script>
