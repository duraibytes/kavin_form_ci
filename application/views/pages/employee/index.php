<div class="block-header">
      <?= $this->load->view( 'layout/_nav_header', '', true )?>
</div>
<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                
                <div class="body">
                    <div class="table-responsive">
                         <div class="text-center hd-font"> 
                            <span class="btn btn-sm inventory-bg text-white">
                                EMPLOYEE MANAGEMENT
                            </span> 
                        </div>
                        <table class="table table-bordered table-hover js-basic-example dataTable table-custom">
                            <thead>
                                <tr class="inventory-bg text-white">
                                    <th> First Name </th>
                                    <th> Last Name</th>
                                    <th> DOB </th>
                                    <th> GENDER </th>
                                    <th> SALARY </th>
                                    <th> Employee Mobile</th>
                                    <th> Employee Address</th>
                                    <th> Employee Email </th>
                                    <th> Date of Joining </th>
                                    <th> Action </th>
                                    
                                </tr>
                            </thead>
                            
                            <tbody>
                            <?php 
                            if( isset( $employee_all ) && !empty( $employee_all ) ) {
                                foreach ( $employee_all as $key => $value ) {
                                // print_r( $value );
                            ?>
                                <tr>
                                    <td> <?= $value->first_name ?> </td>
                                    <td>
                                        <?= $value->last_name ?>
                                    </td>
                                    <td> <?= date( 'd-m-Y', strtotime( $value->dob ) ) ?> </td>
                                    <td> <?= ucfirst( $value->gender ) ?> </td>
                                    <td> Rs. <?= $value->salary_per_month ?> </td>
                                    <td> <?= $value->mobile ?> </td>
                                    <td> <?= $value->address ?> </td>
                                    <td> <?= $value->email ?> </td>
                                    <td> <?= date( 'd-m-Y', strtotime( $value->date_of_joining ) ) ?> </td>
                                    <td>
                                        <a href="<?= base_url() ?>employee/add/<?= $value->slug?>" class="btn btn-outline-warning">
                                            <i class="icon-pencil"></i>
                                        </a>
                                        <a href="javascript:;" class="btn btn-danger btn-sm" onClick="return are_you_sure( 'delete_employee', 'Delete Employee', 'Are you sure want to delete this employee?', '<?= $value->id?>' );"> <i class="icon-trash"></i> </a>
                                    </td>
                                    
                                </tr>
                                <?php } } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
    </div>
    
</div>
<script type="text/javascript">
    function delete_employee( id ) {
        var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&id=' + id;
        $.ajax( {
            type    : "POST",
            url     : '<?= site_url( 'employee/delete_employee' ); ?>',
            data    : data,
            dataType: 'json',
            success: function( msg ) {
                toastr.success( 'Employee deleted successfully', 'Employee' );
                setTimeout( function(){
                    window.location.href = '<?= site_url()?>employee';
                }, 300 );
            }
        });
        return false;
      }
</script>