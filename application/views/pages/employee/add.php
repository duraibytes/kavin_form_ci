
 <div class="block-header">
    <?= $this->load->view( 'layout/_nav_header', '', true )?>
</div>
<style type="text/css">
    .dropify-wrapper {
        height: 150px !important;
    }
</style>
<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                    <?php echo form_open_multipart( '', 'id="employee_form"' ); ?>
                    <div class="demo-masked-input">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12">
                                <div class="text-center hd-font m-b-20"> 
                                    <span class="btn btn-sm inventory-bg text-white">
                                       ADD EMPLOYEE
                                    </span> 
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                 <div class="inline-class-head" style="background: aliceblue;">
                                    <div class="w-100">
                                        <span class="Hfont text-muted"> Personal Info </span>
                                    </div>
                                </div>
                                
                                <div class="inline-class" >
                                    <div class="w-50 vcenter"> 
                                        First Name <span class="text-danger">*</span>
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <input type="text" name="first_name" id="first_name" class="form-control" value="<?= $employee_info->first_name ?? '' ?>"> 
                                    </div>
                                </div>
                                <div class="inline-class" >
                                    <div class="w-50 vcenter"> 
                                        Last Name <span class="text-danger">*</span>
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <input type="text" name="last_name" id="last_name" class="form-control" value="<?= $employee_info->last_name ?? '' ?>"> 
                                    </div>
                                </div>
                                <div class="inline-class" >
                                    <div class="w-50 vcenter"> 
                                        DOB <span class="text-danger">*</span>
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <input name="dob" data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="dd-mm-yyyy" value="<?= ( ( isset( $employee_info->dob ) && !empty( $employee_info->dob ) ) ? date( 'd-m-Y', strtotime( $employee_info->dob ) )  :'' ) ?>">
                                    </div>
                                </div>
                                <div class="inline-class" >
                                    <div class="w-50 vcenter"> 
                                        Gender <span class="text-danger">*</span>
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <label class="fancy-radio custom-color-green">
                                            <input name="gender" value="male" type="radio" <?php if( isset( $employee_info->gender ) && $employee_info->gender == 'male' ) { echo 'checked';} ?>>
                                            <span><i></i>Male</span>
                                        </label>
                                        <label class="fancy-radio custom-color-green">
                                            <input name="gender" value="female" type="radio" <?php if( isset( $employee_info->gender ) && $employee_info->gender == 'female' ) { echo 'checked';}?>>
                                            <span><i></i>Female</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="inline-class" >
                                    <div class="w-50 vcenter"> 
                                        Mobile Number <span class="text-danger">*</span>
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <input type="text" name="mobile_no" id="mobile_no" class="form-control" value="<?= $employee_info->mobile ?? '' ?>"> 
                                    </div>
                                </div>
                                <div class="inline-class" >
                                    <div class="w-50 vcenter"> 
                                        Email <span class="text-danger">*</span>
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <input type="text" name="email" id="email" class="form-control" value="<?= $employee_info->email ?? '' ?>"> 
                                    </div>
                                </div>
                                
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="inline-class" >
                                    <div class="w-50 vcenter"> 
                                        Date Of Joining <span class="text-danger">*</span>
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <input type="text" name="date_of_joining" data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="dd-mm-yyyy" value="<?= ( ( isset( $employee_info->date_of_joining ) && !empty( $employee_info->date_of_joining ) ) ? date( 'd-m-Y', strtotime( $employee_info->date_of_joining ) )  :'' ) ?>">
                                    </div>
                                </div>
                                <div class="inline-class" >
                                    <div class="w-50 vcenter"> 
                                        Salary per month  <span class="text-danger">*</span>
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <input type="text" name="salary" id="salary" class="form-control" value="<?= $employee_info->salary_per_month ?? '' ?>"> 
                                    </div>
                                </div>
                                <div class="inline-class" >
                                    <div class="w-50 vcenter"> 
                                        Benefits
                                    </div>
                                    <div class="w-50 vcenter"> 
                                       <input type="text" name="benefits" id="benefits" class="form-control" value="<?= $employee_info->benefits ?? '' ?>"> 
                                    </div>
                                </div>
                               
                                <div class="inline-class" style="background: #ffc107">
                                    <div class="w-50 vcenter"> 
                                        <label for="is_login_credential" style="margin-bottom: 0px;font-weight: bold;">Is User Login Credential </label>
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <input type="checkbox" name="is_login_credential" id="is_login_credential" <?php if( isset( $employee_info->user_name ) && !empty( $employee_info->user_name ) ) { echo 'checked'; } ?> >
                                    </div>
                                </div>

                                <div class="inline-class user_login" >
                                    <div class="w-50 vcenter"> 
                                        User Name
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <input type="text" class="form-control" name="user_name" id="user_name" placeholder="" value="<?= $employee_info->user_name ?? '' ?>">
                                    </div>
                                </div>
                                <div class="inline-class user_login" >
                                    <div class="w-50 vcenter"> 
                                        Password
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <?php
                                        $Password           = '';
                                        $readonly           = '';
                                        if( isset( $employee_info->password ) && !empty( $employee_info ->password ) ) {
                                            $Password       = $this->encryption->decrypt( $employee_info->password );
                                            $readonly       = 'readonly';
                                        }
                                        ?>
                                        <input type="text" class="form-control" name="user_password" id="user_password" placeholder="" value="<?= $Password ?>" <?= $readonly ?>>
                                    </div>
                                </div>
                                <div class="inline-class user_login" >
                                    <div class="w-50 vcenter"> 
                                        User Type
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <select class="form-control" name="user_type" id="user_type">
                                            <option value=""> -- Select user type -- </option>
                                            <?php
                                            if( isset( $user_type ) && !empty( $user_type ) ) {
                                                foreach ( $user_type as $ukey => $uvalue ) {
                                                    $selected               = '';
                                                    if( isset( $employee_info->user_type_id ) && !empty( $employee_info->user_type_id ) && $employee_info->user_type_id == $uvalue->id  ) {
                                                        $selected           = 'selected';
                                                    }
                                                
                                            ?>
                                            <option value="<?= $uvalue->id?>" <?= $selected ?>> <?= $uvalue->name ?> </option>
                                        <?php } } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 m-t-10">
                                <div class="inline-class" style="border-bottom: none;" >
                                    <div class="w-50 vcenter"> Profile image
                                        <?php
                                        if( isset( $employee_info->profile_photo ) && !empty( $employee_info->profile_photo ) ) {
                                        ?>
                                        <img src="<?= $employee_info->profile_photo?>" style="width:75px;">
                                        <?php } ?>
                                    </div>
                                    <div class="w-50 vcenter"> <input type="file" name="profile_image" class="dropify" > </div>
                                </div>
                                
                            </div>
                            <div class="col-lg-4 col-md-4  m-t-10">
                                <div class="inline-class" style="border-bottom: none;" >
                                    <div class="w-50 vcenter"> Adhaar / Driving License Image 
                                    <?php
                                        if( isset( $employee_info->proof_image ) && !empty( $employee_info->proof_image ) ) {
                                        ?>
                                        <img src="<?= $employee_info->proof_image?>" style="width:75px;">
                                        <?php } ?></div>
                                    <div class="w-50 vcenter"> <input type="file" name="proof" class="dropify"  > </div>
                                </div>
                                 
                            </div>
                            <div class="col-lg-4 col-md-4  m-t-10">
                                <div class="inline-class" style="border-bottom: none;" >
                                    <div class="w-50 vcenter"> Resume 
                                    <?php
                                        if( isset( $employee_info->resume ) && !empty( $employee_info->resume ) ) {
                                        ?>
                                        <a href="<?= $employee_info->resume ?>" target="_blank">View File </a>
                                        <?php } ?></div>
                                    <div class="w-50 vcenter"> <input type="file" name="resume" class=""> </div>
                                    
                                </div>
                            </div>
                            
                            <!-- address and bank info -->
                            <div class="col-lg-6 col-md-6">
                                <div class="inline-class-head m-t-30" style="background: aliceblue;">
                                    <div class="w-100">
                                        <span class="Hfont text-muted"> Address</span>
                                    </div>
                                </div>
                                <div class="inline-class" >
                                    <div class="w-50 vcenter"> 
                                        Address
                                    </div>
                                    <div class="w-50 vcenter"> 
                                        <input type="text" name="address" id="address" class="form-control" value="<?= $employee_info->address ?? '' ?>"> 
                                    </div>
                                </div>
                                <div class="inline-class no-border" >
                                    <div class="" style="display: flex;width: 50%">
                                        <div class="vcenter" style="width: 53%;"> City 
                                        </div>
                                        <div class="w-80 vcenter"> 
                                            <input type="text" name="city" id="city" class="form-control" value="<?= $employee_info->city ?? '' ?>"> 
                                        </div>
                                    </div>
                                    <div class="" style="display: flex;width: 50%">
                                        <div class="w-20 vcenter" style="padding-left: 5px;"> State </div>
                                        <div class="w-80 vcenter"> 
                                            <input type="text" name="state" id="state" class="form-control" value="<?= $employee_info->state ?? '' ?>"> 
                                        </div>
                                    </div>
                                </div>
                                <div class="inline-class no-border" >
                                    <div class="" style="display: flex;width: 50%">
                                        <div class="vcenter" style="width: 53%;"> Zipcode 
                                        </div>
                                        <div class="w-80 vcenter"> 
                                            <input type="text" name="zipcode" id="zipcode" class="form-control" value="<?= $employee_info->zip_code ?? '' ?>"> 
                                        </div>
                                    </div>
                                    <div class="" style="display: flex;width: 50%">
                                        <div class="w-20 vcenter" style="padding-left: 5px;"> Country </div>
                                        <div class="w-80 vcenter"> 
                                            <input type="text" name="country" id="country" class="form-control" value="<?= $employee_info->country ?? '' ?>"> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="inline-class-head m-t-30" style="background: aliceblue;">
                                    <div class="w-100">
                                        <span class="Hfont text-muted"> Bank Info </span>
                                        <span class="float-right"><a href="javascript:;"> + add new </a></span>
                                    </div>
                                </div>
                                <?= $this->load->view( 'pages/employee/form/_form_bank', '', true ) ?>
                            </div>
                        </div>
                        
                       <div class="row clearfix">
                            <div class="col-sm-12">
                                <hr>
                            </div>
                            <input type="hidden" name="id" value="<?= $employee_info->id ?? '' ?>">
                            <div class="col-sm-12 text-right">
                                <a href="<?= base_url()?>employee" class="btn btn-sm btn-outline-danger"> Cancel </a>
                                <a href="javascript:;" class="btn btn-sm btn-outline-dark" onclick="return clear_form( 'employee_form' );"> Clear </a>
                                <a href="javascript:;" class="btn btn-sm btn-success" onclick="return insert_employee()"> <?= $title ?> </a>
                            </div>
                        </div>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $( '.user_login' ).hide();
    $( "#is_login_credential" ).click( function(){
      $( ".user_login").toggle();
    });
    function get_address_form( id='' ) {
        var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&id=' + id;
        $.ajax( {
            type    : "POST",
            url     : '<?= site_url( 'company/get_address_form' ); ?>',
            data    : data,
            success: function( msg ) {
                $( '#defaultModal' ).modal();
                $( '#defaultModal' ).html( msg );
            }
        });
        return false;
    }
    function insert_employee() {
        var employee_form           = $( '#employee_form' ).find('input, textarea, button, select');
        var data      = new FormData( $( '#employee_form' )[0]);
        $.ajax({
            type        : 'POST',
            url         : '<?=site_url( 'employee/insert_employee' );?>',
            data        : data,
            async       : false,
            contentType : false,
            processData : false,
            dataType    : 'json',
            beforeSend: function () {
                employee_form.attr( 'disabled', 'disabled' );
            },    
            success     : function( msg ) {
                if( msg.error == 1 ) {
                    toastr.error( msg.error_msg, 'Employee' );
                    employee_form.attr( 'disabled', false );
                } else {
                    toastr.success( msg.error_msg, 'Employee' );
                    setTimeout( function(){
                        window.location.href = '<?= site_url()?>employee';
                    }, 300 );
                }
            }
        });
        return false;
    }
</script>
 <?php
if( isset( $employee_info->user_name ) && !empty( $employee_info->user_name ) ) {
?>
<script type="text/javascript">
    $( '.user_login' ).show();
</script>
<?php } ?>