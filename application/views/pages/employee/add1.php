
<div class="col-lg-12">
    <div class="block-header">
        <div class="row clearfix">
            <div class="col-md-6 col-sm-12">
                <h2> Add Employee </h2>
            </div>            
            
        </div>
    </div>

    <div class="card">
         
        <div class="body">
            <div class="demo-masked-input">
                <div class="row clearfix">
                    <div class="col-lg-4 col-md-6">
                        <b> First Name</b>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" name="first_name" id="first_name" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                         <b> Last Name </b>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" name="last_name" id="last_name" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <b> DOB </b>
                        <div class="input-group mb-3">
                            <input data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="dd/mm/yyyy">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <b> Gender </b>
                        <div class="input-group mb-3">
                           <label class="fancy-radio custom-color-green"><input name="gender4" value="male" type="radio"><span><i></i>Male</span></label>
                           <label class="fancy-radio custom-color-green"><input name="gender4" value="female" type="radio"><span><i></i>Female</span></label>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                         <b> Mobile Number </b>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" name="mobile_no" id="mobile_no" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                         <b> Email </b>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" name="email" id="email" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                         <b> City </b>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" name="city" id="city" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                         <b> State </b>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" name="state" id="state" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                         <b> Country </b>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" name="country" id="country" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                         <b> Zipcode </b>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" name="zipcode" id="zipcode" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-12">
                         <b> Address </b>
                        <div class="input-group mb-3">
                            <textarea class="form-control" name="address" id="address" rows="3"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-12">
                        <hr>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <b> Date Of Joining </b>
                        <div class="input-group mb-3">
                           <input data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="dd/mm/yyyy" name="date_of_joining">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <b> Salary per month </b>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" name="salary" id="salary" placeholder="">
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6">
                        <b> Benefits </b>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" name="benefits" id="benefits" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        
                        <div class="form-group mb-3">
                            <b> Bank Name </b>    
                            <select class="form-control" name="bank" id="bank">
                                <option value=""> -- Select Bank -- </option>

                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <b> Bank AC No </b>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" name="ac_no" id="ac_no" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <b> Branch </b>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" name="bank_branch" id="bank_branch" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                         <b> IFSC Code </b>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" name="ifsc" id="ifsc" placeholder="">
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-12">
                        <hr>
                    </div>
                    
                    <div class="col-lg-6 col-md-12">

                         <div class="card">
                            <div class="header">
                                <h2> Adhaar / Driving License Image </h2>
                            </div>
                            <div class="body">
                                <input type="file" class="dropify">
                            </div>
                        </div>
                        <label> Adhaar / Driving License Number <span class="text-danger">*</span></label>
                        <div class="input-group mb-3">
                            <input type="text" name="adhaar_no" id="adhaar_no" class="form-control" 
                            placeholder="Adhaar No / Driving License">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12">
                         <div class="card">
                            <div class="header">
                                <h2> Resume </h2>
                            </div>
                            <div class="body">
                                <input type="file" class="dropify">
                            </div>
                        </div>
                    </div>
                </div>
                    
                 <div class="row clearfix">
                    <div class="col-sm-12">
                        <hr>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="form-group mb-3">
                            <b> Is User Login Credential  </b>    
                            <input type="checkbox" name="is_login_credential" id="is_login_credential" >
                        </div>
                        <div id="user_login">
                            <div class="form-group mb-3">
                                <b> User Name </b>    
                                <input type="text" name="user_name" id="user_name" class="form-control">
                            </div>
                            <div class="form-group mb-3">
                                <b> Password </b>    
                                <input type="text" name="user_password" id="user_password" class="form-control">
                            </div>
                            <div class="form-group mb-3">
                                <b> User Type </b>    
                                <select class="form-control" name="user_type" id="user_type">
                                    <option value=""> -- Select user type -- </option>

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="card">
                            <div class="header">
                                <h2> Profile Image </h2>
                            </div>
                            <div class="body">
                                <input type="file" class="dropify">
                            </div>
                        </div>
                        
                    </div>
                </div>
                   <div class="row clearfix">
                        <div class="col-sm-12">
                            <hr>
                        </div>
                        <div class="col-sm-12 text-right">
                            <a href="<?= base_url()?>employee" class="btn btn-sm btn-info"> Cancel </a>
                            <a href="javascript:;" class="btn btn-sm btn-success"> Add Employee </a>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $( '#user_login' ).hide();
    $( "#is_login_credential" ).click( function(){
      $( "#user_login").toggle();
    });
</script>