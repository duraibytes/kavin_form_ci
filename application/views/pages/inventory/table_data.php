<table class="table table-bordered table-hover js-basic-example dataTable table-custom">
    <thead>
        <tr class="inventory-bg text-white">
            <th>
                <div> Product </div>
                <div> Manufacturer </div>
            </th>
            <th>
                <div> Product </div>
                <div> Code </div>
            </th>
            <th>
                <div> Category / </div>
                <div> Variety </div>
            </th>
            <th>
                <div>Product </div>
                <div> Description </div>
            </th>

            <th>
                <div>Lot</div>
                <div>Number</div>
            </th>
            <th>
                <div>Date Of</div>
                <div>Packing</div>
            </th>
            <th>
                <div>Date Of</div>
                <div>Tested</div>
            </th>
            <th>
                <div>Date Of </div>
                <div> Expiry </div>
            </th>
            <th>
                <div>Stage / Level </div>
                <div></div>
            </th>
            <th>
                <div> Germination</div>
                <div>%</div>
            </th>

            <th>
                <div>Physical</div>
                <div>Purity %</div>
            </th>
            <th>
                <div>Genetic</div>
                <div>Purity %</div>
            </th>
            <th>
                <div>Treated </div>
                <div>With</div>
            </th>
            <th>
                <div>Package</div>
                <div>UOM</div>
            </th>
            <th>
                <div>Net </div>
                <div>Rate</div>
            </th>
            <th>
                <div>Total Purchased</div>
                <div>Quantity</div>
            </th>
            <th>
                <div>Purchase</div>
                <div>Inv No</div>
            </th>
            <th>
                <div>Total</div>
                <div>Sold Qty</div>
            </th>
            <th>
                <div>Sales</div>
                <div>Inv No</div>
            </th><th>
                <div>Total Returned</div>
                <div> Quantity </div>
            </th>
            <th>
                <div>Retrun</div>
                <div> Invoice </div>
            </th>
            <th>
                <div>Balance </div>
                <div>Qty</div>
            </th>

        </tr>
    </thead>
   
    <tbody>
    <?php 
        if( isset( $stock ) && !empty( $stock ) ) {
            $i = 1;
            foreach( $stock as $stk ) { 
                
                $balance_qty  = 0;
                

                $balance_qty  += $stk->qty - ( $stk->returned_qty ?? 0 );
    ?>
        <tr>
            <td class="text-left"><?= $stk->product_company ?></td>
            <td  class="text-left"><?= $stk->product_code ?></td>
            <td  class="text-left"><?= $stk->product_category ?></td>
            <td  class="text-left"><?= $stk->description ?? '' ?></td>
            <td  class="text-left"><?= $stk->lot_no ?? '' ?></td>
            <td  class="text-left">
                <?php
                $date_of_packed     = $stk->date_of_packed;
                if( isset( $date_of_packed ) && !empty( $date_of_packed ) ) {
                    echo date( 'd-M-Y', strtotime( $date_of_packed ) );
                }
                ?>

            </td>
            <td  class="text-left"><?php
                $date_of_tested     = $stk->date_of_tested;
                if( isset( $date_of_tested ) && !empty( $date_of_tested ) ) {
                    echo date( 'd-M-Y', strtotime( $date_of_tested ) );
                }
                ?></td>
            <td  class="text-left"><?php
                $date_of_expiry     = $stk->date_of_expiry;
                if( isset( $date_of_expiry ) && !empty( $date_of_expiry ) ) {
                    echo date( 'd-M-Y', strtotime( $date_of_expiry ) );
                }
                ?></td>
            <td  class="text-left">-</td>
            <td  class="text-left">-</td>
            <td  class="text-left">-</td>
            <td  class="text-left">-</td>
            <td  class="text-left">-</td>
            <td  class="text-left"><?= $stk->packaging ?? '' ?></td>
            <td  class="text-left"> Rs <?= $stk->net_rate ?? '0' ?></td>
            <td  class="text-center"> <?= $stk->qty ?></td>
            <td  class="text-left"> <?= $stk->invoice_no ?></td>
            
            <td  class="text-left"></td>
            <td  class="text-left"></td>
            <td  class="text-left"> <?= $stk->returned_qty ?? '' ?></td>
            <td  class="text-left"> <?= $stk->return_no ?? '' ?></td>
            <td  class="text-center"> <?= $balance_qty ?></td>
        </tr>
    <?php   $i++;
            } 
        } 
    ?>
    </tbody>
</table>