<style type="text/css">
    #stock_filter {
        background: #ddd;
        padding: 20px;
    }
</style>
<div class="block-header">
    <?= $this->load->view( 'layout/_nav_header', '', true )?>
</div> 
    <div class="container-fluid">            

        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <div class="text-center hd-font m-b-20"> 
                                    <span class="btn btn-sm inventory-bg text-white">
                                       STOCK MANAGEMENT
                                    </span> 
                                </div>
                            </div>
                        </div>
                        <?= form_open( '', 'id="stock_filter"' )?>
                        
                        <?= $this->load->view( 'pages/inventory/filter_form', '', true )?>
                        <?= form_close() ?>
                    </div>
                    <div class="body">
                        <hr>
                       <div class="table-responsive">
                         <?= $this->load->view( 'pages/inventory/table_data', '', true ) ?>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<script type="text/javascript">
    $(function () {
    $( '.custom-select' ).select2();
  });

    function stock_filter() {
      var stock_filter       = $( '#stock_filter' ).find('input, textarea, button, select');
      var data                = new FormData( $( '#stock_filter' )[0] );
      $.ajax({
        type        : 'POST',
        url         : '<?=site_url( 'inventory/set_filter_data' );?>',
        data        : data,
        async       : false,
        contentType : false,
        processData : false,
        dataType    : 'json',
        beforeSend: function () {
          
        },    
        success     : function( msg ) {
              
            setTimeout( function(){
               window.location.href = '<?= site_url()?>inventory';
            }, 200 );
        }
      });
      return false;
    }

    function clear_stock_filter() {
        var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>';
        $.ajax( {
            type    : "POST",
            url     : '<?= site_url( 'inventory/clear_stock_filter' ); ?>',
            data    : data,
            
            success: function( msg ) {
                
                setTimeout( function(){
                    window.location.href = '<?= site_url()?>inventory';
                }, 300 );
            }
        });
        return false;
      }
</script>