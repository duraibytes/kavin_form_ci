<?php

if( $this->session->has_userdata( 'stock_filter' ) ) {
    $session            = $this->session->userdata( 'stock_filter' );
    
}
?>
<div class="row">
    <div class="col-lg-2 col-md-3">
        <label> Manufacturer </label>
        <div class="form-group">
           <select class="custom-select" name="product_company" id="product_company">
               <option value=" " > -- select -- </option>
              <?php if( isset( $product_manufacturer ) && !empty( $product_manufacturer ) ) { 
                        foreach ($product_manufacturer as $key => $company ) {
                            $selected       = '';
                            if( isset( $session ) && $session[ 'product_company' ] == $company->product_company ) {
                                $selected   = 'selected';
                            }
                            ?>
                            <option value="<?= $company->product_company ?>" <?= $selected ?>><?= $company->product_company ?></option>
                <?php        }
                ?>
              <?php } ?>
           </select>
       </div>
    </div>
    <div class="col-lg-2 col-md-3">
        <label> Product Code </label>
        <div class="form-group">
           <select class="custom-select" name="product_code" id="product_code">
               <option value=" " > -- select -- </option>
               <?php if( isset( $product_code ) && !empty( $product_code ) ) { 
                        foreach ($product_code as $key => $pro ) {
                            $selected       = '';
                            if( isset( $session ) && $session[ 'product_code' ] == $pro->product_code ) {
                                $selected   = 'selected';
                            }
                            ?>
                            <option value="<?= $pro->product_code ?>" <?= $selected ?>><?= $pro->product_code ?></option>
                <?php        }
                ?>
              <?php } ?>
           </select>
       </div>
    </div>
    <!-- <div class="col-lg-2 col-md-3">
        <label> Product Variety </label>
        <div class="form-group">
           <select class="custom-select" name="category" id="category">
               <option value=" " > -- select -- </option>
               <option value="1"> Medicine </option>
               <option value="2s"> Seeds </option>
           </select>
       </div>
    </div> -->
    
    <div class="col-lg-2 col-md-3">
        <label> Product Category </label>
        <div class="form-group">
           <select class="custom-select" name="product_category" id="product_category">
               <option value=" " > -- select  -- </option>
               <?php if( isset( $product_category ) && !empty( $product_category ) ) { 
                        foreach ($product_category as $key => $cat ) {

                            if( !empty( $cat->product_category ) ) {

                                $selected       = '';
                                if( isset( $session ) && $session[ 'product_category' ] == $cat->product_category ) {
                                    $selected   = 'selected';
                                }
                            ?>
                            <option value="<?= $cat->product_category ?>" <?= $selected ?>><?= $cat->product_category ?></option>
                <?php        } }
                ?>
              <?php } ?>
           </select>
       </div>
    </div>
    <div class="col-lg-2 col-md-3">
        <label> Product Lot </label>
        
        <div class="form-group">
           <select class="custom-select" name="lot_no" id="lot_no">
               <option value=" " > -- select  -- </option>
               <?php if( isset( $product_lot ) && !empty( $product_lot ) ) { 
                        foreach ($product_lot as $key => $item ) {
                            if( !empty( $item->lot_no ) ) {
                                $selected       = '';
                                if( isset( $session ) && $session[ 'lot_no' ] == $item->lot_no ) {
                                    $selected   = 'selected';
                                }
                            ?>
                            <option value="<?= $item->lot_no ?>" <?= $selected ?>><?= $item->lot_no ?></option>
                <?php        } }
                ?>
              <?php } ?>
           </select>
       </div>
    </div>
    <div class="col-lg-2 col-md-3">
        <label> Purchase Invoice </label>
        <div class="form-group">
           <select class="custom-select" name="invoice_no" id="invoice_no">
               <option value=" " > -- select  -- </option>
               <?php if( isset( $purchase_invoice ) && !empty( $purchase_invoice ) ) { 
                        foreach ($purchase_invoice as $key => $pur ) {
                            $selected       = '';
                            if( isset( $session ) && $session[ 'invoice_no' ] == $pur->invoice_no ) {
                                $selected   = 'selected';
                            }
                            ?>
                            <option value="<?= $pur->invoice_no ?>" <?= $selected ?>><?= $pur->invoice_no ?></option>
                <?php        }
                ?>
              <?php } ?>
           </select>
       </div>
    </div>
    <div class="col-lg-2 col-md-3">
        <label> Sale Invoice </label>
        <div class="form-group">
           <select class="custom-select" name="sale_invoice_no" id="sale_invoice_no">
               <option value=" " > -- select  -- </option>
               <option value="1"> Medicine </option>
               <option value="2s"> Seeds </option>
           </select>
       </div>
    </div>
    <div class="col-lg-4 col-md-6">
        <div class="form-group m-t-20">
            <a href="javascript:;" class="btn inventory-bg text-white" onclick="return stock_filter()"> Filter </a>
            <a href="javascript:;" class="btn btn-warning text-white" onclick="return clear_stock_filter()"> Clear Filter </a>
        </div>
    </div>
    
</div>