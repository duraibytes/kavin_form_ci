<div class="block-header m-t-20">
    <?= $this->load->view( 'layout/_nav_header', '', true )?>
</div>
<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                
                <div class="body">
                    <div class="table-responsive">
                        <div class="text-center hd-font m-b-10 m-t-10"> 
                            <span class="btn btn-sm inventory-bg text-white">
                               LIST OF COMPANIES
                            </span> 
                        </div>
                        <table class="table table-bordered table-hover js-basic-example dataTable table-custom">
                            <thead>
                                <tr class="inventory-bg text-white">
                                    
                                    <th> Company Name </th>
                                    <th> Company Address </th>
                                    <th> Company Phone no </th>
                                    <th> Regional Manager </th>
                                    <th> Area Manager </th>
                                    <th> Action </th>
                                </tr>
                            </thead>
                            
                            </tfoot>
                            <tbody>
                                <?php
                                    if( isset( $company_info ) && !empty( $company_info ) ) {
                                        $i = 1;
                                        foreach ( $company_info as $key => $value ) {
                                            $address    = $this->Common_model->get_company_address( $value->id );

                                ?>
                                            <tr>
                                                <td class="text-left">
                                                    <?= $value->company_name ?>
                                                </td>
                                                <td class="text-left">
                                                    
                                                 <?= $address->address ?? '-' ?>
                                                 <?= $address->city ?? '' ?>    
                                                 <?= $address->state?? '' ?>
                                                 <?= $address->zip_code ?? '' ?>,
                                                 <?= $address->country ?? '' ?>
                                                 </td>
                                                <td class="text-left"> <?= $address->mobile_no ?? '-' ?></td>
                                                <td class="text-left"> 
                                                    <div>
                                                        <?= $value->regional_manager_name ?? '-' ?>     
                                                    </div>
                                                    <div>
                                                        <?= $value->regional_manager_mobile ?? '-' ?>    
                                                    </div>
                                                    </td>
                                                <td class="text-left">
                                                    <div>
                                                        <?= $value->area_manager_name ?? '-' ?>
                                                    </div>
                                                    <div>
                                                        <?= $value->area_manager_mobile ?? '-' ?>
                                                    </div>
                                                </td>
                                                <td class="text-left">
                                                    <a href="<?= base_url() ?>company/add/<?= $value->slug ?>" class="btn btn-outline-warning ">
                                                        <i class="icon-pencil"></i>
                                                    </a>
                                                    <a href="javascript:;" class="btn btn-danger btn-sm" onClick="return are_you_sure( 'delete_company', 'Delete Company', 'Are you sure want to delete this company?', '<?= $value->id?>' );"> <i class="icon-trash"></i> </a>
                                                </td>
                                            </tr>
                                            <?php 
                                     $i++;   } 
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
    </div>
    
</div>
<script type="text/javascript">
    function delete_company( id ) {
        var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&id=' + id;
        $.ajax( {
            type    : "POST",
            url     : '<?=site_url( 'company/delete_company' );?>',
            data    : data,
            success: function( msg ) {
                toastr.success( 'company deleted successfully', 'Company' );
                setTimeout( function() {
                    window.location.href = '<?= site_url()?>company';
                }, 300 );
            }
        });
        return false;
    }
</script>
