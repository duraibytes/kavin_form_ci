<div class="inline-class-head" >
    <div class="w-100">
        <span class="Hfont text-muted"> Company Info: </span>
    </div>
</div>
<div class="inline-class" >
    <div class="w-50 vcenter"> 
        Company Name <span class="text-danger">*</span>
    </div>
    <div class="w-50 vcenter"> 
        <input type="text" name="company_name" id="company_name" class="form-control" value="<?= $company_info->company_name ?? '' ?>"> 
    </div>
</div>
<div class="inline-class" >
    <div class="w-50 vcenter"> 
         GSTIN NO 
    </div>
    <div class="w-50 vcenter"> 
        <input type="text" name="gstin_no" id="gstin_no" class="form-control" value="<?= $company_info->gstin_no ?? '' ?>"> 
    </div>
</div>
<div class="inline-class" >
    <div class="w-50 vcenter"> 
         PAN NO 
    </div>
    <div class="w-50 vcenter"> 
        <input type="text" name="pan_no" id="pan_no" class="form-control" value="<?= $company_info->pan_no ?? '' ?>"> 
    </div>
</div>
<div class="inline-class" >
    <div class="w-50 vcenter"> Seed License </div>
    <div class="w-50 vcenter"> 
        <input type="text" name="license" id="license" class="form-control" value="<?= $company_info->seed_license ?? '' ?>"> 
    </div>
</div>
<div class="inline-class" >
    <div class="w-50 vcenter"> Pesticides License </div>
    <div class="w-50 vcenter"> 
        <input type="text" name="pesticides_license" id="pesticides_license" class="form-control" value="<?= $company_info->pesticides_license ?? '' ?>"> 
    </div>
</div>
<div class="inline-class" >
    <div class="w-50 vcenter"> Fertilizer License </div>
    <div class="w-50 vcenter"> 
        <input type="text" name="fertilizer_license" id="fertilizer_license" class="form-control" value="<?= $company_info->fertilizer_license ?? '' ?>"> 
    </div>
</div>
<div class="inline-class" >
    <div class="w-50 vcenter"> Company GST </div>
    <div class="w-50 vcenter"> 
        <input type="text" name="gst" id="gst" class="form-control" value="<?= $company_info->gst ?? '' ?>"> 
    </div>
</div>
<div class="inline-class" >
    <div class="w-50 vcenter"> Company CST </div>
    <div class="w-50 vcenter"> <input type="text" name="cst" id="cst" class="form-control" value="<?= $company_info->cst ?? '' ?>"> </div>
</div>
<div class="inline-class" >
    <div class="w-50 vcenter"> Company VAT </div>
    <div class="w-50 vcenter"> <input type="text" name="vat" id="vat" class="form-control" value="<?= $company_info->vat ?? '' ?>"> </div>
</div>