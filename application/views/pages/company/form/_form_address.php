<div class="modal-dialog address-form modal-lg" role="document">
    <div class="modal-content">
        <?=form_open( '', 'id="address-form"');?>
        <div class="modal-header">

            <h4 class="modal-title"> <?= $title ?> Address </h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            
        </div>
        <style type="text/css">
        	
		    .no-border {
		    	border:none !important;
		    }
		    
		    .w-20 {
		    	width:20%;
		    }
		    .w-80 {
		    	width:80%;
		    }
        </style>

        <div class="modal-body">
            <div class="row">
                <div class="col-sm-12">
                    <?= $this->load->view( 'errors/show_error', '', true )?>
                </div>
            </div>

			<div class="col-sm-12">
				<div class="inline-class no-border" >
				    <div class="w-20 vcenter"> Address <span class="text-danger">*</span></div>
				    <div class="w-80 vcenter"> 
				    	<input type="text" name="address" id="address" class="form-control" value="<?= $add_info[ 'address' ] ?? '' ?>"> 
				    </div>
				</div>
				<div class="inline-class no-border" >
					<div class="" style="display: flex;width: 50%">
						<div class="vcenter" style="width: 53%;"> City <span class="text-danger">*</span>
						</div>
					    <div class="w-80 vcenter"> 
					    	<input type="text" name="city" id="city" class="form-control" value="<?= $add_info[ 'city' ] ?? '' ?>"> 
					    </div>
					</div>
				    <div class="" style="display: flex;width: 50%">
						<div class="w-20 vcenter" style="padding-left: 5px;"> State <span class="text-danger">*</span></div>
					    <div class="w-80 vcenter" id="state_dropdown"> 
					    	<select id="state_id" name="state_id" class="form-control">
					    		<option value=""> -- Select State -- </option>
					    		<?php 
				    			if( isset( $state_info ) && !empty( $state_info ) ) {
				    				foreach ( $state_info as $skey => $svalue ) {
				    					$selected 		= '';
				    					if( $svalue->id == $add_info[ 'state_id' ] ) {
				    						$selected 	= 'selected';
				    					} 

					    		?>
					    		<option value="<?= $svalue->id ?>" <?= $selected ?>><?= $svalue->state ?></option>
					    	<?php }  }?>
					    	</select>
					    </div>
					</div>
				</div>
				<div class="inline-class no-border" >
					<div class="" style="display: flex;width: 50%">
						<div class="vcenter" style="width: 53%;"> Zipcode <span class="text-danger">*</span></div>
					    <div class="w-80 vcenter"> 
					    	<input type="text" name="zipcode" id="zipcode" class="form-control" value="<?= $add_info[ 'zipcode' ] ?? '' ?>"> 
					    </div>
					</div>
				    <div class="" style="display: flex;width: 50%">
						<div class="w-20 vcenter" style="padding-left: 5px;"> Country <span class="text-danger">*</span></div>
					    <div class="w-80 vcenter"> 
					    	<select id="country_id" name="country_id" class="form-control" onchange="return get_state( this.value );">
					    		<option value=""> -- Select County -- </option>
					    		<?php 
					    			if( isset( $county_info ) && !empty( $county_info ) ) {
					    				foreach ( $county_info as $key => $value ) {
					    					$selected 		= '';
					    					if( $value->id == $add_info[ 'country_id' ] ) {
					    						$selected 	= 'selected';
					    					} else if( $value->id == '99' ) {
					    						$selected 	= 'selected';
					    					}

					    		?>
					    		<option value="<?= $value->id ?>" <?= $selected ?>><?= $value->name ?></option>
					    	<?php }  }?>
					    	</select>
					    	
					    </div>
					</div>
				</div>
				<div class="inline-class no-border" >
					<div class="" style="display: flex;width: 50%">
						<div class="vcenter" style="width: 53%;"> Email </div>
					    <div class="w-80 vcenter"> 
					    	<input type="text" name="email" id="email" class="form-control" value="<?= $add_info[ 'email' ] ?? '' ?>"> 
					    </div>
					</div>
				    <div class="" style="display: flex;width: 50%">
						<div class="w-20 vcenter" style="padding-left: 5px;"> Mobile </div>
					    <div class="w-80 vcenter"> 
					    	<input type="text" name="mobile_no" id="mobile_no" class="form-control" value="<?= $add_info[ 'mobile_no' ] ?? '' ?>"> 
					    </div>
					</div>
				</div>
				<?php

				?>
				<div class="inline-class no-border" >
				    <div class="w-20 vcenter"> Type <span class="text-danger">*</span></div>
				    <div class="w-80 vcenter"> 
				    	<select class="form-control" name="address_type" id="address_type">
				    		<option value="" > -- select type -- </option>
				    		<?php
				    			if( isset( $address_type ) && !empty( $address_type ) ) {
				    				foreach ( $address_type as $addkey => $value ) {
				    					$selected 			= '';
				    					if( isset( $add_info ) && $add_info[ 'address_type_id' ] == $value->id ) {
				    						$selected 		= 'selected';
				    					}
				    				
				    		?>
					    				<option value="<?= $value->id?>" <?= $selected ?>> <?= $value->name ?> </option>
					    	<?php } } ?>
				    	</select>
				    </div>
				</div>
			</div>            
        </div>
        <div class="modal-footer">
            <input type="hidden" name="key" value="<?= $key ?>">
            <input type="hidden" name="from" value="<?= $from ?? '' ?>">
            <input type="hidden" name="action" value="<?= $action ?? '' ?>">
            <button class="btn btn-secondary" data-dismiss="modal" type="button"> Close</button>
            <button onClick="return add_address()" class="btn btn-primary" type="button"> Save
            	<div class="spinner-border spinner-border-sm float-right" role="status" id="loading" style="display:none;">
				    <span class="sr-only">Loading...</span>
				</div>
            </button>
        </div>
        <?=form_close();?>
    </div>
</div>

<script type="text/javascript">
	$(function () {
	    $( '.custom-select' ).select2();
	 });

    function add_address() {
        var form_name           = $( '#address-form' ).find( 'input, select, button, textarea' );
        var codes_form_data     = form_name.serialize();
        codes_form_data         = codes_form_data + '&<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>';
        $.ajax({
            type        : 'POST',
            url         : '<?=site_url( 'settings/add_address' );?>',
            data        : codes_form_data,
            async       : false,
            dataType    : 'json',
            beforeSend: function () {
                form_name.attr( 'disabled', 'disabled' );
                $( '#form_errors' ).hide();
                $( '#form_success' ).hide();
                $( '#loading' ).show();
            },    
            success     : function( msg ) {
                if( msg.error == 1 ) {
                    toastr.error( msg.error_msg, 'Address' );
                    form_name.attr( 'disabled', false );
                    $( '#loading' ).hide();
                } else {
                    toastr.success( msg.error_msg, 'Address' );
                    $( '#loading' ).hide();
                    $( '#defaultModal' ).modal( 'hide' );
                    // $( '#company_address' ).html( msg.view );
                    address_list();
                    
                }
            }
        });
        return false;
    }

     function get_state( country_id ) {

          var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&country_id=' + country_id;
          $.ajax( {
              type    : "POST",
              url     : '<?= site_url( 'company/get_state' ); ?>',
              data    : data,
              success: function( msg ) {
                 $( '#state_dropdown' ).html( msg );
                  
              }
          });
          return false;
      }

</script>