<div class="inline-class" >
    <div class="w-50 vcenter"> Bank Name </div>
    <div class="w-50 vcenter"> 
        <select class="form-control" name="bank" id="bank">
            <option value=""> -- Select Bank -- </option>
            <?php
            if( isset( $bank_info ) && !empty( $bank_info ) ) {
                foreach ( $bank_info as $key => $value ) {
                    $selected       = '';
                    if( isset( $bank_id ) && $bank_id == $value->id ) {
                        $selected   = 'selected';
                    }
                    ?>
                    <option value="<?= $value->id ?>" <?= $selected ?>> <?= $value->bank_name ?> </option>
           <?php     }
            }
            ?>
        </select> 
    </div>
</div>
<?php

?>
<div class="inline-class" >
    <div class="w-50 vcenter"> Bank AC No </div>
    <div class="w-50 vcenter"> 
        <input type="text" name="ac_no" id="ac_no" class="form-control" value="<?= $company_info->bank_ac_no ?? '' ?>" > 
    </div>
</div>
<div class="inline-class" >
    <div class="w-50 vcenter"> Bank Branch </div>
    <div class="w-50 vcenter"> 
        <input type="text" name="bank_branch" id="bank_branch" class="form-control" value="<?= $bank->branch ?? '' ?>"> 
    </div>
</div>
<div class="inline-class" >
    <div class="w-50 vcenter"> IFSC </div>
    <div class="w-50 vcenter"> 
        <input type="text" name="ifsc" id="ifsc" class="form-control" value="<?= $bank->ifsc ?? '' ?>"> 
    </div>
</div>