<div class="inline-class-head" >
    <div class="w-100">
        <span class="Hfont text-muted"> Company POC </span>
    </div>
</div>
<div class="inline-class" >
    <div class="w-50 vcenter"> 
        Regional Sales Manager Name  
    </div>
    <div class="w-50 vcenter"> 
        <input type="text" name="regional_manager_name" class="form-control" value="<?= $company_info->regional_manager_name ?? '' ?>"> 
    </div>
</div>
<div class="inline-class" >
    <div class="w-50 vcenter"> 
       Regional Sales Manager Mobile
    </div>
    <div class="w-50 vcenter"> 
        <input type="text" name="regional_manager_mobile" id="regional_manager_mobile" class="form-control" value="<?= $company_info->regional_manager_mobile ?? '' ?>"> 
    </div>
</div>
<div class="inline-class" >
    <div class="w-50 vcenter"> Regional Sales Manager Email </div>
    <div class="w-50 vcenter"> 
        <input type="text" name="regional_manager_email" id="regional_manager_email" class="form-control" value="<?= $company_info->regional_manager_email ?? '' ?>"> 
    </div>
</div>
<div class="inline-class" >
    <div class="w-50 vcenter"> 
        Area Sales Manager / Rep Name 
    </div>
    <div class="w-50 vcenter"> 
        <input type="text" name="area_manager_name" id="area_manager_name" class="form-control" value="<?= $company_info->area_manager_name ?? '' ?>"> 
    </div>
</div>
<div class="inline-class" >
    <div class="w-50 vcenter"> 
       Area Sales Manager / Rep Mobile
    </div>
    <div class="w-50 vcenter"> 
        <input type="text" name="area_manager_mobile" id="area_manager_mobile" class="form-control" value="<?= $company_info->area_manager_mobile ?? '' ?>"> 
    </div>
</div>
<div class="inline-class" >
    <div class="w-50 vcenter"> Area Sales Manager / Rep  Email </div>
    <div class="w-50 vcenter"> 
        <input type="text" name="area_manager_email" id="area_manager_email" class="form-control" value="<?= $company_info->area_manager_email ?? '' ?>"> 
    </div>
</div>