<?php

    if( isset( $info ) ) {
        
        foreach ( $info as $key => $value ) {
        
?>
            <div class="inline-class" >
                
                <div class="w-50 vcenter" onclick="return get_address_form( '<?= $key ?>', '<?= $from ?>', 'edit' )" style="cursor: pointer;"> <?= $value[ 'address_type' ] ?> </div>
                <div class="w-50 text-left" onclick="return get_address_form( '<?= $key ?>', '<?= $from ?>', 'edit' )" style="cursor: pointer;"> 
                    <div> <?= $value[ 'address' ] ?? '' ?> </div>
                    <div>   <?= $value[ 'city' ] ?? '' ?> <?= $value[ 'state' ] ?? '' ?>
                            <?= $value[ 'zipcode' ] ?? '' ?> 
                    </div>
                    <div><?= $value[ 'country' ] ?? '' ?> </div>
                    <div><?= $value[ 'email' ] ?? '' ?> </div>
                    <div><?= $value[ 'mobile_no' ] ?? '' ?> </div>


                </div>
                <div style="display: flex;align-items: center;">
                    <i class="icon-trash text-danger" onClick="return are_you_sure( 'delete_address', 'Delete Address', 'Are you sure want to delete this address?', '<?= $key ?>', '<?= $from ?>' );" style="cursor: pointer;">
                        
                    </i> 
                </div>
            </div>
<?php 
        } 
    }

?> 