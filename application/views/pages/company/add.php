
 <div class="block-header">
    <?= $this->load->view( 'layout/_nav_header', '', true )?>
</div>
<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                    <div class="text-center hd-font m-b-10 m-t-10"> 
                        <span class="btn btn-sm inventory-bg text-white">
                           COMPANY MANAGEMENT
                        </span> 
                    </div>
                 <?php echo form_open_multipart( '', 'id="company_form"' ); ?>
                    
                    <div class="demo-masked-input">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6">
                                <?= $this->load->view( 'pages/company/form/_form_company_info', '', true ) ?>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="inline-class" style="border-bottom: none;" >
                                    <div class="w-50 vcenter"> Company Logo
                                        <?php
                                            if( isset( $company_info->company_logo ) && !empty( $company_info->company_logo ) ) {
                                                ?>
                                                <img src="<?= $company_info->company_logo ?>" style="width: 75px;margin-left: 10px;">
                                        <?php    }
                                        ?>
                                    </div>
                                    <div class="w-50 vcenter"> <input type="file" name="company_logo" class="dropify"> </div>
                                </div>
                                <?= $this->load->view( 'pages/company/form/_form_poc', '', true ) ?>
                            </div>
                            <!-- address and bank info -->
                            <div class="col-lg-6 col-md-6">
                                <div class="inline-class-head m-t-30" >
                                    <div class="w-100">
                                        <span class="Hfont text-muted"> Address</span>
                                        <span class="float-right"><a href="javascript:;" onclick="return get_address_form( '', 'company_address' )"> + add new </a></span>
                                    </div>
                                </div>
                                <div id="company_address">
                                    <?= $this->load->view( 'pages/company/form/_form_address_info', '', true ) ?>    
                                </div>
                                
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="inline-class-head m-t-30" >
                                    <div class="w-100">
                                        <span class="Hfont text-muted"> Bank Info </span>
                                        <span class="float-right"><a href="javascript:;" onclick="return get_bank_form()"> + add new </a></span>
                                    </div>
                                </div>
                                <div id="bank_info">
                                    <?= $this->load->view( 'pages/company/form/_form_bank', '', true ) ?>    
                                </div>
                                
                            </div>
                        </div>
                        
                       <div class="row clearfix">
                            <div class="col-sm-12">
                                <hr>
                            </div>
                            <input type="hidden" name="id" value="<?= $id ?? ''?>">
                            <div class="col-sm-12 text-right">
                                <a href="<?= base_url()?>company" class="btn btn-sm btn-outline-danger"> Cancel </a>
                                <?php
                                if( ! $id ) { 
                                ?>
                                    <a href="javascript:;" class="btn btn-sm btn-outline-dark" onclick="return clear_form( 'company_form' )"> Clear </a>
                                <?php } ?>
                                <a href="javascript:;" onclick="return insert_company()" class="btn btn-sm btn-success"> <?= $title ?> </a>
                            </div>
                        </div>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    if( isset( $id ) && !empty( $id ) ) {
        ?>
        <script type="text/javascript">
            setTimeout(function(){
                address_list( '<?= $id ?>' );
            }, 200 );
        </script>
<?php    }
?>

<script type="text/javascript">
    $( '#user_login' ).hide();
    $( "#is_login_credential" ).click( function(){
      $( "#user_login").toggle();
    });

  function insert_company() {
        var company_form           = $( '#company_form' ).find('input, textarea, button, select');
        var data      = new FormData( $( '#company_form' )[0]);
        $.ajax({
            type        : 'POST',
            url         : '<?=site_url( 'company/insert_company' );?>',
            data        : data,
            async       : false,
            contentType : false,
            processData : false,
            dataType    : 'json',
            beforeSend: function () {
                company_form.attr( 'disabled', 'disabled' );
            },    
            success     : function( msg ) {
                if( msg.error == 1 ) {
                    toastr.error( msg.error_msg, 'Company' );
                    company_form.attr( 'disabled', false );
                } else {
                    toastr.success( msg.error_msg, 'Company' );
                    setTimeout( function(){
                        window.location.href = '<?= site_url()?>company';
                    }, 300 );
                }
            }
        });
        return false;
    }

    function address_list( company_id = '' ) {
        var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&company_id=' + company_id;
        $.ajax( {
            type    : "POST",
            url     : '<?= site_url( 'company/address_list' ); ?>',
            data    : data,
            success: function( msg ) {
                $( '#company_address' ).html( msg );
            }
        });
        return false;
    }

     function delete_address( key, session_name ) {

          var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&key=' + key +'&session_name=' + session_name;
          $.ajax( {
              type    : "POST",
              url     : '<?= site_url( 'settings/delete_address' ); ?>',
              data    : data,
              success: function( msg ) {
                  toastr.success( 'Address deleted Successfully', 'Address' );
                  address_list();
              }
          });
          return false;
      }
</script>