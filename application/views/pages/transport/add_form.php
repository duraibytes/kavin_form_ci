<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <?=form_open( '', 'id="module-form"');?>
        <div class="modal-header">

            <h4 class="title" id="defaultModalLabel"><?= $btn_name ?> Transport </h4>
            
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-12">
                    <?= $this->load->view( 'errors/show_error', '', true  )?>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10">
                        <?= $this->load->view( 'pages/transport/_form', '', true ) ?>
                    </div>
                </div>
            </div>
             
        </div>
        <div class="modal-footer">
            <input type="hidden" name="id" value="<?= $id ?>">
            <input type="hidden" name="class" value="<?= $class ?>">
            
            <!-- <button class="btn btn-secondary" data-dismiss="modal" type="button"> Close</button> -->
            <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
         
            <button onClick="return add_transport()" class="btn btn-primary" type="button"> Save</button>
            
        </div>
        <?=form_close();?>
    </div>
</div>

<script type="text/javascript">

    function add_transport() {
        var form_name           = $( '#module-form' ).find( 'input, select, button, textarea' );
        var codes_form_data     = form_name.serialize();
        codes_form_data         = codes_form_data + '&<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>';
        $.ajax({
            type        : 'POST',
            url         : '<?=site_url( 'settings/add_transport' );?>',
            data        : codes_form_data,
            async       : false,
            dataType    : 'json',
            beforeSend: function () {
                form_name.attr( 'disabled', 'disabled' );
                $( '#form_errors' ).hide();
                $( '#form_success' ).hide();
            },    
            success     : function( msg ) {
                if( msg.error == 1 ) {
                    toastr.error( msg.error_msg, 'Transport' );
                    
                    form_name.attr( 'disabled', false );
                } else {
                    toastr.success( msg.error_msg, 'Transport' );
                    if( msg.view ) {
                        $( '#defaultModal' ).modal( 'hide' );
                        $( '.'+msg.class ).html( msg.view );
                    } else {
                        window.location.href = '<?=base_url();?>transport';    
                    }
                    
                    
                }
            }
        });
        return false;
    }

</script>