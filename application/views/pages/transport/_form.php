
<div class="form-group">
    <label for="code"> Transport Name </label>
    <input class="form-control" placeholder="Enter Transport Name" type="text" name="transport_name" id="transport_name" value="<?= $info->name ?? '' ?>" />
</div>
<div class="form-group">
    <label for="code"> Transport Email </label>
    <input class="form-control" placeholder="Enter Transport Email" type="text" name="email" id="email" value="<?= $info->email ?? '' ?>" />
</div>
<div class="form-group">
    <label for="code"> Transport Mobile Number </label>
    <input class="form-control" placeholder="Enter Transport Mobile" type="text" name="mobile_no" id="mobile_no" value="<?= $info->mobile_no ?? '' ?>" />
</div>
<div class="form-group">
    <label for="code"> Address </label>
   	<textarea class="form-control" rows="3" cols="5" name="address"><?= $info->address ?? '' ?></textarea>
</div>