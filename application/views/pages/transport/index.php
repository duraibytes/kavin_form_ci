

<div class="block-header m-t-20">
    <?= $this->load->view( 'layout/_nav_header', '', true )?>
</div>

<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                
                <div class="body">
                    <div class="table-responsive">
                        <div class="text-center hd-font"> 
                            <div class="text-center hd-font m-b-20"> 
                                <span class="btn btn-sm inventory-bg text-white">
                                   TRANSPORT MANANGEMENT
                                </span> 
                            </div>
                        </div>
                        <table class="table table-bordered table-hover js-basic-example dataTable table-custom">
                            <thead>
                                <tr class="inventory-bg text-white">
                                    <th> S.NO </th>
                                    <th> Transport </th>
                                    <th> Address </th>
                                    <th class="text-center"> Action </th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                
                                <?php
                                        if( $all ) {
                                            $i = 1;
                                            foreach ( $all as $key => $value) {
                                    ?>
                                    <tr>
                                        <td> <?= $i?></td>
                                        <td> <?= $value->name ?></td>
                                        <td> <?= $value->address ?></td>
                                        <td class="text-center">
                                            <a href="javascript:;" class="btn btn-warning btn-sm" onclick="return open_add_transport_form( '<?= $value->id ?>' )"> <i class="icon-pencil"></i> </a>
                                            <a href="javascript:;" class="btn btn-danger btn-sm" onClick="return are_you_sure( 'delete_transport', 'Delete Transport', 'Are you sure want to delete this transport?', '<?= $value->id?>' );"> <i class="icon-trash"></i> </a>
                                        </td>
                                    </tr>
                                <?php 
                                     $i++;   } 
                                    }

                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
    </div>
    
</div>



<script type="text/javascript">
    $(document).ready(function() {
        $('#module_table').DataTable();
    } );
   
    

      function delete_transport( id ) {
        var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&id=' + id;
        $.ajax( {
            type    : "POST",
            url     : '<?= site_url( 'settings/delete_transport' ); ?>',
            data    : data,
            dataType: 'json',
            success: function( msg ) {
                toastr.success( 'Transport deleted successfully', 'Transport' );
                setTimeout( function(){
                    window.location.href = '<?= site_url()?>transport';
                }, 300 );
            }
        });
        return false;
      }
    
</script> 