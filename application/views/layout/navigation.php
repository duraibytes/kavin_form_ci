<style>
.scroll1::-webkit-scrollbar {
  width: 5px;
}
.scroll1::-webkit-scrollbar-track {
  background: #ddd;
}
.scroll1::-webkit-scrollbar-thumb {
  background: #666; 
}
</style>

<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img src="<?= base_url() ?>assets/images/icon-light.svg" width="48" height="48" alt="KavinFarms"></div>
        <p>Please wait...</p>        
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<div id="wrapper">

  <nav class="navbar navbar-fixed-top">
        <div class="container-fluid" id="top-kavin-nav" >

            <div class="navbar-left">
                <div class="navbar-btn">
                    <a href="<?= base_url() ?>"><img src="<?= base_url() ?>assets/images/icon-light.svg" alt="KavinFarms Logo" class="img-fluid logo"></a>
                    <button type="button" class="btn-toggle-offcanvas"><i class="lnr lnr-menu fa fa-bars"></i></button>
                </div>
                <a href="javascript:void(0);" class="icon-menu btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a>
                
                <a href="<?= base_url() ?>profile">
                    <img src="<?= base_url() ?>assets/images/user-logo.jpg" class="user-photo" alt="User Profile Picture" style="border-radius: 50%;height: 40px;margin-top: -15px;margin-left:10px;">
                </a>
                <div style="padding-left: 3px;font-weight: 800;margin-left: 10px;display: inline-block;">
                    <div class="text-white ">Welcome</div>
                    <div class="text-white "><?= $this->session->userdata( 'user_name' ) ?>  </div>
                </div>
            </div>
            <div class="navbar-center">
                <div id="navbar-menu">
                    <ul class="nav navbar-nav">
                        <li class="text-cyan font-weight-bold">
                            <div class="text-dark">
                                Financial Year
                            </div>
                            <div style="color: #fb1865;">
                                1st Apr 2021 - 31st Mar 2022
                            </div>
                        </li>
                        <li class="dropdown dropdown-animated scale-left">
                            <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                                <i style="color:green;font-size:20px;"class="icon-arrow-down"></i>
                                
                            </a>
                              <ul class="dropdown-menu feeds_widget ">
                                <div class="not-body scroll1" style="width:250px;height:150px; overflow-y:scroll;">
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="feeds-body">
                                                <h4 class="title text-success">
                                                    1st Apr 2019 - 31st Mar 2020
                                                </h4>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="feeds-body">
                                                <h4 class="title text-success">
                                                    1st Apr 2020 - 31st Mar 2021
                                                </h4>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="feeds-body">
                                                <h4 class="title text-success">
                                                    1st Apr 2021 - 31st Mar 2022
                                                </h4>
                                            </div>
                                        </a>
                                    </li>
                               </div>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="navbar-right">

                <div id="navbar-menu">
                    <ul class="nav navbar-nav">
                        
                        <li class="nav-mob-web-li" style="margin-right: 20px;font-weight: 800;">
                            <div class="text-white">
                                <?php

                                date_default_timezone_set('America/New_York');
                                $est = date( 'h:i A d-M-Y' ).' America ';
                                
                                date_default_timezone_set('Asia/Kolkata');
                                $ist = date( 'h:i A d-M-Y' ). ' India';
                                ?>
                                <span style="color: #de4a1f!important">
                                    <?= $est ?>
                                </span>
                                <br>
                                <span class="text-primary">
                                    <?= $ist ?>
                                </span>
                            </div>
                        </li>
                        <li class="font-weight-bold nav-mob-web-li">
                          <a href="<?= base_url()?>auth/logout" class="icon-menu btn btn-sm btn-danger text-white">
                            <i class="icon-power text-white"></i> &nbsp;<b> Sign Out</b>
                          </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <style type="text/css">
        .variations tbody {
            display: inline-flex;
        }
        td.label {
            background: gray;
            color: white;
            padding: 4px;
        }
        
    </style>