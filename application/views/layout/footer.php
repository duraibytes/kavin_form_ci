	</div>
	<script type="text/javascript">
		function are_you_sure( function_name, title, message = 'Are you sure want to delete?', arg, arg_2, arg_3 ) {
            
            if ( typeof arg === 'undefined' ) {
                var arg = '';
            }
            if ( arg_2 === undefined ) {
                var arg_2   = '';
            }
            if ( arg_3 === undefined ) {
                var arg_3   = '';
            }

            var continue_proceess = 0;
            $( '#are_you_sure' ).modal();
            $( '#are_you_sure .modal-body' ).html( '<div align="center"><p>' + message + '</p></div>' );

            $( '#are_you_sure .modal-body' ).append( '<br /><br /><div align="center"><button class="btn btn-info btn-sm" id="continuemodal" data-dismiss="modal">Yes</button>&nbsp;&nbsp;<button aria-hidden="true" data-dismiss="modal" class="btn btn-danger btn-sm">Cancel</button></div>' );
            $( '#are_you_sure .modal-title' ).text( title );
            $( '#continuemodal' ).click( function() {
                if( arg_2 == '' && arg_3 == '' ) {
                    eval( function_name + '(' + arg +')' );
                } else if ( arg_2 != '' && arg_3 == '' ) {
                    eval( function_name + '(' + arg +', \'' + arg_2 + '\' )' );
                } else {
                    eval( function_name + '(' + arg +', \'' + arg_2 + '\', \'' + arg_3 + '\' )' );
                }
            });
            return false;
        }

        function open_add_module_form( id='', value = '', slug = '' ) {
          var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&value=' + value +'&id=' + id + '&slug=' + slug;
          $.ajax( {
              type    : "POST",
              url     : '<?= site_url( 'settings/open_add_module_form' ); ?>',
              data    : data,
              success: function( msg ) {
                  $( '#defaultModal' ).modal();
                  $( '#defaultModal' ).html( msg );
              }
          });
          return false;
      }

      function delete_module( id, slug = '' ) {
        var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&id=' + id + '&slug='+slug;
        $.ajax( {
            type    : "POST",
            url     : '<?= site_url( 'settings/delete_module' ); ?>',
            data    : data,
            dataType: 'json',
            success: function( msg ) {
              toastr.success( 'Deleted Successfully', 'delete' );
              setTimeout( function(){
                if( msg.slug ){
                  window.location.href = '<?= site_url()?>options/'+msg.slug;
                } else {
                  window.location.href = '<?= site_url()?>options';
                }
              }, 300 );
              
            }
        });
        return false;
      }

      function PrintMydetailReprint( divName ) {
        /*var w = window.open();
        w.document.write( $( '#' + divName ).html() );
        w.print();
        w.close();   
        return false;*/
        var printContents     = document.getElementById( divName ).innerHTML;
        var originalContents  = document.body.innerHTML;
        var myWindow          = window.open( "", "printwindow" );
        myWindow.document.write( printContents );
        myWindow.print();
        myWindow.close();
      }

      function print_purchase( slug ) {
        var data   = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&slug=' + slug;
        $.ajax({
            type    : 'POST',
            url     : '<?=site_url( 'purchase/print_purchase' );?>',
          data    : data,
          success : function( msg ) {
            $( '#printableArea' ).html( msg );
            setTimeout(function(){
              PrintMydetailReprint( 'printableArea' );  
            }, 500 );
          }
        });    
      }

      function print_purchase_return( slug ) {
        var data   = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&slug=' + slug;
        $.ajax({
            type    : 'POST',
            url     : '<?=site_url( 'purchase/print_purchase_return' );?>',
          data    : data,
          success : function( msg ) {
            $( '#printableArea' ).html( msg );
            setTimeout(function(){
              PrintMydetailReprint( 'printableArea' );  
            }, 500 );
          }
        });    
      }

      function print_purchase_payment( slug ) {
        var data   = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&slug=' + slug;
        $.ajax({
            type    : 'POST',
            url     : '<?=site_url( 'purchase/print_purchase_payment' );?>',
          data    : data,
          success : function( msg ) {
            $( '#printableArea' ).html( msg );
            setTimeout(function(){
              PrintMydetailReprint( 'printableArea' );  
            }, 500 );
          }
        });    
      }
      
       
      function get_bank_form( class_name = ''  ) {
        
        var data      = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&class_name='+class_name;
        $.ajax( {
            type    : "POST",
            url     : '<?= site_url( 'settings/open_add_bank_form' ); ?>',
            data    : data,
            success: function( msg ) {
                $( '#defaultModal' ).modal();
                $( '#defaultModal' ).html( msg );
            }
        });
        return false;
      }

      function get_bank_branch_form( class_name = ''  ) {
        var bank_id    = $( '#bank' ).val();
        if( bank_id == '' || bank_id == undefined ) {
          
            toastr.error( 'Select Bank First', 'Bank Error' );
            $( '#bank' ).focus();
            return false;
          
          bank_id     = '';
        }
        var data      = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&class_name='+class_name+'&bank_id='+bank_id;
        $.ajax( {
            type    : "POST",
            url     : '<?= site_url( 'settings/open_add_bank_form' ); ?>',
            data    : data,
            success: function( msg ) {
                $( '#defaultModal' ).modal();
                $( '#defaultModal' ).html( msg );
            }
        });
        return false;
      }

      function get_bank_branch( bank_id = '', class_name = '', own_bank_id = ''  ) {
          var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&bank_id='+bank_id+'&class_name='+class_name+'&own_bank_id='+own_bank_id;
          $.ajax( {
              type    : "POST",
              url     : '<?= site_url( 'settings/get_bank_branch' ); ?>',
              data    : data,
              dataType: 'json',
              success: function( msg ) {
                if( msg.view ) {
                  $( '.'+msg.class_name ).html( msg.view );  
                }
                
              }
          });
          return false;
      }

      function get_ifsc( branch_id ='' ) {
        var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&branch_id=' + branch_id;
        $.ajax( {
            type    : "POST",
            url     : '<?= site_url( 'settings/get_ifsc' ); ?>',
            data    : data,
            dataType: 'json',
            success: function( msg ) {
              $( '#ifsc' ).val( msg.ifsc );
            }
        });
        return false;
    }

    function get_address_form( id='', from = '', action = '' ) {
      var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&id=' + id + '&from=' + from + '&action='+action;
      $.ajax( {
          type    : "POST",
          url     : '<?= site_url( 'company/get_address_form' ); ?>',
          data    : data,
          success: function( msg ) {
              $( '#defaultModal' ).modal();
              $( '#defaultModal' ).html( msg );
          }
      });
      return false;
    }

    function add_farmer( class_name = ''  ) {
        
        var data      = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&class_name='+class_name;
        $.ajax( {
            type    : "POST",
            url     : '<?= site_url( 'sale/add_farmer' ); ?>',
            data    : data,
            success: function( msg ) {
                $( '#defaultModal' ).modal();
                $( '#defaultModal' ).html( msg );
            }
        });
        return false;
      }

    function clear_form( id ) {
      
       document.getElementById( id ).reset();
       
       $( '#amount_in_word' ).html( '' );
    }

    var a = ['','one ','two ','three ','four ', 'five ','six ','seven ','eight ','nine ','ten ','eleven ','twelve ','thirteen ','fourteen ','fifteen ','sixteen ','seventeen ','eighteen ','nineteen '];
  var b = ['', '', 'twenty','thirty','forty','fifty', 'sixty','seventy','eighty','ninety'];

  function inWords (num) {

      if ((num = num.toString()).length > 9) return 'overflow';
      n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
      if (!n) return; var str = '';
      str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'crore ' : '';
      str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'lakh ' : '';
      str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'thousand ' : '';
      str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'hundred ' : '';
      str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) + 'only ' : '';

      return str;
  }


  // 

  var th = ['','thousand','million', 'billion','trillion'];
// uncomment this line for English Number System
// var th = ['','thousand','million', 'milliard','billion'];

var dg = ['zero','one','two','three','four', 'five','six','seven','eight','nine']; var tn = ['ten','eleven','twelve','thirteen', 'fourteen','fifteen','sixteen', 'seventeen','eighteen','nineteen']; var tw = ['twenty','thirty','forty','fifty', 'sixty','seventy','eighty','ninety']; function toWords(s){s = s.toString(); s = s.replace(/[\, ]/g,''); if (s != parseFloat(s)) return 'not a number'; var x = s.indexOf('.'); if (x == -1) x = s.length; if (x > 15) return 'too big'; var n = s.split(''); var str = ''; var sk = 0; for (var i=0; i < x; i++) {if ((x-i)%3==2) {if (n[i] == '1') {str += tn[Number(n[i+1])] + ' '; i++; sk=1;} else if (n[i]!=0) {str += tw[n[i]-2] + ' ';sk=1;}} else if (n[i]!=0) {str += dg[n[i]] +' '; if ((x-i)%3==0) str += 'hundred ';sk=1;} if ((x-i)%3==1) {if (sk) str += th[(x-i-1)/3] + ' ';sk=0;}} if (x != s.length) {var y = s.length; str += 'point '; for (var i=x+1; i<y; i++) str += dg[n[i]] +' ';} return str.replace(/\s+/g,' ');}
    
    function add_form( id, modules = '' ) {
        var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&id=' + id + '&module='+ modules;
        $.ajax( {
            type    : "POST",
            url     : '<?= site_url( 'settings/add_form' ); ?>',
            data    : data,
            success: function( msg ) {
                $( '#defaultModal' ).modal();
                $( '#defaultModal' ).html( msg );
            }
        });
        return false;
    }

    function open_add_transport_form( id='', class_name = '' ) {
          var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&id=' + id + '&class_name='+class_name;
          $.ajax( {
              type    : "POST",
              url     : '<?= site_url( 'settings/open_add_transport_form' ); ?>',
              data    : data,
              success: function( msg ) {
                  $( '#defaultModal' ).modal();
                  $( '#defaultModal' ).html( msg );
              }
          });
          return false;
    }


    function open_add_product_form( count = '' ) {
          var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&count=' + count;
          $.ajax( {
              type    : "POST",
              url     : '<?= site_url( 'product/open_add_product_form' ); ?>',
              data    : data,
              success: function( msg ) {
                  $( '#defaultModal' ).modal();
                  $( '#defaultModal' ).html( msg );
              }
          });
          return false;
    }
    // product purchase items rate calculation
    function get_item_rate( value, count, qty_check = '' ) {
      

      var rate            = $( '#rate_'+count ).val();
      var qty             = $( '#qty_'+count ).val();
      var discount        = $( '#discount_'+count ).val();

      if( isNaN( qty ) || qty == '' ) {
        qty               = 1;
      }
      rate                = parseFloat( rate );
      qty                 = parseInt( qty );
      discount            = parseInt( discount );
      
      //check qty has stock
      if( qty_check ){
        var product_id    = $( '#product_id_'+count ).val();
        var check         = check_product_instock( product_id, count, qty );
        
      }

      var net_rate        = rate * qty;
      net_rate            = parseFloat( net_rate );
      if( isNaN( discount ) || discount == '' ) {
        total_net_price   = net_rate;
      } else {
        total_net_price   = net_rate - (net_rate * (discount / 100));
      }
      
      if( isNaN( total_net_price ) ) {
        total_net_price   = 0;
      }
      $( '#net_value_'+count ).val( total_net_price );
      get_total_rate( 'item_rate' );
    }

    function get_total_rate( class_name ) {
      var tot = 0;
      $( "."+class_name ).each(function() {
        
        let price = parseFloat( $(this).val());
        if( isNaN(price) ) {
          price   = 0
        }
        tot += price;
      });

      // set total purchae item amont
      $( '#total_item_amount' ).val( tot );
      // 
      var transport_charge  = $( '#transport_charge' ).val();
      var t_charge          = 0;
      console.log( transport_charge );
      if( transport_charge != '' || transport_charge != 'undefined' ) {
        t_charge            = transport_charge;
      } else {
        t_charge            = 0;
      }
      console.log( t_charge, 't_charge')
      if( isNaN( t_charge ) || t_charge == '' ) {
        t_charge            = 0;
      }
      t_charge              = parseFloat( t_charge );
      tot                   = parseFloat( tot ) + t_charge;
      console.log( 'tot', tot );
      $( '#total_amount' ).val( tot );
      if( tot ) {
        var words = inWords( tot );
      }
      
      $( '#amount_in_word' ).html( words );
      $( '#amount_in_word' ).css( 'text-transform', 'uppercase' );
    }

    function check_product_instock( product_id, count, qty = '' ) {
      var data        = '<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>&product_id=' + product_id + '&count=' +count + '&qty=' + qty;
        $.ajax( {
            type    : "POST",
            url     : '<?= site_url( 'product/get_product_instock' ); ?>',
            data    : data,
            dataType: 'json',
            success: function( msg ) {
              if( msg.balance_qty == 0 ) {
                toastr.error( 'product has no stock, please purchase to sale', 'Sale' );
                $( '#qty_'+count ).val( '' );
                return false;
              }
              
              if( msg.balance_qty ) {
                if( msg.qty ) {
                  
                  if(msg.balance_qty < msg.qty ) {
                    toastr.error( 'product qty greater than stock qty, please stock qty', 'Sale' );
                    $( '#qty_'+count ).val( '' );
                    return false;
                  } else {

                  }

                }
                
              }
            }
        });
        
      }
	</script>
		<div class="modal" id="are_you_sure" data-backdrop="static" data-keyboard="false">
	        <div class="modal-dialog">
	            <div class="modal-content">
	                <div class="modal-header text-center">
	                    <h4 class="modal-title"></h4>
	                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
	                </div>
	                <div class="modal-body"></div>
	            </div>
	        </div>
	    </div>
        <div class="modal fade" id="baseModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: auto;">
          <div class="modal-dialog">
             <div class="modal-content">
                 <div class="modal-header">
                    <h4 class="modal-title" id="modalTitle"></h4>
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      
                 </div>
                 <div class="modal-body" id="modalBody"><div class="te"></div></div>
             </div>
             <!-- /.modal-content -->
          </div>
        <!-- /.modal-dialog -->
        </div>

        <!-- Default Size -->
<div class="modal" id="defaultModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="defaultModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary">SAVE CHANGES</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>
	    <style type="text/css">
		    #are_you_sure .modal-dialog { z-index: 10081; }
		</style>

    <!-- Javascript -->
    

    <script src="<?= base_url() ?>assets/bundles/libscripts.bundle.js"></script>    
    <script src="<?= base_url() ?>assets/bundles/vendorscripts.bundle.js"></script>

    <script src="<?= base_url() ?>assets/bundles/morrisscripts.bundle.js"></script><!-- Morris Plugin Js -->
    <script src="<?= base_url() ?>assets/bundles/chartist.bundle.js"></script>
    <script src="<?= base_url() ?>assets/bundles/jvectormap.bundle.js"></script><!-- JVectorMap Plugin Js -->

    <script src="<?= base_url() ?>assets/bundles/mainscripts.bundle.js"></script>
    <script src="<?= base_url() ?>assets/js/widgets/infobox/infobox-1.js"></script>
    <script src="<?= base_url() ?>assets/js/pages/blog.js"></script>

    <script src="<?= base_url() ?>assets/bundles/datatablescripts.bundle.js"></script>
    <script src="<?= base_url() ?>assets/vendor/jquery-datatable/buttons/dataTables.buttons.min.js"></script>
    <script src="<?= base_url() ?>assets/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
    <script src="<?= base_url() ?>assets/vendor/jquery-datatable/buttons/buttons.colVis.min.js"></script>
    <script src="<?= base_url() ?>assets/vendor/jquery-datatable/buttons/buttons.html5.min.js"></script>
    <script src="<?= base_url() ?>assets/vendor/jquery-datatable/buttons/buttons.print.min.js"></script>

    <script src="<?= base_url() ?>assets/js/pages/tables/jquery-datatable.js"></script>
    <script src="<?= base_url() ?>assets/js/pages/bootstrap3-typeahead.min.js"></script>

    <script src="<?= base_url() ?>assets/vendor/dropify/js/dropify.min.js"></script>
    <script src="<?= base_url() ?>assets/js/pages/forms/dropify.js"></script>


    <script src="<?= base_url() ?>assets/vendor/multi-select/js/jquery.multi-select.js"></script>
    <!-- Multi Select Plugin Js -->
    <script src="<?= base_url() ?>assets/js/select2.min.js"></script>

    <script src="<?= base_url() ?>assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>

    <script src="<?= base_url() ?>assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script src="<?= base_url() ?>assets/vendor/toastr/toastr.js"></script>

	</body>
</html>