<?php $this->load->view(  'layout/header' ); ?>

<?php $this->load->view(  'layout/navigation' ); ?>
<?php $this->load->view(  'layout/_side_bar' ); ?>


    <div id="main-content">
      	<?php echo $content; ?>
   	</div>
   <!-- END Main section-->
<?php $this->load->view(  'layout/footer' ); ?>   