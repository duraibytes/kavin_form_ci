<?php
    $slug           = $this->uri->segment( 1 );
    global $setting_menu, $purchase_menu, $sale_menu;
    $company_info           = $this->Common_model->find( 'owner', array( 'id' => $this->session->userdata( 'company_id' ) ) );
    
?>
<div id="left-sidebar" class="sidebar">
    <div class="navbar-brand text-center"> 
        <a href="<?= base_url()?>"> 
            <?php
                if( $this->session->has_userdata( 'company_id' ) && $this->session->userdata( 'company_id' ) == 1  ) {
                    if( isset( $company_info->image ) && !empty( $company_info->image ) ) { ?>
                        <img src="<?= $company_info->image ?>" alt="KavinFarms Logo" class="img-fluid logo" style="width:100px;">
            <?php } else { 
            ?>
            <img src="<?= base_url() ?>assets/images/logo.svg" alt="KavinFarms Logo" class="img-fluid logo" style="width: 100px;">
        <?php } } else {
                
                if( isset( $company_info->image ) && !empty( $company_info->image ) ) {
                        ?>
                    <img src="<?= $company_info->image ?>" alt="HoneyBee Logo" class="img-fluid logo" style="width:100px;">
                    
        <?php   } else  {
        ?>
              
            <img src="<?= base_url() ?>assets/images/honeybee.png" alt="HoneyBee Logo" class="img-fluid logo" style="width:100px;">
        <?php }  } ?>
        </a>
        <button type="button" class="btn-toggle-offcanvas btn btn-sm btn-default float-right"><i class="lnr lnr-menu fa fa-chevron-circle-left"></i></button>
    </div>
    <div class="sidebar-scroll">
        <nav id="left-sidebar-nav" class="sidebar-nav">
            <ul id="main-menu" class="metismenu">
                <li class="<?= ( $slug == '' ) ? 'active' : '' ?>">
                    <a href="<?= base_url() ?>"><i class="icon-home"></i><span>Dashboard</span></a>
                </li>
               
                <?php 
                    $ul_active          = '';
                    if( in_array( $slug, $purchase_menu ) ) {
                        $ul_active      = 'active';
                    }

                ?>
                <li class="<?= $ul_active ?>">
                    <a href="#uiElements" class="has-arrow">
                        <i class="fa fa-shopping-cart"></i>
                        <span> Purchase </span>
                    </a>
                    <ul class="collapse">
                        <li class="<?= ( $slug == 'purchase' ) ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>purchase"> Purchase Invoices </a>
                        </li>
                        <li class="<?= ( $slug == 'purchase-payment' || $slug == 'purchase-payment-add' ) ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>purchase-payment"> Purchase Payments </a>
                        </li>
                        <li class="<?= ( $slug == 'purchase-return' || $slug == 'purchase-return-add' ) ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>purchase-return"> Purchase Returns </a>
                        </li>
                    </ul>
                </li>
                <?php 
                    $ul_active          = '';
                    if( in_array( $slug, $sale_menu ) ) {
                        $ul_active      = 'active';
                    }

                ?>
                <li class="<?= $ul_active ?>">
                    <a href="#forms" class="has-arrow">
                        <i class="fa fa-shopping-cart"></i>
                        <span> Sale </span>
                    </a>
                    <ul>
                        <li class="<?= ( $slug == 'sale' ) ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>sale"> Sales Invoices </a>
                        </li>
                        <li class="<?= ( $slug == 'sale-payment' || $slug == 'sale-payment-add' ) ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>sale-payment"> Sales Payments </a>
                        </li>
                        <li class="<?= ( $slug == 'sale-return' || $slug == 'sale-return-add' ) ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>sale-return"> Sales Returns </a>
                        </li>
                    </ul>
                </li>
                <li class="<?= ( $slug == 'transaction' ) ? 'active' : '' ?>">
                    <a href="<?= base_url() ?>transaction"><i class="fa fa-inr"></i><span> Bank Transaction </span></a>
                </li>
                 <li class="<?= ( $slug == 'stock' ) ? 'active' : '' ?>">
                    <a href="<?= base_url() ?>stock"><i class="icon-diamond"></i><span>Stock</span></a>
                </li>
                <li class="<?= ( $slug == 'expense' ) ? 'active' : '' ?>">
                    <a href="<?= base_url()?>expense"><i class="fa fa-university"></i><span> Expenses </span></a>
                </li>
                <li class="<?= ( $slug == 'report' ) ? 'active' : '' ?>">
                    <a href="<?= base_url() ?>report"><i class="fa fa-file-text-o"></i>
                        <span>Reports </span>
                    </a>
                </li>
                <?php 
                    $ul_active          = '';
                    $in                 = '';

                    if( in_array( $slug, $setting_menu ) ) {
                        $ul_active      = 'active';
                    }

                ?>
                <li class="<?= $ul_active ?>">
                    <a href="#Tables" class="has-arrow"><i class="icon-settings"></i>
                        <span> Settings </span>
                    </a>
                    <ul class="collapse <?= $in ?>">

                       
                        <li class="<?= ( $slug == 'company' ) ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>company"> Company </a>
                        </li>
                        <li class="<?= ( $slug == 'company-settings' ) ? 'active' : '' ?>">
                            <a href="<?= base_url() ?>company-settings"> My Company Settings </a>
                        </li>
                        <li class="<?= ( $slug == 'customer' ) ? 'active' : '' ?>" >
                            <a href="<?= base_url() ?>customer"> Dealer / Farmer </a>
                        </li>
                        <li class="<?= ( $slug == 'employee' ) ? 'active' : '' ?>" >
                            <a href="<?= base_url() ?>employee"> Employee </a>
                        </li>
                        <li class="<?= ( $slug == 'options' ) ? 'active' : '' ?>" >
                            <a href="<?= base_url() ?>options"> Modules </a>
                        </li>
                        <li class="<?= ( $slug == 'permission' ) ? 'active' : '' ?>" >
                            <a href="<?= base_url() ?>permission"> Permission </a>
                        </li>
                        <li class="<?= ( $slug == 'product' ) ? 'active' : '' ?>" >
                            <a href="<?= base_url() ?>product"> Products </a>
                        </li>
                       
                        <li class="<?= ( $slug == 'transport' ) ? 'active' : '' ?>" >
                            <a href="<?= base_url() ?>transport"> Transport </a>
                        </li>
                         <li class="<?= ( $slug == 'bank' ) ? 'active' : '' ?>">
                            <a href="<?= base_url()?>bank"> Bank </a>
                        </li>
                    </ul>
                </li>
               
            </ul>
        </nav>     
    </div>
</div>