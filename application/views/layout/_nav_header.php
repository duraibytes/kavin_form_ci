<div class="row clearfix">
    <div class="col-md-8 col-sm-12 text-left">
        <ul class="breadcrumb" style="text-transform: lowercase;font-size: 12px;">
            <li class="breadcrumb-item"><a href="<?= base_url()?>"><i class="icon-home"></i> Home</a></li>
            <?= $breadcrum ?>
            
        </ul>
        
    </div>
    <div class="col-md-4 col-sm-12 text-right">
        <?= $anchor_btn ?>
    </div>            
</div>