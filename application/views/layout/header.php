
<!doctype html>
<html lang="en">

	<head>
		<title>KavinFarm :: <?= $title ?? '' ?></title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
		<meta name="description" content="Kavin Farms Admin Template">
		<meta name="author" content="kavin farms, breaktalks.in">

		<link rel="icon" href="favicon.ico" type="image/x-icon">
		<!-- VENDOR CSS -->
		<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/jvectormap/jquery-jvectormap-2.0.3.min.css"/>
		<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"> 

		<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/chartist/css/chartist.min.css">
		<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
		<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/morrisjs/morris.css" />

		<!-- MAIN CSS -->
		<link rel="stylesheet" href="<?= base_url() ?>assets/css/main.css">
		<link rel="stylesheet" href="<?= base_url() ?>assets/css/color_skins.css">
		<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/dropify/css/dropify.min.css">

		<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css">
		<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css">
		<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css">
		<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/toastr/toastr.min.css">
		<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/multi-select/css/multi-select.css">

		<link href="<?= base_url() ?>assets/css/select2.min.css" rel="stylesheet" />

		<script src="<?= base_url() ?>assets/js/jqureyv3-5-1.js"></script>
		<style type="text/css">
			.dataTables_filter {
				float: right;
			}
			.pagination {
				float: right;
			}

			.navbar {
				transform: translateY(-63px);
    			transition: all 0s;
			}
			.navbar:hover {
				transform: translateY(0px) !important;
			}
			.navbar:hover~#main-content{
			    margin-top: 65px !important;
			}
			#main-content {
				margin-top: 0px !important;
			}
			@media only screen and (min-device-width: 360px) and (max-device-width: 360px) {
				.dataTables_filter {
					float: right;
				}
				.pagination {
					float: right;
				}

				.navbar {
					transform: translateY(-211px);
	    			transition: all 0s;
				}
				.navbar:hover {
					transform: translateY(0px) !important;
				}
				.navbar:hover~#main-content{
				    margin-top: 225px !important;
				}
				#main-content {
					margin-top: 18px !important;
				}	
				.navbar-left {
					width: 100%;
					text-align: center;
				}
				.navbar-center {
					width: 100%;
					text-align: center;
				}
				.navbar-menu {
					width: 100%;
					text-align: center;
				}
				.navbar-nav {
					width: 100%;
				}
			}
			@media only screen and (min-device-width: 361px) and (max-device-width: 410px) {
				.dataTables_filter {
					float: right;
				}
				.pagination {
					float: right;
				}

				.navbar {
					transform: translateY(-211px);
	    			transition: all 0s;
				}
				.navbar:hover {
					transform: translateY(0px) !important;
				}
				.navbar:hover~#main-content{
				    margin-top: 225px !important;
				}
				#main-content {
					margin-top: 18px !important;
				}	
				.navbar-left {
					width: 100%;
					text-align: center;
				}
				.navbar-center {
					width: 100%;
					text-align: center;
				}
				.navbar-menu {
					width: 100%;
					text-align: center;
				}
				.navbar-nav {
					width: 100%;
				}
			}
			@media only screen and (min-device-width: 411px) and (max-device-width: 411px) {
				.dataTables_filter {
					float: right;
				}
				.pagination {
					float: right;
				}

				.navbar {
					transform: translateY(-211px);
	    			transition: all 0s;
				}
				.navbar:hover {
					transform: translateY(0px) !important;
				}
				.navbar:hover~#main-content{
				    margin-top: 225px !important;
				}
				#main-content {
					margin-top: 18px !important;
				}	
				.navbar-left {
					width: 100%;
					text-align: center;
				}
				.navbar-center {
					width: 100%;
					text-align: center;
				}
				.navbar-menu {
					width: 100%;
					text-align: center;
				}
				.navbar-nav {
					width: 100%;
				}
				
			}
			@media only screen and (min-device-width: 320px) and (max-device-width: 320px) {
				.dataTables_filter {
					float: right;
				}
				.pagination {
					float: right;
				}

				.navbar {
					transform: translateY(-211px);
	    			transition: all 0s;
				}
				.navbar:hover {
					transform: translateY(0px) !important;
				}
				.navbar:hover~#main-content{
				    margin-top: 211px !important;
				}
				#main-content {
					margin-top: 0px !important;
				}
				.navbar-left {
					width: 100%;
					text-align: center;
				}
				.navbar-center {
					width: 100%;
					text-align: center;
				}
				.navbar-menu {
					width: 100%;
					text-align: center;
				}
				.navbar-nav {
					width: 100%;
				}	
			}
			
			@media only screen and (min-device-width: 414px) and (max-device-width: 414px) {
				.dataTables_filter {
					float: right;
				}
				.pagination {
					float: right;
				}

				.navbar {
					transform: translateY(-211px);
	    			transition: all 0s;
				}
				.navbar:hover {
					transform: translateY(0px) !important;
				}
				.navbar:hover~#main-content{
				    margin-top: 145px !important;
				}
				#main-content {
					margin-top: 18px !important;
				}	
				.navbar-left {
					width: 100%;
					text-align: center;
				}
				.navbar-center {
					width: 100%;
					text-align: center;
				}
				.navbar-menu {
					width: 100%;
					text-align: center;
				}
				.navbar-nav {
					width: 100%;
				}
				
			}
			@media only screen and (min-device-width: 768px) and (max-device-width: 768px) {
			.dataTables_filter {
				float: right;
			}
			.pagination {
				float: right;
			}

			.navbar {
				transform: translateY(-90px);
    			transition: all 0s;
			}
			.navbar:hover {
				transform: translateY(0px) !important;
			}
			.navbar:hover~#main-content{
			    margin-top: 105px !important;
			}
			#main-content {
				margin-top: 0px !important;
			}	
				
				
			}
			@media only screen and (min-device-width: 540px) and (max-device-width: 540px) {
			.dataTables_filter {
				float: right;
			}
			.pagination {
				float: right;
			}

			.navbar {
				transform: translateY(-211px);
    			transition: all 0s;
			}
			.navbar:hover {
				transform: translateY(0px) !important;
			}
			.navbar:hover~#main-content{
			    margin-top: 225px !important;
			}
			#main-content {
				margin-top: 18px !important;
			}	
				
				
			}
			@media only screen and (min-device-width: 280px) and (max-device-width: 280px) {
			.dataTables_filter {
				float: right;
			}
			.pagination {
				float: right;
			}

			.navbar {
				transform: translateY(-225px);
    			transition: all 0s;
			}
			.navbar:hover {
				transform: translateY(0px) !important;
			}
			.navbar:hover~#main-content{
			    margin-top: 250px !important;
			}
			#main-content {
				margin-top: 18px !important;
			}	
				
				
			}
			@media only screen and (min-device-width: 425px) and (max-device-width: 425px) {
			.dataTables_filter {
				float: right;
			}
			.pagination {
				float: right;
			}

			.navbar {
				transform: translateY(-211px);
    			transition: all 0s;
			}
			.navbar:hover {
				transform: translateY(0px) !important;
			}
			.navbar:hover~#main-content{
			    margin-top: 225px !important;
			}
			#main-content {
				margin-top: 18px !important;
			}	
				
				
			}
			@media only screen and (min-device-width: 375px) and (max-device-width: 375px) {
				.dataTables_filter {
					float: right;
				}
				.pagination {
					float: right;
				}

				.navbar {
					transform: translateY(-211px);
	    			transition: all 0s;
				}
				.navbar:hover {
					transform: translateY(0px) !important;
				}
				.navbar:hover~#main-content{
				    margin-top: 225px !important;
				}
				#main-content {
					margin-top: 18px !important;
				}	
					
					
			}
			@media only screen and (min-device-width: 604px) and (max-device-width: 604px) {
				.dataTables_filter {
					float: right;
				}
				.pagination {
					float: right;
				}

				.navbar {
					transform: translateY(-211px);
	    			transition: all 0s;
				}
				.navbar:hover {
					transform: translateY(0px) !important;
				}
				.navbar:hover~#main-content{
				    margin-top: 225px !important;
				}
				#main-content {
					margin-top: 0px !important;
				}	
			}
			@media (max-width: 640px) {
				.nav-mob-web-li {
				    font-weight: 800 !important;
				    width: 100% !important;
				    text-align: center !important;
				    padding: 5px !important;
				}
				.nav-mob-web-li:hover {
				    background: #eaeac47d;
				}
			}

			.toastr-center {
			    top: 15%;
			    left:50%;
			    margin:0 0 0 -150px;
			}

			@media only screen and (max-device-width: 980px) {
				.input-group-text {
    				padding: none !important;
    			}
    			.input-group { 
    				flex-wrap: nowrap !important;
    			}

			}
			.select2-container {
				width: 100% !important;
			}
		</style>
	</head>
	<body class="theme-green">
		<?php
		
	?>