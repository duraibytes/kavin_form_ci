<?= $this->load->view( 'auth/layout/header', '', true ) ?>
<div id="wrapper" class="auth-main">
        <div class="container">
            <div class="row clearfix">
                <div class="col-12">
                    <!-- <nav class="navbar navbar-expand-lg">
                        <a class="navbar-brand" href="javascript:void(0);"><img src="<?= base_url()?>assets/images/logo.png" width="160" height="80" class="align-top mr-2" alt=""></a>

                    </nav>   -->                  
                </div>
                <!-- <div class="col-lg-7" >
                    <div class="auth_detail">
                       
                        
                    </div>
                </div> -->
                <div class="col-lg-4 col-md-4 col-sm-1"></div>
                 <div class="col-lg-5">
                    <div class="card" >
                        <?= $content ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?= $this->load->view( 'auth/layout/footer', '', true ) ?>