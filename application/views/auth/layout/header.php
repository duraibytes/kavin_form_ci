<!doctype html>
<html lang="en">

<head>
	<title>KavinFarms - Login</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta name="description" content="Kavin Farms Admin Template">
	<meta name="author" content="KavinFarms, breaktalks.in">

	<link rel="icon" href="favicon.ico" type="image/x-icon">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="<?= base_url()?>assets/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?= base_url()?>assets/vendor/font-awesome/css/font-awesome.min.css">

	<!-- MAIN CSS -->
	<link rel="stylesheet" href="<?= base_url()?>assets/css/main.css">
	<link rel="stylesheet" href="<?= base_url()?>assets/css/color_skins.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>

<body class="theme-orange">
    <div id="bg">
	  <img src="<?= base_url() ?>assets/images/land.jpg" alt="">
	</div>