<!-- <!DOCTYPE html>
<html>
<head>
	<title> Register </title>
</head>
<body>
	<div >
		<?php echo validation_errors(); ?>
		<div id="login_errors"></div>
		<?= form_open( '', 'id="forgot_form"') ?>
			<div>
				<input type="text" name="email" id="email" placeholder="Enter Email">
			</div>
			<div>
				<input type="button" name="login" id="" onclick="return loginValidate()" value="Reset Password" >
			</div>
		<?= form_close() ?>
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

	<script type="text/javascript">
		function loginValidate() {
	       var email      	= $( '#email' ).val();
	       var csrf        	= $( "input[name=csrf_test_name]" ).val();
	       var data        	= { email : email, csrf_test_name: csrf  }
	       $.ajax({
	           type    : 'POST',
	           url     : '<?= site_url()?>send-reset-password',
	           data    : data,
	           dataType: 'json',
	           success : function( msg ) {
	               if( msg.error == 1 ) {
	                  	$( '#login_errors' ).html( msg.error_msg );
	               } else {
	               		$( '#login_errors' ).html( msg.error_msg );
	                   	window.location.href = '<?= site_url()?>dashboard';
	               }
	           }
	       });
	       return false;
	   }
	</script>
</body>
</html> -->


  
<div class="header">
     <p style="font-weight:bold;text-align:center;font-size:150%;">Recover my password</p>
</div>
<div class="body">
    <?= form_open( '', 'id="login_form"') ?>
    <div id="login_errors" class="alert alert-danger" style="display: none;"></div>
    <div id="login_success" class="alert alert-success" style="display: none;"></div>
        <div class="form-group">
            <label for="user_name" class="control-label sr-only">Email</label>
            <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Email">
        </div>
        <button type="button" class="btn btn-primary btn-lg btn-block"  onclick="return resetPassword()">
        	Reset Password
        </button>
        <div class="bottom">
           <span class="helper-text">Know your password? <a href="<?= base_url() ?>login">Login</a></span>
        </div>
       
    <?= form_close() ?>
</div>

<script type="text/javascript">
		function loginValidate() {
	      
	       var login_form   		= $( '#login_form' ).find('input, textarea, button, select');
	       var data                	= login_form.serialize();
	       data                    	= data + '&<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>';
	       $.ajax({
	           type    : 'POST',
	           url     : '<?= site_url()?>validte-login',
	           data    : data,
	           dataType: 'json',
	           beforeSend: function() {
	           		$( '#login_errors' ).hide();
	           		$( '#login_success' ).hide();
	           },
	           success : function( msg ) {
	               if( msg.error == 1 ) {
	               		$( '#login_errors' ).show();
	                  	$( '#login_errors' ).html( msg.error_msg );
	               } else {
	               		$( '#login_success' ).show();
	               		$( '#login_success' ).html( msg.error_msg );
	                   window.location.href = '<?= site_url()?>dashboard';
	               }
	           }
	       });
	       return false;
	   }


	</script>