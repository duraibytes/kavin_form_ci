
  
<div class="header">
     <p style="font-weight:bold;text-align:center;font-size:150%;">Login to your account</p>
</div>
<div class="body">
    <?= form_open( '', 'id="login_form"') ?>
    <div id="login_errors" class="alert alert-danger" style="display: none;"></div>
    <div id="login_success" class="alert alert-success" style="display: none;"></div>
        <div class="form-group">
            <label for="user_name" class="control-label sr-only">Email</label>
            <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Email">
        </div>
        <div class="form-group">
            <label for="password" class="control-label sr-only">Password</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
        </div>
        <div class="form-group clearfix" >
        	<div class="row">
                <?php 

                if( isset( $company_info ) && !empty( $company_info) ) {
                    foreach ( $company_info as $key => $value ) {
                        
                        if( empty( $value->image ) ) {
                            if( $value->company_name == 'KAVIN FARMS' ) {
                                $image      = base_url().'assets/images/logo.png';
                            } else {
                                $image      = base_url().'assets/images/honeybee.png';
                            }
                            
                        } else {
                            $image          = $value->image;
                        }  
                        $selected           = '';
                        if( $value->is_primary == 'YES' ) {
                            $selected       = 'checked';
                        }     
                ?>
                <div class="col-lg-6" style="display: flex;align-items: center;">
                    <input name="company" value="<?= $value->id ?>" type="radio" id="<?= str_replace( ' ', "_", $value->company_name )?>" <?= $selected?>>
                    <label for="<?= str_replace( ' ', "_", $value->company_name )?>" style="padding-left: 10px;">
                        <img src="<?= $image ?>" style="width:150px;" class="align-top mr-2" alt="" for="<?= str_replace( ' ', "_", $value->company_name )?>">
                    </label>
                </div>
            <?php } } else { ?>
        		<div class="col-lg-6" style="display: flex;align-items: center;">
        			<input name="company" value="1" type="radio" id="kavin">
	        		<label for="kavin" style="padding-left: 10px;">
	        			<img src="<?= base_url() ?>assets/images/logo.png" style="width:150px;" class="align-top mr-2" alt="" for="kavin">
	        		</label>
        		</div>
        		<div class="col-lg-6" style="display: flex;align-items: center;">
        			<input name="company" id="honey" value="2" type="radio">
		        	<label for="honey" style="padding-left: 10px;">
		            	<img src="<?= base_url() ?>assets/images/honeybee.png" style="width:150px;" class="align-top mr-2" alt="">
	            	</label>
        		</div>
            <?php } ?>
        	</div>
        	
        </div>
       
        <div class="form-group clearfix" >
        	<div class="row">
        		<div class="col-lg-6" style="display: flex;align-items: center;">
        			<div class="form-group clearfix">
	                    <label class="fancy-checkbox element-left">
	                        <input type="checkbox">
	                        <span>Remember me</span>
	                    </label>								
	                </div>
        		</div>
        		<div class="col-lg-6" style="display: flex;align-items: center;">
        			<div class="form-group clearfix">
        				<span class="helper-text m-b-10"><i class="fa fa-lock"></i>
        					<a href="<?= base_url() ?>forgot-password"> Forgot password?</a></span>
        			</div>
        		</div>

        	</div>
        	
        </div>
        		
        <button type="button" class="btn btn-primary btn-lg btn-block"  onclick="return loginValidate()">LOGIN</button>
        
       
    <?= form_close() ?>
</div>

<script type="text/javascript">
		function loginValidate() {
	      
	       var login_form   		= $( '#login_form' ).find('input, textarea, button, select');
	       var data                	= login_form.serialize();
	       data                    	= data + '&<?php echo $this->security->get_csrf_token_name(); ?>=<?php echo $this->security->get_csrf_hash(); ?>';
	       $.ajax({
	           type    : 'POST',
	           url     : '<?= site_url()?>validte-login',
	           data    : data,
	           dataType: 'json',
	           beforeSend: function() {
	           		$( '#login_errors' ).hide();
	           		$( '#login_success' ).hide();
	           },
	           success : function( msg ) {
	               if( msg.error == 1 ) {
	               		$( '#login_errors' ).show();
	                  	$( '#login_errors' ).html( msg.error_msg );
	               } else {
	               		$( '#login_success' ).show();
	               		$( '#login_success' ).html( msg.error_msg );
	                   window.location.href = '<?= site_url()?>dashboard';
	               }
	           }
	       });
	       return false;
	   }


	</script>