<!DOCTYPE html>
<html>
<head>
	<title> Register </title>
</head>
<body>
	<div >
		<?php echo validation_errors(); ?>

		
		<?= form_open( 'validate-register' )?>
			<div>
				<input type="text" name="user_name" id="user_name" placeholder="User Name" value="<?= $user_name ?? '' ?>">
				<?php echo form_error('user_name', '<div class="error">', '</div>'); ?>

			</div>
			<div>
				<input type="text" name="email" id="email" placeholder="Email" value="<?= $email ?? '' ?>">
			</div>
			<div>
				<input type="text" name="mobile" id="mobile" placeholder="mobile number" value="<?= $mobile ?? '' ?>"> 
			</div>
			<div>
				<input type="password" name="password" id="password" placeholder="password">
			</div>
			<div>
				<input type="submit" name="register" id="" value="Register" >
			</div>
		</form>
		<?= form_close() ?>
	</div>
</body>
</html>