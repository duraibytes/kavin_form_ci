<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends CI_Model {

	################ All PRODUCT DETAILS SERVER SIDE DATATABLE ###############
    var $select_column 				= array( 'product.id','product.product_name', 'product.product_company', 'product.product_code', 'product.product_variety', 'product.product_category','product.description', 'product.packaging', 'product.net_purchase_price', 'product.farmer_sale_price', 'product.dealer_sale_price', 'product.retail_sale_price', 'product.slug'  );
    var $order_product_column      	= array( 'product.product_name', 'product.product_company', 'product.product_code', 'product.category', 'product.description', 'product.packaging', null );
    public function make_new_product_query() {
    	$slug 						= $this->input->post( 'slug', true );
        $this->db->select( $this->select_column );
        $this->db->from( 'product' );
       	
        if( isset( $_POST[ 'search' ][ 'value' ] ) && !empty( $_POST[ 'search' ][ 'value' ] ) ) {
            $this->db->group_start();
                $this->db->like( 'product.product_name', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'product.product_company', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'product.product_code', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'product.product_variety', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'product.product_category', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'product.description', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'product.packaging', $_POST[ 'search' ][ 'value' ] );
            $this->db->group_end();
        }
        if( isset( $_POST[ 'order' ] ) ) {
            $this->db->order_by( $this->order_product_column[ $_POST[ 'order' ][ '0' ][ 'column' ] ], $_POST[ 'order' ][ '0' ][ 'dir' ] );
        } else {
            $this->db->order_by( 'product.product_name', 'ASC' );    
        }
        
        $this->db->group_by( 'product.id' );
        
    }

    function make_product_datatables() {
        $this->make_new_product_query();
        if( isset( $_POST[ 'length' ] ) && $_POST[ 'length' ] != -1 ) {
            $this->db->limit( $_POST[ 'length' ], $_POST[ 'start' ] );
        }
        $query = $this->db->get();
        // echo $this->db->last_query();
        return $query->result();
    }

    function get_product_filtered_data() {
        $this->make_new_product_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function get_all_product_data() {
    	$slug 						= $this->input->post( 'slug', true );
        $this->db->select( $this->select_column );
        $this->db->from( 'product' );
       	
        if( isset( $_POST[ 'search' ][ 'value' ] ) && !empty( $_POST[ 'search' ][ 'value' ] ) ) {
            $this->db->group_start();
                $this->db->like( 'product.product_name', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'product.product_company', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'product.product_code', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'product.product_variety', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'product.product_category', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'product.description', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'product.packaging', $_POST[ 'search' ][ 'value' ] );
            $this->db->group_end();
        }
        if( isset( $_POST[ 'order' ] ) ) {
            $this->db->order_by( $this->order_product_column[ $_POST[ 'order' ][ '0' ][ 'column' ] ], $_POST[ 'order' ][ '0' ][ 'dir' ] );
        } else {
            $this->db->order_by( 'product.product_name', 'ASC' );    
        }

        return $this->db->count_all_results();
    }

    ################ SERVER SIDE DATATABLE ENDS HERE

    public function get_product_info( $id = '', $slug = '' ) {
    	if( $id ) {
    		$this->db->where( 'id', $id );	
    	}
    	if( $slug ) {
    		$this->db->where( 'slug', $slug );	
    	}
    	
    	$info 			= $this->db->get( 'product' );
    	if( $info->num_rows() > 0 ) {
    		return $info->row();
    	} else {
    		return false;
    	}
    }

    //Customer code list typeahead
    function typehead_product_list( $term, $status = '' ) {
        $owner_id                  = $this->session->userdata( 'company_id' );

        $this->db->group_start();
            $this->db->where( "product.product_code LIKE '%$term%'" );
            $this->db->or_where( "product.product_name LIKE '%$term%'" );
            $this->db->or_where( "product.product_variety LIKE '%$term%'" );
            $this->db->or_where( "product.product_company LIKE '%$term%'" );
        $this->db->group_end(); 
        if( isset( $owner_id ) ) {
            $this->db->where( 'product.owner_id', $owner_id ); 
        } 
        if( $status ) {
            $this->db->where( 'product.status', $status );
        }
        $query          = $this->db->get( 'product' );
        $numRows        = $query->num_rows();
        if( $numRows > 0 ) {
            return $query->result();
        } else { return false; }
    }

    function purchased_product( $product_id, $where_in = '' ) {
        $this->db->select( 'sum(inventory.qty) as qty, inventory.mode');
        if( $where_in ) {
            $this->db->where_in( 'inventory.mode', $where_in );
        }
        $this->db->where( 'inventory.product_id', $product_id );
        $this->db->group_by( 'inventory.mode' );
        $info               = $this->db->get( 'inventory' );
        if( $info->num_rows() > 0 ) {
            return $info->result();
        } else {
            return false;
        }
    }

    

}