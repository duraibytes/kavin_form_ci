<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bank_model extends CI_Model {

	################ All BANK DETAILS SERVER SIDE DATATABLE ###############
    var $table          		= 'bank';
    
    var $select_column 			= array( 'bank.id','bank.bank_name', 'bank.email', 'bank.phone', 'bank.address', 'bank.slug','bank.branch', 'bank.ifsc', 'bank.micr_code', 'bank.status', 'bank.created_at', 'COALESCE( count( bank_branch.parent_id ), 0 ) as branch_count', 'bank_branch.slug as bank_slug', 'bank_branch.bank_name as parent_bank_name'  );
    var $order_bank_column      = array( '', 'bank.bank_name', 'bank.bank_name', 'ifsc', 'address' );
    public function make_new_bank_query() {
    	$slug 						= $this->input->post( 'slug', true );
        $this->db->select( $this->select_column );
        $this->db->from( 'bank' );
        
        if( $slug ) {
        	$this->db->join( 'bank AS bank_branch', 'bank_branch.id = bank.parent_id' );
        	$this->db->where( 'bank.parent_id !=', null );
        	$this->db->where( 'bank_branch.slug', $slug );
        } else {
        	$this->db->join( 'bank AS bank_branch', 'bank_branch.parent_id = bank.id', 'left' );
        	$this->db->where( 'bank.parent_id', null );	
        }
       	
        if( isset( $_POST[ 'search' ][ 'value' ] ) && !empty( $_POST[ 'search' ][ 'value' ] ) ) {
            $this->db->group_start();
                $this->db->like( 'bank.bank_name', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'bank.email', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'bank.phone', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'bank.address', $_POST[ 'search' ][ 'value' ] );
            $this->db->group_end();
        }
        if( isset( $_POST[ 'order' ] ) ) {
            $this->db->order_by( $this->order_bank_column[ $_POST[ 'order' ][ '0' ][ 'column' ] ], $_POST[ 'order' ][ '0' ][ 'dir' ] );
        } else {
            $this->db->order_by( 'bank.bank_name', 'ASC' );    
        }
        
        $this->db->group_by( 'bank.id' );
        
    }

    function make_bank_datatables() {
        $this->make_new_bank_query();
        if( isset( $_POST[ 'length' ] ) && $_POST[ 'length' ] != -1 ) {
            $this->db->limit( $_POST[ 'length' ], $_POST[ 'start' ] );
        }
        $query = $this->db->get();
        // echo $this->db->last_query();
        return $query->result();
    }

    function get_bank_filtered_data() {
        $this->make_new_bank_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function get_all_bank_data() {
    	$slug 						= $this->input->post( 'slug', true );
        $this->db->select( $this->select_column );
        $this->db->from( 'bank' );
        if( $slug ) {
        	$this->db->join( 'bank AS bank_branch', 'bank_branch.id = bank.parent_id' );
        	$this->db->where( 'bank.parent_id !=', null );
        	$this->db->where( 'bank_branch.slug', $slug );
        } else {
        	$this->db->join( 'bank AS bank_branch', 'bank_branch.parent_id = bank.id', 'left' );
        	$this->db->where( 'bank.parent_id', null );	
        }
       
        if( isset( $_POST[ 'search' ][ 'value' ] ) && !empty( $_POST[ 'search' ][ 'value' ] ) ) {
            $this->db->group_start();
                $this->db->like( 'bank.bank_name', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'bank.email', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'bank.phone', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'bank.address', $_POST[ 'search' ][ 'value' ] );
            $this->db->group_end();
        }
        if( isset( $_POST[ 'order' ] ) ) {
            $this->db->order_by( $this->order_bank_column[ $_POST[ 'order' ][ '0' ][ 'column' ] ], $_POST[ 'order' ][ '0' ][ 'dir' ] );
        } else {
            $this->db->order_by( 'bank.bank_name', 'ASC' );    
        }

        return $this->db->count_all_results();
    }

    ################ SERVER SIDE DATATABLE ENDS HERE

    public function bank_info( $slug = '', $id = '' ) {
    	if( $id ) {
    		$this->db->where( 'id', $id );
    	} 
    	if( $slug ) {
    		$this->db->where( 'slug', $slug );	
    	}
    	
    	$info 				= $this->db->get( 'bank' );
    	if( $info->num_rows() > 0 ) {
    		return $info->row();
    	} else {
    		return false;
    	}
    }

    function get_all_bank() {
        $this->db->where( 'parent_id', null );
        $this->db->order_by( 'bank.bank_name', 'ASC' );
        $info               = $this->db->get( 'bank' );
        if( $info->num_rows() > 0 ) {
            return $info->result();
        } else {
            return false;
        }
    }

    function check_bank_branch_exists( $bank_id, $ifsc ) {
        $this->db->where( 'parent_id', $bank_id );
        $this->db->where( 'ifsc', $ifsc );
        $exists                 = $this->db->get( 'bank' );
        if( $exists->num_rows() > 0 ) {
            return $exists->row();
        } else {
            return false;
        }
    }

     function bank_branch_all( $bank_id ) {
        $this->db->where( 'parent_id', $bank_id );
        $exists                 = $this->db->get( 'bank' );
        if( $exists->num_rows() > 0 ) {
            return $exists->result();
        } else {
            return false;
        }
    }

    function own_bank_list( $owner_id, $user_type, $group_by = '', $bank_id = ''  ) {
        $this->db->where( 'own_bank_info.user_type', $user_type );
        $this->db->where( 'own_bank_info.user_id', $owner_id );
        if( $bank_id ) {
            $this->db->where( 'own_bank_info.bank_id', $bank_id );    
        }
        $this->db->where( 'own_bank_info.status', 'ACTIVE' );
        $this->db->select( 'own_bank_info.*, bank.bank_name, branch.branch' );
        $this->db->join( 'bank', 'bank.id = own_bank_info.bank_id' );
        $this->db->join( 'bank as branch', 'branch.id = own_bank_info.branch_id' );
        if( $group_by ) {
            $this->db->group_by( $group_by );
        }
        $info               = $this->db->get( 'own_bank_info' );
        // echo $this->db->last_query();
        if( $info->num_rows() > 0 ) {
            return $info->result();
        } else {
            return false;
        }
    }

    function own_bank_info( $owner_id, $user_type, $branch_id = ''  ) {
        $this->db->where( 'own_bank_info.user_type', $user_type );
        $this->db->where( 'own_bank_info.user_id', $owner_id );
        if( $branch_id ) {
            $this->db->where( 'own_bank_info.branch_id', $branch_id );    
        }
        $this->db->where( 'own_bank_info.status', 'ACTIVE' );
        $this->db->select( 'own_bank_info.*, bank.bank_name, branch.branch' );
        $this->db->join( 'bank', 'bank.id = own_bank_info.bank_id' );
        $this->db->join( 'bank as branch', 'branch.id = own_bank_info.branch_id' );
        $info               = $this->db->get( 'own_bank_info' );
        // echo $this->db->last_query();
        if( $info->num_rows() > 0 ) {
            return $info->row_array();
        } else {
            return false;
        }
    }

}