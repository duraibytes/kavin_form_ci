<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sale_model extends CI_Model {

	################ All SALE DETAILS SERVER SIDE DATATABLE ###############
   
    var $order_purchase_column      	= array( 'purchase.purchase_name', 'purchase.purchase_company', 'purchase.purchase_code', 'purchase.category', 'purchase.description', 'purchase.packaging', null );

    public function make_new_purchase_query() {
        $owner_id                   = $this->session->userdata( 'company_id' );
    	$slug 					    = $this->input->post( 'slug', true );
        $this->db->select( 'purchase.*, company.company_name, code_options.name as payment_method,  transport.name as transport_name, users.fname as verified_name' );
        $this->db->from( 'purchase' );
        $this->db->join( 'company', 'company.id = purchase.from_company' );
        $this->db->join( 'code_options', 'code_options.id = purchase.mode_of_payment', 'left' );
        $this->db->join( 'transport', 'transport.id = purchase.transport_id', 'left' );
        $this->db->join( 'users', 'users.id = purchase.invoice_verified_by' );
        $this->db->where( 'purchase.owner_id', $owner_id );
        if( isset( $_POST[ 'search' ][ 'value' ] ) && !empty( $_POST[ 'search' ][ 'value' ] ) ) {
            $this->db->group_start();
                $this->db->like( 'purchase.company_invoice_no', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'company.company_name', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'purchase.invoice_no', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'purchase.purchase_date', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'purchase.invoice_due_date', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'purchase.transport_charge', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'purchase.name_of_transport', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'purchase.total_amount', $_POST[ 'search' ][ 'value' ] );
            $this->db->group_end();
        }
        if( isset( $_POST[ 'order' ] ) ) {
            $this->db->order_by( $this->order_purchase_column[ $_POST[ 'order' ][ '0' ][ 'column' ] ], $_POST[ 'order' ][ '0' ][ 'dir' ] );
        } else {
            $this->db->order_by( 'purchase.id', 'DESC' );    
        }
        $this->db->group_by( 'purchase.id' );
    }

    function make_purchase_datatables() {
        $this->make_new_purchase_query();
        if( isset( $_POST[ 'length' ] ) && $_POST[ 'length' ] != -1 ) {
            $this->db->limit( $_POST[ 'length' ], $_POST[ 'start' ] );
        }
        $query = $this->db->get();
        // echo $this->db->last_query();
        return $query->result();
    }

    function get_purchase_filtered_data() {
        $this->make_new_purchase_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function get_all_purchase_data() {
        $owner_id                   = $this->session->userdata( 'company_id' );
    	$slug                         = $this->input->post( 'slug', true );
        $this->db->select( 'purchase.*, company.company_name, code_options.name as payment_method,  transport.name as transport_name, users.fname as verified_name' );
        $this->db->from( 'purchase' );
        $this->db->join( 'company', 'company.id = purchase.from_company' );
        $this->db->join( 'code_options', 'code_options.id = purchase.mode_of_payment', 'left' );
        $this->db->join( 'transport', 'transport.id = purchase.transport_id', 'left' );
        $this->db->join( 'users', 'users.id = purchase.invoice_verified_by' );
        $this->db->where( 'purchase.owner_id', $owner_id );

        if( isset( $_POST[ 'search' ][ 'value' ] ) && !empty( $_POST[ 'search' ][ 'value' ] ) ) {
            $this->db->group_start();
                $this->db->like( 'purchase.company_invoice_no', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'company.company_name', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'purchase.invoice_no', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'purchase.purchase_date', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'purchase.invoice_due_date', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'purchase.transport_charge', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'purchase.name_of_transport', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'purchase.total_amount', $_POST[ 'search' ][ 'value' ] );
            $this->db->group_end();
        }
        if( isset( $_POST[ 'order' ] ) ) {
            $this->db->order_by( $this->order_purchase_column[ $_POST[ 'order' ][ '0' ][ 'column' ] ], $_POST[ 'order' ][ '0' ][ 'dir' ] );
        } else {
            $this->db->order_by( 'purchase.id', 'DESC' );    
        }

        return $this->db->count_all_results();
    }

    ################ SERVER SIDE DATATABLE ENDS HERE

    function get_sale_total( $where ) {
    	$this->db->select( 'sum( sale.total_amount ) as total_amount' );
    	$this->db->where( 'status', 'ACTIVE' );
    	if( $where ) {
    		$this->db->where( $where );
    	}
    	$all 			= $this->db->get( 'sale' );
    	if( $all->num_rows() > 0  ) {
    		return $all->row();
    	} else {
    		return false;
    	}
    }
}