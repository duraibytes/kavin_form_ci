<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee_model extends CI_Model {

	public function insert_user( $data ) {
		$this->db->insert( 'users', $data );
		return $this->db->insert_id();
	}

	public function get_employee() {
		$this->db->select( 'employee.*, users.fname as user_name, users.user_type_id, users.password' );
		$this->db->join( 'users', 'users.employee_id = employee.id', 'left' );
		$this->db->where( 'employee.status', 'ACTIVE' );
		$all 				= $this->db->get( 'employee' );
		if( $all->num_rows() > 0 ) {
			return $all->result();
		} else {
			return false;
		}

	}

	public function get_employee_by_id( $id = '', $slug = '' ) {
		$this->db->select( 'employee.*, users.fname as user_name, users.user_type_id, users.password' );
		$this->db->join( 'users', 'users.employee_id = employee.id', 'left' );
		
		if( $id ) {
			$this->db->where( 'employee.id', $id );
		} 
		if( $slug ) {
			$this->db->where( 'employee.slug', $slug );
		}
		$info 			= $this->db->get( 'employee' );
		if( $info->num_rows() > 0 ) {
			return $info->row();
		} else {
			return false;
		}
	}

}