<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction_model extends CI_Model {

	################ All BANK TRANSACTION DETAILS SERVER SIDE DATATABLE ###############
    var $table          		= 'payment';
    
    var $select_column 			= array( 'payment.id', 'payment.receipt_no', 'payment.mode', 'payment.mode_type', 'payment.mode_type_id', 'payment.invoice_no', 'payment.payment_date', 'payment.amount', 'payment.bank_charges', 'payment.bank_branch', 'payment.ifsc', 'payment.bank_ac_no', 'payment.cheque_no', 'payment.advance_description', 'payment.payment_method_id', 'pay_method.name as payment_method', 'bank.bank_name', 'COALESCE( purchase_comp.company_name, company.company_name ) as company_name' );
    var $order_payment_column      = array( 'payment.payment_date', 'payment.mode_type', 'company_name', '', 'payment.amount', 'pay_method.name', 'payment.cheque_no', 'payment.bank_ac_no', 'bank.bank_name', 'payment.bank_branch', 'payment.ifsc' );
    public function make_new_payment_query() {
        $this->db->select( $this->select_column );
        $this->db->from( 'payment' );
        $this->db->join( 'code_options as pay_method', 'pay_method.id = payment.payment_method_id' );
        $this->db->join( 'bank', 'bank.id = payment.bank_id', 'left' );
       	$this->db->join( 'purchase', 'purchase.id = payment.mode_type_id and payment.mode = "purchase" and payment.mode_type="add"', 'left' );
        $this->db->join( 'company as purchase_comp', 'purchase_comp.id = purchase.from_company', 'left' );
        $this->db->join( 'company', 'company.id = payment.mode_type_id and payment.mode_type="company"', 'left' );
        //session where condition for filter 
        if( $this->session->has_userdata( 'transaction_filter' ) ) {
            $sess_filter            = $this->session->userdata( 'transaction_filter' );
            if( isset( $sess_filter[ 'bank' ] ) && !empty( $sess_filter[ 'bank'] ) ) {
                $this->db->where( 'payment.bank_id', $sess_filter[ 'bank' ] );
            }
            if( isset( $sess_filter[ 'start' ] ) && !empty( $sess_filter[ 'start'] ) ) {
                $start_date             = str_replace( '/', '-', $sess_filter[ 'start' ] );
                $start_date             = date( 'Y-m-d', strtotime( $start_date ) ); 
            }
            if( isset( $sess_filter[ 'end' ] ) && !empty( $sess_filter[ 'end'] ) ) {
                $end_date             = str_replace( '/', '-', $sess_filter[ 'end' ] );
                $end_date             = date( 'Y-m-d', strtotime( $end_date ) ); 
            }
            if( isset( $start_date ) && isset( $end_date ) ) {
                $this->db->group_start();
                    $this->db->where( 'payment.payment_date >=', $start_date );
                    $this->db->where( 'payment.payment_date <=', $end_date );
                $this->db->group_end();
            }
        }

        if( isset( $_POST[ 'search' ][ 'value' ] ) && !empty( $_POST[ 'search' ][ 'value' ] ) ) {
            $this->db->group_start();
                $this->db->like( 'payment.receipt_no', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'payment.invoice_no', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'payment.payment_method', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'payment.amount', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'payment.payment_date', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'payment.bank_ac_no', $_POST[ 'search' ][ 'value' ] );
            $this->db->group_end();
        }
        if( isset( $_POST[ 'order' ] ) ) {
            $this->db->order_by( $this->order_payment_column[ $_POST[ 'order' ][ '0' ][ 'column' ] ], $_POST[ 'order' ][ '0' ][ 'dir' ] );
        } else {
            $this->db->order_by( 'payment.id', 'DESC' );    
        }
        $this->db->group_by( 'payment.id' );
        
    }

    function make_payment_datatables() {
        $this->make_new_payment_query();
        if( isset( $_POST[ 'length' ] ) && $_POST[ 'length' ] != -1 ) {
            $this->db->limit( $_POST[ 'length' ], $_POST[ 'start' ] );
        }
        $query = $this->db->get();
        // echo $this->db->last_query();
        return $query->result();
    }

    function get_payment_filtered_data() {
        $this->make_new_payment_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function get_all_payment_data() {
    	$this->db->select( $this->select_column );
        $this->db->from( 'payment' );
        $this->db->join( 'code_options as pay_method', 'pay_method.id = payment.payment_method_id' );
        $this->db->join( 'bank', 'bank.id = payment.bank_id', 'left' );
        //session where condition for filter 
        if( $this->session->has_userdata( 'transaction_filter' ) ) {
            $sess_filter            = $this->session->userdata( 'transaction_filter' );
            if( isset( $sess_filter[ 'bank' ] ) && !empty( $sess_filter[ 'bank'] ) ) {
                $this->db->where( 'payment.bank_id', $sess_filter[ 'bank' ] );
            }
            if( isset( $sess_filter[ 'start' ] ) && !empty( $sess_filter[ 'start'] ) ) {
                $start_date             = str_replace( '/', '-', $sess_filter[ 'start' ] );
                $start_date             = date( 'Y-m-d', strtotime( $start_date ) ); 
            }
            if( isset( $sess_filter[ 'end' ] ) && !empty( $sess_filter[ 'end'] ) ) {
                $end_date             = str_replace( '/', '-', $sess_filter[ 'end' ] );
                $end_date             = date( 'Y-m-d', strtotime( $end_date ) ); 
            }
            if( isset( $start_date ) && isset( $end_date ) ) {
                $this->db->group_start();
                    $this->db->where( 'payment.payment_date >=', $start_date );
                    $this->db->where( 'payment.payment_date <=', $end_date );
                $this->db->group_end();
            }
        }
        if( isset( $_POST[ 'search' ][ 'value' ] ) && !empty( $_POST[ 'search' ][ 'value' ] ) ) {
            $this->db->group_start();
                $this->db->like( 'payment.receipt_no', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'payment.invoice_no', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'payment.payment_method', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'payment.amount', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'payment.payment_date', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'payment.bank_ac_no', $_POST[ 'search' ][ 'value' ] );
            $this->db->group_end();
        }
        if( isset( $_POST[ 'order' ] ) ) {
            $this->db->order_by( $this->order_payment_column[ $_POST[ 'order' ][ '0' ][ 'column' ] ], $_POST[ 'order' ][ '0' ][ 'dir' ] );
        } else {
            $this->db->order_by( 'payment.id', 'DESC' );    
        }
        return $this->db->count_all_results();
    }

    ################ SERVER SIDE DATATABLE ENDS HERE

}