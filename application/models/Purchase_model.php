<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Purchase_model extends CI_Model {

	################ All purchase DETAILS SERVER SIDE DATATABLE ###############
   
    var $order_purchase_column      	= array( 'purchase.purchase_name', 'purchase.purchase_company', 'purchase.purchase_code', 'purchase.category', 'purchase.description', 'purchase.packaging', null );

    public function make_new_purchase_query() {
        $owner_id                   = $this->session->userdata( 'company_id' );
    	$slug 					    = $this->input->post( 'slug', true );
        $this->db->select( 'purchase.*, company.company_name, code_options.name as payment_method,  transport.name as transport_name, users.fname as verified_name' );
        $this->db->from( 'purchase' );
        $this->db->join( 'company', 'company.id = purchase.from_company' );
        $this->db->join( 'code_options', 'code_options.id = purchase.mode_of_payment', 'left' );
        $this->db->join( 'transport', 'transport.id = purchase.transport_id', 'left' );
        $this->db->join( 'users', 'users.id = purchase.invoice_verified_by' );
        $this->db->where( 'purchase.owner_id', $owner_id );
        if( isset( $_POST[ 'search' ][ 'value' ] ) && !empty( $_POST[ 'search' ][ 'value' ] ) ) {
            $this->db->group_start();
                $this->db->like( 'purchase.company_invoice_no', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'company.company_name', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'purchase.invoice_no', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'purchase.purchase_date', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'purchase.invoice_due_date', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'purchase.transport_charge', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'purchase.name_of_transport', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'purchase.total_amount', $_POST[ 'search' ][ 'value' ] );
            $this->db->group_end();
        }
        if( isset( $_POST[ 'order' ] ) ) {
            $this->db->order_by( $this->order_purchase_column[ $_POST[ 'order' ][ '0' ][ 'column' ] ], $_POST[ 'order' ][ '0' ][ 'dir' ] );
        } else {
            $this->db->order_by( 'purchase.id', 'DESC' );    
        }
        $this->db->group_by( 'purchase.id' );
    }

    function make_purchase_datatables() {
        $this->make_new_purchase_query();
        if( isset( $_POST[ 'length' ] ) && $_POST[ 'length' ] != -1 ) {
            $this->db->limit( $_POST[ 'length' ], $_POST[ 'start' ] );
        }
        $query = $this->db->get();
        // echo $this->db->last_query();
        return $query->result();
    }

    function get_purchase_filtered_data() {
        $this->make_new_purchase_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function get_all_purchase_data() {
        $owner_id                   = $this->session->userdata( 'company_id' );
    	$slug                         = $this->input->post( 'slug', true );
        $this->db->select( 'purchase.*, company.company_name, code_options.name as payment_method,  transport.name as transport_name, users.fname as verified_name' );
        $this->db->from( 'purchase' );
        $this->db->join( 'company', 'company.id = purchase.from_company' );
        $this->db->join( 'code_options', 'code_options.id = purchase.mode_of_payment', 'left' );
        $this->db->join( 'transport', 'transport.id = purchase.transport_id', 'left' );
        $this->db->join( 'users', 'users.id = purchase.invoice_verified_by' );
        $this->db->where( 'purchase.owner_id', $owner_id );

        if( isset( $_POST[ 'search' ][ 'value' ] ) && !empty( $_POST[ 'search' ][ 'value' ] ) ) {
            $this->db->group_start();
                $this->db->like( 'purchase.company_invoice_no', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'company.company_name', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'purchase.invoice_no', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'purchase.purchase_date', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'purchase.invoice_due_date', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'purchase.transport_charge', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'purchase.name_of_transport', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'purchase.total_amount', $_POST[ 'search' ][ 'value' ] );
            $this->db->group_end();
        }
        if( isset( $_POST[ 'order' ] ) ) {
            $this->db->order_by( $this->order_purchase_column[ $_POST[ 'order' ][ '0' ][ 'column' ] ], $_POST[ 'order' ][ '0' ][ 'dir' ] );
        } else {
            $this->db->order_by( 'purchase.id', 'DESC' );    
        }

        return $this->db->count_all_results();
    }

    ################ SERVER SIDE DATATABLE ENDS HERE

    public function get_purchase_info( $id = '', $slug = '' ) {
    	if( $id ) {
    		$this->db->where( 'purchase.id', $id );	
    	}
    	if( $slug ) {
    		$this->db->where( 'purchase.slug', $slug );	
    	}
    	$this->db->select( 'purchase.*, users.fname, purchase.id as purchase_id, company.company_name, company.email as company_email, company.mobile_no as company_mobile_no, company.phone_no as company_phone_no, company.fax as company_fax, company.seed_license, company.pesticides_license, company.fertilizer_license, company.company_logo, company.pan_no, company.gstin_no, transport.name as transport_name, code_options.name as pay_method' );
        $this->db->join( 'users', 'users.id = purchase.invoice_verified_by', 'left' );
        $this->db->join( 'code_options', 'code_options.id = purchase.mode_of_payment', 'left' );
        $this->db->join( 'company', 'company.id = purchase.from_company' );
        $this->db->join( 'transport', 'transport.id = purchase.transport_id', 'left' );
    	$info 			= $this->db->get( 'purchase' );
        // echo $this->db->last_query();
    	if( $info->num_rows() > 0 ) {
    		return $info->row();
    	} else {
    		return false;
    	}
    }

    public function purchase_items( $purchase_id, $status = '' ) {

        $this->db->where( 'purchase_id', $purchase_id );
        if( $status ) {
            $this->db->where( 'purchase_items.status', $status );
        }
        $this->db->select( 'purchase_items.*, product.product_name, product.product_code, product.product_category, product.description, product.packaging' );
        $this->db->join( 'product', 'product.id = purchase_items.product_id' );
        $items          = $this->db->get( 'purchase_items' );
        if( $items->num_rows() > 0 ) {
            return $items->result();
        } else {
            return false;
        } 

    }

    //Customer code list typeahead
    function typehead_purchase_list( $term, $return_company = '', $status = '' ) {
        $owner_id                  = $this->session->userdata( 'company_id' );

        $this->db->group_start();
            $this->db->where( "purchase.invoice_no LIKE '%$term%'" );
            $this->db->or_where( "purchase.company_invoice_no LIKE '%$term%'" );
        $this->db->group_end(); 
        if( isset( $owner_id ) ) {
            $this->db->where( 'purchase.owner_id', $owner_id ); 
        } 
        if( $return_company ) {
            $this->db->where( 'purchase.from_company', $return_company );
        }
        if( $status ) {
            $this->db->where( 'purchase.status', $status );
        }
        $query          = $this->db->get( 'purchase' );
        $numRows        = $query->num_rows();
        if( $numRows > 0 ) {
            return $query->result();
        } else { return false; }
    }


    ################ All Purchase Return DETAILS SERVER SIDE DATATABLE ###############
   
    var $order_purchase_return_column          = array( 'purchase_return.return_no', 'purchase_return.return_date', 'purchase_return.transport_charge', 'purchase_return.transport_name', 'purchase_return.total_amount', 'purchase_return.reason', null );

    public function make_new_purchase_return_query() {
        $owner_id                       = $this->session->userdata( 'company_id' );
        $slug                           = $this->input->post( 'slug', true );
        $this->db->select( 'purchase_return.*, company.company_name, transport.name as transport_name, users.fname as verified_name' );
        $this->db->from( 'purchase_return' );
        $this->db->join( 'company', 'company.id = purchase_return.return_to' );
        
        $this->db->join( 'transport', 'transport.id = purchase_return.transport_id', 'left' );
        $this->db->join( 'users', 'users.id = purchase_return.created_by' );
        $this->db->where( 'purchase_return.owner_id', $owner_id );
        if( isset( $_POST[ 'search' ][ 'value' ] ) && !empty( $_POST[ 'search' ][ 'value' ] ) ) {
            $this->db->group_start();
                $this->db->like( 'purchase_return.purchase_invoice_no', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'company.company_name', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'purchase_return.return_no', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'purchase_return.return_date', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'purchase_return.transport_charge', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'purchase_return.transport_name', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'purchase_return.reason', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'purchase_return.total_amount', $_POST[ 'search' ][ 'value' ] );
            $this->db->group_end();
        }
        if( isset( $_POST[ 'order' ] ) ) {
            $this->db->order_by( $this->order_purchase_return_column[ $_POST[ 'order' ][ '0' ][ 'column' ] ], $_POST[ 'order' ][ '0' ][ 'dir' ] );
        } else {
            $this->db->order_by( 'purchase_return.id', 'DESC' );    
        }
        $this->db->group_by( 'purchase_return.id' );
    }

    function make_purchase_return_datatables() {
        $this->make_new_purchase_return_query();
        if( isset( $_POST[ 'length' ] ) && $_POST[ 'length' ] != -1 ) {
            $this->db->limit( $_POST[ 'length' ], $_POST[ 'start' ] );
        }
        $query = $this->db->get();
        // echo $this->db->last_query();
        return $query->result();
    }

    function get_purchase_return_filtered_data() {
        $this->make_new_purchase_return_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function get_all_purchase_return_data() {
        $owner_id                       = $this->session->userdata( 'company_id' );
        $slug                           = $this->input->post( 'slug', true );
        $this->db->select( 'purchase_return.*, company.company_name, transport.name as transport_name, users.fname as verified_name' );
        $this->db->from( 'purchase_return' );
        $this->db->join( 'company', 'company.id = purchase_return.return_to' );
        
        $this->db->join( 'transport', 'transport.id = purchase_return.transport_id', 'left' );
        $this->db->join( 'users', 'users.id = purchase_return.created_by' );
        $this->db->where( 'purchase_return.owner_id', $owner_id );
        if( isset( $_POST[ 'search' ][ 'value' ] ) && !empty( $_POST[ 'search' ][ 'value' ] ) ) {
            $this->db->group_start();
                $this->db->like( 'purchase_return.purchase_invoice_no', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'company.company_name', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'purchase_return.return_no', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'purchase_return.return_date', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'purchase_return.transport_charge', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'purchase_return.transport_name', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'purchase_return.reason', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'purchase_return.total_amount', $_POST[ 'search' ][ 'value' ] );
            $this->db->group_end();
        }
        if( isset( $_POST[ 'order' ] ) ) {
            $this->db->order_by( $this->order_purchase_return_column[ $_POST[ 'order' ][ '0' ][ 'column' ] ], $_POST[ 'order' ][ '0' ][ 'dir' ] );
        } else {
            $this->db->order_by( 'purchase_return.id', 'DESC' );    
        }

        return $this->db->count_all_results();
    }

    ################ SERVER SIDE DATATABLE ENDS HERE
    
    public function get_purchase_return_info( $id = '', $slug = '' ) {
        if( $id ) {
            $this->db->where( 'purchase_return.id', $id );  
        }
        if( $slug ) {
            $this->db->where( 'purchase_return.slug', $slug );  
        }
        $this->db->select( 'purchase_return.*,users.fname,  company.company_name, company.email as company_email, company.mobile_no as company_mobile_no, company.phone_no as company_phone_no, company.fax as company_fax, company.seed_license, company.pesticides_license, company.fertilizer_license, company.company_logo, company.pan_no, company.gstin_no, transport.name as transport' );
        $this->db->join( 'users', 'users.id = purchase_return.created_by', 'left' );
        $this->db->join( 'company', 'company.id = purchase_return.return_to' );
        $this->db->join( 'transport', 'transport.id = purchase_return.transport_id', 'left' );
        
        $info           = $this->db->get( 'purchase_return' );
        if( $info->num_rows() > 0 ) {
            return $info->row();
        } else {
            return false;
        }
    }

    public function purchase_return_items( $purchase_return_id = '', $status = '', $purchase_id = '' ) {
        if( $purchase_return_id ) {
            $this->db->where( 'purchase_return_id', $purchase_return_id );
        }
        if( $status ) {
            $this->db->where( 'purchase_return_items.status', $status );
        }
        if( $purchase_id ) {
            $this->db->where( 'purchase_return.purchase_id', $purchase_id );
        }
        $this->db->select( 'purchase_return_items.*, product.product_name, product.product_code, product.product_category, product.description, product.packaging' );
        $this->db->join( 'product', 'product.id = purchase_return_items.product_id' );
        $this->db->join( 'purchase_return', 'purchase_return.id = purchase_return_items.purchase_return_id', 'left' );
        $items          = $this->db->get( 'purchase_return_items' );
        if( $items->num_rows() > 0 ) {
            return $items->result();
        } else {
            return false;
        } 

    }

    function get_company_purchase_amount( $company_id ) {
        $owner_id               = $this->session->userdata( 'company_id' );
        $this->db->select( 'SUM( purchase.total_amount ) as total_amount' );
        $this->db->where( 'purchase.from_company', $company_id );
        $this->db->where( 'purchase.owner_id', $owner_id );

        $info           = $this->db->get( 'purchase' );
        // echo $this->db->last_query();
        if( $info->num_rows() > 0 ) {
            return $info->row();
        } else {
            return false;
        }

    }

    function get_company_purchase_paid_amount( $company_id ) {
        $owner_id               = $this->session->userdata( 'company_id' );
        $this->db->select( 'SUM( payment.amount ) as paid_amount' );
        $this->db->join( 'purchase', 'purchase.id = payment.mode_type_id and payment.mode = "purchase"' );
        $this->db->where( 'purchase.from_company', $company_id );
        $this->db->where( 'purchase.owner_id', $owner_id );
        $this->db->where( 'payment.status', 'ACTIVE' );
        $info           = $this->db->get( 'payment' );

        if( $info->num_rows() > 0 ) {
            return $info->row();
        } else {
            return false;
        }
    }

    function get_company_paid_amount( $company_id ) {

        $owner_id    = $this->session->userdata( 'company_id' );
        $this->db->select( 'SUM( payment.amount ) as paid_amount' );
        $this->db->group_start();
            $this->db->where( 'payment.mode', 'purchase' );
            $this->db->where( 'payment.mode_type', 'company' );
            $this->db->where( 'payment.mode_type_id', $company_id );
        $this->db->group_end();
        $this->db->where( 'payment.status', 'ACTIVE' );
        $this->db->where( 'payment.owner_id', $owner_id );
        $info        = $this->db->get( 'payment' );
        // echo $this->db->last_query();
        if( $info->num_rows() > 0 ) {
            return $info->row();
        } else {
            return false;
        }
    }

     ################ All Purchase Payment DETAILS SERVER SIDE DATATABLE ###############
   
    var $order_payment_column          = array( 'purchase_return.return_no', 'purchase_return.return_date', 'purchase_return.transport_charge', 'purchase_return.transport_name', 'purchase_return.total_amount', 'purchase_return.reason', null );

    public function make_new_payment_query() {
        $owner_id               = $this->session->userdata( 'company_id' );
        $this->db->select( 'payment.*, users.fname as verified_name, COALESCE( company.company_name, purchase_company.company_name ) as company_name, code_options.name as payment_method' );
        $this->db->from( 'payment' );
        $this->db->join( 'users', 'users.id = payment.created_by' );
        $this->db->join( 'code_options', 'code_options.id = payment.payment_method_id' );

        $this->db->join( 'company', 'company.id = payment.mode_type_id and payment.mode_type = "company"', 'left'  );
        $this->db->join( 'purchase', 'purchase.invoice_no = payment.invoice_no', 'left' );
        $this->db->join( 'company as purchase_company', 'purchase_company.id = purchase.from_company', 'left' );
        $this->db->where( 'payment.owner_id', $owner_id );
        $this->db->where( 'payment.status', 'ACTIVE' );

        if( isset( $_POST[ 'search' ][ 'value' ] ) && !empty( $_POST[ 'search' ][ 'value' ] ) ) {
            $this->db->group_start();
                $this->db->like( 'payment.receipt_no', $_POST[ 'search' ][ 'value' ] );        
                $this->db->or_like( 'payment.purchase_invoice_no', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'payment.payment_date', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'payment.amount', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'payment.bank_ac_no', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'payment.cheque_no', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'payment.cheque_issue_amount', $_POST[ 'search' ][ 'value' ] );
            $this->db->group_end();
        }

        $this->db->where( 'payment.receipt_no !=', '' );
        if( isset( $_POST[ 'order' ] ) ) {
            $this->db->order_by( $this->order_payment_column[ $_POST[ 'order' ][ '0' ][ 'column' ] ], $_POST[ 'order' ][ '0' ][ 'dir' ] );
        } else {
            $this->db->order_by( 'payment.id', 'DESC' );    
        }
        $this->db->group_by( 'payment.id' );
    }

    function make_payment_datatables() {
        $this->make_new_payment_query();
        if( isset( $_POST[ 'length' ] ) && $_POST[ 'length' ] != -1 ) {
            $this->db->limit( $_POST[ 'length' ], $_POST[ 'start' ] );
        }
        $query = $this->db->get();
        // echo $this->db->last_query();
        return $query->result();
    }

    function get_payment_filtered_data() {
        $this->make_new_payment_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function get_all_payment_data() {
        $owner_id               = $this->session->userdata( 'company_id' );
        $this->db->select( 'payment.*, users.fname as verified_name, COALESCE( company.company_name, purchase_company.company_name ) as company_name, code_options.name as payment_method' );
        $this->db->from( 'payment' );
        $this->db->join( 'users', 'users.id = payment.created_by' );
        $this->db->join( 'code_options', 'code_options.id = payment.payment_method_id' );

        $this->db->join( 'company', 'company.id = payment.mode_type_id and payment.mode_type = "company"', 'left'  );
        $this->db->join( 'purchase', 'purchase.invoice_no = payment.invoice_no', 'left' );
        $this->db->join( 'company as purchase_company', 'purchase_company.id = purchase.from_company', 'left' );
        $this->db->where( 'payment.owner_id', $owner_id );
        $this->db->where( 'payment.status', 'ACTIVE' );
        if( isset( $_POST[ 'search' ][ 'value' ] ) && !empty( $_POST[ 'search' ][ 'value' ] ) ) {
            $this->db->group_start();
                $this->db->like( 'payment.receipt_no', $_POST[ 'search' ][ 'value' ] );        
                $this->db->or_like( 'payment.purchase_invoice_no', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'payment.payment_date', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'payment.amount', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'payment.bank_ac_no', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'payment.cheque_no', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'payment.cheque_issue_amount', $_POST[ 'search' ][ 'value' ] );
            $this->db->group_end();
        }

        $this->db->where( 'payment.receipt_no !=', '' );
        if( isset( $_POST[ 'order' ] ) ) {
            $this->db->order_by( $this->order_payment_column[ $_POST[ 'order' ][ '0' ][ 'column' ] ], $_POST[ 'order' ][ '0' ][ 'dir' ] );
        } else {
            $this->db->order_by( 'payment.id', 'DESC' );    
        }

        return $this->db->count_all_results();
    }

    ################ SERVER SIDE DATATABLE ENDS HERE
    function purchase_payment_info( $receipt_no ) {
        $this->db->where( 'payment.receipt_no', $receipt_no );
        $this->db->select( 'payment.*, COALESCE( purchase.from_company, payment.mode_type_id ) as from_company, purchase.id as purchase_id, bank.bank_name, users.fname' );
        $this->db->join( 'purchase', 'purchase.id = payment.mode_type_id and payment.mode = "purchase"', 'left' );
        $this->db->join( 'bank', 'bank.id = payment.bank_id', 'left' );
        $this->db->join( 'users', 'users.id = payment.created_by', 'left' );
        $this->db->where( 'payment.status', 'ACTIVE' );
        $info               = $this->db->get( 'payment' );
        // echo $this->db->last_query();
        if( $info->num_rows() > 0 ) {
            return $info->row();
        } else {
            return false;
        }
    }

}