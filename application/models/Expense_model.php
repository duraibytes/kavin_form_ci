<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Expense_model extends CI_Model {

	################ All PRODUCT DETAILS SERVER SIDE DATATABLE ###############
    var $select_column 				= array( 'expense.id','expense.expense_name', 'expense.description', 'expense.amount', 'expense.payment_method', 'expense.payment_method_id', 'expense.created_at'  );
    var $order_expense_column      	= array( 'expense.expense_name', 'expense.description', 'expense.amount', null );
    public function make_new_expense_query() {
    	$slug 						= $this->input->post( 'slug', true );
        $this->db->select( $this->select_column );
        $this->db->from( 'expense' );
       	
        if( isset( $_POST[ 'search' ][ 'value' ] ) && !empty( $_POST[ 'search' ][ 'value' ] ) ) {
            $this->db->group_start();
                $this->db->like( 'expense.expense_name', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'expense.description', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'expense.amount', $_POST[ 'search' ][ 'value' ] );
               
               
               
                
            $this->db->group_end();
        }
        if( isset( $_POST[ 'order' ] ) ) {
            $this->db->order_by( $this->order_expense_column[ $_POST[ 'order' ][ '0' ][ 'column' ] ], $_POST[ 'order' ][ '0' ][ 'dir' ] );
        } else {
            $this->db->order_by( 'expense.id', 'DESC' );    
        }
        
        $this->db->group_by( 'expense.id' );
        
    }

    function make_expense_datatables() {
        $this->make_new_expense_query();
        if( isset( $_POST[ 'length' ] ) && $_POST[ 'length' ] != -1 ) {
            $this->db->limit( $_POST[ 'length' ], $_POST[ 'start' ] );
        }
        $query = $this->db->get();
        // echo $this->db->last_query();
        return $query->result();
    }

    function get_expense_filtered_data() {
        $this->make_new_expense_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function get_all_expense_data() {
    	$slug 						= $this->input->post( 'slug', true );
        $this->db->select( $this->select_column );
        $this->db->from( 'expense' );
       	
        if( isset( $_POST[ 'search' ][ 'value' ] ) && !empty( $_POST[ 'search' ][ 'value' ] ) ) {
            $this->db->group_start();
                $this->db->like( 'expense.expense_name', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'expense.description', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'expense.amount', $_POST[ 'search' ][ 'value' ] );
               
               
                
               
            $this->db->group_end();
        }
        if( isset( $_POST[ 'order' ] ) ) {
            $this->db->order_by( $this->order_expense_column[ $_POST[ 'order' ][ '0' ][ 'column' ] ], $_POST[ 'order' ][ '0' ][ 'dir' ] );
        } else {
           $this->db->order_by( 'expense.expense_name', 'ASC' );    
        }

        return $this->db->count_all_results();
    }

    ################ SERVER SIDE DATATABLE ENDS HERE

    public function get_expense_info( $id = '', $slug = '' ) {
    	if( $id ) {
    		$this->db->where( 'id', $id );	
    	}
    	if( $slug ) {
    		$this->db->where( 'slug', $slug );	
    	}
    	
    	$info 			= $this->db->get( 'product' );
    	if( $info->num_rows() > 0 ) {
    		return $info->row();
    	} else {
    		return false;
    	}
    }
	
	
	function expense_info( $id ) {
		$this->db->where( 'expense.id', $id );
        $this->db->select( 'expense.*, payment.bank_id, payment.bank_branch, payment.bank_branch_id, payment.bank_ac_no' );
        $this->db->join( 'payment', 'payment.mode_type_id = expense.id and payment.mode = "expense"' );
		$info 			= $this->db->get( 'expense' );
		if( $info->num_rows() > 0 ) {
			return $info->row();
		} else {
			return false;
		}
	}

	function get_all_expense() {
		$all 		= $this->db->get( 'expense' ); 
		if( $all->num_rows() > 0 ) {
			return $all->result();
		} else {
			return false;
		}
	}
	
}