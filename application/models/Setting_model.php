<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_model extends CI_Model {

	function get_all_codes() {
		$this->db->where( 'codes.status', 'ACTIVE' );
		$all 	= $this->db->get( 'codes' );
		if( $all->num_rows() > 0 ) {
			return $all->result();
		} else {
			return false;
		}
	}

	function code_info( $id = '', $slug = '' ) {
		if( $id ) {
			$this->db->where( 'id', $id );
		} 
		if( $slug ) {
			$this->db->where( 'slug', $slug );	
		}
		
		$info 			= $this->db->get( 'codes' );
		if( $info->num_rows() > 0 ) {
			return $info->row();
		} else {
			return false;
		}
	}

	function get_all_options( $slug, $where = '', $order_col = '', $order_by = '' ) {
		$this->db->select( 'codes.name as code_name, codes.slug, code_options.*' );
		$this->db->join( 'codes', 'codes.id = code_options.code_id' );
		$this->db->where( 'codes.slug', $slug );
		$this->db->where( 'code_options.status', 'ACTIVE' );
		if( $where ) {
			$this->db->where( $where );
		}
		if( $order_col && $order_by ) {
			$this->db->order_by( $order_col, $order_by );
		}
		$all 	= $this->db->get( 'code_options' );
		// echo $this->db->last_query();
		if( $all->num_rows() > 0 ) {
			return $all->result();
		} else {
			return false;
		}
	}

	function option_info( $id = '' ) {
		if( $id ) {
			$this->db->where( 'id', $id );
		} 
		
		$info 			= $this->db->get( 'code_options' );
		if( $info->num_rows() > 0 ) {
			return $info->row();
		} else {
			return false;
		}
	}

	function transport_info( $id ) {
		$this->db->where( 'id', $id );
		$info 			= $this->db->get( 'transport' );
		if( $info->num_rows() > 0 ) {
			return $info->row();
		} else {
			return false;
		}
	}

	function get_all_transport() {
		$this->db->order_by( 'transport.name', 'ASC' );
		$all 		= $this->db->get( 'transport' ); 
		if( $all->num_rows() > 0 ) {
			return $all->result();
		} else {
			return false;
		}
	}


	function rep_info( $id ) {
		$this->db->where( 'id', $id );
		$info 			= $this->db->get( 'sales_rep' );
		if( $info->num_rows() > 0 ) {
			return $info->row();
		} else {
			return false;
		}
	}

	function get_all_rep() {
		$all 		= $this->db->get( 'sales_rep' ); 
		if( $all->num_rows() > 0 ) {
			return $all->result();
		} else {
			return false;
		}
	}

	function get_company_info( $id ) {
		$this->db->where( 'id', $id );
		$info 			= $this->db->get( 'owner' );
		if( $info->num_rows() > 0 ) {
			return $info->row();
		} else {
			return false;
		}
	}

	function get_invoice( $company_id ) {
		$this->db->where( 'company_id', $company_id );
		$info 	= $this->db->get( 'own_doc_number' );
		if( $info->num_rows() > 0 ) {
			return $info->row();
		} else {
			return false;
		}
	}

}