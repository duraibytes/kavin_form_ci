<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stock_model extends CI_Model {

	################ All BANK DETAILS SERVER SIDE DATATABLE ###############
    var $table          		= 'bank';
    
    var $select_column 			= array( 'bank.id','bank.bank_name', 'bank.email', 'bank.phone', 'bank.address', 'bank.slug','bank.branch', 'bank.ifsc', 'bank.micr_code', 'bank.status', 'bank.created_at', 'COALESCE( count( bank_branch.parent_id ), 0 ) as branch_count', 'bank_branch.slug as bank_slug', 'bank_branch.bank_name as parent_bank_name'  );
    var $order_bank_column      = array( '', 'bank.bank_name', 'bank.bank_name', 'ifsc', 'address' );
    public function make_new_bank_query() {
    	$slug 						= $this->input->post( 'slug', true );
        $this->db->select( $this->select_column );
        $this->db->from( 'bank' );
        
        if( $slug ) {
        	$this->db->join( 'bank AS bank_branch', 'bank_branch.id = bank.parent_id' );
        	$this->db->where( 'bank.parent_id !=', null );
        	$this->db->where( 'bank_branch.slug', $slug );
        } else {
        	$this->db->join( 'bank AS bank_branch', 'bank_branch.parent_id = bank.id', 'left' );
        	$this->db->where( 'bank.parent_id', null );	
        }
       	
        if( isset( $_POST[ 'search' ][ 'value' ] ) && !empty( $_POST[ 'search' ][ 'value' ] ) ) {
            $this->db->group_start();
                $this->db->like( 'bank.bank_name', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'bank.email', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'bank.phone', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'bank.address', $_POST[ 'search' ][ 'value' ] );
            $this->db->group_end();
        }
        if( isset( $_POST[ 'order' ] ) ) {
            $this->db->order_by( $this->order_bank_column[ $_POST[ 'order' ][ '0' ][ 'column' ] ], $_POST[ 'order' ][ '0' ][ 'dir' ] );
        } else {
            $this->db->order_by( 'bank.bank_name', 'ASC' );    
        }
        
        $this->db->group_by( 'bank.id' );
        
    }

    function make_bank_datatables() {
        $this->make_new_bank_query();
        if( isset( $_POST[ 'length' ] ) && $_POST[ 'length' ] != -1 ) {
            $this->db->limit( $_POST[ 'length' ], $_POST[ 'start' ] );
        }
        $query = $this->db->get();
        // echo $this->db->last_query();
        return $query->result();
    }

    function get_bank_filtered_data() {
        $this->make_new_bank_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function get_all_bank_data() {
    	$slug 						= $this->input->post( 'slug', true );
        $this->db->select( $this->select_column );
        $this->db->from( 'bank' );
        if( $slug ) {
        	$this->db->join( 'bank AS bank_branch', 'bank_branch.id = bank.parent_id' );
        	$this->db->where( 'bank.parent_id !=', null );
        	$this->db->where( 'bank_branch.slug', $slug );
        } else {
        	$this->db->join( 'bank AS bank_branch', 'bank_branch.parent_id = bank.id', 'left' );
        	$this->db->where( 'bank.parent_id', null );	
        }
       
        if( isset( $_POST[ 'search' ][ 'value' ] ) && !empty( $_POST[ 'search' ][ 'value' ] ) ) {
            $this->db->group_start();
                $this->db->like( 'bank.bank_name', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'bank.email', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'bank.phone', $_POST[ 'search' ][ 'value' ] );
                $this->db->or_like( 'bank.address', $_POST[ 'search' ][ 'value' ] );
            $this->db->group_end();
        }
        if( isset( $_POST[ 'order' ] ) ) {
            $this->db->order_by( $this->order_bank_column[ $_POST[ 'order' ][ '0' ][ 'column' ] ], $_POST[ 'order' ][ '0' ][ 'dir' ] );
        } else {
            $this->db->order_by( 'bank.bank_name', 'ASC' );    
        }

        return $this->db->count_all_results();
    }

    ################ SERVER SIDE DATATABLE ENDS HERE

    function purchase_stock_all() {
        if( $this->session->has_userdata( 'stock_filter' ) ) {
            $session            = $this->session->userdata( 'stock_filter' );

            if( isset( $session[ 'product_company' ] ) && !empty( $session[ 'product_company' ] ) ) {
                $this->db->where( 'product.product_company', $session[ 'product_company' ] );
            }
            if( isset( $session[ 'product_code' ] ) && !empty( $session[ 'product_code' ] ) ) {
                $this->db->where( 'product.product_code', $session[ 'product_code' ] );
            }
            if( isset( $session[ 'product_category' ] ) && !empty( $session[ 'product_category' ] ) ) {
                $this->db->where( 'product.product_category', $session[ 'product_category' ] );
            }
            if( isset( $session[ 'lot_no' ] ) && !empty( $session[ 'lot_no' ] ) ) {
                $this->db->where( 'purchase_items.lot_no', $session[ 'lot_no' ] );
            }
            if( isset( $session[ 'invoice_no' ] ) && !empty( $session[ 'invoice_no' ] ) ) {
                $this->db->where( 'purchase.invoice_no', $session[ 'invoice_no' ] );
            }
        }
        $this->db->select( 'purchase_items.*, product.product_name, product.product_company, product.product_code, product.product_variety, product.product_category, product.packaging, product.description, purchase.invoice_no, purchase_return_items.qty as returned_qty, purchase_return.return_no' );
        $this->db->join( 'product', 'product.id = purchase_items.product_id' );
        $this->db->join( 'purchase', 'purchase.id = purchase_items.purchase_id' );
        $this->db->join( 'purchase_return_items', 'purchase_return_items.purchase_item_id = purchase_items.id', 'left' );
        $this->db->join( 'purchase_return', 'purchase_return.id = purchase_return_items.purchase_return_id', 'left' );

        $this->db->where( 'purchase_items.status', 'ACTIVE' );
        $info       = $this->db->get( 'purchase_items' );
        // echo $this->db->last_query();
        if( $info->num_rows() > 0 ) {
            return $info->result();
        } else {
            return false;
        }
    }

}