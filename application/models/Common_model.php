<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common_model extends CI_Model {

	public function insert_table( $table, $data ) {
		$this->db->insert( $table, $data );
		return $this->db->insert_id();
	}

	public function update_table( $table, $where, $data ) {
		$this->db->where( $where );
		$this->db->update( $table, $data );
		return true;
	}

	public function delete_table( $table, $where ) {
		$this->db->where( $where );
		$this->db->delete( $table );
	}

	public function all( $table, $where = '', $status = '', $order_by = '' ) {
		if( $where ) {
			$this->db->where( $where );
		}
		if( $status ) {
			$this->db->where( 'status', $status );
		}
		if( $order_by ) {
			$this->db->order_by( $order_by, 'ASC' );
		}
		$info 		= $this->db->get( $table );
		if( $info->num_rows() > 0 ) {
			return $info->result();
		} else {
			return false;
		}
	}
	public function find( $table, $where = '') {
		if( $where ) {
			$this->db->where( $where );
		}
		$info 		= $this->db->get( $table );
		if( $info->num_rows() > 0 ) {
			return $info->row();
		} else {
			return false;
		}
	}

	// company records starts here
	public function get_company_info( $id = '', $slug = '' ) {
		if( $id ) {
			$this->db->where( 'id', $id );	
		}
		if( $slug ) {
			$this->db->where( 'slug', $slug );	
		}
		$info 			= $this->db->get( 'company' );
		if( $info->num_rows() > 0 ) {
			return $info->row();
		} else {
			return false;
		}
	}

	public function get_company_all() {
		$this->db->where( 'status', 'ACTIVE' );
		$info 			= $this->db->get( 'company' );
		if( $info->num_rows() > 0 ) {
			return $info->result();
		} else {
			return false;
		}
	}

	public function get_company_address( $company_id, $all = '' ) {
		$this->db->where( 'address.mode_id', $company_id );
		$this->db->where( 'address.mode_type', 'company' );	
		$this->db->select( 'address.*, company.company_name, company.company_logo, company.seed_license, company.pesticides_license, company.fertilizer_license, company.gstin_no, company.pan_no' );
		$this->db->join( 'company', 'company.id = address.mode_id and address.mode_type = "company"' );
		$address 			= $this->db->get( 'address' );
		if( $address->num_rows() > 0 ) {
			if( $all ) {
				return $address->result();
			} else {
				return $address->row();	
			}
		}
	}

	public function get_customer_all() {
		$this->db->where( 'status', 'ACTIVE' );
		$info 			= $this->db->get( 'customer' );
		if( $info->num_rows() > 0 ) {
			return $info->result();
		} else {
			return false;
		}
	}

	public function get_common_address( $mode_id , $mode_type , $all = '' ) {
		$this->db->where( 'mode_id', $mode_id );
		$this->db->where( 'mode_type', $mode_type );	
		$address 			= $this->db->get( 'address' );
		if( $address->num_rows() > 0 ) {
			if( $all ) {
				return $address->result();
			} else {
				return $address->row();	
			}
		}
	}

	// company records starts here
	public function get_customer_info( $id = '', $slug = '' ) {
		if( $id ) {
			$this->db->where( 'id', $id );	
		}
		if( $slug ) {
			$this->db->where( 'slug', $slug );	
		}
		$info 			= $this->db->get( 'customer' );
		if( $info->num_rows() > 0 ) {
			return $info->row();
		} else {
			return false;
		}
	}

	public function get_latest( $table, $where = '' ) {
		if( $where ) {
			$this->db->where( $where );
		}
		$this->db->order_by( 'id', 'desc' );
		$latest 		= $this->db->get( $table );
		if( $latest->num_rows() > 0 ) {
			return $latest->row();
		} else {
			return false;
		}
	}

	//Branch list typeahead
    function typehead_branch_list( $term, $status = '' ) {
        $bank_id 					= $this->input->get( 'bank_id', true );

        $this->db->group_start();
            $this->db->where( "bank.branch LIKE '%$term%'" );
            $this->db->or_where( "bank.ifsc LIKE '%$term%'" );
        $this->db->group_end(); 
        
        if( $bank_id ) {
            $this->db->where( 'bank.parent_id', $bank_id );
        }
        if( $status ) {
            $this->db->where( 'bank.status', $status );
        }
        $query          = $this->db->get( 'bank' );
        // echo $this->db->last_query();
        $numRows        = $query->num_rows();
        if( $numRows > 0 ) {
            return $query->result();
        } else { return false; }
    }

	

}