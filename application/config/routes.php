<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] 	= 'dashboard';
$route[ '/' ] 					= 'dashboard';
$route[ 'upcoming-expiry' ]  	= 'dashboard/upcoming_expiry';
$route[ 'creditnote-pending' ] 	= 'dashboard/creditnote_pending';
$route[ 'purchase-due' ] 		= 'dashboard/purchase_due';
$route[ 'sales-due' ] 			= 'dashboard/sales_due';

$route[ 'register' ] 			= 'auth/register';
$route[ 'validate-register' ] 	= 'auth/add_register';
$route[ 'login' ] 				= 'auth';
$route[ 'validte-login' ] 		= 'auth/validate_login';
$route[ 'forgot-password' ] 	= 'auth/forgot_password';
$route[ 'send-reset-password' ] = 'auth/send_reset_password';
$route[ 'expired' ] 			= 'auth/expired';

$route[ 'bank-list/(:any)' ] 	= 'bank/bank_list';
$route[ 'bank-edit/(:any)' ] 	= 'bank/add/$1';
$route[ 'bank-add' ]			= 'bank/add';
$route[ 'bank-branch-add/(:any)' ] 	= 'bank/add_branch';
$route[ 'bank-branch-add/(:any)/(:any)' ] 	= 'bank/add_branch';

$route[ 'options' ] 						= 'settings/module';
$route[ 'options/(:any)' ] 					= 'settings/sub_module';

$route[ 'transport' ] 						= 'settings/transport';
$route[ 'sales-rep' ] 						= 'settings/sales_rep';

$route[ 'purchase-return' ] 			= 'purchase/return';
$route[ 'purchase-return/view/(:any)' ] = 'purchase/return_view/$1';
$route[ 'purchase-return-add/(:any)' ] 	= 'purchase/return_add/$1';
$route[ 'purchase-return-add' ] 		= 'purchase/return_add';
$route[ 'purchase-payment' ] 			= 'purchase/payment';
$route[ 'purchase-payment/view/(:any)' ]= 'purchase/payment_view/$1;';
$route[ 'purchase-payment-add' ]		= 'purchase/payment_add';
$route[ 'purchase-payment-add/(:any)' ]	= 'purchase/payment_add/$1';
$route[ 'purchase/payment/(:any)' ] 	= 'purchase/purchase_payment/$1';


$route[ 'sale-return' ] 		= 'sale/return';
$route[ 'sale-return/view' ] 	= 'sale/return_view';
$route[ 'sale-return-add' ] 	= 'sale/return_add';
$route[ 'sale-payment' ] 		= 'sale/payment';
$route[ 'sale-payment/view' ]   = 'sale/payment_view';
$route[ 'sale-payment-add' ]	= 'sale/payment_add';

$route[ 'profile' ] 			= 'myaccount';

$route[ 'company/add/(:any)' ] 	= 'company/add/$i';
$route[ 'customer/add/(:any)' ] = 'customer/add/$i';

$route[ 'company-settings' ]  	= 'company_settings';
$route[ 'stock' ]  				= 'inventory';

$route['404_override'] 			= '';
$route['translate_uri_dashes'] 	= FALSE;
