
<?php 

function validate_login( $user_name, $password ) {
	$CI     					= get_instance();
    $query  					= "SELECT id, fname as user_name, password, email, mobile_no, user_type_id, user_type, employee_id, is_logged_in
                  FROM users
                  WHERE 
                    ( fname = '". $user_name ."' OR email = '". $user_name ."' ) 
                 ";
    $data   					= array( $user_name );         
    $result 					= $CI->db->query( $query, $data );
    $rows   					= $result->num_rows();   
    if( $rows > 0 ) {
        
        $userInfo   			= $result->row();

        if( $userInfo->is_logged_in == 'YES' ) {
            return 3;//already logged in;
        }

        $oPass 					= $CI->encryption->decrypt( $userInfo->password );
        if( $password == $oPass ) {
            $sessionVal = array( 'user_id'          => $userInfo->id, 
                                 'user_name'        => $userInfo->user_name, 
                                 'user_email'       => $userInfo->email, 
                                 'user_type'        => $userInfo->user_type,
                                 'user_type_id'     => $userInfo->user_type_id,
                                 'employee_id'      => $userInfo->employee_id,
                                 'company_id'       => $CI->input->post( 'company', true )
                                );
            $CI->session->set_userdata( $sessionVal );
           return 1;
        } else {
            return 2;
        }  
    } else { 
        
        return 2; 
    }         
}

function mail_exists( $email )
{
    $CI                     = get_instance();
    $CI->db->where( 'email', $email );
    $query                  = $CI->db->get( 'users' );
    if ( $query->num_rows() > 0 ) {
        return true;
    }
    else{
        return false;
    }
}	


 function sessionset() {
    $CI = get_instance();
    if( $CI->session->has_userdata( 'user_id' ) ) {}
    else { redirect( 'login' ); }
    return true;
}

function send_smtp_mail( $email_from,$email_to,$subject,$message ) {

        $CI                = get_instance(); 
        
        if( $subject ) {
            $subject         = $CI->config->item( 'site_name' ) . " - " . $subject;
        } else {
            $subject         = $CI->config->item( 'site_name' );
        }
        
    $CI->load->library('email');

    $CI->email->initialize(array(
          'protocol'        => 'smtp',
          'smtp_host'       => 'smtp.sendgrid.net',
          'smtp_user'       => 'sivabalan',
          'smtp_pass'       => 'sivabalan!986',
          'smtp_port'       => 587,
          'crlf'            => "\r\n",
          'newline'         => "\r\n",
          'smtp_timeout'    => '4',
          'mailtype'        => 'html',
          'charset'         => 'iso-8859-1'
        ));

        $CI->email->from( $email_from, 'Kavin Email' );
        $CI->email->to( $email_to );

        $CI->email->subject( $subject );
        $CI->email->message( $message );
        $CI->email->send();

  return true;
}


function verify_license() {
    $CI                   = get_instance();
    
    $host                 = $_SERVER[ 'HTTP_HOST' ];
    $uri                  = $_SERVER['REQUEST_URI'];
    $exploe               = explode("/", $uri );
    $baseurl              = $CI->config->item('base_url');
    $project_name         = $exploe[1];

    $check_date           = '';
    if( file_exists( "./assets/readme.txt" ) ) {
        $myfile           = fopen( "./assets/readme.txt", "r" );
        $enc_text         = fread( $myfile,filesize( "./assets/readme.txt" ) );
        $dec_text         = $CI->encryption->decrypt( $enc_text );
        
        if( !empty( $dec_text ) ) {
          $check_date     = date( 'Y-m-d', strtotime( $dec_text ) )  ;
        }
        
        fclose( $myfile );
    } 
    $result               = false;
    $date                 = date( 'Y-m-d' );

    //check if accessing from localhost 
    if( $host == 'localhost' ) {
      
        $result           = true;
    } else {
        // check expiry date
      if( $date <= $check_date ) {
          $result         = true;
      }
    } 
    
    return $result;
}




