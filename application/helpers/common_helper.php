<?php

function get_auto_ref_no( $invoice_type ) {
	//get company doc number
	$CI 						= get_instance();
	$doc 						= $CI->Common_model->find( 'own_doc_number', array( 'company_id' => $CI->session->userdata( 'company_id' ) ) );

	//get latest produt row
	$where[ 'owner_id' ] 		= $CI->session->userdata( 'company_id' );
	if( $invoice_type == 'purchase' ) {
		$table 					= 'purchase';
		$invoice_no 			= $doc->purchase_inv_no;
	} else if( $invoice_type == 'purchase_return' ) {
		$table 					= 'purchase_return';
		$invoice_no 			= $doc->purchase_return_no;
	}  

	if( $invoice_type == 'purchase_payment' ) {
		$invoice_no 			= $doc->purchase_payment_no;
		$where[ 'mode' ] 		= 'purchase';
		$latest 				= $CI->Common_model->get_latest( 'payment', $where );	
	} else {
		$latest 				= $CI->Common_model->get_latest( $table, $where );	
	}
	
	
	if( isset( $latest ) && !empty( $latest ) ) {
		if( $invoice_type == 'purchase' ) {
			$invoice 			= $latest->invoice_no;
		}  else if( $invoice_type == 'purchase_return' ) {
			$invoice 			= $latest->return_no;
		} else if( $invoice_type == 'purchase_payment' ) {
			$invoice 			= $latest->receipt_no;
		}
		
		$invoice 				= explode( '/', $invoice );
		$new_purchase  			= end( $invoice );
		$new_ref_no 			= end( $invoice ) + 1;
		$base_purchase_length 	= strlen( $new_purchase );
		$ref_no 				= '';
		$for_count 				=  $base_purchase_length - strlen( $new_ref_no );
		
		if( strlen( $new_ref_no ) != $base_purchase_length ) {
			for ( $i=0; $i < $for_count; $i++) { 
				$ref_no 		.= '0';
			}
			$ref_no 			= $ref_no.$new_ref_no;
			array_pop( $invoice );
			$invoice[]  		= $ref_no;
			$reference_no 		= implode( "/", $invoice );

		} else {
			array_pop( $invoice );
			$invoice[]  		= $new_ref_no;
			$reference_no 		= implode( "/", $invoice );
		}
		return $reference_no;

	} else {

		$purchase 				= $invoice_no;
		$purchase 				= explode( '/', $purchase );
		$new_purchase 			= end( $purchase );
		$new_ref_no 			= end( $purchase ) + 1;
		$base_purchase_length 	= strlen( $new_purchase );
		$ref_no 				= '';
		
		$for_count 				=  $base_purchase_length - strlen( $new_ref_no );
		
		if( strlen( $new_ref_no ) != $base_purchase_length ) {
			for ( $i=0; $i < $for_count; $i++) { 
				$ref_no 		.= '0';
			}
			$ref_no 			= $ref_no.$new_ref_no;
			array_pop( $purchase );
			$purchase[]  		= $ref_no;
			$reference_no 		= implode( "/", $purchase );

		} else {
			array_pop( $purchase );
			$purchase[]  		= $new_ref_no;
			$reference_no 		= implode( "/", $purchase );
		}
		return $reference_no;

	}

}


function sale_auto_ref_no( $invoice_type ) {
	//get company doc number
	$CI 															= get_instance();
	$doc 															= $CI->Common_model->find( 'own_doc_number', array( 'company_id' => $CI->session->userdata( 'company_id' ) ) );
	//check previous doc number
	$table 														= 'sale';
	$where[ 'owner_id' ] 							= $CI->session->userdata( 'company_id' );
	if( $invoice_type == 'sale-dealer' ) {
		$where[ 'customer_type' ]				= 'dealer';
	} else if( $invoice_type == 'sale-farmer' ) {
		$where[ 'customer_type' ]				= 'farmer';
	}
	$latest 													= $CI->Common_model->get_latest( $table, $where );	

	if( $latest ) {
		$invoice 												= $latest->invoice_no;
		$invoice 												= explode( '/', $invoice );
		$new_sale  											= end( $invoice );
		$new_ref_no 										= end( $invoice ) + 1;
		$base_sale_length 							= strlen( $new_sale );
		$ref_no 												= '';
		$for_count 											=  $base_sale_length - strlen( $new_ref_no );
		
		if( strlen( $new_ref_no ) != $base_sale_length ) {
			for ( $i=0; $i < $for_count; $i++) { 
				$ref_no 										.= '0';
			}
			$ref_no 											= $ref_no.$new_ref_no;
			array_pop( $invoice );
			$invoice[]  									= $ref_no;
			$reference_no 								= implode( "/", $invoice );

		} else {
			array_pop( $invoice );
			$invoice[]  			= $new_ref_no;
			$reference_no 		= implode( "/", $invoice );
		}
		return $reference_no;
	} else {
		if( $invoice_type == 'sale-dealer' ) {
			$invoice_no 									= $doc->dealer_sales_inv_no;
		} else if( $invoice_type == 'sale-farmer' ) {
			$invoice_no 									= $doc->farmer_sales_inv_no;
		}
	}
	


	return $invoice_no; 

}



  
  function number_to_word($n){
    $a 			= $n;
    $w 			= "";
    $crore 		= (int)($n/10000000);
    $n 			= $n%10000000;
    $w 			.= get_word($crore,"Cror ");
    $lakh 		=(int)($n/100000);
    $n 			= $n%100000;
    $w 			.= get_word($lakh,"Lakh ");
    $thousand 	= (int)($n/1000);
    $n 			= $n%1000;
    $w.=get_word($thousand,"Thousand  ");
    $hundred=(int)($n/100);
    $w.=get_word($hundred,"Hundred ");
    $ten=$n%100;
    $w.=get_word($ten,"");
    $w.="Rupees ";
    $v=explode(".",$a);
    if(isset($v[1])){
      if(strlen($v[1])==1)
      {
         $v[1]=$v[1]*10;
      }
      $w.=" and ".get_word($v[1]," Paisa");
    }
    

    return $w." Only";
  }
  
  function get_word($n,$txt){
    $t="";
    if($n<=19){

	    if( $n == '00' ) {
	    	$n 	= 0;
	    }
      $t=words_array($n);
    }else{
      $a=$n-($n%10);
      $b=$n%10;

      if( $a == '00' ) {
	    	$a 	= 0;
	    }
	    if( $b == '00' ) {
	    	$b 	= 0;
	    }
      $t=words_array($a)." ".words_array($b);
    }
    if($n==0){
      $txt="";
    }
    
    return $t." ".$txt;
  }
  
  function words_array($num){

    $n=[0=>"", 1=>"One", 2=>"Two", 3=>"Three", 4=>"Four", 5=>"Five", 6=>"Six", 7=>"Seven", 8=>"Eight", 9=>"Nine", 10=>"Ten", 11=>"Eleven", 12=>"Twelve", 13=>"Thirteen", 14=>"Fourteen", 15=>"Fifteen", 16=>"Sixteen", 17=>"Seventeen", 18=>"Eighteen", 19=>"Nineteen", 20=>"Twenty", 30=>"Thirty", 40=>"Forty", 50=>"Fifty", 60=>"Sixty", 70=>"Seventy", 80=>"Eighty", 90=>"Ninety", 100=>"Hundred",];
    return $n[$num];
  }


  function get_product_columns( $column_name, $table ) {
		$CI 						= get_instance();
		$CI->db->group_by( $column_name );
		$CI->db->where( 'status', 'ACTIVE' );
		$info 					= $CI->db->get( $table );
		if( $info->num_rows() > 0 ) {
			return $info->result();
		} else {
			return false;
		}
	}