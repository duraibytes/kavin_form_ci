<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Session_check {
    //Current Rate
    var $CI;
    public function __construct()
    {
        $this->CI =& get_instance(); 
        $this->CI->load->helper( 'auth_helper' );

    }
	public function check_session_set() {
        $route          = uri_string();
        
        if( verify_license() ) {
           
            if( ! $this->CI->session->has_userdata( 'user_id' ) ) {
               
                // echo $route;
                if( $route == 'login' || $route == 'auth' || $route == 'validte-login' || $route == 'forgot-password' || $route == 'send-reset-password' ) {
                } else {
                    redirect( 'login' );    
                }
                
            } else {

                // $route      = $this->CI->uri->segment(1);
                $route          = uri_string();
                if( $route == 'login' || $route == 'auth' || $route == 'forgot-password' ) {
                    redirect( 'dashboard' );
                }

            }
        } else {
            if( $route == 'auth/renew' ) {

            } else {
                $this->CI->load->view( 'errors/html/error_expired' );    
            }
            
        }
    }
}

