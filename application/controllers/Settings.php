
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// functions for transport and options
class Settings extends CI_Controller {
    function __construct() {
       parent::__construct();
       $this->load->model( 'Setting_model' );
       $this->load->model( 'Bank_model' );
       $this->load->library('session');
    }

    function module() {
    	$module 					= $this->Setting_model->get_all_codes();
    	$params[ 'module' ] 		= $module;
    	$title                      = 'Options';
        $anchor_btn                 = '<a href="javascript:;" onclick="return open_add_module_form()" class="btn btn-sm inventory-bg text-white" title=""> Create New Module </a>';
        $breadcrum                  = '<li class="breadcrumb-item"><a href="javascript:;">Settings</a></li><li class="breadcrumb-item"> <a href="'.base_url().'options"> Module </a></li>';
        $params[ 'title' ]          = $title;
        $params[ 'anchor_btn' ]     = $anchor_btn;
        $params[ 'breadcrum' ]      = $breadcrum;
    	$content 					= $this->load->view( 'pages/options/index', $params, true );
    	$data[ 'content' ]          = $content;
        $this->template( $data );
        return true;
    }

        
    function open_add_module_form() {
    	if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $value 						= $this->input->post( 'value', true );
        $id 						= $this->input->post( 'id', true );
        $slug 						= $this->input->post( 'slug', true );
        $btn_name 					= 'Add';
        $info 						= '';
        if( $id ) {
        	if( isset( $value ) && !empty( $value ) && $value == 'sub' ){
        		$info 				= $this->Setting_model->option_info( $id );
        		$btn_name 			= 'Update';
        	} else {
        		$info 				= $this->Setting_model->code_info( $id );
        		$btn_name 			= 'Update';
        	}
        	
        }

        $data[ 'value' ] 			= $value;
        $data[ 'id' ] 				= $id;
        $data[ 'info' ] 			= $info;
        $data[ 'slug' ] 			= $slug;
        $data[ 'btn_name' ] 		= $btn_name;
        echo $this->load->view( 'pages/options/form/add', $data, true );
    }

    function add_module() {

    	if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $id 							= $this->input->post( 'id', true );
        $slug 							= '';
        if( isset( $id ) && !empty( $id ) ) {
        	$this->form_validation->set_rules( 'module', 'Module', 'required' );
        } else {
        	$this->form_validation->set_rules( 'module', 'Module', 'required|is_unique[codes.name]' );
        }
        
        if ($this->form_validation->run() == FALSE)
        {
            $error          			= 1;
            $error_msg      			= validation_errors();
        } else {
        	$module 					= $this->input->post( 'module', true  );
        	$code_description 			= $this->input->post( 'code_description', true  );

        	$ins[ 'name' ] 				= $module;
        	$ins[ 'decription' ] 		= $code_description;
        	$slug 						= $this->input->post( 'slug', true );
        	$value 						= $this->input->post( 'value', true );
        	if( isset( $id ) && !empty( $id ) ) {
        		$where 					= array( 'id' => $id );
        		if( isset( $value ) && $value == 'sub' ) {
        			$code_id 			= $this->Common_model->update_table( 'code_options', $where, $ins );
        			$error_msg 			= '<p> Sub Module Updated Successfully </p>';	
        		} else{
        			$ins[ 'slug' ] 		= url_title( $module, 'dash', true );
        			$code_id 			= $this->Common_model->update_table( 'codes', $where, $ins );
        			$error_msg 			= '<p> Module Updated Successfully </p>';	
        		}
        		
        	} else {
        		
        		$ins[ 'created_at' ] 	= date( 'Y-m-d H:i:s' );	
        		if( isset( $value ) && $value == 'sub' ) {
        			$code_info 			= $this->Setting_model->code_info( '', $slug );
        			$ins[ 'code_id' ] 	= $code_info->id;
        			$code_id 			= $this->Common_model->insert_table( 'code_options', $ins );
        			$error_msg 			= '<p>Sub Module Added Successfully </p>';
        		} else {
        			$ins[ 'slug' ] 		= url_title( $module, 'dash', true );
        			$code_id 			= $this->Common_model->insert_table( 'codes', $ins );
        			$error_msg 			= '<p> Module Added Successfully </p>';	
        		}
        		
        	}
        	
        	if( $code_id ) {
        		$error 					= 0;
        		
        	} else {
        		$error 					= 1;
        		$error_msg 				= '<p> Error Occured while adding Module </p>';
        	}
        }

        $data[ 'error_msg' ]    		= $error_msg;
        $data[ 'error' ]        		= $error;
        $data[ 'slug' ] 				= $slug;
        echo json_encode( $data );
        return true;
    }

    function delete_module() {
    	if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }

        $id 					= $this->input->post( 'id', true );
        $slug 					= $this->input->post( 'slug', true );
        
        $where[ 'id' ] 			= $id;
        $where1[ 'code_id' ] 	= $id;

        if( isset( $slug ) && !empty( $slug ) ) {
        	$this->Common_model->delete_table( 'code_options', $where );
        } else {
        	$this->Common_model->delete_table( 'codes', $where );
        	$this->Common_model->delete_table( 'code_options', $where1 );
        }
        
        $data[ 'slug' ] 		= $slug;
        
        echo json_encode( $data );
        return true;
    }

    function sub_module() {
    	$slug 						= $this->uri->segment( 2 );
    	$optons 					= $this->Setting_model->get_all_options( $slug );
    	$params[ 'optons' ] 		= $optons;
    	$params[ 'slug' ] 			= $slug;
        $title                      = 'Add Dealer or Farmer';
        $anchor_btn                 = '<a href="'.base_url().'options" class="btn btn-dark btn-sm"> Back to Module </a>
                <a href="javascript:;" onclick="return open_add_module_form( \'\', \'sub\', \''. $slug.'\' )" class="btn btn-sm inventory-bg text-white"> Add Sub Module </a>';
        $breadcrum                  = '<li class="breadcrumb-item">Settings</li><li class="breadcrumb-item"> <a href="'.base_url().'options"> Module </a></li><li class="breadcrumb-item"> Sub Module </li>';
        $params[ 'title' ]          = $title;
        $params[ 'anchor_btn' ]     = $anchor_btn;
        $params[ 'breadcrum' ]      = $breadcrum;
    	$content 					= $this->load->view( 'pages/options/sub_module_index', $params, true );
    	$data[ 'content' ]          = $content;
        $this->template( $data );
        return true;
    }

    function transport() {
        $all                        = $this->Setting_model->get_all_transport();
        $params[ 'all' ]            = $all;
        $title                      = 'Transport';
        $anchor_btn                 = '<a href="javascript:;" onclick="return open_add_transport_form()" class="btn btn-sm inventory-bg text-white" title="">Create New</a>';

        $breadcrum                  = '<li class="breadcrumb-item"><a href="javascript:;">Settings</a></li><li class="breadcrumb-item"> <a href="'.base_url().'transport"> Transport </a></li>';
        $params[ 'title' ]          = $title;
        $params[ 'anchor_btn' ]     = $anchor_btn;
        $params[ 'breadcrum' ]      = $breadcrum;
        $content                    = $this->load->view( 'pages/transport/index', $params, true );
        $data[ 'content' ]          = $content;
        $this->template( $data );
        return true;
    }

    function open_add_transport_form() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $id                         = $this->input->post( 'id', true );
        $class_name                 = $this->input->post( 'class_name', true );
        $btn_name                   = 'Add';
        $info                       = '';
        if( $id ) {
            $info                   = $this->Setting_model->transport_info( $id );
            $btn_name               = 'Update';
            
        }
        if( isset( $class_name ) && !empty( $class_name ) ) {
            $class                  = $class_name;
        } else {
            $class                  = '';
        }
        $data[ 'id' ]               = $id;
        $data[ 'info' ]             = $info;
        $data[ 'btn_name' ]         = $btn_name;
        $data[ 'class' ]            = $class;
        echo $this->load->view( 'pages/transport/add_form', $data, true );
    }

    function add_transport() {
         if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $id                             = $this->input->post( 'id', true );
        $slug                           = '';
        if( isset( $id ) && !empty( $id ) ) {
            $this->form_validation->set_rules( 'transport_name', 'Transport Name', 'required' );
        } else {
            $this->form_validation->set_rules( 'transport_name', 'Transport Name', 'required|is_unique[transport.name]' );
        }
        $this->form_validation->set_rules( 'email', 'Email', 'valid_email' );
        $this->form_validation->set_rules( 'mobile_no', 'Mobile No', 'numeric' );
        
        if ($this->form_validation->run() == FALSE)
        {
            $error                      = 1;
            $error_msg                  = validation_errors();
        } else {
            $class                      = $this->input->post( 'class', true );
            $ins[ 'name' ]              = $this->input->post( 'transport_name', true );
            $ins[ 'type' ]              = $this->input->post( 'type', true );
            $ins[ 'email' ]             = $this->input->post( 'email', true );
            $ins[ 'mobile_no' ]         = $this->input->post( 'mobile_no', true );
            $ins[ 'address' ]           = $this->input->post( 'address', true );
            $ins[ 'slug' ]              = url_title( $this->input->post( 'transport_name', true ), 'dash', true );
            if( isset( $id ) && !empty( $id ) ) {
                $where[ 'id' ]          = $id;
                $transport_id           = $this->Common_model->update_table( 'transport', $where, $ins );  
                $error_msg              = '<p> Transport Updated Successfully </p>';
            } else{
                $ins[ 'created_at' ]    = date( 'Y-m-d H:i:s' );

                $transport_id           = $this->Common_model->insert_table( 'transport', $ins );    
                $error_msg              = '<p> Transport Updated Successfully </p>';
            }
            if( $transport_id ) {
                $error                  = 0;
            } else {
                $error                  = 1;
                $error_msg              = '<p> Error Occured while adding setting </p>';
            }
            $view                           = '';
            if( isset( $class ) && !empty( $class ) ) {
                $transport_all              = $this->Common_model->all( 'transport', '', 'ACTIVE' );
                $params[ 'transport_all' ]  = $transport_all;
                $view                       = $this->load->view( 'pages/common/custom_transport_select', $params, true );
                $data[ 'class' ]            = $class;
            }
            
        }
        $data[ 'view' ]                 = $view;
        $data[ 'error' ]                = $error;
        $data[ 'error_msg' ]            = $error_msg;
        echo json_encode( $data );
        return true;
    }

    function delete_transport() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $id                         = $this->input->post( 'id', true );
        $where[ 'id' ]              = $id;
        $table                      = 'transport';
        $this->Common_model->delete_table( $table, $where );
        
        echo 1;
        return true;
    }


    function sales_rep() {
         $all                       = $this->Setting_model->get_all_rep();
        $params[ 'all' ]            = $all;
        $title                      = 'Sales Representation';
        $anchor_btn                 = '<a href="javascript:;" onclick="return open_add_sale_rep_form()" class="btn btn-sm btn-primary" title="">Create New</a>';
        $params[ 'title' ]          = $title;
        $params[ 'anchor_btn' ]     = $anchor_btn;
        $content                    = $this->load->view( 'pages/sales_rep/index', $params, true );
        $data[ 'content' ]          = $content;
        $this->template( $data );
        return true;
    }

    function open_add_sale_rep_form() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $id                         = $this->input->post( 'id', true );
        $btn_name                   = 'Add';
        $info                       = '';
        if( $id ) {
            $info                   = $this->Setting_model->rep_info( $id );
            $btn_name               = 'Update';
            
        }
        $data[ 'id' ]               = $id;
        $data[ 'info' ]             = $info;
        $data[ 'btn_name' ]         = $btn_name;
        echo $this->load->view( 'pages/sales_rep/add', $data, true );
    }

    function add_rep() {
         if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $id                             = $this->input->post( 'id', true );
        $slug                           = '';
        if( isset( $id ) && !empty( $id ) ) {
            $this->form_validation->set_rules( 'name', 'Sales Representative Name', 'required' );
            $this->form_validation->set_rules( 'email', 'Email', 'valid_email' );
            $this->form_validation->set_rules( 'mobile_no', 'Mobile No', 'numeric' );
        } else {
            $this->form_validation->set_rules( 'name', 'Sales Representative', 'required|is_unique[transport.name]' );
            $this->form_validation->set_rules( 'email', 'Email', 'valid_email|is_unique[sales_rep.email]' );
            $this->form_validation->set_rules( 'mobile_no', 'Mobile No', 'numeric|is_unique[sales_rep.mobile_no]' );
        }
        
        if ($this->form_validation->run() == FALSE)
        {
            $error                      = 1;
            $error_msg                  = validation_errors();
        } else {
            $ins[ 'name' ]              = $this->input->post( 'name', true );
            $ins[ 'email' ]             = $this->input->post( 'email', true );
            $ins[ 'mobile_no' ]         = $this->input->post( 'mobile_no', true );
            $ins[ 'address' ]           = $this->input->post( 'address', true );
            if( isset( $id ) && !empty( $id ) ) {
                $where[ 'id' ]          = $id;
                $rep_id                 = $this->Common_model->update_table( 'sales_rep', $where, $ins );  
                $error_msg              = '<p> Sales Representative Updated Successfully </p>';
            } else{
                $ins[ 'created_at' ]    = date( 'Y-m-d H:i:s' );

                $rep_id                 = $this->Common_model->insert_table( 'sales_rep', $ins );    
                $error_msg              = '<p> Sales Representative Updated Successfully </p>';
            }
            if( $rep_id ) {
                $error                  = 0;
            } else {
                $error                  = 1;
                $error_msg              = '<p> Error Occured while adding setting </p>';
            }
            
        }
        $data[ 'error' ]                = $error;
        $data[ 'error_msg' ]            = $error_msg;
        echo json_encode( $data );
        return true;
    }

    function delete_rep() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $id                         = $this->input->post( 'id', true );
        $where[ 'id' ]              = $id;
        $table                      = 'sales_rep';
        $this->Common_model->delete_table( $table, $where );
        
        echo 1;
        return true;
    }

    function add_address() {

        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $view                           = '';
        
        $this->form_validation->set_rules( 'address', 'Address', 'required' );
        $this->form_validation->set_rules( 'city', 'City', 'required' );
        $this->form_validation->set_rules( 'state_id', 'State', 'required' );
        $this->form_validation->set_rules( 'zipcode', 'zipcode', 'required' );
        $this->form_validation->set_rules( 'country_id', 'country', 'required' );
        $this->form_validation->set_rules( 'address_type', 'Address Type', 'required' );
        $this->form_validation->set_rules( 'email', 'Email', 'valid_email' );
        $this->form_validation->set_rules( 'mobile_no', 'Mobile Number', 'numeric' );
        
        if ($this->form_validation->run() == FALSE)
        {
            $error                      = 1;
            $error_msg                  = validation_errors();
        } else {
            $key                        = $this->input->post( 'key', true );
            $from                       = $this->input->post( 'from', true );
            $country_id                 = $this->input->post( 'country_id', true );
            $country_info               = $this->Common_model->find( 'country', array( 'id' => $country_id ) );
            $state_id                   = $this->input->post( 'state_id', true );
            $state_info                 = $this->Common_model->find( 'state', array( 'id' => $state_id ) );

            $address_type               = $this->input->post( 'address_type', true );
            $sess[ 'address' ]          = $this->input->post( 'address', true );
            $sess[ 'city' ]             = $this->input->post( 'city', true );
            $sess[ 'state_id' ]         = $state_id;
            $sess[ 'state' ]            = $state_info->state;
            $sess[ 'zipcode' ]          = $this->input->post( 'zipcode', true );
            $sess[ 'country_id' ]       = $country_id;
            $sess[ 'country' ]          = $country_info->name;
            $sess[ 'email' ]            = $this->input->post( 'email', true );
            $sess[ 'mobile_no' ]        = $this->input->post( 'mobile_no', true );

            $sess[ 'address_type_id' ]  = $address_type;
            //get address type name  

            $op_info                    = $this->Setting_model->option_info( $address_type );
            $sess[ 'address_type' ]     = $op_info->name;
            $action                     = $this->input->post( 'action', true );
            if( isset( $from ) && !empty( $from ) && $from == 'customer_address' ) {
                if( $this->session->has_userdata( 'customer_address' ) ) {
                    $tmp                = $this->session->userdata( 'customer_address' );
                    
                    if( isset( $action ) && !empty( $action ) ) {
                        $tmp[$key]      = $sess;  
                    } else {
                        $tmp[]          = $sess;    
                    }

                } else {
                    $tmp[]              = $sess;
                }
                $this->session->set_userdata( 'customer_address', $tmp );
            } else if( $from == 'company_address' ) {
                if( $this->session->has_userdata( 'company_address' ) ) {
                    $tmp                = $this->session->userdata( 'company_address' );
                    
                    if( isset( $action ) && !empty( $action ) ) {
                        $tmp[$key]      = $sess;  
                    } else {
                        $tmp[]          = $sess;    
                    }

                } else {
                    $tmp[]              = $sess;
                }
                $this->session->set_userdata( 'company_address', $tmp );
            }
            
            $error                      = 0;
            $error_msg                  = 'Address added successfully';
        }
        $data[ 'error_msg' ]            = $error_msg;
        $data[ 'error' ]                = $error;
        
        echo json_encode( $data );
        return true;
    }

    function delete_address() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }

        $key                        = $this->input->post( 'key', true );
        $session_name               = $this->input->post( 'session_name', true );
        $tmp                        = [];
        if( $this->session->has_userdata( $session_name ) ) {
            $sess                   = $this->session->userdata( $session_name );
            unset( $sess[$key] );
            $tmp                    = $sess;
            $this->session->set_userdata( $session_name, $tmp ) ;
        }
        $params[ 'info' ]           = $tmp;
        
        echo  $this->load->view( 'pages/company/form/_form_address_info', $params, true );
        return true;
    }

    function open_add_bank_form() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $class_name                 = $this->input->post( 'class_name', true );
        $bank_id                    = $this->input->post( 'bank_id', true );
        $data[ 'class_name' ]       = $class_name;
        $data[ 'btn_name' ]         = 'Add';
        if( isset( $bank_id ) && !empty( $bank_id ) ) {
            $data[ 'bank_id' ]      = $bank_id;
            echo $this->load->view( 'pages/bank/form/_common_branch_form', $data, true );
        } else {
            echo $this->load->view( 'pages/bank/form/_common_bank_form', $data, true );
        }
        

    }

    function add_bank() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $this->form_validation->set_rules( 'bank_name', 'Bank Name', 'required|is_unique[bank.bank_name]' );
        $view                           = '';
        if ($this->form_validation->run() == FALSE)
        {
            $error                      = 1;
            $error_msg                  = validation_errors();
        } else {

            
            $ins[ 'bank_name' ]         = $this->input->post( 'bank_name', true );
            $ins[ 'created_at' ]        = date( 'Y-m-d H:i:s' );
            $ins[ 'slug' ]              = url_title( $this->input->post( 'bank_name', true ), 'dash', true );
            $bank_id                    = $this->Common_model->insert_table( 'bank', $ins ); 
            $error                      = 0;
            $error_msg                  = 'Bank Added Successfully';
            $bank_info                  = $this->Bank_model->get_all_bank();
            $params[ 'bank_id' ]        = $bank_id;
            $params[ 'bank_info' ]      = $bank_info; 
            $class_name                 = $this->input->post( 'class_name', true );
            
            if( isset( $class_name ) && !empty( $class_name ) ) {
                $cparams[ 'bank_info' ] = $bank_info; 
                $view                   = $this->load->view( 'pages/settings/bank/_bank_drop', $cparams, true ); 
            } else {
                $view                   = $this->load->view( 'pages/company/form/_form_bank', $params, true );
            }
            $data[ 'class_name' ]       = $this->input->post( 'class_name', true );
        }

        $data[ 'error' ]                = $error;
        $data[ 'error_msg' ]            = $error_msg;
        $data[ 'view' ]                 = $view;
        echo json_encode( $data );
    }

    function add_form() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }

        $id                             = $this->input->post( 'id' );
        $module                         = $this->input->post( 'module', true );
        if( isset( $id ) && $id == 'mode_of_payment_ajax' ) {
            $title                      = 'Payment term';
            $id_name                    = 'mode_of_payment';
            $label                      = 'Term';
            $parent_id                  = 'payment-term';
        } else if( isset( $id ) && $id == 'commission_shop_ajax' ) {
            $title                      = 'Commission Shop';
            $id_name                    = 'commission_shop';
            $label                      = 'Commission shop Reference';
            $parent_id                  = 'commission-shop';
        }
        $data[ 'id' ]                   = $id;
        $data[ 'title' ]                = $title;
        $data[ 'id_name' ]              = $id_name;
        $data[ 'label' ]                = $label;
        $data[ 'parent_id' ]            = $parent_id;
        $data[ 'module' ]               = $module;
        echo $this->load->view( 'pages/common/common_add_form', $data, true );
    }

    function insert_form() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $this->form_validation->set_rules( 'form_value', 'All Fields Mandatory', 'required|is_unique[code_options.name]' );
        $view                           = '';
        if ($this->form_validation->run() == FALSE)
        {
            $error                      = 1;
            $error_msg                  = validation_errors();
        } else {
            $form_value                 = $this->input->post( 'form_value', true );
            $parent_id                  = $this->input->post( 'parent_id', true );
            //get parent_id             
            $parent_info                = $this->Common_model->find( 'codes', array( 'slug' => $parent_id ) );
            $code_id                    = $parent_info->id;
           
            $ins[ 'name' ]              = $this->input->post( 'form_value', true );
            $ins[ 'code_id' ]           = $code_id;
            $ins[ 'created_at' ]        = date( 'Y-m-d H:i:s' );
            $option_id                  = $this->Common_model->insert_table( 'code_options', $ins ); 
            if( $this->input->post( 'module', true ) ) {
                $payment                = $this->Setting_model->get_all_options( 'payment-term', array( 'code_options.name !=' => 'ADVANCE' ) );
            } else {
                $payment                = $this->Setting_model->get_all_options( 'payment-term' );
            }
            
            $error                      = 0;
            $error_msg                  = 'Successfully Added ';
            $data[ 'id' ]               = $this->input->post( 'id', true );
            if( $parent_id == 'commission-shop' ) {
                $commission             = $this->Setting_model->get_all_options( 'commission-shop' );
                $params[ 'commission' ] = $commission;
                $view                   = $this->load->view( 'pages/common/custom_commission_payment', $params, true );
            } else {
                $params[ 'payment' ]    = $payment;
                $view                   = $this->load->view( 'pages/common/custom_payment_select', $params, true );
            }
            
        }

        $data[ 'error' ]                = $error;
        $data[ 'error_msg' ]            = $error_msg;
        $data[ 'view' ]                 = $view;
        echo json_encode( $data );
    }


    function get_bank_branch() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }

        $bank_id                    = $this->input->post( 'bank_id', true );
        $class_name                 = $this->input->post( 'class_name', true );
        $own_bank_id                = $this->input->post( 'own_bank_id', true );
        $table                      = 'bank';
        $where                      = array( 'parent_id' => $bank_id );
        $status                     = 'ACTIVE';
        $own_bank_info              = $this->Common_model->find( 'own_bank_info', array( 'id' => $own_bank_id ) );
        $branch                     = $this->Common_model->all( $table, $where, $status );
        $params[ 'branch_info' ]    = $branch;
        $params[ 'bank_id' ]        = $bank_id;
        $params[ 'own_bank_info' ]  = $own_bank_info;
        
        $view                       = $this->load->view( 'pages/settings/bank/_branch_drop', $params, true );
        $data[ 'view' ]             = $view;
        $data[ 'class_name' ]       = $class_name;
        echo json_encode( $data );
        return true;
    }

    function get_ifsc() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }    
        $branch_id                  = $this->input->post( 'branch_id', true );
        $branch_info                = $this->Common_model->find( 'bank', array( 'id' => $branch_id ) );
        
        $data[ 'ifsc' ]             = $branch_info->ifsc;
        echo json_encode( $data );
        return true;
    }

    function add_bank_branch() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $this->form_validation->set_rules( 'bank_id', 'Bank', 'required|is_unique[bank.bank_name]' );
        $this->form_validation->set_rules( 'branch', 'Bank Branch', 'required' );
        $this->form_validation->set_rules( 'ifsc_code', 'IFSC CODE', 'required' );

        $view                           = '';
        if ($this->form_validation->run() == FALSE)
        {
            $error                      = 1;
            $error_msg                  = validation_errors();
        } else {

            
            $ins[ 'parent_id' ]         = $this->input->post( 'bank_id', true );
            $ins[ 'branch' ]            = $this->input->post( 'branch', true );
            $ins[ 'ifsc' ]              = $this->input->post( 'ifsc_code', true );
            $ins[ 'created_at' ]        = date( 'Y-m-d H:i:s' );
            $ins[ 'slug' ]              = url_title( $ins[ 'parent_id' ].' '.$ins[ 'branch' ], 'dash', true );
            $bank_id                    = $this->Common_model->insert_table( 'bank', $ins ); 
            $error                      = 0;
            $error_msg                  = 'Bank Branch Added Successfully';
            $branch_info                = $this->Bank_model->bank_branch_all( $ins[ 'parent_id' ] );
            $class_name                 = $this->input->post( 'class_name', true );
            
            if( isset( $class_name ) && !empty( $class_name ) ) {
                $cparams[ 'branch_info' ] = $branch_info; 
                $view                   = $this->load->view( 'pages/settings/bank/_branch_drop', $cparams, true ); 
            } 
            $data[ 'class_name' ]       = $this->input->post( 'class_name', true );
        }

        $data[ 'error' ]                = $error;
        $data[ 'error_msg' ]            = $error_msg;
        $data[ 'view' ]                 = $view;
        echo json_encode( $data );
    }

    function template( $data ) {
        echo $this->load->view( 'layout/template', $data, true );
        return true;
    }

}

?>