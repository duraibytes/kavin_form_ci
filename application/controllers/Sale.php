<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sale extends CI_Controller {
    function __construct() {
       parent::__construct();
       $this->load->helper( 'common_helper' );
       $this->load->model( 'Setting_model' );
    }

    public function index() {
    	$title                      = 'Sales';
        $anchor_btn                 = '<a href="'.base_url().'sale/add" class="btn btn-sm sales-invoice-bg text-white" title=""> Create Sales Invoice </a>';
        $breadcrum                  = '<li class="breadcrumb-item"><a href="javascript:;">SALES</a></li><li class="breadcrumb-item"> <a href="'.base_url().'sale"> SALES INVOICES </a></li>';
        $params[ 'title' ]          = $title;
        $params[ 'anchor_btn' ]     = $anchor_btn;
        $params[ 'breadcrum' ]      = $breadcrum;
    	$content 					= $this->load->view( 'pages/sale/index', $params, true );
    	$data[ 'content' ]          = $content;
        $this->template( $data );
        return true;
    }

    public function view() {
        $title                      = 'Sales Invoice View';
        $anchor_btn                 = ' <a href="javascript:;" class="btn btn-sm btn-primary" onclick="return printpage();" title=""> Print
            </a>';
        $breadcrum                  = '<li class="breadcrumb-item"><a href="javascript:;">SALES</a></li><li class="breadcrumb-item"> <a href="'.base_url().'sale"> SALES INVOICES </a></li><li class="breadcrumb-item"><a href="javascript:;"> VIEW SALES INVOICES </a></li>';
        $params[ 'title' ]          = $title;
        $params[ 'anchor_btn' ]     = $anchor_btn;
        $params[ 'breadcrum' ]      = $breadcrum;
        $content                    = $this->load->view( 'pages/sale/view', $params, true );
        $data[ 'content' ]          = $content;
        $this->template( $data );
        return true;
    }

    public function add() {
    	$title                      = 'Add Sales';
        $anchor_btn                 = '';
        $breadcrum                  = '<li class="breadcrumb-item"><a href="javascript:;">SALES</a></li><li class="breadcrumb-item"> <a href="'.base_url().'sale"> SALES INVOICES </a></li><li class="breadcrumb-item"> <a href="javascript:;"> CREATE SALES INVOICES </a></li>';
        //get customer dealer/farmer details
        $customer_all               = $this->Common_model->all( 'customer', array( 'type' => 'dealer', 'status' => 'ACTIVE' ) );
        $payment                    = $this->Setting_model->get_all_options( 'payment-term', array( 'code_options.name !=' => 'ADVANCE' ) );
        $transport_all                  = $this->Common_model->all( 'transport', '', 'ACTIVE', 'name' );

        $params[ 'customer_type' ]  = 'dealer';
        $params[ 'total_item' ]     = 1; 
        $params[ 'payment' ]        = $payment;
        $params[ 'invoice_no' ]     = sale_auto_ref_no( 'sale-dealer' );
        $params[ 'transport_all' ]  = $transport_all;
        $params[ 'customer_all' ]   = $customer_all;
        $params[ 'title' ]          = $title;
        $params[ 'anchor_btn' ]     = $anchor_btn;
        $params[ 'breadcrum' ]      = $breadcrum;
    	$content 					= $this->load->view( 'pages/sale/add', $params, true );
    	$data[ 'content' ]          = $content;
        $this->template( $data );
        return true;
    } 

    function get_ajax_sales_tab() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $tab_type                       = $this->input->post( 'tab_type', true );

        if( $tab_type == 'dealer' ) {
            $customer_all               = $this->Common_model->all( 'customer', array( 'type' => 'dealer', 'status' => 'ACTIVE' ) );
            $payment                    = $this->Setting_model->get_all_options( 'payment-term', array( 'code_options.name !=' => 'ADVANCE' ) );
            $params[ 'payment' ]        = $payment;
            $params[ 'customer_all' ]   = $customer_all;
            $params[ 'customer_type' ]  = $tab_type;
            $params[ 'invoice_no' ]     = sale_auto_ref_no( 'sale-dealer' );
            $view                       = $this->load->view( 'pages/sale/form/_dealer_form', $params, true );
        } else {
            $customer_all               = $this->Common_model->all( 'customer', array( 'type' => 'farmer', 'status' => 'ACTIVE' ) );
            $commission                 = $this->Setting_model->get_all_options( 'commission-shop' );
            $params[ 'commission' ]     = $commission;
            $params[ 'customer_type' ]  = $tab_type;
            $params[ 'customer_all' ]   = $customer_all;
            $params[ 'invoice_no' ]     = sale_auto_ref_no( 'sale-farmer' );
            $view                       = $this->load->view( 'pages/sale/form/_farmer_form', $params, true );
        }
        $data[ 'view' ]                 = $view;
        echo json_encode( $data );
        return true;
    }

    public function payment() {
        $title                      = 'Sales Payment';
        $anchor_btn                 = '<a href="'.base_url().'sale-payment-add" class="btn btn-sm sale-payment-bg text-white" title=""> Create Sales Payment </a>';

        $breadcrum                  = '<li class="breadcrumb-item"><a href="javascript:;">SALES</a></li><li class="breadcrumb-item"> <a href="'.base_url().'sale-payment"> SALES PAYMENTS </a></li>';

        $params[ 'title' ]          = $title;
        $params[ 'anchor_btn' ]     = $anchor_btn;
        $params[ 'breadcrum' ]      = $breadcrum;
        $content                    = $this->load->view( 'pages/sale/payment/index', $params, true );
        $data[ 'content' ]          = $content;
        $this->template( $data );
        return true;
    }

    public function payment_add() {
        $title                      = 'Add Sales Payment';
        $anchor_btn                 = '';
        $breadcrum                  = '<li class="breadcrumb-item"><a href="javascript:;">SALES</a></li><li class="breadcrumb-item"> <a href="'.base_url().'sale-payment"> SALES PAYMENTS </a></li><li class="breadcrumb-item"> <a href="javascript:;" > CREATE SALES PAYMENT </a> </li>';
        $params[ 'title' ]          = $title;
        $params[ 'anchor_btn' ]     = $anchor_btn;
        $params[ 'breadcrum' ]      = $breadcrum;
        $content                    = $this->load->view( 'pages/sale/payment/add', $params, true );
        $data[ 'content' ]          = $content;
        $this->template( $data );
        return true;
    }
    public function payment_view() {
        $title                      = 'Sales Invoice View';
        $anchor_btn                 = '<a href="javascript:;" class="btn btn-sm btn-primary" onclick="return printpage();" title=""> Print </a>';

        $breadcrum                  = '<li class="breadcrumb-item"><a href="javascript:;">SALES</a></li><li class="breadcrumb-item"><a href="'.base_url().'sale-payment"> SALES PAYMENTS </a></li><li class="breadcrumb-item"><a href="javascript:;"> VIEW SALES PAYMENT </a></li>';

        $params[ 'title' ]          = $title;
        $params[ 'anchor_btn' ]     = $anchor_btn;
        $params[ 'breadcrum' ]      = $breadcrum;
        $content                    = $this->load->view( 'pages/sale/view', $params, true );
        $data[ 'content' ]          = $content;
        $this->template( $data );
        return true;
    }


    public function return() {
        $title                      = 'Sales Return';
        $anchor_btn                 = '<a href="'.base_url().'sale-return-add" class="btn btn-sm sale-return-bg text-white" title=""> Create Sales Return </a>';

        $breadcrum                  = '<li class="breadcrumb-item"><a href="javascript:;">SALES</a></li><li class="breadcrumb-item"> <a href="'.base_url().'sale-return"> SALES RETURNS </a></li>';
        $params[ 'title' ]          = $title;
        $params[ 'anchor_btn' ]     = $anchor_btn;
        $params[ 'breadcrum' ]      = $breadcrum;
        $content                    = $this->load->view( 'pages/sale/return/index', $params, true );
        $data[ 'content' ]          = $content;
        $this->template( $data );
        return true;
    }

    public function return_add() {
        $title                      = 'Add Sales Return';
        $anchor_btn                 = '';
         $breadcrum                  = '<li class="breadcrumb-item"><a href="javascript:;">SALES</a></li><li class="breadcrumb-item"> <a href="'.base_url().'sale-return"> SALES RETURNS </a></li><li class="breadcrumb-item"><a href="javascript:;"> CREATE SALES RETURN </a></li>';
        $params[ 'title' ]          = $title;
        $params[ 'anchor_btn' ]     = $anchor_btn;
        $params[ 'breadcrum' ]      = $breadcrum;
        $content                    = $this->load->view( 'pages/sale/return/add', $params, true );
        $data[ 'content' ]          = $content;
        $this->template( $data );
        return true;
    }

    public function return_view() {
        $title                      = 'Sales Invoice View';
        $anchor_btn                 = '<a href="javascript:;" class="btn btn-sm btn-primary" onclick="return printpage();" title=""> Print
            </a>';
        $breadcrum                  = '<li class="breadcrumb-item"><a href="javascript:;">SALES</a></li><li class="breadcrumb-item"> <a href="'.base_url().'sale-return"> SALES RETURNS </a></li><li class="breadcrumb-item"> <a href="javascript:;"> VIEW SALES RETURN </a></li>';
        $params[ 'title' ]          = $title;
        $params[ 'anchor_btn' ]     = $anchor_btn;
        $params[ 'breadcrum' ]      = $breadcrum;
        $content                    = $this->load->view( 'pages/sale/view', $params, true );
        $data[ 'content' ]          = $content;
        $this->template( $data );
        return true;
    }

    function get_due_days() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }

        $payment_id                 = $this->input->post( 'payment_id', true );
        
        $pay_info                   = $this->Common_model->find( 'code_options', array( 'id' => $payment_id ) );
        $due_day                    = '';
        
        if( $pay_info ) {
            $day_name               = $pay_info->name;
            
            $numbers                = preg_replace('/[^0-9]/', '', $day_name);
            if( isset( $numbers ) && !empty( $numbers ) ) {
                $today              = date( 'Y-m-d' );
                $due_day            = date( "Y-m-d", strtotime( $today. "+ $numbers day"));
                $due_day            = date( "d/m/Y", strtotime( $due_day ) );
            } 
        }

        $data[ 'due_day' ]          = $due_day;
        echo json_encode( $data );
        return false;
    }

    function get_ajax_items() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $total_item                 = $this->input->post( 'total_item', true );
        $params[ 'total_item' ]     = $total_item;
        echo $this->load->view( 'pages/sale/_sale_items', $params, true );
    }

    function insert_sale() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        
        $sale_id                    = $this->input->post( 'sale_id', true );
        $tranport_payment           = $this->input->post( 'tranport_payment', true );

        $this->form_validation->set_rules( 'dealer', 'Dealer', 'required' );
        if( ! $sale_id ) {
            $this->form_validation->set_rules( 'sale_invoice_no', 'Sale Invoice no', 'required|is_unique[sale.invoice_no]' );    
        }
        
        $this->form_validation->set_rules( 'mode_of_payment', 'Mode of Payment', 'required' );
        $this->form_validation->set_rules( 'tranport_payment', 'Mode of Transport', 'required' );
        $this->form_validation->set_rules( 'mode_of_transport', 'Mode of Transport', 'required' );
        $this->form_validation->set_rules( 'name_of_transport', 'Name of Transport', 'required' );
        
        $this->form_validation->set_rules( 'sale_due_date', 'Sale Due Date', 'required' );
        
        $this->form_validation->set_rules( 'total_amount', 'Total Amount', 'required' );
        if( strtolower( $tranport_payment ) == 'topay' ) {
            $this->form_validation->set_rules( 'transport_charge', 'Transport Charge', 'required|numeric' );
        }

        $total_item                             = $this->input->post( 'total_item', true );
        $code                                   = [];
        for ( $i=1; $i <= $total_item; $i++ ) { 
            $code[]                             = $this->input->post( 'product_id_'.$i, true );
            $this->form_validation->set_rules( 'rate_'.$i, 'Product Rate '.$i, 'numeric' );
            $this->form_validation->set_rules( 'qty_'.$i, 'Product Qty '.$i, 'numeric' );
            $this->form_validation->set_rules( 'discount_'.$i, 'Product Discount '.$i, 'numeric' );
            $this->form_validation->set_rules( 'net_value_'.$i, 'Product Net Value '.$i, 'numeric' );
        }
        $code                                   = array_filter( $code );
        if( empty( $code ) ) {
            $this->form_validation->set_rules( 'product_id', 'Product Items', 'required' );
        }

        if ( $this->form_validation->run() == FALSE )
        {
            $error                              = 1;
            $error_msg                          = validation_errors();
        } else {

            $file_full_path                     = null;
            if( isset($_FILES['sale_image']['name']) && !empty($_FILES['sale_image']['name']) ) {

                $file                           = 'sale_image'; 
                $temp_file                      = explode('.', $_FILES["$file"]['name']);  
                $config['upload_path']          = 'assets/images/sale';
                $config['allowed_types']        = '*';
                $config['max_size']             = 500000;
                $filetype                       = $_FILES[ "$file" ][ 'type' ];
                $expfiletype                    = explode( '.', $_FILES[ "$file" ][ 'name' ] );  
                $config['file_name']            = strtolower($temp_file[0]).'-'.rand( 101, 999 ) . '.' . $expfiletype[ 1 ];
                $this->upload->initialize( $config );
                $this->load->library( 'upload', $config );
                if ( $this->upload->do_upload( $file ) ) {  
                    $upload_data                = $this->upload->data(); 
                    $filename                   = $upload_data[ 'file_name' ];  
                    $file_full_path             = base_url().'assets/images/sale/'.$filename;
                    $filepath                   = $upload_data[ 'full_path' ];
                } else {
                   $error_msg                   = 'Image upload failed!';
                   $error                       = 1;
                }

            }

            if( isset( $file_full_path ) && !empty( $file_full_path ) ) {
                $ins[ 'attachment' ]            = $file_full_path;
            }

            $sale_due_date                      = $this->input->post( 'sale_due_date', true );
            
            if( isset( $sale_due_date ) && !empty( $sale_due_date ) ) {
                $sale_due_date                  = str_replace('/', '-', $sale_due_date);
                $date2                          = strtotime($sale_due_date);
                $due_date                       = date( 'Y-m-d', $date2 );
            } else {
                $due_date                       = null;
            }

            if( $this->input->post( 'dealer', true ) ) {
                $customer_id                    = $this->input->post( 'dealer', true );
                $customer_type                  = 'dealer';
            } else {
                $customer_id                    = $this->input->post( 'dealer', true );
                $customer_type                  = 'farmer';
            }

            $ins[ 'customer_id' ]               = $customer_id; 
            $ins[ 'customer_type' ]             = $customer_type;
            $ins[ 'invoice_no' ]                = $this->input->post( 'sale_invoice_no', true );
            $ins[ 'invoice_date' ]              = date( 'Y-m-d' );
            $ins[ 'invoice_due_date' ]          = $due_date;
            $ins[ 'payment_term_id' ]           = $this->input->post( 'mode_of_payment', true );
            $ins[ 'mode_of_transport' ]         = $this->input->post( 'tranport_payment', true );
            $ins[ 'name_of_transport' ]         = $this->input->post( 'name_of_transport', true ); 
            $ins[ 'transport_id ' ]             = $this->input->post( 'mode_of_transport', true ); 
            $ins[ 'transport_charge' ]          = $this->input->post( 'transport_charge', true );
            $ins[ 'total_amount' ]              = $this->input->post( 'total_amount', true ); 
            $ins[ 'remarks' ]                   = $this->input->post( 'remarks', true ); 
            $ins[ 'item_amount' ]               = $this->input->post( 'total_item_amount', true );
            $ins[ 'discount' ]                  = $this->input->post( 'sale_discount', true );
            $ins[ 'discount_amount' ]           = $this->input->post( 'total_item_amount_discount_amount', true );
            $slug                               = base64_encode( $this->input->post( 'sale_invoice_no', true ));

            if( ! $sale_id ) {
                $ins[ 'invoice_verified_by' ]   = $this->session->userdata( 'user_id' );
                $ins[ 'created_by' ]            = $this->session->userdata( 'user_id' );
                $ins[ 'status' ]                = 'ACTIVE';
                $ins[ 'created_at' ]            = date( 'Y-m-d H:i:s' );
                $ins[ 'slug' ]                  = $slug;
                $ins[ 'owner_id' ]              = $this->session->userdata( 'company_id' );
                $sale_id                        = $this->Common_model->insert_table( 'sale', $ins );
                $error_msg                      = "Sale Added Successfully";
            } else {
                $where[ 'id' ]                  = $sale_id;
                $this->Common_model->update_table( 'sale', $where, $ins );
                $error_msg                      = 'Sales Updated Successfully';
                //delete purchase items datat
                $where_delete[ 'purchase_id' ]  = $purchase_id;
                $this_update[ 'status' ]        = 'INACTIVE';
                $this->Common_model->update_table( 'purchase_items', $where_delete, $this_update );
                $inventory_update[ 'mode' ]     = 'purchase'; 
                $inventory_update[ 'mode_id' ]  = $purchase_id;
                $this->Common_model->update_table( 'inventory', $inventory_update, $this_update );
            }
            
            //insert purchase items details
            for ( $i=1; $i <= $total_item; $i++ ) { 
                $items                              = [];
                $product_id                         = $this->input->post( 'product_id_'.$i, true );
                if( isset( $product_id ) && !empty( $product_id ) ) {
                    $items[ 'date_of_packed' ]      = null;
                    $items[ 'date_of_tested' ]      = null;
                    $items[ 'date_of_expiry' ]      = null;

                    $product_code                   = $this->input->post( 'code_'.$i, true );
                    $lot                            = $this->input->post( 'lot_'.$i, true );
                    $uom                            = $this->input->post( 'uom_'.$i, true );
                    $rate                           = $this->input->post( 'rate_'.$i, true );
                    $packed_date                    = $this->input->post( 'packed_date_'.$i, true );
                    if( $packed_date ) {
                        $packed_date                = str_replace('/', '-', $packed_date);
                        $pk_Date                    = strtotime($packed_date);
                        $pack_Date                  = date( 'Y-m-d', $pk_Date );
                        $items[ 'date_of_packed' ]  = $pack_Date;
                    }

                    $tested_date                    = $this->input->post( 'tested_date_'.$i, true );
                    $expiry_date                    = $this->input->post( 'expiry_date_'.$i, true );
                    if( $tested_date ) {
                        $tested_date                = str_replace('/', '-', $tested_date);
                        $tt_date                    = strtotime($tested_date);
                        $tst_date                   = date( 'Y-m-d', $tt_date );
                        $items[ 'date_of_tested' ]  = $tst_date;
                    }

                    if( $expiry_date ) {
                        $expiry_date                = str_replace('/', '-', $expiry_date);
                        $ee_date                    = strtotime($expiry_date);
                        $exp_date                   = date( 'Y-m-d', $ee_date );
                        $items[ 'date_of_expiry' ]  = $exp_date;
                    } 

                    $items[ 'sale_id' ]             = $sale_id;
                    $items[ 'product_id' ]          = $product_id;
                    $items[ 'lot_no' ]              = $lot ?? '';
                    $items[ 'net_rate' ]            = $rate;
                    $items[ 'qty' ]                 = $this->input->post( 'qty_'.$i, true );
                    
                    $items[ 'discount' ]            = $this->input->post( 'discount_'.$i, true );
                    $items[ 'total_amount' ]        = $this->input->post( 'net_value_'.$i, true );
                    $items[ 'status' ]              = 'ACTIVE';
                    $items[ 'created_by' ]          = $this->session->userdata( 'user_id' );
                    $items[ 'created_at' ]          = date( 'Y-m-d H:i:s' ); 

                    $sale_item_id                   = $this->Common_model->insert_table( 'sale_items', $items );
                    unset( $items[ 'sale_id' ] );
                    $items[ 'purchase_item_id' ]    = $sale_item_id;
                    $items[ 'mode' ]                = 'sale';
                    $items[ 'mode_id' ]             = $sale_id;
                    $inventory_id                   = $this->Common_model->insert_table( 'inventory', $items );
                }
            }
            $error                                  = 0;
            $data[ 'slug' ]                         = $slug; 
        }
        $data[ 'error' ]                            = $error;
        $data[ 'error_msg' ]                        = $error_msg;
        
        echo json_encode( $data );
    }

    function add_farmer() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }

        $class_name             = $this->input->post( 'class_name', true );
        $params[ 'class_name' ] = $class_name;
        $params[ 'ajax_type' ]  = 'ajax';
        $params[ 'title' ]      = 'Add Farmer';
        echo $this->load->view( 'pages/common/common_add_farmer', $params, true );
        return true;
    }

    function template( $data ) {
        echo $this->load->view( 'layout/template', $data, true );
        return true;
    }

}


?>