
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {
    function __construct() {
       parent::__construct();
       $this->load->model( 'Bank_model' );
    }

    public function index() {
        $this->session->unset_userdata( 'customer_address' );
    	$title                      = 'Dealer & Farmer';
        $anchor_btn                 = '<a href="'.base_url().'customer/add" class="btn btn-sm inventory-bg text-white" title=""> Create Dealer / Farmer </a>';
        $breadcrum                  = '<li class="breadcrumb-item"><a href="javascript:;">Settings</a></li><li class="breadcrumb-item"><a href="javascript:;"> Dealer - Farmer </a></li>';
        $params[ 'title' ]          = $title;
        $params[ 'anchor_btn' ]     = $anchor_btn;
        $customer_info              = $this->Common_model->get_customer_all();
        $params[ 'customer_info' ]  = $customer_info;
        $params[ 'breadcrum' ]      = $breadcrum;
    	$content 					= $this->load->view( 'pages/dealer/index', $params, true );
    	$data[ 'content' ]          = $content;
        $this->template( $data );
        return true;
    }

    public function add() {
        // $this->session->unset_userdata( 'customer_address' );
        $title                              = 'Add Dealer or Farmer';
        $customer_info                      = '';
        $bank_id                            = '';
        $bank                               = '';
        $id                                 = $this->uri->segment( 3 );
        $breadcrum                          = '<li class="breadcrumb-item"><a href="javascript:;">Settings</a></li><li class="breadcrumb-item"> <a href="'.base_url().'customer" >Dealer - Farmer</a> </li><li class="breadcrumb-item"><a href="javascript:;"> Add  Dealer - Farmer </a></li>';
        if( isset( $id ) && !empty( $id ) ) {
            $customer_info                  = $this->Common_model->get_customer_info( $id );
            $id                             = $customer_info->id;
            $title                          = 'Update Company';
            $bank                           = $this->Bank_model->bank_info( '', $customer_info->bank_id );
            $bank_id                        = $bank->parent_id;
            $title                          = 'Update Dealer or Farmer';
            $breadcrum                      = '<li class="breadcrumb-item">Settings</li><li class="breadcrumb-item"> <a href="'.base_url().'customer" >Dealer - Farmer</a> </li><li class="breadcrumb-item"> Update  Dealer - Farmer </li>';
        }

    	
        $anchor_btn                         = '';
        $bank_info                          = $this->Bank_model->get_all_bank();
        $params[ 'bank_info' ]              = $bank_info; 
        $params[ 'title' ]                  = $title;
        $params[ 'anchor_btn' ]             = $anchor_btn;
        $params[ 'customer_info' ]          = $customer_info;
        $params[ 'id' ]                     = $id;
        $params[ 'bank_id' ]                = $bank_id;
        $params[ 'bank' ]                   = $bank;
        $params[ 'breadcrum' ]              = $breadcrum;
    	$content 					        = $this->load->view( 'pages/dealer/add', $params, true );
    	$data[ 'content' ]                  = $content;
        $this->template( $data );
        return true;
    }

    public function insert_customer() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $id                                 = $this->input->post( 'id', true );
        $bank                               = $this->input->post( 'bank', true );
        $ac_no                              = $this->input->post( 'ac_no', true );
        $customer_type                      = $this->input->post( 'customer_type', true );
        $customer_info                      = $this->Common_model->get_customer_info( $id );
        $this->form_validation->set_rules( 'customer_type', 'Customer Type', 'required' );
        if( ! $id ) {
            if( isset( $customer_type ) && $customer_type == 'dealer' ) {
                $this->form_validation->set_rules( 'dealer_name', 'Dealer Name', 'required' );
                $this->form_validation->set_rules( 'proprietor_name', 'Proprietor Name', 'required' );
                $this->form_validation->set_rules( 'proprietor_mobile', 'Proprietor Mobile', 'required|is_unique[customer.proprietor_mobile]' );
                $this->form_validation->set_rules( 'proprietor_email', 'Proprietor Name', 'valid_email' );
                
                if( ( isset( $bank ) && !empty( $bank ) ) || ( isset( $ac_no ) && !empty( $ac_no ) ) ) {
                    $this->form_validation->set_rules( 'ac_no', 'Bank Account No', 'required|numeric|is_unique[customer.bank_ac_no]' );
                    $this->form_validation->set_rules( 'bank', 'Bank', 'required' );
                    $this->form_validation->set_rules( 'bank_branch', 'Bank Branch', 'required' );
                    $this->form_validation->set_rules( 'ifsc', 'Bank IFSC', 'required' );
                }
            } else {
                $this->form_validation->set_rules( 'farmer_name', 'Farmer Name', 'required' );
                $this->form_validation->set_rules( 'farmer_mobile', 'Farmer Mobile', 'required|is_unique[customer.mobile_no]' );
                $this->form_validation->set_rules( 'alter_mobile', 'Farmer Alter Mobile', 'numeric' );
                $this->form_validation->set_rules( 'farmer_email', 'Farmer Email', 'valid_email' );
            }
        } else {
            if( isset( $customer_type ) && $customer_type == 'dealer' ) {

                $this->form_validation->set_rules( 'dealer_name', 'Dealer Name', 'required' );
                $this->form_validation->set_rules( 'proprietor_name', 'Proprietor Name', 'required' );
                if( $this->input->post( 'proprietor_mobile' ) != $customer_info->proprietor_mobile && empty( $customer_info->proprietor_mobile ) ) {
                    $this->form_validation->set_rules( 'proprietor_mobile', 'Proprietor Mobile', 'required|is_unique[customer.proprietor_mobile]' );
                }
                
                $this->form_validation->set_rules( 'proprietor_email', 'Proprietor Name', 'valid_email' );
                
                if( ( isset( $bank ) && !empty( $bank ) ) || ( isset( $ac_no ) && !empty( $ac_no ) ) ) {
                    if( $this->input->post( 'ac_no' ) != $customer_info->bank_ac_no && empty( $customer_info->bank_ac_no ) ) {
                        $this->form_validation->set_rules( 'ac_no', 'Bank Account No', 'required|numeric|is_unique[customer.bank_ac_no]' );
                    }

                    $this->form_validation->set_rules( 'bank', 'Bank', 'required' );
                    $this->form_validation->set_rules( 'bank_branch', 'Bank Branch', 'required' );
                    $this->form_validation->set_rules( 'ifsc', 'Bank IFSC', 'required' );
                }
            } else {
                $this->form_validation->set_rules( 'farmer_name', 'Farmer Name', 'required' );

                if( $this->input->post( 'farmer_mobile' ) != $customer_info->mobile_no && empty( $customer_info->mobile_no ) ) {
                    $this->form_validation->set_rules( 'farmer_mobile', 'Farmer Mobile', 'required|is_unique[customer.mobile_no]' );
                }
               
                $this->form_validation->set_rules( 'alter_mobile', 'Farmer Alter Mobile', 'numeric' );
                $this->form_validation->set_rules( 'farmer_email', 'Farmer Email', 'valid_email' );
            }
        }
        

        if ($this->form_validation->run() == FALSE)
        {
            $error                              = 1;
            $error_msg                          = validation_errors();
        } else {

            $file_full_path                     = null;
            if( isset($_FILES['customer_image']['name']) && !empty($_FILES['customer_image']['name']) ) {

                $file                           = 'customer_image'; 
                $temp_file                      = explode('.', $_FILES["$file"]['name']);  
                $config['upload_path']          = 'assets/images/customer';
                $config['allowed_types']        = '*';
                $config['max_size']             = 500000;
                $filetype                       = $_FILES[ "$file" ][ 'type' ];
                $expfiletype                    = explode( '.', $_FILES[ "$file" ][ 'name' ] );  
                $config['file_name']            = strtolower($temp_file[0]).'-'.rand( 101, 999 ) . '.' . $expfiletype[ 1 ];
                $this->upload->initialize( $config );
                $this->load->library( 'upload', $config );
                if ( $this->upload->do_upload( $file ) ) {  
                    $upload_data                = $this->upload->data(); 
                    $filename                   = $upload_data[ 'file_name' ];  
                    $file_full_path             = base_url().'assets/images/customer/'.$filename;
                    $filepath                   = $upload_data[ 'full_path' ];
                } else {
                   $error_msg                   = 'Image upload failed!';
                   $error                       = 1;
                }
            }
            

            $bank_branch                        = $this->input->post( 'bank_branch', true );
            $ifsc                               = $this->input->post( 'ifsc', true );
            $branch_exists                      = $this->Bank_model->check_bank_branch_exists( $bank, $ifsc );
            if( ! $branch_exists ) {
                $ins_branch[ 'parent_id' ]      = $bank;
                $ins_branch[ 'branch' ]         = $bank_branch;
                $ins_branch[ 'ifsc' ]           = $ifsc;
                $ins_branch[ 'created_by' ]     = $this->session->userdata( 'user_id' );
                $bank_id                        = $this->Common_model->insert_table( 'bank', $ins_branch ); 
            } else {
                $bank_id                        = $branch_exists->id;
            }
            if( isset( $file_full_path ) && !empty( $file_full_path ) ) {
                $ins[ 'image' ]                 = $file_full_path;    
            }
            
            $ins[ 'type' ]                      = $customer_type;
            if( $customer_type == 'dealer' ) {
                $customer_name                  = $this->input->post( 'dealer_name', true );

                $ins[ 'fname' ]                 = $customer_name; 
                $ins[ 'proprietor_name' ]       = $this->input->post( 'proprietor_name', true );
                $ins[ 'proprietor_mobile' ]     = $this->input->post( 'proprietor_mobile', true );
                $ins[ 'proprietor_email' ]      = $this->input->post( 'proprietor_email', true );
                
                $ins[ 'seed_license' ]          = $this->input->post( 'license', true );
                $ins[ 'pesticides_license' ]    = $this->input->post( 'pesticides_license', true );
                $ins[ 'fertilizer_license' ]    = $this->input->post( 'fertilizer_license', true );
                $ins[ 'gst' ]                   = $this->input->post( 'gst', true );
                $ins[ 'cst' ]                   = $this->input->post( 'cst', true );
                $ins[ 'vat' ]                   = $this->input->post( 'vat', true );
                $ins[ 'bank_id' ]               = $bank_id;
                $ins[ 'bank_ac_no' ]            = $this->input->post( 'ac_no', true );
                
                $ins[ 'slug' ]                  = url_title( $customer_name, 'dash', true );

            } else {
                $customer_name                  = $this->input->post( 'farmer_name', true );  

                $ins[ 'fname' ]                 = $customer_name;
                $ins[ 'mobile_no' ]             = $this->input->post( 'farmer_mobile', true );
                $ins[ 'alter_mobile_no' ]       = $this->input->post( 'alter_mobile', true );
                $ins[ 'email' ]                 = $this->input->post( 'farmer_email', true );
                $ins[ 'address' ]               = $this->input->post( 'farmer_address', true );
                $ins[ 'state' ]                 = $this->input->post( 'farmer_state', true );
                $ins[ 'zipcode' ]               = $this->input->post( 'zip_code', true );
                $ins[ 'slug' ]                  = url_title( $customer_name, 'dash', true );
            }
            if( isset( $id ) && !empty( $id ) ) {
               //update
                $where[ 'id' ]                      = $id;
                $this->Common_model->update_table( 'customer', $where, $ins );
                $error_msg                          = 'Customer Updated Successfully';
                $customer_id                        = $id;

                  //delete address from address tatble
                $del_where[ 'mode_type' ]           = 'customer';
                $del_where[ 'mode_id' ]             = $customer_id;
                $this->Common_model->delete_table( 'address', $del_where );
            } else {
                $ins[ 'created_at' ]                = date( 'Y-m-d H:i:s' );
                $ins[ 'owner_id' ]                  = $this->session->userdata( 'company_id' );
                $customer_id                        = $this->Common_model->insert_table( 'customer', $ins );
                $error_msg                          = 'Customer Added Successfully';
            }

            ######### step 3
            if( $this->session->has_userdata( 'customer_address' ) ) {
                $address                            = $this->session->userdata( 'customer_address' );
                foreach ( $address as $addkey => $addvalue ) {
                    $adr_ins[ 'address_type_id' ]   = $addvalue[ 'address_type_id' ];
                    $adr_ins[ 'address_type' ]      = $addvalue[ 'address_type' ];
                    $adr_ins[ 'address' ]           = $addvalue[ 'address' ];
                    $adr_ins[ 'city' ]              = $addvalue[ 'city' ];
                    $adr_ins[ 'state' ]             = $addvalue[ 'state' ];
                    $adr_ins[ 'zip_code' ]          = $addvalue[ 'zipcode' ];   
                    $adr_ins[ 'country' ]           = $addvalue[ 'country' ];
                    $adr_ins[ 'email' ]             = $addvalue[ 'email' ];
                    $adr_ins[ 'mobile_no' ]         = $addvalue[ 'mobile_no' ];

                    $adr_ins[ 'mode_type' ]         = 'customer';
                    $adr_ins[ 'mode_id' ]           = $customer_id;
                    $adr_ins[ 'created_at' ]        = date( 'Y-m-d H:i:s' );
                    $this->Common_model->insert_table( 'address', $adr_ins ); 
                }
                $this->session->unset_userdata( 'customer_address' );
            } 
            $error                              = 0;
            $ajax_type                          = $this->input->post( 'ajax_type', true );
            if( isset( $ajax_type ) && !empty( $ajax_type ) ) {
                $customer_all                   = $this->Common_model->all( 'customer', array( 'type' => 'farmer', 'status' => 'ACTIVE' ) );
                $params[ 'customer_all' ]       = $customer_all;
                $data[ 'view' ]                 = $this->load->view( 'pages/common/common_farmer_select', $params, true );
            }
        }

        $data[ 'error' ]                        = $error;
        $data[ 'error_msg' ]                    = $error_msg;
        echo json_encode( $data );
    }

    function address_list() {
        
        $customer_id                                 = $this->input->post( 'customer_id', true );
        if( isset( $customer_id ) && !empty( $customer_id ) ) {
            $address                                = $this->Common_model->get_common_address( $customer_id, 'customer', 'all' );
            ////sert session values for address
            if( isset( $address ) && !empty( $address ) ) {
                foreach ( $address as $akey => $avalue ) {
                    $adr_ins[ 'address_type_id' ]   = $avalue->address_type_id;
                    $adr_ins[ 'address_type' ]      = $avalue->address_type;
                    $adr_ins[ 'address' ]           = $avalue->address;
                    $adr_ins[ 'city' ]              = $avalue->city;
                    $adr_ins[ 'state' ]             = $avalue->state;
                    $adr_ins[ 'zipcode' ]           = $avalue->zip_code;   
                    $adr_ins[ 'country' ]           = $avalue->country;
                    $adr_ins[ 'email' ]             = $avalue->email;
                    $adr_ins[ 'mobile_no' ]         = $avalue->mobile_no;

                    $tmp[]                          = $adr_ins;
                }
            }
            
            $this->session->set_userdata( 'customer_address', $tmp );
        }

        $tmp                        = $this->session->userdata( 'customer_address' );
        $params[ 'info' ]           = $tmp;
        $params[ 'from' ]           = 'customer_address';
        echo $this->load->view( 'pages/company/form/_form_address_info', $params, true );
        return true;
    }

    function delete_customer() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $id                         = $this->input->post( 'id', true );
        $where[ 'id' ]              = $id;
        $table                      = 'customer';
        $data[ 'status' ]           = 'INACTIVE';
        $this->Common_model->update_table( $table, $where, $data );
        echo 1;
        return true;
    }

    function template( $data ) {
        echo $this->load->view( 'layout/template', $data, true );
        return true;
    }

}

?>