<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Purchase extends CI_Controller {
    function __construct() {
       parent::__construct();
       $this->load->model( 'Setting_model' );
       $this->load->model( 'Purchase_model' );
       $this->load->model( 'Bank_model' );
       $this->load->helper( 'common_helper' );
    }

    public function index() {
    	
        $anchor_btn                 = '<a href="'.base_url().'purchase/add" class="btn btn-sm purchase-invoice-bg text-white" title=""> Create Purchase Invoice </a>';
        $title                      = 'Purchase';

        $breadcrum                  = '<li class="breadcrumb-item"><a href="javascript:;">PURCHASE</a></li><li class="breadcrumb-item"> <a href="'.base_url().'purchase"> PURCHASE INVOICES </a></li>';

        $params[ 'title' ]          = $title;
        $params[ 'anchor_btn' ]     = $anchor_btn;
        $params[ 'breadcrum' ]      = $breadcrum;
    	$content 					= $this->load->view( 'pages/purchase/index', $params, true );
    	$data[ 'content' ]          = $content;
        $this->template( $data );
        return true;
    }

    public function view() {
        $slug                       = $this->uri->segment( 3 );

        $title                      = 'View Purchase Invoice';
        $breadcrum                  = '<li class="breadcrumb-item"> <a href="'.base_url().'purchase"> PURCHASE </a></li><li class="breadcrumb-item"><a href="javascript:;"> VIEW PURCHASE INVOICE </a></li>';

        $anchor_btn                 = '<a href="javascript:;" class="btn btn-sm btn-primary" onclick="return print_purchase( \''.$slug.'\' );" title=""> Print
            </a>';

        $purchase_info              = $this->Purchase_model->get_purchase_info( '', $slug );
        $items                      = $this->Purchase_model->purchase_items( $purchase_info->id , 'ACTIVE' );
        $company_address            = $this->Common_model->get_company_address( $purchase_info->from_company );
        $own_info                   = $this->Common_model->find( 'owner', array( 'id' => $this->session->userdata( 'company_id' ) ) );

        $params[ 'own_info' ]       = $own_info;
        $params[ 'purchase_info' ]  = $purchase_info;
        $params[ 'items' ]          = $items;
        $params[ 'company_address' ]= $company_address; 
        $params[ 'title' ]          = $title;
        $params[ 'anchor_btn' ]     = $anchor_btn;
        $params[ 'breadcrum' ]      = $breadcrum;
        $content                    = $this->load->view( 'pages/purchase/view', $params, true );
        $data[ 'content' ]          = $content;
        $this->template( $data );
        return true;
    }

    public function add() {
        $slug                           = $this->uri->segment( 3 );
        if( $slug ) {
            $purchase_info              = $this->Purchase_model->get_purchase_info( '', $slug );
            
            $items                      = $this->Purchase_model->purchase_items( $purchase_info->id, 'ACTIVE' );
            
            $params[ 'purchase_info' ]  = $purchase_info;
            $params[ 'items' ]          = $items;
            $purchase_ref_no            = $purchase_info->invoice_no;
            $total_item                 = count( $items );
            $title                      = 'Update Purchase';
            $breadcrum                  = '<li class="breadcrumb-item"> <a href="'.base_url().'purchase"> PURCHASE</a></li><li class="breadcrumb-item"><a href="javascript:;">  UPDATE PURCHASE INVOICE </a></li>';

        } else {
            $purchase_ref_no            = get_auto_ref_no( 'purchase' );
            $total_item                 = 1;
            $title                      = 'Add Purchase';
            $breadcrum                  = '<li class="breadcrumb-item"> <a href="'.base_url().'purchase"> PURCHASE</a></li><li class="breadcrumb-item"><a href="javascript:;">  CREATE PURCHASE INVOICE </a></li>';
        }

    	
        $anchor_btn                     = '';

        $company_all                    = $this->Common_model->all( 'company', '', 'ACTIVE' );
        $transport_all                  = $this->Common_model->all( 'transport', '', 'ACTIVE', 'name' );

        $payment                        = $this->Setting_model->get_all_options( 'payment-term', '', 'code_options.name', 'ASC' );
        

        $params[ 'title' ]              = $title;
        $params[ 'anchor_btn' ]         = $anchor_btn;
        $params[ 'breadcrum' ]          = $breadcrum;
        $params[ 'company_all' ]        = $company_all;
        $params[ 'payment' ]            = $payment;
        $params[ 'transport_all' ]      = $transport_all;
        $params[ 'purchase_ref_no' ]    = $purchase_ref_no; 
        $params[ 'total_item' ]         = $total_item;
    	$content 					    = $this->load->view( 'pages/purchase/add', $params, true );
    	$data[ 'content' ]              = $content;
        $this->template( $data );
        return true;
    } 

    function insert_purchase() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $purchase_id                = $this->input->post( 'purchase_id', true );
        $tranport_payment           = $this->input->post( 'tranport_payment', true );
        $mode_of_payment            = $this->input->post( 'mode_of_payment', true );
        $pay_val_info               = $this->Common_model->find( 'code_options', array( 'id' => $mode_of_payment ) );

        $this->form_validation->set_rules( 'company_invoice_no', 'Purchase Invoice no', 'required' );
        if( ! $purchase_id ) {
            $this->form_validation->set_rules( 'purchase_ref_no', 'Purchase Invoice no', 'required|is_unique[purchase.invoice_no]' );    
        }
        
        $this->form_validation->set_rules( 'from_company', 'From Company', 'required' );
        $this->form_validation->set_rules( 'mode_of_payment', 'Mode of Payment', 'required' );
        $this->form_validation->set_rules( 'tranport_payment', 'Mode of Transport', 'required' );
        $this->form_validation->set_rules( 'mode_of_transport', 'Mode of Transport', 'required' );
        $this->form_validation->set_rules( 'name_of_transport', 'Name of Transport', 'required' );
        
        $this->form_validation->set_rules( 'purchase_date', 'Purchase Date', 'required' );
        if( isset( $pay_val_info->name ) && !empty( $pay_val_info->name ) && $pay_val_info->name == 'ADVANCE' ) {

        }else {
            $this->form_validation->set_rules( 'invoice_due_date', 'Purchase Invoice Due date(format)', 'required' );
        }
        
        $this->form_validation->set_rules( 'total_amount', 'Total Amount', 'required' );
        if( strtolower( $tranport_payment ) == 'topay' ) {
            $this->form_validation->set_rules( 'transport_charge', 'Transport Charge', 'required|numeric' );
        }

        $total_item                             = $this->input->post( 'total_item', true );
        $code                                   = [];
        for ( $i=1; $i <= $total_item; $i++ ) { 
            $code[]                             = $this->input->post( 'product_id_'.$i, true );
            $this->form_validation->set_rules( 'rate_'.$i, 'Product Rate '.$i, 'numeric' );
            $this->form_validation->set_rules( 'qty_'.$i, 'Product Qty '.$i, 'numeric' );
            $this->form_validation->set_rules( 'discount_'.$i, 'Product Discount '.$i, 'numeric' );
            $this->form_validation->set_rules( 'net_value_'.$i, 'Product Net Value '.$i, 'numeric' );
        }
        $code                                   = array_filter( $code );
        if( empty( $code ) ) {
            $this->form_validation->set_rules( 'product_id', 'Product Items', 'required' );
        }

        if ( $this->form_validation->run() == FALSE )
        {
            $error                              = 1;
            $error_msg                          = validation_errors();
        } else {

            $file_full_path                     = null;
            if( isset($_FILES['upload']['name']) && !empty($_FILES['upload']['name']) ) {

                $file                           = 'upload'; 
                $temp_file                      = explode('.', $_FILES["$file"]['name']);  
                $config['upload_path']          = 'assets/images/purchase';
                $config['allowed_types']        = '*';
                $config['max_size']             = 500000;
                $filetype                       = $_FILES[ "$file" ][ 'type' ];
                $expfiletype                    = explode( '.', $_FILES[ "$file" ][ 'name' ] );  
                $config['file_name']            = strtolower($temp_file[0]).'-'.rand( 101, 999 ) . '.' . $expfiletype[ 1 ];
                $this->upload->initialize( $config );
                $this->load->library( 'upload', $config );
                if ( $this->upload->do_upload( $file ) ) {  
                    $upload_data                = $this->upload->data(); 
                    $filename                   = $upload_data[ 'file_name' ];  
                    $file_full_path             = base_url().'assets/images/purchase/'.$filename;
                    $filepath                   = $upload_data[ 'full_path' ];
                } else {
                   $error_msg                   = 'Image upload failed!';
                   $error                       = 1;
                }
            }
            if( isset( $file_full_path ) && !empty( $file_full_path ) ) {
                $ins[ 'attachment' ]            = $file_full_path;
            }
            $purchase_date                      = $this->input->post( 'purchase_date', true );
            $invoice_due_date                   = $this->input->post( 'invoice_due_date', true );
            
            $purchase_date                      = str_replace('/', '-', $purchase_date);
            $date1                              = strtotime($purchase_date);
            $pur_Date                           = date( 'Y-m-d', $date1 );
            if( isset( $invoice_due_date ) && !empty( $invoice_due_date ) ) {
                $invoice_due_date               = str_replace('/', '-', $invoice_due_date);
                $date2                          = strtotime($invoice_due_date);
                $due_date                       = date( 'Y-m-d', $date2 );
            } else {
                $due_date                       = null;
            }

            $ins[ 'invoice_no' ]                = $this->input->post( 'purchase_ref_no', true ); 
            $ins[ 'from_company' ]              = $this->input->post( 'from_company', true ); 
            $ins[ 'company_invoice_no' ]        = $this->input->post( 'company_invoice_no', true ); 
            $ins[ 'purchase_date' ]             = $pur_Date;
            $ins[ 'invoice_due_date' ]          = $due_date;
            $ins[ 'mode_of_payment' ]           = $this->input->post( 'mode_of_payment', true );
            $ins[ 'tranport_payment' ]          = $tranport_payment;
            $ins[ 'transport_charge' ]          = $this->input->post( 'transport_charge', true ); 
            $ins[ 'name_of_transport' ]         = $this->input->post( 'name_of_transport', true ); 
            $ins[ 'transport_id ' ]             = $this->input->post( 'mode_of_transport', true ); 
            
            $ins[ 'total_amount' ]              = $this->input->post( 'total_amount', true ); 
            $ins[ 'remarks' ]                   = $this->input->post( 'remarks', true ); 
            $slug                               = base64_encode( $this->input->post( 'purchase_ref_no', true ));
            if( ! $purchase_id ) {
                $ins[ 'invoice_verified_by' ]   = $this->session->userdata( 'user_id' );
                $ins[ 'created_by' ]            = $this->session->userdata( 'user_id' );
                $ins[ 'status' ]                = 'ACTIVE';
                $ins[ 'created_at' ]            = date( 'Y-m-d H:i:s' );
                $ins[ 'slug' ]                  = $slug;
                $ins[ 'owner_id' ]              = $this->session->userdata( 'company_id' );
                $purchase_id                    = $this->Common_model->insert_table( 'purchase', $ins );
                $error_msg                      = "Purchase Added Successfully";
            } else {
                $where[ 'id' ]                  = $purchase_id;
                $this->Common_model->update_table( 'purchase', $where, $ins );
                $error_msg                      = 'Purchase Updated Successfully';
                //delete purchase items datat
                $where_delete[ 'purchase_id' ]  = $purchase_id;
                $this_update[ 'status' ]        = 'INACTIVE';
                $this->Common_model->update_table( 'purchase_items', $where_delete, $this_update );
                $inventory_update[ 'mode' ]     = 'purchase'; 
                $inventory_update[ 'mode_id' ]  = $purchase_id;
                $this->Common_model->update_table( 'inventory', $inventory_update, $this_update );
            }
            
            //insert purchase items details
            for ( $i=1; $i <= $total_item; $i++ ) { 
                $items                              = [];
                $product_id                         = $this->input->post( 'product_id_'.$i, true );
                if( isset( $product_id ) && !empty( $product_id ) ) {
                    $items[ 'date_of_packed' ]      = null;
                    $items[ 'date_of_tested' ]      = null;
                    $items[ 'date_of_expiry' ]      = null;

                    $product_code                   = $this->input->post( 'code_'.$i, true );
                    $lot                            = $this->input->post( 'lot_'.$i, true );
                    $uom                            = $this->input->post( 'uom_'.$i, true );
                    $rate                           = $this->input->post( 'rate_'.$i, true );
                    $packed_date                    = $this->input->post( 'packed_date_'.$i, true );
                    if( $packed_date ) {
                        
                        $packed_date                = str_replace('/', '-', $packed_date);

                        $pk_Date                    = strtotime($packed_date);
                        $pack_Date                  = date( 'Y-m-d', $pk_Date );
                        $items[ 'date_of_packed' ]  = $pack_Date;
                    }

                    $tested_date                    = $this->input->post( 'tested_date_'.$i, true );
                    $expiry_date                    = $this->input->post( 'expiry_date_'.$i, true );
                    if( $tested_date ) {
                       
                        $tested_date                = str_replace('/', '-', $tested_date);
                        $tt_date                    = strtotime($tested_date);
                        $tst_date                   = date( 'Y-m-d', $tt_date );
                        $items[ 'date_of_tested' ]  = $tst_date;
                    }

                    if( $expiry_date ) {
                        $expiry_date                = str_replace('/', '-', $expiry_date);
                        $ee_date                    = strtotime($expiry_date);
                        $exp_date                   = date( 'Y-m-d', $ee_date );
                        $items[ 'date_of_expiry' ]  = $exp_date;
                    } 

                    $items[ 'purchase_id' ]         = $purchase_id;
                    $items[ 'product_id' ]          = $product_id;
                    $items[ 'lot_no' ]              = $lot ?? '';
                    $items[ 'net_rate' ]            = $rate;
                    $items[ 'qty' ]                 = $this->input->post( 'qty_'.$i, true );
                    
                    $items[ 'discount' ]            = $this->input->post( 'discount_'.$i, true );
                    $items[ 'total_amount' ]        = $this->input->post( 'net_value_'.$i, true );
                    $items[ 'status' ]              = 'ACTIVE';
                    $items[ 'created_by' ]          = $this->session->userdata( 'user_id' );
                    $items[ 'created_at' ]          = date( 'Y-m-d H:i:s' ); 

                    $purchase_item_id               = $this->Common_model->insert_table( 'purchase_items', $items );
                    unset( $items[ 'purchase_id' ] );
                    $items[ 'purchase_item_id' ]    = $purchase_item_id;
                    $items[ 'mode' ]                = 'purchase';
                    $items[ 'mode_id' ]             = $purchase_id;
                    $inventory_id                   = $this->Common_model->insert_table( 'inventory', $items );
                }
            }
            $error                              = 0;
            $data[ 'slug' ]                     = $slug; 
        }
        $data[ 'error' ]                        = $error;
        $data[ 'error_msg' ]                    = $error_msg;
        
        echo json_encode( $data );
    }

    function get_purchase() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $data                       = [];
        $fetch_data                 = $this->Purchase_model->make_purchase_datatables();
        
        if( $fetch_data ) {
            $i                      = 1;
            foreach ( $fetch_data as $key => $value ) {
                
                $edit_btn           = '';
                $delete_btn         = '';
                $view_btn           = '';
                $pay_btn            = '';
               
                $edit_btn           = '<a href="'.base_url().'purchase/add/'.$value->slug.'" class="btn btn-outline-warning"> <i class="icon-pencil"></i>  </a>&nbsp;';
                $delete_btn         = '<a href="javascript:;" class="btn btn-outline-danger" onClick="return are_you_sure( \'delete_purchase\' ,\'Delete Purchase\', \'Are you sure want to delete this purchase? \', '.$value->id.' );" ><i class="icon-trash"></i></a>';
                $view_btn           = '<a href="'. base_url() .'purchase/view/'.$value->slug.'" class="btn btn-outline-success"><i class="fa fa-eye"></i></a>';
                $pay_btn            = '<a href="'.base_url().'purchase/payment/'.$value->slug.'" class="btn btn-outline-primary"><i class="fa fa-money"></i></a> ';

                $status             = '<span class="badge badge-danger">Payment Pending</span>';
                if( $value->total_amount <= $value->paid_amount ) {
                    $status         = '<span class="badge badge-success">Payment Completed </span>';
                }
                $sub_array          = array();
                $sub_array[]        = $value->company_name;
                $sub_array[]        = date( 'd-m-Y', strtotime( $value->purchase_date ) );
                $sub_array[]        = date( 'd-m-Y', strtotime( $value->invoice_due_date ) );
                $sub_array[]        = $value->invoice_no;
                $sub_array[]        = $value->company_invoice_no;
                $sub_array[]        = $value->transport_name;
                $sub_array[]        = 'Rs '.number_format( $value->total_amount, 2 );
                $sub_array[]        = 'Rs '.number_format( $value->paid_amount, 2 );
                $sub_array[]        = $status;
                $sub_array[]        = $view_btn . $edit_btn . $pay_btn . $delete_btn;
                
                $data[]             = $sub_array;

                $i++;
            }
        }
        $draw                       = ( isset( $_POST[ 'draw' ] ) ? $_POST[ 'draw' ] : 0 );
        $output                     = array( 
                                        'draw'                  => $draw,
                                        'recordsTotal'          => $this->Purchase_model->get_all_purchase_data(),
                                        'recordsFiltered'       => $this->Purchase_model->get_purchase_filtered_data(),
                                        'data'                  => $data 
                                    );
        echo json_encode( $output );
        return true;
    }

    function delete_purchase() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $id                         = $this->input->post( 'id', true );
        $where[ 'id' ]              = $id;
        $table                      = 'purchase';

        $this->Common_model->delete_table( $table, $where );
        $where_del[ 'purchase_id' ] = $id;
        $this->Common_model->delete_table( 'purchase_items', $where_del );
        echo 1;
        return true;
    }

    function get_purchase_typeahead() {
        $c_query                                        = $this->input->get( 'query' );
        $return_company                                 = $this->input->get( 'return_company' );
        $purchase_list                                  = $this->Purchase_model->typehead_purchase_list( $c_query, $return_company );
        if( $purchase_list ) {
            foreach ( $purchase_list as $key => $value ) {
                $items                                  = $this->Purchase_model->purchase_items( $value->id );
                
                $data[ $key ][ 'name' ]                 = $value->invoice_no;
                $data[ $key ][ 'company_invoice_no' ]   = $value->company_invoice_no;
                $data[ $key ][ 'total_amount' ]         = $value->total_amount; 
                $data[ $key ][ 'topay_amount' ]         = number_format( $value->total_amount - $value->paid_amount, 2 ); 
                $data[ $key ][ 'total_item' ]           = count( $items );
                // $data[ $key ][ 'words' ]                = number_to_word( $value->total_amount ); 
                $data[ $key ][ 'id' ]                   = $value->id;
            }
        } else {
            $data[0]['name']                            = 'No data found';
            $data[0]['id']                              = 0;
        }
        echo json_encode( $data );
        return true;
    }

    function get_purchase_items() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $purchase_id                        = $this->input->post( 'purchase_id', true );
        $items                              = $this->Purchase_model->purchase_items( $purchase_id, 'ACTIVE' );
        $params[ 'items' ]                  = $items;
        //get returns items and return view
        $return_items                       = $this->Purchase_model->purchase_return_items( '', 'ACTIVE', $purchase_id );
        $return_params[ 'return_items' ]    = $return_items;
        $return_view                        = $this->load->view( 'pages/purchase/return/purchased_item_list', $return_params, true );

        $item_view                          = $this->load->view( 'pages/purchase/return/_return_items', $params, true );

        $data[ 'return_view' ]              = $return_view;
        $data[ 'item_view' ]                = $item_view;
        echo json_encode( $data );
        return true;
    }

    public function purchase_payment() {

        $slug                           = $this->uri->segment( 3 );
        $current_balance                = '0.00';
        if( $slug ) {
            
            $purchase_info              = $this->Purchase_model->get_purchase_info( '', $slug );
            $topay_invoice_amount       = $purchase_info->total_amount - $purchase_info->paid_amount; 
            $current_balance            = $this->get_company_current_balance( $purchase_info->from_company );
            
            $title                      = 'Create Purchase Payment';    
            $breadcrum                  = '<li class="breadcrumb-item"><a href="'.base_url().'purchase">PURCHASE</a></li><li class="breadcrumb-item"> <a href="'.base_url().'purchase-payment"> PURCHASE PAYMENTS </a></li><li class="breadcrumb-item">
        <a href="javascript:;"> CREATE PURCHASE PAYMENTS </a></li>';

        } 
        $purchase_pay_no                = get_auto_ref_no( 'purchase_payment' );
        $anchor_btn                     = '';
        $company_all                    = $this->Common_model->all( 'company', '', 'ACTIVE' );
        $transport_all                  = $this->Common_model->all( 'transport', '', 'ACTIVE' );
        $payment                        = $this->Setting_model->get_all_options( 'payment-method' );
        $bank_info                      = $this->Bank_model->get_all_bank();

        $params[ 'current_balance' ]    = $current_balance;
        $params[ 'bank_info' ]          = $bank_info; 
        $params[ 'company_all' ]        = $company_all;
        $params[ 'payment' ]            = $payment;
        $params[ 'purchase_info' ]       = $purchase_info;
        $params[ 'title' ]              = $title;
        $params[ 'topay_invoice_amount' ] = $topay_invoice_amount;
        $params[ 'purchase_pay_no' ]    = $purchase_pay_no;
        $params[ 'anchor_btn' ]         = $anchor_btn;
        $params[ 'breadcrum' ]          = $breadcrum;
        $content                        = $this->load->view( 'pages/purchase/payment/add', $params, true );
        $data[ 'content' ]              = $content;
        $this->template( $data );
        return true;
    }

    public function return() {
        $title                      = 'Purchase Return';
        $anchor_btn                 = '<a href="'.base_url().'purchase-return-add" class="btn btn-sm purchase-return-bg text-white" title=""> Create Purchase Return </a>';


        $breadcrum                  = '<li class="breadcrumb-item"><a href="'.base_url().'purchase">PURCHASE</a></li><li class="breadcrumb-item"> <a href="'.base_url().'purchase"> PURCHASE RETURNS </a></li>';

        
        $params[ 'title' ]          = $title;
        $params[ 'anchor_btn' ]     = $anchor_btn;
        $params[ 'breadcrum' ]      = $breadcrum;
        $content                    = $this->load->view( 'pages/purchase/return/index', $params, true );
        $data[ 'content' ]          = $content;
        $this->template( $data );
        return true;
    }

    public function return_add() {
        $slug                           = $this->uri->segment( 2 );
        $purchase_return_ref_no     = get_auto_ref_no( 'purchase_return' );
        if( $slug ) {
            $return_info                = $this->Purchase_model->get_purchase_return_info( '', $slug );
            
            $items                      = $this->Purchase_model->purchase_return_items( $return_info->id, 'ACTIVE' );
            
            $params[ 'return_info' ]    = $return_info;
            $params[ 'items' ]          = $items;
            $purchase_ref_no            = $return_info->return_no;
            $purchase_return_ref_no     = $return_info->return_no;
            $total_item                 = count( $items );
            $title                      = 'Update Purchase';
            $breadcrum                  = '<li class="breadcrumb-item"> <a href="'.base_url().'purchase"> PURCHASE</a></li><li class="breadcrumb-item"><a href="javascript:;">  UPDATE PURCHASE RETURNS </a></li>';

        } else {
            $title                      = 'Add Purchase Return';
            $breadcrum                  = '<li class="breadcrumb-item"><a href="'.base_url().'purchase">PURCHASE</a></li><li class="breadcrumb-item"> <a href="'.base_url().'purchase-return"> PURCHASE RETURNS </a></li><li class="breadcrumb-item"><a href="javascript:;"> CREATE PURCHASE RETURNS </a></li>';
        }

        
        $anchor_btn                 = '';

        $company_all                = $this->Common_model->all( 'company', '', 'ACTIVE' );
         $transport_all                  = $this->Common_model->all( 'transport', '', 'ACTIVE', 'name' );
        $payment                    = $this->Setting_model->get_all_options( 'payment-method' );

        
        $params[ 'company_all' ]    = $company_all;
        $params[ 'transport_all' ]  = $transport_all;
        $params[ 'payment' ]        = $payment;

        $params[ 'title' ]          = $title;
        $params[ 'anchor_btn' ]     = $anchor_btn;
        $params[ 'breadcrum' ]      = $breadcrum;
        $params[ 'total_item' ]     = $total_item ?? '';
        $params[ 'purchase_return_ref_no' ] = $purchase_return_ref_no;
        $content                    = $this->load->view( 'pages/purchase/return/add', $params, true );
        $data[ 'content' ]          = $content;
        $this->template( $data );
        return true;
    }

    function insert_purchase_return() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        
        $purchase_return_id                    = $this->input->post( 'id', true );
        $transport_payment                      = $this->input->post( 'transport_payment', true );

        $this->form_validation->set_rules( 'return_company', 'Return Company', 'required' );
        $this->form_validation->set_rules( 'purchase_id', 'Purchase Invoice No', 'required' );
        $this->form_validation->set_rules( 'return_date', 'Return Date', 'required' );
        $this->form_validation->set_rules( 'reason', 'Return Reason', 'required' );
        $this->form_validation->set_rules( 'name_of_transport', 'Name of Transport', 'required' );
        if( isset( $tranport_payment ) && strtolower( $tranport_payment ) == 'topay' ) {
            $this->form_validation->set_rules( 'transport_charge', 'Transport Charge', 'required' );
        }
        $this->form_validation->set_rules( 'mode_of_transport', 'Mode of Transport', 'required' );
        $this->form_validation->set_rules( 'total_amount', 'Total Amount', 'required' );

        $total_item                             = $this->input->post( 'total_item', true );
        $code                                   = [];
        for ( $i=1; $i <= $total_item; $i++ ) { 
            $code[]                             = $this->input->post( 'product_id_'.$i, true );
            $this->form_validation->set_rules( 'rate_'.$i, 'Product Rate '.$i, 'numeric' );
            $this->form_validation->set_rules( 'qty_'.$i, 'Product Qty '.$i, 'numeric' );
            $this->form_validation->set_rules( 'discount_'.$i, 'Product Discount '.$i, 'numeric' );
            $this->form_validation->set_rules( 'net_value_'.$i, 'Product Net Value '.$i, 'numeric' );
        }
        $code                                   = array_filter( $code );
        if( empty( $code ) ) {
            $this->form_validation->set_rules( 'product_id', 'Product Items', 'required' );
        }

        if ($this->form_validation->run() == FALSE)
        {
            $error                              = 1;
            $error_msg                          = validation_errors();
        } else {

            $file_full_path                     = null;
            if( isset($_FILES['upload']['name']) && !empty($_FILES['upload']['name']) ) {

                $file                           = 'upload'; 
                $temp_file                      = explode('.', $_FILES["$file"]['name']);  
                $config['upload_path']          = 'assets/images/purchase_return';
                $config['allowed_types']        = '*';
                $config['max_size']             = 500000;
                $filetype                       = $_FILES[ "$file" ][ 'type' ];
                $expfiletype                    = explode( '.', $_FILES[ "$file" ][ 'name' ] );  
                $config['file_name']            = strtolower($temp_file[0]).'-'.rand( 101, 999 ) . '.' . $expfiletype[ 1 ];
                $this->upload->initialize( $config );
                $this->load->library( 'upload', $config );
                if ( $this->upload->do_upload( $file ) ) {  
                    $upload_data                = $this->upload->data(); 
                    $filename                   = $upload_data[ 'file_name' ];  
                    $file_full_path             = base_url().'assets/images/purchase_return/'.$filename;
                    $filepath                   = $upload_data[ 'full_path' ];
                } else {
                   $error_msg                   = 'Image upload failed!';
                   $error                       = 1;
                }
            }
            if( isset( $file_full_path ) && !empty( $file_full_path ) ) {
                $ins[ 'attachment' ]            = $file_full_path;
            }
       
            $ins[ 'purchase_id' ]               = $this->input->post( 'purchase_id', true ); 
            $ins[ 'purchase_invoice_no' ]       = $this->input->post( 'purchase_invoice_no', true ); 

            $ins[ 'return_no' ]                 = $this->input->post( 'purchase_return_no', true ); 
            $ins[ 'return_to' ]                 = $this->input->post( 'return_company', true );
            $ins[ 'return_date' ]               = date( 'Y-m-d', strtotime( $this->input->post( 'return_date' ) ) );
            $ins[ 'transport_charge' ]          = $this->input->post( 'transport_charge', true ); 
            $ins[ 'transport_payment' ]         = $this->input->post( 'transport_payment', true );
            $ins[ 'transport_name' ]            = $this->input->post( 'name_of_transport', true ); 
            $ins[ 'transport_id ' ]             = $this->input->post( 'mode_of_transport', true ); 
            
            $ins[ 'total_amount' ]              = $this->input->post( 'total_amount', true ); 
            $ins[ 'remarks' ]                   = $this->input->post( 'remarks', true ); 
            $ins[ 'reason' ]                    = $this->input->post( 'reason', true ); 
            $data[ 'slug' ]                     = base64_encode( $this->input->post( 'purchase_return_no', true ));
            if( ! $purchase_return_id ) {
                
                $ins[ 'created_by' ]            = $this->session->userdata( 'user_id' );
                $ins[ 'status' ]                = 'ACTIVE';
                $ins[ 'created_at' ]            = date( 'Y-m-d H:i:s' );
                $ins[ 'slug' ]                  = base64_encode( $this->input->post( 'purchase_return_no', true ));
                $ins[ 'owner_id' ]              = $this->session->userdata( 'company_id' );
                
                $purchase_return_id             = $this->Common_model->insert_table( 'purchase_return', $ins );

                $error_msg                      = "Purchase Return Added Successfully";
            } else {
                $where[ 'id' ]                  = $purchase_return_id;
                $this->Common_model->update_table( 'purchase_return', $where, $ins );
                $error_msg                      = 'Purchase Return Updated Successfully';
                //delete purchase items datat
                $where_delete[ 'purchase_return_id' ]  = $purchase_return_id;
                $update[ 'status' ]             = 'INACTIVE';
                $this->Common_model->update_table( 'purchase_return_items', $where_delete, $update );

                $inventory_update[ 'mode' ]     = 'purchase-return'; 
                $inventory_update[ 'mode_id' ]  = $purchase_return_id;
                $this->Common_model->update_table( 'inventory', $inventory_update, $update );
            }
         
            //insert purchase items details
            $total_qty                              = 0;
            for ( $i=1; $i <= $total_item; $i++ ) { 
                $items                              = [];
                $product_id                         = $this->input->post( 'product_id_'.$i, true );
                if( isset( $product_id ) && !empty( $product_id ) ) {
                   $product_code                    = $this->input->post( 'code_'.$i, true );
                    $lot                            = $this->input->post( 'lot_'.$i, true );
                    $uom                            = $this->input->post( 'uom_'.$i, true );
                    $rate                           = $this->input->post( 'rate_'.$i, true );
                    $packed_date                    = $this->input->post( 'packed_date_'.$i, true );
                    $item_id                        = $this->input->post( 'item_id_'.$i );
                    if( $packed_date ) {
                        $packed_date                = str_replace('/', '-', $packed_date);
                        $pk_Date                    = strtotime($packed_date);
                        $pack_Date                  = date( 'Y-m-d', $pk_Date );
                    } else {
                        $pack_Date                  = null;
                    }

                    $tested_date                    = $this->input->post( 'tested_date_'.$i, true );
                    $expiry_date                    = $this->input->post( 'expiry_date_'.$i, true );
                    if( $tested_date ) {
                       
                        $tested_date                = str_replace('/', '-', $tested_date);
                        $tt_date                    = strtotime($tested_date);
                        $tst_date                   = date( 'Y-m-d', $tt_date );
                    } else {
                        $tst_date                   = null;
                    }
                    if( $expiry_date ) {
                        $expiry_date                = str_replace('/', '-', $expiry_date);
                        $ee_date                    = strtotime($expiry_date);
                        $exp_date                   = date( 'Y-m-d', $ee_date );
                    } else {
                        $exp_date                   = null;
                    }

                    $items[ 'purchase_return_id' ]  = $purchase_return_id;
                    $items[ 'product_id' ]          = $product_id;
                    $items[ 'purchase_item_id' ]    = $item_id;
                    $items[ 'lot_no' ]              = $lot ?? '';
                    $items[ 'net_rate' ]            = $rate;
                    $items[ 'qty' ]                 = $this->input->post( 'qty_'.$i, true );
                    $items[ 'date_of_packed' ]      = $pack_Date;
                    $items[ 'date_of_expiry' ]      = $exp_date;
                    $items[ 'date_of_tested' ]      = $tst_date;
                    $items[ 'discount' ]            = $this->input->post( 'discount_'.$i, true );
                    $items[ 'total_amount' ]        = $this->input->post( 'net_value_'.$i, true );
                    $items[ 'status' ]              = 'ACTIVE';
                    $items[ 'created_by' ]          = $this->session->userdata( 'user_id' );
                    $items[ 'created_at' ]          = date( 'Y-m-d H:i:s' ); 

                    $total_qty                      += $items[ 'qty' ];
                    $purchase_item_id               = $this->Common_model->insert_table( 'purchase_return_items', $items );

                    unset( $items[ 'purchase_return_id' ] );

                    $items[ 'mode' ]                = 'purchase-return';
                    $items[ 'mode_id' ]             = $purchase_return_id;
                    $purchase_item_id               = $this->Common_model->insert_table( 'inventory', $items );
                }
            }
            $upda[ 'total_qty' ]                    = $total_qty;
            $where1[ 'id' ]                         = $purchase_return_id;
            $this->Common_model->update_table( 'purchase_return', $where1, $upda );
            $error                                  = 0;
        }
        $data[ 'error' ]                            = $error;
        $data[ 'error_msg' ]                        = $error_msg;
        
        echo json_encode( $data );
    }


     function delete_purchase_return() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $id                                 = $this->input->post( 'id', true );
        $where[ 'id' ]                      = $id;
        $table                              = 'purchase_return';

        $this->Common_model->delete_table( $table, $where );
        $where_del[ 'purchase_return_id' ]  = $id;
        $this->Common_model->delete_table( 'purchase_return_items', $where_del );
        echo 1;
        return true;
    }

    public function get_purchase_return() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $data                       = [];
        $fetch_data                 = $this->Purchase_model->make_purchase_return_datatables();

        if( $fetch_data ) {
            $i                      = 1;
            foreach ( $fetch_data as $key => $value ) {
                
                $edit_btn           = '';
                $delete_btn         = '';
                $view_btn           = '';
                $pay_btn            = '';
               
                $edit_btn           = '<a href="'.base_url().'purchase-return-add/'.$value->slug.'" class="btn btn-outline-warning"> <i class="icon-pencil"></i>  </a>&nbsp;';
                $delete_btn         = '<a href="javascript:;" class="btn btn-outline-danger" onClick="return are_you_sure( \'delete_purchase_return\' ,\'Delete Purchase Return\', \'Are you sure want to delete this purchase return? \', '.$value->id.' );" ><i class="icon-trash"></i></a>';
                $view_btn           = '<a href="'.base_url().'purchase-return/view/'.$value->slug.'" class="btn btn-outline-success"><i class="fa fa-eye"></i></a>';
                $pay_btn            = '<a href="javascript:;" class="btn btn-outline-primary"><i class="fa fa-money"></i></a> ';

                $sub_array          = array();
                $sub_array[]        = $value->return_no;
                $sub_array[]        = date( 'd-m-Y', strtotime( $value->return_date ) );
                $sub_array[]        = $value->company_name;
                $sub_array[]        = $value->purchase_invoice_no;
                $sub_array[]        = date( 'd-m-Y', strtotime( $value->created_at ) );
                $sub_array[]        = $value->reason;
                $sub_array[]        = $value->total_qty;
                $sub_array[]        = 'Rs '.number_format( $value->total_amount, 2 );
                $sub_array[]        = '<span class="badge badge-danger">Payment Pending</span>';
                $sub_array[]        = $view_btn . $edit_btn . $pay_btn . $delete_btn;
                
                $data[]             = $sub_array;

                $i++;
            }
        }
        $draw                       = ( isset( $_POST[ 'draw' ] ) ? $_POST[ 'draw' ] : 0 );
        $output                     = array( 
                                        'draw'                  => $draw,
                                        'recordsTotal'          => $this->Purchase_model->get_all_purchase_return_data(),
                                        'recordsFiltered'       => $this->Purchase_model->get_purchase_return_filtered_data(),
                                        'data'                  => $data 
                                    );
        echo json_encode( $output );
        return true;
    }
    
    public function return_view() {
        $slug                       = $this->uri->segment( 3 );

        $title                      = 'View Purchase Return Invoice';
        $breadcrum                  = '<li class="breadcrumb-item"> <a href="'.base_url().'purchase-return"> PURCHASE RETURN </a></li><li class="breadcrumb-item"><a href="javascript:;"> VIEW PURCHASE Return INVOICE </a></li>';

        $anchor_btn                 = '<a href="javascript:;" class="btn btn-sm btn-primary" onclick="return print_purchase_return( \''.$slug.'\' );" title=""> Print
            </a>';

        $purchase_return_info      = $this->Purchase_model->get_purchase_return_info( '', $slug );
        $items                      = $this->Purchase_model->purchase_return_items( $purchase_return_info->id, 'ACTIVE' );
        $company_address            = $this->Common_model->get_company_address( $purchase_return_info->return_to );
        $own_info                   = $this->Common_model->find( 'owner', array( 'id' => $this->session->userdata( 'company_id' ) ) );

        $params[ 'own_info' ]       = $own_info;
        $params[ 'purchase_info' ]  = $purchase_return_info;
        $params[ 'items' ]          = $items;
        $params[ 'company_address' ]= $company_address; 
        $params[ 'title' ]          = $title;
        $params[ 'anchor_btn' ]     = $anchor_btn;
        $params[ 'breadcrum' ]      = $breadcrum;
        $content                    = $this->load->view( 'pages/purchase/return/return_view', $params, true );
        $data[ 'content' ]          = $content;
        $this->template( $data );
        return true;
    }

    function get_ajax_items() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }

        $total_item                 = $this->input->post( 'total_item', true );
        $params[ 'total_item' ]     = $total_item;
        echo $this->load->view( 'pages/purchase/_purchase_items', $params, true );
    }

    public function payment() {
        $title                      = 'Purchase Payment';
        $anchor_btn                 = '<a href="'.base_url().'purchase-payment-add" class="btn btn-sm purchase-payment-bg text-white" title=""> Create Purchase Payment </a>';
        $breadcrum                  = '<li class="breadcrumb-item" > <a href="'.base_url().'purchase"> PURCHASE </a></li><li class="breadcrumb-item"> <a href="javascript:;"> PURCHASE PAYMENTS </a></li>';
        $params[ 'title' ]          = $title;
        $params[ 'anchor_btn' ]     = $anchor_btn;
        $params[ 'breadcrum' ]      = $breadcrum;
        $content                    = $this->load->view( 'pages/purchase/payment/index', $params, true );
        $data[ 'content' ]          = $content;
        $this->template( $data );
        return true;
    }

    public function payment_add() {

        $slug                           = $this->uri->segment( 2 );

        $current_balance                = '0.00';
        if( $slug ) {
            $receipt_no                 = base64_decode( $slug );
            
            $payment_info               = $this->Purchase_model->purchase_payment_info( $receipt_no );
            
            $purchase_pay_no            = $receipt_no;
            
            $current_balance            = $this->get_company_current_balance( $payment_info->from_company );

            $params[ 'payment_info' ]   = $payment_info;

            $title                      = 'Update Purchase Payment';    
            $breadcrum                  = '<li class="breadcrumb-item"><a href="'.base_url().'purchase">PURCHASE</a></li><li class="breadcrumb-item"> <a href="'.base_url().'purchase-payment"> PURCHASE PAYMENTS </a></li><li class="breadcrumb-item">
        <a href="javascript:;"> UPDATE PURCHASE PAYMENTS </a></li>';

        } else {
            $purchase_pay_no            = get_auto_ref_no( 'purchase_payment' );
            $title                      = 'Add Purchase Payment';    
            $breadcrum                  = '<li class="breadcrumb-item"><a href="'.base_url().'purchase">PURCHASE</a></li><li class="breadcrumb-item"> <a href="'.base_url().'purchase-payment"> PURCHASE PAYMENTS </a></li><li class="breadcrumb-item">
        <a href="javascript:;"> CREATE PURCHASE PAYMENTS </a></li>';
        }
        
        $anchor_btn                     = '';
        

        $company_all                    = $this->Common_model->all( 'company', '', 'ACTIVE' );
        $transport_all                  = $this->Common_model->all( 'transport', '', 'ACTIVE' );
        $payment                        = $this->Setting_model->get_all_options( 'payment-method' );
        $bank_info                      = $this->Bank_model->own_bank_list( $this->session->userdata( 'company_id' ), 'own', 'own_bank_info.bank_id' );

        $params[ 'current_balance' ]= $current_balance;
        $params[ 'bank_info' ]          = $bank_info; 
        $params[ 'company_all' ]        = $company_all;
        $params[ 'payment' ]            = $payment;

        $params[ 'title' ]              = $title;
        $params[ 'purchase_pay_no' ]    = $purchase_pay_no;
        $params[ 'anchor_btn' ]         = $anchor_btn;
        $params[ 'breadcrum' ]          = $breadcrum;
        $content                        = $this->load->view( 'pages/purchase/payment/add', $params, true );
        $data[ 'content' ]              = $content;
        $this->template( $data );
        return true;
    }

    public function payment_view() {
        $slug                       = $this->uri->segment( 3 );
        $receipt_no                 = base64_decode( $slug );
        $title                      = 'View Purchase Payment';
        $breadcrum                  = '<li class="breadcrumb-item"> <a href="'.base_url().'purchase"> PURCHASE </a></li><li class="breadcrumb-item"> <a href="'.base_url().'purchase-payment"> PURCHASE PAYMENTS </a></li><li class="breadcrumb-item"><a href="javascript:;"> VIEW PURCHASE PAYMENT </a></li>';

        $anchor_btn                 = '<a href="javascript:;" class="btn btn-sm btn-primary" onclick="return print_purchase_payment( \''.$receipt_no.'\');" title=""> Print
            </a>';

        $payment_info               = $this->Purchase_model->purchase_payment_info( $receipt_no );
        
        $company_address            = $this->Common_model->get_company_address( $payment_info->from_company );
        
        $own_info                   = $this->Common_model->find( 'owner', array( 'id' => $this->session->userdata( 'company_id' ) ) );
        $params[ 'title' ]          = $title;
        $params[ 'anchor_btn' ]     = $anchor_btn;
        $params[ 'breadcrum' ]      = $breadcrum;
        $params[ 'payment_info' ]   = $payment_info;
        $params[ 'company_address' ]= $company_address;
        $params[ 'own_info' ]       = $own_info;
        $content                    = $this->load->view( 'pages/purchase/payment/view', $params, true );
        $data[ 'content' ]          = $content;
        $this->template( $data );
        return true;
    }


    public function get_current_balance() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $company_id                 = $this->input->post( 'company_id', true );
        $data[ 'amount' ]           = $this->get_company_current_balance( $company_id );
        echo json_encode( $data );
        return true;
    }

    public function get_company_current_balance( $company_id ) {
        $balance                    = 0;
        $to_pay                     = $this->Purchase_model->get_company_purchase_amount( $company_id );
        $purchase_paid_amount       = $this->Purchase_model->get_company_purchase_paid_amount( $company_id );
        $company_paid_amount        = $this->Purchase_model->get_company_paid_amount( $company_id );
       

        if( $to_pay && ( $purchase_paid_amount || $company_paid_amount ) ) {
            $company_paid           = $purchase_paid_amount->paid_amount ?? 0;
            $purchase_paid          = $company_paid_amount->paid_amount ?? 0;
            $balance                = $to_pay->total_amount - (  $company_paid + $purchase_paid );
        }
        return $balance;
    }

    function get_branch_typeahead() {
        $c_query                                        = $this->input->get( 'query' );
        
        $branch_list                                    = $this->Common_model->typehead_branch_list( $c_query );
        if( $branch_list ) {
            foreach ( $branch_list as $key => $value ) {
                $data[ $key ][ 'name' ]                 = $value->branch;
                $data[ $key ][ 'ifsc' ]                 = $value->ifsc;
                $data[ $key ][ 'id' ]                   = $value->id;
            }
        } else {
            $data[0]['name']                            = 'No data found';
            $data[0]['id']                              = 0;
        }
        echo json_encode( $data );
        return true;
    }

    function insert_purchase_payment() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }

        $payment_method                         = $this->input->post( 'payment_method', true );

        $this->form_validation->set_rules( 'to_company', 'Pay To Company', 'required' );
        $this->form_validation->set_rules( 'payment_method', 'Payment Method', 'required' );
        $this->form_validation->set_rules( 'bank_id', 'Bank', 'required' );
        $this->form_validation->set_rules( 'bank_branch_id', 'Bank Branch', 'required' );
        $this->form_validation->set_rules( 'ifsc_code', 'IFSC code', 'required' );
        $this->form_validation->set_rules( 'bank_ac_no', 'AC Number', 'required' );
        $this->form_validation->set_rules( 'payment_receipt_no', 'Payment Receipt No', 'required' );
        $this->form_validation->set_rules( 'payment_date', 'Payment Date', 'required' );
        $this->form_validation->set_rules( 'total_amount', 'Total Amount', 'required' );

        if( isset( $payment_method ) && $payment_method == 'CHEQUE' ) {
            $this->form_validation->set_rules( 'cheque_dd_no', 'Cheque / DD No', 'required' );
            $this->form_validation->set_rules( 'cheque_issued_date', 'Cheque Issued Date', 'required' );
            $this->form_validation->set_rules( 'cheque_issued_amount', 'Cheque Issued Amount', 'required' );
        } else if( isset( $payment_method ) && $payment_method == 'DD' ) {
            $this->form_validation->set_rules( 'cheque_dd_no', 'Cheque / DD No', 'required' );
        }

        if ($this->form_validation->run() == FALSE)
        {
            $error                              = 1;
            $error_msg                          = validation_errors();
        } else {
            $error                              = 0;
            
            $file_full_path                     = null;
            if( isset($_FILES['attachment']['name']) && !empty($_FILES['attachment']['name']) ) {

                $file                           = 'attachment'; 
                $temp_file                      = explode('.', $_FILES["$file"]['name']);  
                $config['upload_path']          = 'assets/images/purchase_payment';
                $config['allowed_types']        = '*';
                $config['max_size']             = 500000;
                $filetype                       = $_FILES[ "$file" ][ 'type' ];
                $expfiletype                    = explode( '.', $_FILES[ "$file" ][ 'name' ] );  
                $config['file_name']            = strtolower($temp_file[0]).'-'.rand( 101, 999 ) . '.' . $expfiletype[ 1 ];
                $this->upload->initialize( $config );
                $this->load->library( 'upload', $config );
                if ( $this->upload->do_upload( $file ) ) {  
                    $upload_data                = $this->upload->data(); 
                    $filename                   = $upload_data[ 'file_name' ];  
                    $file_full_path             = base_url().'assets/images/purchase_payment/'.$filename;
                    $filepath                   = $upload_data[ 'full_path' ];
                } else {
                   $error_msg                   = 'Image upload failed!';
                   $error                       = 1;
                }
            }
            if( isset( $file_full_path ) && !empty( $file_full_path ) ) {
                $ins[ 'attachment' ]            = $file_full_path;
            }

            $id                                 = $this->input->post( 'id', true );
            
            $purchase_id                        = $this->input->post( 'purchase_id', true );
            if( isset( $purchase_id ) && !empty( $purchase_id  ) ) {
                $mode_type                      = 'add';
                $mode_type_id                   = $purchase_id;
            } else {
                $mode_type                      = 'company';    
                $mode_type_id                   = $this->input->post( 'to_company', true );
            }
            
            $bank_branch_id                     = $this->input->post( 'bank_branch_id', true );
            $ifsc_code                          = $this->input->post( 'ifsc_code', true );

            // $bank_branch                        = $this->input->post( 'bank_branch', true );
            
            // if( ! $bank_branch_id ) {
            //     $bank_id                        = $this->input->post( 'bank_id', true );
            //     $bank                           = $this->Common_model->find( 'bank', array( 'id' => $bank_id ) );
            //     $exists                         = $this->Common_model->find( 'bank', array( 'id' => $bank_id, 'branch' => $bank_branch  ) );
            //     if( ! $exists ) {
            //         $ins_bank[ 'branch' ]       = $bank_branch;
            //         $ins_bank[ 'parent_id' ]    = $bank_id;
            //         $ins_bank[ 'ifsc' ]         = $ifsc_code;
            //         $ins_bank[ 'created_at' ]   = date( 'Y-m-d H:i:s' );
            //         $ins_bank[ 'slug' ]         = url_title( $bank_branch.' '.$bank->bank_name, 'dash', true);
            //         $bank_branch_id             = $this->Common_model->insert_table( 'bank', $ins_bank );
            //     } else {
            //         $bank_branch_id             = $exists->id;
            //     }
            // }

            $payment_date                       = $this->input->post( 'payment_date', true );
            $payment_date                       = str_replace( '/', '-', $payment_date );
            $date1                              = strtotime( $payment_date) ;
            $pay_date                           = date( 'Y-m-d', $date1 );

            $issued_date                        = $this->input->post( 'cheque_issued_date', true );
            if( isset( $issued_date ) && !empty( $issued_date ) )  {
                $issued_date                    = str_replace( '/', '-', $issued_date );
                $date2                          = strtotime( $issued_date) ;
                $issue_date                     = date( 'Y-m-d', $date2 );
                $ins[ 'issue_date' ]            = $issue_date;
            } 
            $branch_info                        = $this->Common_model->find( 'bank', array( 'id' => $bank_branch_id ) );
            $data[ 'slug' ]                     = $this->input->post( 'payment_receipt_no', true );
            $ins[ 'bank_branch' ]               = $branch_info->branch;
            $ins[ 'payment_date' ]              = $pay_date;
            $ins[ 'ifsc' ]                      = $ifsc_code;
            $ins[ 'bank_branch_id' ]            = $bank_branch_id;
            //get post data
            $ins[ 'receipt_no' ]                = $this->input->post( 'payment_receipt_no', true );
            $ins[ 'mode' ]                      = 'purchase';
            $ins[ 'mode_type' ]                 = $mode_type;
            $ins[ 'mode_type_id' ]              = $mode_type_id;
            
            $ins[ 'bank_charges' ]              = $this->input->post( 'bank_add_charge', true );
            $ins[ 'payment_method_id' ]         = $this->input->post( 'payment_method', true );
            $ins[ 'amount' ]                    = $this->input->post( 'total_amount', true );
            $ins[ 'bank_id' ]                   = $this->input->post( 'bank_id', true );
            $ins[ 'bank_ac_no' ]                = $this->input->post( 'bank_ac_no', true );
            $ins[ 'cheque_no' ]                 = $this->input->post( 'cheque_dd_no', true );
            $ins[ 'cheque_issue_amount' ]       = $this->input->post( 'cheque_issued_amount', true );
            $is_booking                         = $this->input->post( 'bookin', true );
            $ins[ 'is_advance_booking' ]        = $is_booking;
            if( isset( $is_booking ) && $is_booking == 'no' ) {
                $ins[ 'invoice_no' ]            = $this->input->post( 'purchase_invoice_number', true );
            }
            $ins[ 'advance_description' ]       = $this->input->post( 'advance_description', true );

            if( !$id ) {
                $ins[ 'status' ]                = 'ACTIVE';
                $ins[ 'created_by' ]            = $this->session->userdata( 'user_id' );
                $ins[ 'created_at' ]            = date( 'Y-m-d H:i:s' );
                $ins[ 'owner_id' ]              = $this->session->userdata( 'company_id' );

                $payment_id                     = $this->Common_model->insert_table( 'payment', $ins );
                
            } else {
                //get previous paid amount for purchase and payment
                $pay_info                       = $this->Common_model->find( 'payment', array( 'id' => $id ) );
                $prev_amount                    = $pay_info->amount;
                if( isset( $purchase_id ) && !empty( $purchase_id  ) ) {
                    $purchase_info              = $this->Common_model->find( 'purchase', array( 'id' => $purchase_id ) );
                    $update[ 'paid_amount' ]    = $purchase_info->paid_amount - $prev_amount;
                    $where1[ 'id' ]             = $purchase_id;
                    $this->Common_model->update_table( 'purchase', $where1, $update );
                } //remove previous amount 
                $where[ 'id' ]                  = $id;
                $this->Common_model->update_table( 'payment', $where, $ins );
            }

            if( isset( $purchase_id ) && !empty( $purchase_id  ) ) {

                $purchase_info                  = $this->Common_model->find( 'purchase', array( 'id' => $purchase_id ) );
                $total_amount                   = $this->input->post( 'total_amount', true );
                $update2[ 'paid_amount' ]       = $purchase_info->paid_amount + $total_amount;
                $where2[ 'id' ]                 = $purchase_id;
                $this->Common_model->update_table( 'purchase', $where2, $update2 );
            }
            $error_msg                          = "Purchase Payment Added Successfully";
        }
        $data[ 'error' ]                        = $error;
        $data[ 'error_msg' ]                    = $error_msg;
        echo json_encode( $data );
        return true;

    }

    public function get_purchase_payment() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $data                       = [];
        $fetch_data                 = $this->Purchase_model->make_payment_datatables();
        
        if( $fetch_data ) {
            $i                      = 1;
            foreach ( $fetch_data as $key => $value ) {
                
                $edit_btn           = '';
                $delete_btn         = '';
                $view_btn           = '';
                $pay_btn            = '';
                $id                 = base64_encode( $value->receipt_no );
                $edit_btn           = '<a href="'.base_url().'purchase-payment-add/'.$id.'" class="btn btn-outline-warning"> <i class="icon-pencil"></i>  </a>&nbsp;';
                $delete_btn         = '<a href="javascript:;" class="btn btn-outline-danger" onClick="return are_you_sure( \'delete_purchase_payment\' ,\'Delete Purchase Payment\', \'Are you sure want to delete this purchase return? \', '.$value->id.' );" ><i class="icon-trash"></i></a>';
                $view_btn           = '<a href="'.base_url().'purchase-payment/view/'.$id.'" class="btn btn-outline-success"><i class="fa fa-eye"></i></a>';
                
                $issue_date         = '';
                if( isset( $value->issue_date ) && !empty( $value->issue_date ) && $value->issue_date != '0000-00-00' && $value->issue_date != '1970-01-01' ) {
                    $issue_date     = date( 'd-m-Y', strtotime( $value->issue_date ) );
                }
                if( isset( $value->payment_date ) && !empty( $value->payment_date  ) ) {
                    $payment_date   = date( 'd-m-Y', strtotime( $value->payment_date ) );
                }
                $sub_array          = array();
                $sub_array[]        = $payment_date ?? '' ;
                $sub_array[]        = $value->receipt_no;
                $sub_array[]        = $value->invoice_no;
                $sub_array[]        = $value->company_name;
                $sub_array[]        = $value->amount;
                $sub_array[]        = $value->payment_method;
                $sub_array[]        = $issue_date ?? '';
                $sub_array[]        = '<span class="badge badge-danger">Payment Pending</span>';
                $sub_array[]        = $view_btn . $edit_btn . $delete_btn;
                
                $data[]             = $sub_array;

                $i++;
            }
        }
        $draw                       = ( isset( $_POST[ 'draw' ] ) ? $_POST[ 'draw' ] : 0 );
        $output                     = array( 
                                        'draw'                  => $draw,
                                        'recordsTotal'          => $this->Purchase_model->get_all_payment_data(),
                                        'recordsFiltered'       => $this->Purchase_model->get_payment_filtered_data(),
                                        'data'                  => $data 
                                    );
        echo json_encode( $output );
        return true;
    }


    function delete_purchase_payment() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $id                                 = $this->input->post( 'id', true );
        $payment_info                       = $this->Common_model->find( 'payment', array( 'id' => $id ) );
        $where[ 'id' ]                      = $id;
        $table                              = 'payment';
        $update[ 'status' ]                 = 'INACTIVE';
        $this->Common_model->update_table( $table, $where, $update );
        if( isset( $payment_info->invoice_no ) && !empty( $payment_info->invoice_no ) ) {
            $purchase                       = $this->Common_model->find( 'purchase', array( 'invoice_no' => $payment_info->invoice_no ) );
            if( $purchase ) {
                $up[ 'paid_amount' ]        = $purchase->paid_amount - $payment_info->amount;  
                $where_p[ 'id' ]            = $purchase->id;
                $this->Common_model->update_table( 'purchase', $where_p, $up );  
            }
            
        }
        echo 1;
        return true;
    }

    function print_purchase() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $slug                                 = $this->input->post( 'slug', true );
        $purchase_info              = $this->Purchase_model->get_purchase_info( '', $slug );
        $items                      = $this->Purchase_model->purchase_items( $purchase_info->id, 'ACTIVE' );
        $company_address            = $this->Common_model->get_company_address( $purchase_info->from_company );
        $own_info                   = $this->Common_model->find( 'owner', array( 'id' => $this->session->userdata( 'company_id' ) ) );

        $params[ 'own_info' ]       = $own_info;
        $params[ 'purchase_info' ]  = $purchase_info;
        $params[ 'items' ]          = $items;
        $params[ 'company_address' ]= $company_address; 
        
        echo $this->load->view( 'pages/purchase/print/invoice', $params, true );
        return true;
    }
     
    public function get_due_days() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }

        $payment_id                 = $this->input->post( 'payment_id', true );
        $purchase_date              = $this->input->post( 'purchase_date', true );
        $pay_info                   = $this->Common_model->find( 'code_options', array( 'id' => $payment_id ) );
        $due_day                    = '';
        $is_advance                 = '';
        if( $pay_info ) {
            $day_name               = $pay_info->name;
            
            $numbers                = preg_replace('/[^0-9]/', '', $day_name);
            if( isset( $numbers ) && !empty( $numbers ) ) {
                $purchase_date      = str_replace( '/', '-', $purchase_date );
                $due_day            = date( "Y-m-d", strtotime( $purchase_date. "+ $numbers day"));
                $due_day            = date( "d/m/Y", strtotime( $due_day ) );
            } else {
                $is_advance         = 'yes';
            }
            
        }

        $data[ 'due_day' ]          = $due_day;
        $data[ 'is_advance' ]       = $is_advance;
        echo json_encode( $data );
        return false;
    }

    function get_bank_branch_info() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }

        $bank_id                        = $this->input->post( 'bank_id', true );
        $owener_id                      = $this->session->userdata( 'company_id' );

        $own_bank_info                  = $this->Bank_model->own_bank_list( $owener_id, 'own', '', $bank_id );
        $params[ 'branch_info' ]        = $own_bank_info;

        echo $this->load->view( 'pages/purchase/_branch_dropdown', $params, true );
        
        return true;
    }

    function get_branch_ifsc_info() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }

        $branch_id                      = $this->input->post( 'branch_id', true );
        $owener_id                      = $this->session->userdata( 'company_id' );

        $own_bank_info                  = $this->Bank_model->own_bank_info( $owener_id, 'own', $branch_id );
        echo json_encode( $own_bank_info );
        return true;

    }

    function print_purchase_return() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $slug                       = $this->input->post( 'slug', true );

        $purchase_info              = $this->Purchase_model->get_purchase_return_info( '', $slug );
        $items                      = $this->Purchase_model->purchase_return_items( $purchase_info->id, 'ACTIVE' );
        $company_address            = $this->Common_model->get_company_address( $purchase_info->return_to );
        $own_info                   = $this->Common_model->find( 'owner', array( 'id' => $this->session->userdata( 'company_id' ) ) );

        $params[ 'own_info' ]       = $own_info;
        $params[ 'purchase_info' ]  = $purchase_info;
        $params[ 'items' ]          = $items;
        $params[ 'company_address' ]= $company_address; 
        
        echo $this->load->view( 'pages/purchase/print/return_invoice', $params, true );
        return true;
    }

    function print_purchase_payment() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $receipt_no                 = $this->input->post( 'slug', true );
        $payment_info               = $this->Purchase_model->purchase_payment_info( $receipt_no );
        $company_address            = $this->Common_model->get_company_address( $payment_info->from_company );
        
        $own_info                   = $this->Common_model->find( 'owner', array( 'id' => $this->session->userdata( 'company_id' ) ) );

        $params[ 'own_info' ]       = $own_info;
        $params[ 'payment_info' ]   = $payment_info;
        $params[ 'company_address' ]= $company_address; 
        
        echo $this->load->view( 'pages/purchase/print/payment_invoice', $params, true );
        return true;
    }
    function template( $data ) {
        echo $this->load->view( 'layout/template', $data, true );
        return true;
    }

}


?>