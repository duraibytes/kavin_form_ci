<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction extends CI_Controller {
    function __construct() {
       parent::__construct();
       $this->load->model( 'Bank_model' );
       $this->load->model( 'Transaction_model' );
    }

    public function index() {
    	$title                      = 'Bank Transaction';
        $anchor_btn                 = '<a href="javascript:;" class="btn btn-sm transaction-bg text-white" title="" onclick="return get_deposit_form()"> Create Deposit / Withdraw </a>';
        $breadcrum                  = '<li class="breadcrumb-item"><a href="javascript:;"> BANK TRANSACTION </a></li>';
        $bank_info                  = $this->Bank_model->own_bank_list( $this->session->userdata( 'company_id' ), 'own', 'own_bank_info.bank_id' );
        $params[ 'title' ]          = $title;
        $params[ 'anchor_btn' ]     = $anchor_btn;
        $params[ 'breadcrum' ]      = $breadcrum;
        $params[ 'bank_info' ]       = $bank_info;
    	$content 					= $this->load->view( 'pages/transaction/index', $params, true );
    	$data[ 'content' ]          = $content;
        $this->template( $data );
        return true;
    }

    function get_deposit_form() {
         if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }

        $value                      = $this->input->post( 'value', true );
        $id                         = $this->input->post( 'id', true );
        $slug                       = $this->input->post( 'slug', true );
        $btn_name                   = 'Add';
        $info                       = '';
        if( $id ) {
            if( isset( $value ) && !empty( $value ) && $value == 'sub' ){
                $info               = $this->Setting_model->option_info( $id );
                $btn_name           = 'Update';
            } else {
                $info               = $this->Setting_model->code_info( $id );
                $btn_name           = 'Update';
            }
            
        }

        $params[ 'value' ]            = $value;
        $params[ 'id' ]               = $id;
        $params[ 'info' ]             = $info;
        $params[ 'slug' ]             = $slug;
        $params[ 'btn_name' ]         = $btn_name;

        echo $this->load->view( 'pages/transaction/form', $params, true );

    }

    function get_bank_transaction() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $data                       = [];
        $fetch_data                 = $this->Transaction_model->make_payment_datatables();
        if( $fetch_data ) {
            $i                      = 1;
            foreach ( $fetch_data as $key => $value ) {

                
                $transaction_type   = '';
                if( $value->mode == 'purchase' && $value->mode_type == 'company' ) {
                    $transaction_type   = 'Purchase Advance';
                } else if( $value->mode == 'purchase' && $value->mode_type == 'add' ) {
                    $transaction_type   = 'Purchase';
                }
                $cheque_info        = '';
                if( isset( $value->cheque_no ) && !empty( $value->cheque_no ) ) {
                    $cheque_info    = '<div>Cheque NO: '.$value->cheque_no.'</div><div>Issued Date: '. date( 'd-M-Y', strtotime( $value->issue_date ) ).'</div><div>CHQ AMT:'. $value->cheque_issue_amount ?? "0" .'</div>';
                }
               
                $sub_array          = array();
                $sub_array[]        = date( 'd-M-Y', strtotime( $value->payment_date ) );
                $sub_array[]        = $transaction_type;
                $sub_array[]        = $value->company_name ?? '';
                $sub_array[]        = $value->advance_description ?? '';
                $sub_array[]        = 'Rs '. number_format( $value->amount, 2 );
                $sub_array[]        = strtoupper( $value->payment_method );
                $sub_array[]        = $cheque_info;
                $sub_array[]        = $value->bank_ac_no ?? '';
                $sub_array[]        = $value->bank_name ?? '';
                $sub_array[]        = $value->bank_branch ?? '';
                $sub_array[]        = $value->ifsc ?? '';   
                
                $data[]             = $sub_array;

                $i++;
            }
        }
        $draw                       = ( isset( $_POST[ 'draw' ] ) ? $_POST[ 'draw' ] : 0 );
        $output                     = array( 
                                        'draw'                  => $draw,
                                        'recordsTotal'          => $this->Transaction_model->get_all_payment_data(),
                                        'recordsFiltered'       => $this->Transaction_model->get_payment_filtered_data(),
                                        'data'                  => $data 
                                    );
        echo json_encode( $output );
        return true;
    }

    function transaction_filter() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }

        $bank                   = $this->input->post( 'bank', true );
        $start                  = $this->input->post( 'start', true );
        $end                    = $this->input->post( 'end', true );
        
        if( trim($bank) ) {
            $arr[ 'bank' ]      = $bank;
        } 
        if( trim($start) ) {
            $arr[ 'start' ]     = $start;
        } 
        if( trim($end) ) {
            $arr[ 'end' ]       = $end;
        } 
        
        $this->session->set_userdata( 'transaction_filter', $arr );

        echo json_encode( array( 'result' => true ) );
        return true;
    }

    function clear_transaction_filter() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $this->session->unset_userdata( 'transaction_filter' );
        return false;

    }


    function template( $data ) {
        
        echo $this->load->view( 'layout/template', $data, true );
        return true;
    }

}


?>