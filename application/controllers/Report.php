
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {
    function __construct() {
       parent::__construct();
    }

    public function index() {
    	$title                      = 'Reports';
        $anchor_btn                 = '';
        $breadcrum                  = '<li class="breadcrumb-item"> REPORTS </li>';
        $params[ 'title' ]          = $title;
        $params[ 'anchor_btn' ]     = $anchor_btn;
        $params[ 'breadcrum' ]      = $breadcrum;
    	$content 					= $this->load->view( 'pages/report/index', $params, true );
    	$data[ 'content' ]          = $content;
        $this->template( $data );
        return true;
    }

    function template( $data ) {
        echo $this->load->view( 'layout/template', $data, true );
        return true;
    }

}

?>