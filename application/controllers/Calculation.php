
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calculation extends CI_Controller {
   function __construct() {
      parent::__construct();
      $this->load->model( 'Bank_model' );
      $this->load->helper( 'common_helper' );
      $this->load->model( 'Setting_model' );
      $this->load->model( 'Sale_model' );
   }

   function get_outstanding_sale() {
      if( !$this->input->is_ajax_request() ) {
         echo 'No Direct Access Denied';
         return false;
         exit();
      }

      $dealer_id                    = $this->input->post( 'dealer_id', true );
      //get dealer outstanding amount
      $where[ 'customer_id' ]       = $dealer_id;
      $where[ 'customer_type' ]     = 'dealer';

      $sale_amount                  = $this->Sale_model->get_sale_total( $where );
      $data[ 'out_standing' ]       = $sale_amount->total_amount ?? 0;
      echo json_encode( $data );
   }


}