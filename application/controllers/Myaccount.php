
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Myaccount extends CI_Controller {
    function __construct() {
       parent::__construct();
    }

    public function index() {
        $title                      = 'My Profile';
        $anchor_btn                 = '';
        $params[ 'title' ]          = $title;
        $params[ 'anchor_btn' ]     = $anchor_btn;
    	$content 					= $this->load->view( 'pages/myaccount/index', $params, true );
    	$data[ 'content' ]          = $content;
        $this->template( $data );
        return true;
    }

    function template( $data ) {
        
        echo $this->load->view( 'layout/template', $data, true );
        return true;
    }
}

?>