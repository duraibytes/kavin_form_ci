
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Expense extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model( 'Setting_model' );
        $this->load->model( 'Bank_model' );
        $this->load->model( 'Expense_model' );
    }

    public function index() {
        $title                      = 'Expenses';
        $anchor_btn                 = '<a href="javascript:;" onclick="return open_add_expense_form()" class="btn btn-sm expense-bg text-white" title=""> Add Expenses </a>';

        $breadcrum                  = '<li class="breadcrumb-item"><a href="javascript:;"> EXPENSES </a> </li>';
        $params[ 'title' ]          = $title;
        $params[ 'anchor_btn' ]     = $anchor_btn;
        $params[ 'breadcrum' ]      = $breadcrum;
        $content                    = $this->load->view( 'pages/expense/index', $params, true );
        $data[ 'content' ]          = $content;
        $this->template( $data );
        return true;
    }

    function open_add_expense_form() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $id                         = $this->input->post( 'id', true );
        $btn_name                   = 'Add';
        $info                       = '';
        if( $id ) {
            $info                   = $this->Expense_model->expense_info( $id );
            $btn_name               = 'Update';
        }
        $payment                    = $this->Setting_model->get_all_options( 'payment-method' );
        $bank_info                  = $this->Bank_model->get_all_bank();
        $params[ 'bank_info' ]      = $bank_info; 
        $content1                   = $this->load->view( 'pages/expense/_form', $params, true );
        
        $data[ 'id' ]               = $id;
        $data[ 'info' ]             = $info;
        $data[ 'btn_name' ]         = $btn_name;
        $data[ 'payment' ]          = $payment;
        echo $this->load->view( 'pages/expense/add_form', $data, true );
    }

    function get_expense() {

        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $data                       = [];
        $fetch_data                 = $this->Expense_model->make_expense_datatables();
        if( $fetch_data ) {
            $i                      = 1;
            foreach ( $fetch_data as $key => $value ) {
                
                $edit_btn           = '';
                $delete_btn         = '';
              $ids                  = $value->id;
                $edit_btn           = ' <a href="javascript:void(0);" onclick="return open_add_expense_form('.$value->id.')" class="btn btn-outline-warning" title=""><i class="icon-pencil"></i>  </a>&nbsp;';
                $delete_btn         = '<a href="javascript:;" class="btn btn-outline-danger" onClick="return are_you_sure( \'delete_expense\' ,\'Delete Expense\', \'Are you sure want to delete this Expense? \', '.$value->id.' );" ><i class="icon-trash"></i></a>';
                $sub_array          = array();
                $created_dt         = $value->created_at;
                $sub_array[]        = date("d-m-Y", strtotime($created_dt));
                $sub_array[]        = $value->expense_name;
                $sub_array[]        = $value->description;
                $sub_array[]        = $value->amount;
               
               
                $sub_array[]        = $edit_btn. $delete_btn;
                
                $data[]             = $sub_array;

                $i++;
            }
        }
        $draw                       = ( isset( $_POST[ 'draw' ] ) ? $_POST[ 'draw' ] : 0 );
        $output                     = array( 
                                        'draw'                  => $draw,
                                        'recordsTotal'          => $this->Expense_model->get_all_expense_data(),
                                        'recordsFiltered'       => $this->Expense_model->get_expense_filtered_data(),
                                        'data'                  => $data 
                                    );
        echo json_encode( $output );
        return true;
    }

    function add_expense() {

         if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }

        $bank                                   = $this->input->post( 'bank', true );
        $id                                     = $this->input->post( 'id', true );
        $slug                                   = '';
        
        $this->form_validation->set_rules( 'expense_name', 'Expense Name', 'required' );
        $this->form_validation->set_rules( 'payment_method_id', 'Payment Method', 'required' );
        $this->form_validation->set_rules( 'bank_id', 'Bank', 'required|numeric' );
        $this->form_validation->set_rules( 'bank_branch', 'Bank Branch', 'required' );
        $this->form_validation->set_rules( 'bank_ac_no', 'Bank  Ac No', 'required|numeric' );
        $this->form_validation->set_rules( 'amount', 'Amount', 'required|numeric' );
         
        if ($this->form_validation->run() == FALSE)
        {
            $error                              = 1;
            $error_msg                          = validation_errors();
        } else {

            $ins[ 'expense_name' ]              = $this->input->post( 'expense_name', true );
            $ins[ 'payment_method_id' ]         = $this->input->post( 'payment_method_id', true );
            $ins[ 'amount' ]                    = $this->input->post( 'amount', true );
            $ins[ 'description' ]               = $this->input->post( 'description', true );

            
           if( isset( $id ) && !empty( $id ) ) {
                $where[ 'id' ]                  = $id;
                $this->Common_model->update_table( 'expense', $where, $ins );
                $error_msg                      = '<p> Expense Updated Successfully </p>';
            } else{
                $ins[ 'status' ]                = 'ACTIVE';
                $ins[ 'created_at' ]            = date( 'Y-m-d H:i:s' );
                $ins[ 'created_by' ]            = $this->session->userdata( 'user_id' ); 
                $ins[ 'owner_id' ]              = $this->session->userdata( 'company_id' );
                $expense_id                     = $this->Common_model->insert_table( 'expense', $ins );  

                //update slug in expense table
                $up[ 'slug' ]                   = base64_encode( $expense_id );
                $where[ 'id' ]                  = $expense_id;
                $this->Common_model->update_table( 'expense', $where, $up );

                $ins1[ 'mode' ]                 = "expense";
                $ins1[ 'mode_type' ]            = "add";
                $ins1[ 'mode_type_id' ]         = $expense_id;
                $ins1[ 'payment_method_id' ]    = $this->input->post( 'payment_method_id', true );
                $ins1[ 'amount' ]               = $this->input->post( 'amount', true );
                $ins1[ 'bank_id' ]              = $this->input->post( 'bank_id', true );
                $ins1[ 'bank_branch' ]          = $this->input->post( 'bank_branch', true );
                $ins1[ 'bank_ac_no' ]           = $this->input->post( 'bank_ac_no', true );
                $ins1[ 'cheque_no' ]            = $this->input->post( 'cheque_no', true );
                if( $this->input->post( 'issue_date', true ) ) {
                    $issue_date                 = $this->input->post( 'issue_date', true );
                    $issue_date                 = str_replace('/', '-', $issue_date);
                    $date1                      = strtotime( $issue_date );
                    $issued_date                = date( 'Y-m-d', $date1 );

                    $ins1[ 'issue_date' ]       = $issued_date;
                }
                $ins1[ 'cheque_issue_amount' ]  = $this->input->post( 'cheque_issued_amount', true );
                $ins1[ 'created_at' ]           = date( 'Y-m-d H:i:s' );
                $ins1[ 'created_by' ]           = $this->session->userdata( 'user_id' );  
                $ins1[ 'owner_id' ]             = $this->session->userdata( 'company_id' );
                $payment_id                     = $this->Common_model->insert_table( 'payment', $ins1 );   
                
                $error_msg                      = '<p> Expense Added Successfully </p>';
            }
            $error                              = 0;
            
        }
        $data[ 'error' ]                        = $error;
        $data[ 'error_msg' ]                    = $error_msg;
        echo json_encode( $data );
        return true;
    }

    function delete_expense() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $id                         = $this->input->post( 'id', true );
        $where[ 'id' ]              = $id;
        $table                      = 'expense';
        $this->Common_model->delete_table( $table, $where );
        
        echo 1;
        return true;
    }

    function template( $data ) {
        
        echo $this->load->view( 'layout/template', $data, true );
        return true;
    }

}

?>