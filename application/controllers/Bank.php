
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bank extends CI_Controller {
    function __construct() {
       parent::__construct();
       $this->load->model( 'Bank_model' );
    }

    public function index() {
    	$title                      = 'Bank';
        $anchor_btn                 = '<a href="'.base_url().'bank-add" class="btn btn-sm inventory-bg text-white" title=""> Create New Bank </a>';
        $breadcrum                  = '<li class="breadcrumb-item"><a href="javascript:;" >Settings</a></li><li class="breadcrumb-item"><a href="javascript:;"> Bank </a></li>';
        $params[ 'title' ]          = $title;
        $params[ 'anchor_btn' ]     = $anchor_btn;
        $params[ 'breadcrum' ]      = $breadcrum;
    	$content 					= $this->load->view( 'pages/bank/index', $params, true );
    	$data[ 'content' ]          = $content;
        $this->template( $data );
        return true;
    }

    public function add() {
    	$slug 						= $this->uri->segment( 2 );
    	$bank_info 					= '';
    	$btn_name 					= 'Add';
        $breadcrum                  = '<li class="breadcrumb-item"><a href="javascript:;">Settings</a></li><li class="breadcrumb-item"> <a href="'.base_url().'bank" > Bank </a> </li><li class="breadcrumb-item"><a href="javascript:;"> Add Bank </a></li>';
    	if( $slug ) {
    		$bank_info 				= $this->Bank_model->bank_info( $slug );
    		$btn_name 				= 'Update';
            $breadcrum              = '<li class="breadcrumb-item">Settings</li><li class="breadcrumb-item"> <a href="'.base_url().'bank" > Bank </a> </li><li class="breadcrumb-item"> Update Bank </li>';
    	}
    	$params[ 'bank_info' ] 		= $bank_info;
    	$params[ 'slug' ] 			= $slug;
    	$params[ 'btn_name' ] 		= $btn_name;
        $title                      = 'Add Bank';
        $anchor_btn                 = '';
        $params[ 'title' ]          = $title;
        $params[ 'anchor_btn' ]     = $anchor_btn;
        $params[ 'breadcrum' ]      = $breadcrum;
    	$content 					= $this->load->view( 'pages/bank/add', $params, true );
    	$data[ 'content' ]          = $content;
        $this->template( $data );
        return true;

    }

    public function add_bank() {

    	if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $slug 							= $this->input->post( 'slug', true );
        if( isset( $slug ) && !empty( $slug ) ) {
        	$this->form_validation->set_rules( 'bank_name', 'Bank Name', 'required' );
        } else {
        	$this->form_validation->set_rules( 'bank_name', 'Bank Name', 'required|is_unique[bank.bank_name]' );
        }
        
        $this->form_validation->set_rules( 'email', 'Email', 'valid_email' );
        $this->form_validation->set_rules( 'phone', 'Phone number', 'numeric' );
        $this->form_validation->set_message( 'is_unique', 'The %s is already taken' );
        if ($this->form_validation->run() == FALSE)
        {
            $error          			= 1;
            $error_msg      			= validation_errors();
        } else {
        	$bank_name 					= $this->input->post( 'bank_name', true );
        	$ins[ 'bank_name' ] 		= $bank_name;
        	$ins[ 'email' ] 			= $this->input->post( 'email', true );
        	$ins[ 'phone' ] 			= $this->input->post( 'phone', true );
        	$ins[ 'address' ] 			= $this->input->post( 'address', true );
        	$ins[ 'slug' ] 				= url_title( $bank_name, 'dash', true);

        	if( isset( $slug ) && !empty( $slug ) ) {
        		$where 					=  array( 'slug' => $slug );
        		$bank_id 				= $this->Common_model->update_table( 'bank', $where, $ins );
        		$error_msg 				= '<p> Bank Updated Successfully </p>';
        	} else {
        		$ins[ 'created_by' ] 	= $this->session->userdata( 'user_id' );
        		$ins[ 'created_at' ] 	= date( 'Y-m-d H:i:s' );	
        		
        		$bank_id 				= $this->Common_model->insert_table( 'bank', $ins );
        		$error_msg 				= '<p> Bank Added Successfully </p>';
        	}
        	
        	
        	if( $bank_id ) {
        		$error 					= 0;
        		
        	} else {
        		$error 					= 1;
        		$error_msg 				= '<p> Error Occured while adding bank </p>';
        	}
        }

        $data[ 'error_msg' ]    		= $error_msg;
        $data[ 'error' ]        		= $error;
        
        echo json_encode( $data );
        return true;

    }

    function get_bank() {
    	if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $data 						= [];
        $fetch_data 				= $this->Bank_model->make_bank_datatables();
        if( $fetch_data ) {
        	$i 						= 1;
        	foreach ( $fetch_data as $key => $value ) {
        		$branch_list 		= '';
        		$edit_btn 			= '';
        		$delete_btn 		= '';
        		$branch_list 		= '<a href="bank-list/'.$value->slug.'" class="badge badge-success"> Branch List <span class="badge badge-info">'.$value->branch_count.'</span></a>';
        		$edit_btn 			= '<a href="bank-edit/'.$value->slug.'" class="btn btn-warning"> <i class="icon-pencil"></i>  </a>&nbsp;';
        		$delete_btn 		= '<a href="javascript:;" class="btn btn-danger" onClick="return are_you_sure( \'delete_bank\' ,\'Delete Bank\', \'Are you sure want to delete this bank with all branches? Once deleted, it cannot be reverted.\', '.$value->id.' );" ><i class="icon-trash"></i></a>';
        		$sub_array          = array();
                $sub_array[]        = $i;
                $sub_array[]        = $value->bank_name;
                $sub_array[]        = $branch_list;
                $sub_array[]        = $edit_btn. $delete_btn;
                
                $data[]             = $sub_array;

                $i++;
        	}
        }
        $draw            			= ( isset( $_POST[ 'draw' ] ) ? $_POST[ 'draw' ] : 0 );
        $output 					= array( 
				                        'draw'                  => $draw,
				                        'recordsTotal'          => $this->Bank_model->get_all_bank_data(),
				                        'recordsFiltered'       => $this->Bank_model->get_bank_filtered_data(),
				                        'data'                  => $data 
							        );
        echo json_encode( $output );
        return true;
    }

    function delete_bank() {
    	if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $id 						= $this->input->post( 'id', true );
        $where[ 'id' ] 				= $id;
        $table 						= 'bank';
        $where1[ 'parent_id' ] 		= $id;
        $this->Common_model->delete_table( $table, $where );
        $this->Common_model->delete_table( $table, $where1 );
        echo 1;
        return true;
    }
    #### bank branch list , add, edit ,delete starts from here 
    function bank_list() {
    	$slug 						= $this->uri->segment( 2 );
    	$params[ 'slug' ] 			= $slug;
        $title                      = 'Bank Branch';
        $anchor_btn                 = '<a href="'.base_url().'bank" class="btn btn-sm btn-dark"> Back to Bank List </a>
                <a href="'.base_url().'bank-branch-add/'.$slug.'" class="btn btn-sm inventory-bg text-white"> Add Branch </a>';
        $breadcrum                  = '<li class="breadcrumb-item">Settings</li><li class="breadcrumb-item"> <a href="'.base_url().'bank"> Bank </a></li><li class="breadcrumb-item"> Bank Branch </li>';
        $params[ 'title' ]          = $title;
        $params[ 'anchor_btn' ]     = $anchor_btn;
        $params[ 'breadcrum' ]      = $breadcrum;
    	$content 					= $this->load->view( 'pages/bank/branch/index', $params, true );
    	$data[ 'content' ]          = $content;
        $this->template( $data );
        return true;
    }

    function get_bank_branch() {
    	if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $data 						= [];
        $slug 						= $this->input->post( 'slug', true );
        $fetch_data 				= $this->Bank_model->make_bank_datatables( $slug );
        if( $fetch_data ) {
        	$i 						= 1;
        	foreach ( $fetch_data as $key => $value ) {
        		$branch_list 		= '';
        		$edit_btn 			= '';
        		$delete_btn 		= '';
        		
        		$edit_btn 			= '<a href="'.base_url().'bank-branch-add/'.$value->bank_slug.'/'.$value->slug.'" class="btn btn-warning"> <i class="icon-pencil"></i> </a>&nbsp;';
        		$delete_btn 		= '<a href="javascript:;" class="btn btn-danger" onClick="return are_you_sure( \'delete_bank_branch\' ,\'Delete Bank Branch\', \'Are you sure want to delete this bank branch with all branches? Once deleted, it cannot be reverted.\', '.$value->id.', \''.$value->bank_slug.'\' );" ><i class="icon-trash"></i></a>';
        		$sub_array          = array();
                $sub_array[]        = $i;
                $sub_array[]        = $value->parent_bank_name;
                $sub_array[]        = $value->branch;
                $sub_array[]        = $value->ifsc;
                $sub_array[]        = $value->address;
                $sub_array[]        = $edit_btn. $delete_btn;
                
                $data[]             = $sub_array;

                $i++;
        	}
        }
        $draw            			= ( isset( $_POST[ 'draw' ] ) ? $_POST[ 'draw' ] : 0 );
        $output 					= array( 
				                        'draw'                  => $draw,
				                        'recordsTotal'          => $this->Bank_model->get_all_bank_data(),
				                        'recordsFiltered'       => $this->Bank_model->get_bank_filtered_data(),
				                        'data'                  => $data 
							        );
        echo json_encode( $output );
        return true;
    }

    public function add_branch() {

    	$bank_slug 					= $this->uri->segment( 2 );
    	$bank_info 					= '';
    	$slug 						= $this->uri->segment( 3 );
    	$btn_name 					= 'Add';
        $breadcrum                  = '<li class="breadcrumb-item">Settings</li><li class="breadcrumb-item"> <a href="'.base_url().'bank"> Bank </a></li><li class="breadcrumb-item"> Add Bank Branch </li>';
    	if( $slug ) {
    		$bank_info 				= $this->Bank_model->bank_info( $slug );
    		$btn_name 				= 'Update';
            $breadcrum              = '<li class="breadcrumb-item">Settings</li><li class="breadcrumb-item"> <a href="'.base_url().'bank"> Bank </a></li><li class="breadcrumb-item"> Update Bank Branch </li>';
    	}
    	$params[ 'bank_info' ] 		= $bank_info;
    	$params[ 'slug' ] 			= $slug;
    	$params[ 'bank_slug' ] 		= $bank_slug;
    	$params[ 'btn_name' ] 		= $btn_name;
        $title                      = 'Add Dealer or Farmer';
        $anchor_btn                 = '';
        $params[ 'title' ]          = $title;
        $params[ 'anchor_btn' ]     = $anchor_btn;
        $params[ 'breadcrum' ]      = $breadcrum;
    	$content 					= $this->load->view( 'pages/bank/branch/add', $params, true );
    	$data[ 'content' ]          = $content;
        $this->template( $data );
        return true;

    }

    public function add_bank_branch() {

    	if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $slug 							= $this->input->post( 'slug', true );
        
        $this->form_validation->set_rules( 'branch', 'Branch Name', 'required' );
        $this->form_validation->set_rules( 'email', 'Email', 'valid_email' );
        $this->form_validation->set_rules( 'phone', 'Phone number', 'numeric' );
        if( isset( $slug ) && !empty( $slug ) ) {} else {
        	$this->form_validation->set_rules( 'ifsc', 'IFSC Code', 'is_unique[bank.ifsc]' );
	        $this->form_validation->set_rules( 'micr_code', 'MICR Code', 'is_unique[bank.micr_code]' );
	        $this->form_validation->set_message( 'is_unique', 'The %s is already taken' );
        }
        

        if ($this->form_validation->run() == FALSE)
        {
            $error          			= 1;
            $error_msg      			= validation_errors();
        } else {

        	$bank_slug 					= $this->input->post( 'bank_slug', true );
        	$bank_info 					= $this->Bank_model->bank_info( $bank_slug );
        	$bank_name 					= $bank_info->bank_name;
        	$branch 					= $this->input->post( 'branch', true );
        	$ins[ 'branch' ] 			= $branch;
        	$ins[ 'parent_id' ] 		= $bank_info->id;
        	$ins[ 'ifsc' ] 				= $this->input->post( 'ifsc', true );
        	$ins[ 'micr_code' ] 		= $this->input->post( 'micr_code', true );
        	$ins[ 'email' ] 			= $this->input->post( 'email', true );
        	$ins[ 'phone' ] 			= $this->input->post( 'phone', true );
        	$ins[ 'address' ] 			= $this->input->post( 'address', true );
        	$ins[ 'slug' ] 				= url_title( $branch.' '.$bank_name, 'dash', true);

        	if( isset( $slug ) && !empty( $slug ) ) {
        		$where 					=  array( 'slug' => $slug );
        		$bank_id 				= $this->Common_model->update_table( 'bank', $where, $ins );
        		$error_msg 				= '<p> Bank Branch Updated Successfully </p>';
        	} else {
        		$ins[ 'created_by' ] 	= $this->session->userdata( 'user_id' );
        		$ins[ 'created_at' ] 	= date( 'Y-m-d H:i:s' );	
        		
        		$bank_id 				= $this->Common_model->insert_table( 'bank', $ins );
        		$error_msg 				= '<p> Bank Branch Added Successfully </p>';
        	}
        	
        	
        	if( $bank_id ) {
        		$error 					= 0;
        		
        	} else {
        		$error 					= 1;
        		$error_msg 				= '<p> Error Occured while adding bank branch </p>';
        	}
        }

        $data[ 'error_msg' ]    		= $error_msg;
        $data[ 'error' ]        		= $error;
        
        echo json_encode( $data );
        return true;

    }

    function delete_bank_branch() {
    	if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $id 						= $this->input->post( 'id', true );
        $where[ 'id' ] 				= $id;
        $table 						= 'bank';
        $this->Common_model->delete_table( $table, $where );
        
        echo 1;
        return true;
    }

    #### bank branch  ends here

    function template( $data ) {
        
        echo $this->load->view( 'layout/template', $data, true );
        return true;
    }
}

?>