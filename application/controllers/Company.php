
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company extends CI_Controller {
    function __construct() {
       parent::__construct();
       $this->load->model( 'Setting_model' );
       $this->load->model( 'Bank_model' );
    }

    public function index() {
    	$title                      = 'Company';
        $breadcrum                  = '<li class="breadcrumb-item"><a href="javascript:;">Settings</a></li><li class="breadcrumb-item"><a href="javascript:;"> Company </a></li>';

        $params[ 'title' ]          = $title;
        $params[ 'anchor_btn' ]     = '<a href="'.base_url().'company/add" class="btn btn-sm inventory-bg text-white" title=""> Create New Company </a>';
        $company_info               = $this->Common_model->get_company_all();
        $params[ 'company_info' ]   = $company_info;
        $params[ 'breadcrum' ]      = $breadcrum; 
    	$content 					= $this->load->view( 'pages/company/index', $params, true );
    	$data[ 'content' ]          = $content;
        $this->template( $data );
        return true;
    }

    public function add() {
        
        // $this->session->unset_userdata( 'company_address' );
        $slug                               = $this->uri->segment( 3 );
        $company_info                       = '';
        $id                                 = '';
        $info                               = '';
        $bank_id                            = '';
        $bank                               = '';
        $title                              = 'Add Company';
        $breadcrum                          = '<li class="breadcrumb-item"> <a href="javascript:;">Settings</a></li><li class="breadcrumb-item"> <a href="'.base_url().'company"> Company</a> </li><li class="breadcrumb-item"><a href="javascript:;"> Add Company </a></li>';
        $tmp                                = [];
        if( isset( $slug ) && !empty( $slug ) ) {
            $company_info                   = $this->Common_model->get_company_info( '', $slug );
            $id                             = $company_info->id;
            $title                          = 'Update Company';

            $bank                           = $this->Bank_model->bank_info( '', $company_info->bank_id );
            $bank_id                        = $bank->parent_id;
            $breadcrum                      = '<li class="breadcrumb-item">Settings</li><li class="breadcrumb-item"> <a href="'.base_url().'company"> Company</a> </li><li class="breadcrumb-item"> Update Company </li>';
        }

    	
        $anchor_btn                         = '';
        $params[ 'title' ]                  = $title;
        $params[ 'anchor_btn' ]             = $anchor_btn;
        $bank_info                          = $this->Bank_model->get_all_bank();
        $params[ 'bank_info' ]              = $bank_info; 
        $params[ 'company_info' ]           = $company_info;
        $params[ 'id' ]                     = $id;
        $params[ 'bank_id' ]                = $bank_id;
        $params[ 'bank' ]                   = $bank;
        $params[ 'breadcrum' ]              = $breadcrum;                    
    	$content 					        = $this->load->view( 'pages/company/add', $params, true );
    	$data[ 'content' ]                  = $content;
        
        $this->template( $data );
        return true;
    }

    public function get_address_form() {
        $key                                = $this->input->post( 'id', true );
        $from                               = $this->input->post( 'from', true );
        $action                             = $this->input->post( 'action', true );
        $add_info                           = '';
        $title                              = 'Add';
        $county_info                        = $this->Common_model->all( 'country' );
        $state_info                         = $this->Common_model->all( 'state', array( 'country_id' => '99' ) );
        if( isset( $from ) && !empty( $from ) && $from == 'customer_address' ) {
            $address_type                   = $this->Setting_model->get_all_options( 'dealer-address-type' );
            if( isset( $action) && !empty( $action ) ) {
                if( $this->session->has_userdata( 'customer_address' ) ) {
                    $address                = $this->session->userdata( 'customer_address' );
                    $add_info               = $address[ $key ];
                    $title                  = 'Update';
                }
            }
            
            
        } else if( $from == 'company_address' ) {
            $address_type                   = $this->Setting_model->get_all_options( 'company-address-type' );
            if( isset( $action) && !empty( $action ) ) {
                if( $this->session->has_userdata( 'company_address' ) ) {
                    $address                = $this->session->userdata( 'company_address' );
                    $add_info               = $address[ $key ];
                    $title                  = 'Update';
                }
            }
        }

       
        $params[ 'address_type' ]           = $address_type;
        $params[ 'add_info' ]               = $add_info;
        $params[ 'key' ]                    = $key;
        $params[ 'title' ]                  = $title;
        $params[ 'from' ]                   = $from; 
        $params[ 'action' ]                 = $action; 
        $params[ 'county_info' ]            = $county_info;
        $params[ 'state_info' ]             = $state_info;
        echo $this->load->view( 'pages/company/form/_form_address', $params, true );
    }

    public function insert_company() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $bank                               = $this->input->post( 'bank', true );
        $ac_no                              = $this->input->post( 'ac_no', true );
        $id                                 = $this->input->post( 'id', true );
        $company_info                       = $this->Common_model->get_company_info( $id );
        
        if( !$id ) {
            $this->form_validation->set_rules( 'company_name', 'Company Name', 'required|is_unique[company.company_name]' );
            $this->form_validation->set_rules( 'regional_manager_mobile', 'Regional Manager Mobile', 'numeric|is_unique[company.regional_manager_mobile]' );
            $this->form_validation->set_rules( 'area_manager_mobile', 'Area Manager Mobile', 'numeric|is_unique[company.area_manager_mobile]' );
            $this->form_validation->set_rules( 'regional_manager_email', 'Area Manager Email', 'valid_email|is_unique[company.regional_manager_email]' );
            $this->form_validation->set_rules( 'area_manager_email', 'Area Manager Email', 'valid_email|is_unique[company.area_manager_email]' );
            if( ( isset( $bank ) && !empty( $bank ) ) || ( isset( $ac_no ) && !empty( $ac_no ) ) ) {
                $this->form_validation->set_rules( 'ac_no', 'Bank Account No', 'required|is_unique[company.bank_ac_no]' );
                $this->form_validation->set_rules( 'bank', 'Bank', 'required' );
                $this->form_validation->set_rules( 'bank_branch', 'Bank Branch', 'required' );
                $this->form_validation->set_rules( 'ifsc', 'Bank IFSC', 'required' );
            }
        } else {

            if( $this->input->post( 'company_name' ) != $company_info->company_name ) {
                $this->form_validation->set_rules( 'company_name', 'Company Name', 'required|is_unique[company.company_name]' );    
            } else {
                $this->form_validation->set_rules( 'company_name', 'Company Name', 'required' );    
            }
            if( $this->input->post( 'regional_manager_mobile' ) != $company_info->regional_manager_mobile && empty( $company_info->regional_manager_mobile ) ) {
                $this->form_validation->set_rules( 'regional_manager_mobile', 'Regional Manager Mobile', 'numeric|is_unique[company.regional_manager_mobile]' );
            }
            if( $this->input->post( 'area_manager_mobile' ) != $company_info->area_manager_mobile && empty( $company_info->area_manager_mobile )  ) {
                $this->form_validation->set_rules( 'area_manager_mobile', 'Area Manager Mobile', 'numeric|is_unique[company.area_manager_mobile]' );
            }
            if( $this->input->post( 'regional_manager_email' ) != $company_info->regional_manager_email && !empty( $company_info->regional_manager_email )  ) {
                $this->form_validation->set_rules( 'regional_manager_email', 'Area Manager Email', 'valid_email|is_unique[company.regional_manager_email]' ); 
            }
            if( $this->input->post( 'area_manager_email' ) != $company_info->area_manager_email && !empty( $company_info->area_manager_email )   ) {
                $this->form_validation->set_rules( 'area_manager_email', 'Area Manager Email', 'valid_email|is_unique[company.area_manager_email]' );   
            }
            if( $this->input->post( 'ac_no' ) != $company_info->bank_ac_no ) {
                $this->form_validation->set_rules( 'ac_no', 'Bank Account No', 'required|is_unique[company.bank_ac_no]' );  
            }
        }
       
        $view                                   = '';
        if ($this->form_validation->run() == FALSE)
        {
            $error                              = 1;
            $error_msg                          = validation_errors();
        } else {

            $file_full_path                     = null;
            if( isset($_FILES['company_logo']['name']) && !empty($_FILES['company_logo']['name']) ) {

                $file                           = 'company_logo'; 
                $temp_file                      = explode('.', $_FILES["$file"]['name']);  
                $config['upload_path']          = 'assets/images/company';
                $config['allowed_types']        = '*';
                $config['max_size']             = 500000;
                $filetype                       = $_FILES[ "$file" ][ 'type' ];
                $expfiletype                    = explode( '.', $_FILES[ "$file" ][ 'name' ] );  
                $config['file_name']            = strtolower($temp_file[0]).'-'.rand( 101, 999 ) . '.' . $expfiletype[ 1 ];
                $this->upload->initialize( $config );
                $this->load->library( 'upload', $config );
                if ( $this->upload->do_upload( $file ) ) {  
                    $upload_data                = $this->upload->data(); 
                    $filename                   = $upload_data[ 'file_name' ];  
                    $file_full_path             = base_url().'assets/images/company/'.$filename;
                    $filepath                   = $upload_data[ 'full_path' ];
                } else {
                   $error_msg                   = 'Image upload failed!';
                   $error                       = 1;
                }
            }
          
            ###################   --- > < ---    
            // step 1:  first insert bank details in bank branch table
            // step 2: then insert company
            // step 3: then address   
            ########### step 1
            
            $bank_branch                        = $this->input->post( 'bank_branch', true );
            $ifsc                               = $this->input->post( 'ifsc', true );
            $branch_exists                      = $this->Bank_model->check_bank_branch_exists( $bank, $ifsc );
            if( ! $branch_exists ) {
                $ins_branch[ 'parent_id' ]      = $bank;
                $ins_branch[ 'branch' ]         = $bank_branch;
                $ins_branch[ 'ifsc' ]           = $ifsc;
                $ins_branch[ 'created_by' ]     = $this->session->userdata( 'user_id' );
                $ins_branch[ 'slug' ]           = url_title( $bank.' '.$bank_branch, 'dash', true );
                $bank_id                        = $this->Common_model->insert_table( 'bank', $ins_branch ); 
            } else {
                $bank_id                        = $branch_exists->id;
            }
            ########### step 2
            $company_name                       = $this->input->post( 'company_name', true );
            $license                            = $this->input->post( 'license', true );
            $pesticides_license                 = $this->input->post( 'pesticides_license', true );

            $fertilizer_license                 = $this->input->post( 'fertilizer_license', true );
            $gst                                = $this->input->post( 'gst', true );
            $cst                                = $this->input->post( 'cst', true );
            $vat                                = $this->input->post( 'vat', true );
            $ac_no                              = $this->input->post( 'ac_no', true );
            $pan_no                             = $this->input->post( 'pan_no', true );
            $regional_manager_name              = $this->input->post( 'regional_manager_name', true );
            $regional_manager_mobile            = $this->input->post( 'regional_manager_mobile', true );
            $regional_manager_email             = $this->input->post( 'regional_manager_email', true );
            $area_manager_name                  = $this->input->post( 'area_manager_name', true );
            $area_manager_mobile                = $this->input->post( 'area_manager_mobile', true );
            $area_manager_email                 = $this->input->post( 'area_manager_email', true );

            $ins[ 'company_name' ]              = $company_name;
            $ins[ 'gstin_no' ]                  = $this->input->post( 'gstin_no', true );
            if( isset( $file_full_path ) && !empty( $file_full_path ) ) {
                $ins[ 'company_logo' ]              = $file_full_path;    
            }
            $ins[ 'seed_license' ]                  = $license;
            $ins[ 'pan_no' ]                        = $pan_no;
            $ins[ 'pesticides_license' ]            = $pesticides_license;
            $ins[ 'fertilizer_license' ]            = $fertilizer_license;
            $ins[ 'gst' ]                           = $gst;
            $ins[ 'cst' ]                           = $cst;
            $ins[ 'vat' ]                           = $vat;
            $ins[ 'bank_ac_no' ]                    = $ac_no;
            $ins[ 'bank_id' ]                       = $bank_id;
            $ins[ 'regional_manager_name' ]         = $regional_manager_name;
            $ins[ 'regional_manager_mobile' ]       = $regional_manager_mobile;
            $ins[ 'regional_manager_email' ]        = $regional_manager_email;
            $ins[ 'area_manager_name' ]             = $area_manager_name;
            $ins[ 'area_manager_mobile' ]           = $area_manager_mobile;
            $ins[ 'area_manager_email' ]            = $area_manager_email;
            $ins[ 'slug' ]                          = url_title( $company_name, 'dash', true );
            if( isset( $id ) && !empty( $id ) ) {
                //update
                $where[ 'id' ]                      = $id;

                $this->Common_model->update_table( 'company', $where, $ins );
                $company_id                         = $id;
                //delete address from address tatble
                $del_where[ 'mode_type' ]           = 'company';
                $del_where[ 'mode_id' ]             = $company_id;
                $this->Common_model->delete_table( 'address', $del_where );
                $error_msg                          = 'Company Updated Successfully';
            }  else {
                //insert
                $ins[ 'created_at' ]                = date( 'Y-m-d H:i:s' );
                $ins[ 'created_by' ]                = $this->session->userdata( 'user_id' );

                $company_id                         = $this->Common_model->insert_table( 'company', $ins );
                $error_msg                          = 'Company Added Successfully';
            }
             
            ######### step 3
            if( $this->session->has_userdata( 'company_address' ) ) {
                $address                            = $this->session->userdata( 'company_address' );
                foreach ( $address as $addkey => $addvalue ) {
                    $adr_ins[ 'address_type_id' ]   = $addvalue[ 'address_type_id' ];
                    $adr_ins[ 'address_type' ]      = $addvalue[ 'address_type' ];
                    $adr_ins[ 'address' ]           = $addvalue[ 'address' ];
                    $adr_ins[ 'city' ]              = $addvalue[ 'city' ];
                    $adr_ins[ 'state' ]             = $addvalue[ 'state' ];
                    $adr_ins[ 'state_id' ]          = $addvalue[ 'state_id' ];
                    $adr_ins[ 'zip_code' ]          = $addvalue[ 'zipcode' ];   
                    $adr_ins[ 'country' ]           = $addvalue[ 'country' ];
                    $adr_ins[ 'country_id' ]        = $addvalue[ 'country_id' ];
                    $adr_ins[ 'email' ]             = $addvalue[ 'email' ];
                    $adr_ins[ 'mobile_no' ]         = $addvalue[ 'mobile_no' ];

                    $adr_ins[ 'mode_type' ]         = 'company';
                    $adr_ins[ 'mode_id' ]           = $company_id;
                    $adr_ins[ 'created_at' ]        = date( 'Y-m-d H:i:s' );
                    $this->Common_model->insert_table( 'address', $adr_ins ); 
                }
                $this->session->unset_userdata( 'company_address' );
            } 
            
            $error                                  = 0;
            
        }

        $data[ 'error' ]                            = $error;
        $data[ 'error_msg' ]                        = $error_msg;
        $data[ 'view' ]                             = $view;
        echo json_encode( $data );
    }

    function delete_company() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $id                         = $this->input->post( 'id', true );
        $where[ 'id' ]              = $id;
        $table                      = 'company';
        $data[ 'status' ]           = 'INACTIVE';
        $this->Common_model->update_table( $table, $where, $data );
        echo 1;
        return true;
    }

    function address_list() {
        
        $company_id                 = $this->input->post( 'company_id', true );
        if( isset( $company_id ) && !empty( $company_id ) ) {
            $address                                = $this->Common_model->get_company_address( $company_id, 'all' );
            ////sert session values for address
            if( isset( $address ) && !empty( $address ) ) {
                foreach ( $address as $akey => $avalue ) {
                    $adr_ins[ 'address_type_id' ]   = $avalue->address_type_id;
                    $adr_ins[ 'address_type' ]      = $avalue->address_type;
                    $adr_ins[ 'address' ]           = $avalue->address;
                    $adr_ins[ 'city' ]              = $avalue->city;
                    $adr_ins[ 'state' ]             = $avalue->state;
                    $adr_ins[ 'state_id' ]          = $avalue->state_id;
                    $adr_ins[ 'zipcode' ]           = $avalue->zip_code;   
                    $adr_ins[ 'country' ]           = $avalue->country;
                    $adr_ins[ 'country_id' ]        = $avalue->country_id;
                    $adr_ins[ 'email' ]             = $avalue->email;
                    $adr_ins[ 'mobile_no' ]         = $avalue->mobile_no;

                    $tmp[]                          = $adr_ins;
                }
            }
            if( isset( $tmp ) ) {
                $this->session->set_userdata( 'company_address', $tmp );    
            }
            
        }

        $tmp                        = $this->session->userdata( 'company_address' );
        $params[ 'info' ]           = $tmp;
        $params[ 'from' ]           = 'company_address';
        echo $this->load->view( 'pages/company/form/_form_address_info', $params, true );
        return true;
    }

    function get_state() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }

        $country_id                 = $this->input->post( 'country_id', true );
        $state_info                 = $this->Common_model->all( 'state', array( 'country_id' => $country_id ) );
        $dp = '<select id="state_id" name="state_id" class="form-control">
                                <option value=""> -- Select State -- </option>';
        if( isset( $state_info ) && !empty( $state_info ) ) {
            foreach ( $state_info as $skey => $svalue ) {
            $dp .= '<option value="'.$svalue->id.'">'.$svalue->state.'</option>';  
            }
        }
        $dp .= '</select>';

        echo $dp;
        return true;             
    }

    function template( $data ) {
        
        echo $this->load->view( 'layout/template', $data, true );
        return true;
    }

}

?>