
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permission extends CI_Controller {
    function __construct() {
       parent::__construct();
    }

     public function index() {
    	
    	$content 					= $this->load->view( 'pages/permission/index', '', true );
    	$data[ 'content' ]          = $content;
        $this->template( $data );
        return true;
    }

    

    function template( $data ) {
        
        echo $this->load->view( 'layout/template', $data, true );
        return true;
    }

}

?>