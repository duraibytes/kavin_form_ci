
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {
    function __construct() {
       parent::__construct();
       $this->load->model( 'Product_model' );
    }

    public function index() {
    	$title                      = 'Product';
        $anchor_btn                 = '<a href="'.base_url().'product/add" class="btn btn-sm inventory-bg text-white" title=""> Create New Product </a>';
        $breadcrum                  = '<li class="breadcrumb-item"> <a href="javascript:;">Settings</a></li><li class="breadcrumb-item"><a href="javascript:;">Products</a></li>';

        $params[ 'title' ]          = $title;
        $params[ 'anchor_btn' ]     = $anchor_btn;
        $params[ 'breadcrum' ]      = $breadcrum;
    	$content 					= $this->load->view( 'pages/product/index', $params, true );
    	$data[ 'content' ]          = $content;
        $this->template( $data );
        return true;
    }

    public function add() {
        $slug                       = $this->uri->segment( 3 );
        $title                      = 'Add Product';
        $product_info               = '';
        $breadcrum                  = '<li class="breadcrumb-item"><a href="javascript:;">Settings</a></li><li class="breadcrumb-item"><a href="'.base_url().'product"> Product</a></li><li class="breadcrumb-item"> <a href="javascript:;">Add Product</a></li>';
        if( isset( $slug ) && !empty( $slug  ) ) {
            $product_info           = $this->Product_model->get_product_info( '', $slug );
            $title                  = 'Update Product';
            $breadcrum              = '<li class="breadcrumb-item"><a href="javascript:;">Settings</a></li><li class="breadcrumb-item"><a href="'.base_url().'product"> Product</a></li><li class="breadcrumb-item"><a href="javascript:;">Update Product</a></li>';
        }
    	
        $anchor_btn                 = '';
        $params[ 'title' ]          = $title;
        $params[ 'anchor_btn' ]     = $anchor_btn;
        $params[ 'product_info' ]   = $product_info;
        $params[ 'slug' ]           = $slug;
        $params[ 'breadcrum' ]      = $breadcrum;
    	$content 					= $this->load->view( 'pages/product/add', $params, true );
    	$data[ 'content' ]          = $content;
        $this->template( $data );
        return true;
    }

    public function insert() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }

        $id                                     = $this->input->post( 'id', true );

        if( !$id ) {
            $this->form_validation->set_rules( 'product_code', 'Product Code', 'required|is_unique[product.product_code]' );
        } else {
            $product_info                       = $this->Product_model->get_product_info( $id );
            if( $this->input->post( 'product_code' ) != $product_info->product_code ) {
                $this->form_validation->set_rules( 'product_code', 'Product Code', 'required|is_unique[product.product_code]' );
            } else {
                $this->form_validation->set_rules( 'product_code', 'Product Code', 'required' );  
            }
        }
        $this->form_validation->set_rules( 'product_code', 'Product Name', 'required' );
        $this->form_validation->set_rules( 'product_manufacturer', 'Product Manufacturer', 'required' );
        
        $this->form_validation->set_rules( 'product_category', 'Product Category', 'required' );
        $this->form_validation->set_rules( 'product_description', 'Product Description', 'required' );
        $this->form_validation->set_rules( 'product_uom', 'Product UOM', 'required' );
        $this->form_validation->set_rules( 'net_purchase_price', 'Net Purchase Price', 'numeric' );
        $this->form_validation->set_rules( 'dealer_sale_price', 'Dealer Sale Price', 'numeric' );
        $this->form_validation->set_rules( 'farmer_sale_price', 'Farmer Sale Price', 'numeric' );
        $this->form_validation->set_rules( 'retail_sale_price', 'Retail Sale Price', 'numeric' );
        

        if ($this->form_validation->run() == FALSE)
        {
            $error                              = 1;
            $error_msg                          = validation_errors();
        } else {
            
            $file_full_path                     = null;
            if( isset($_FILES['product_image']['name']) && !empty($_FILES['product_image']['name']) ) {

                $file                           = 'product_image'; 
                $temp_file                      = explode('.', $_FILES["$file"]['name']);  
                $config['upload_path']          = 'assets/images/product';
                $config['allowed_types']        = '*';
                $config['max_size']             = 500000;
                $filetype                       = $_FILES[ "$file" ][ 'type' ];
                $expfiletype                    = explode( '.', $_FILES[ "$file" ][ 'name' ] );  
                $config['file_name']            = strtolower($temp_file[0]).'-'.rand( 101, 999 ) . '.' . $expfiletype[ 1 ];
                $this->upload->initialize( $config );
                $this->load->library( 'upload', $config );
                if ( $this->upload->do_upload( $file ) ) {  
                    $upload_data                = $this->upload->data(); 
                    $filename                   = $upload_data[ 'file_name' ];  
                    $file_full_path             = base_url().'assets/images/product/'.$filename;
                    $filepath                   = $upload_data[ 'full_path' ];
                } else {
                   $error_msg                   = 'Image upload failed!';
                   $error                       = 1;
                }
            }
            if( isset( $file_full_path ) && !empty( $file_full_path ) ) {
                $ins[ 'image' ]                 = $file_full_path;
            }
            $slug_name                          = $this->input->post( 'product_name', true ).' '. $this->input->post( 'product_code', true );
            $ins[ 'product_name' ]              = $this->input->post( 'product_name', true );
            $ins[ 'product_company' ]           = $this->input->post( 'product_manufacturer', true );
            $ins[ 'product_code' ]              = $this->input->post( 'product_code', true );
            $ins[ 'product_category' ]          = $this->input->post( 'product_category', true );
            $ins[ 'description' ]               = $this->input->post( 'product_description', true );
            $ins[ 'packaging' ]                 = $this->input->post( 'product_uom', true );
            $ins[ 'net_purchase_price' ]        = $this->input->post( 'net_purchase_price', true );
            $ins[ 'farmer_sale_price' ]         = $this->input->post( 'farmer_sale_price', true );
            $ins[ 'dealer_sale_price' ]         = $this->input->post( 'dealer_sale_price', true );
            $ins[ 'retail_sale_price' ]         = $this->input->post( 'retail_sale_price', true );
            $ins[ 'slug' ]                      = url_title( $slug_name, 'dash', true );
            
            if( isset( $id ) && !empty( $id ) ) {
                $where[ 'id' ]                  = $id;
                $this->Common_model->update_table( 'product', $where, $ins );
                $error_msg                      = 'Product Updated Successfully';
            } else {
                //insert
                $ins[ 'created_at' ]            = date( 'Y-m-d H:i:s' );
                $ins[ 'created_by' ]            = $this->session->userdata( 'user_id' );
                $ins[ 'owner_id' ]              = $this->session->userdata( 'company_id' );
                $ins[ 'status' ]                = 'ACTIVE';

                $product_id                     = $this->Common_model->insert_table( 'product', $ins );
                $error_msg                      = 'Product Added Successfully';

                $data[ 'id' ]                   = $product_id;
                $data[ 'description' ]          = $ins[ 'description' ];
                $data[ 'net_purchase_price' ]   = $ins[ 'net_purchase_price' ];
                $data[ 'product_category' ]     = $ins[ 'product_category' ];
                $data[ 'packaging' ]            = $ins[ 'packaging' ];
                $data[ 'product_code' ]         = $ins[ 'product_code' ];
                

            }

            $error                              = 0;
        }
        $data[ 'error' ]                        = $error;
        $data[ 'error_msg' ]                    = $error_msg;
        
        echo json_encode( $data );
    }

    function get_product() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $data                       = [];
        $fetch_data                 = $this->Product_model->make_product_datatables();
        if( $fetch_data ) {
            $i                      = 1;
            foreach ( $fetch_data as $key => $value ) {
                
                $edit_btn           = '';
                $delete_btn         = '';
               
                $edit_btn           = '<a href="'.base_url().'product/add/'.$value->slug.'" class="btn btn-outline-warning"> <i class="icon-pencil"></i>  </a>&nbsp;';
                $delete_btn         = '<a href="javascript:;" class="btn btn-outline-danger" onClick="return are_you_sure( \'delete_product\' ,\'Delete Product\', \'Are you sure want to delete this product? \', '.$value->id.' );" ><i class="icon-trash"></i></a>';
                $sub_array          = array();
                $sub_array[]        = $value->product_name;
                $sub_array[]        = $value->product_company;
                $sub_array[]        = $value->product_code;
                $sub_array[]        = $value->product_category;
                $sub_array[]        = $value->description;
                $sub_array[]        = $value->packaging;
                $sub_array[]        = $edit_btn. $delete_btn;
                
                $data[]             = $sub_array;

                $i++;
            }
        }
        $draw                       = ( isset( $_POST[ 'draw' ] ) ? $_POST[ 'draw' ] : 0 );
        $output                     = array( 
                                        'draw'                  => $draw,
                                        'recordsTotal'          => $this->Product_model->get_all_product_data(),
                                        'recordsFiltered'       => $this->Product_model->get_product_filtered_data(),
                                        'data'                  => $data 
                                    );
        echo json_encode( $output );
        return true;
    }

    function delete_product() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $id                         = $this->input->post( 'id', true );
        $where[ 'id' ]              = $id;
        $table                      = 'product';
        
        $this->Common_model->delete_table( $table, $where );
        echo 1;
        return true;
    }

    function get_product_typeahead() {
        $c_query                        = $this->input->get( 'query' );
        $total_item                     = $this->input->get( 'total_item' );
        $customer_type                  = $this->input->get( 'customer_type' );
        $product_list                   = $this->Product_model->typehead_product_list( $c_query );
        if( $product_list ) {
            foreach ( $product_list as $key => $value ) {
                
                $data[ $key ][ 'product_name' ]         = $value->product_name;
                $data[ $key ][ 'product_company' ]      = $value->product_company;
                $data[ $key ][ 'product_variety' ]      = $value->product_variety;
                $data[ $key ][ 'product_category' ]     = $value->product_category;
                $data[ $key ][ 'packaging' ]            = $value->packaging;
                $data[ $key ][ 'description' ]          = $value->description;
                $data[ $key ][ 'net_purchase_price' ]   = $value->net_purchase_price;
                $data[ $key ][ 'farmer_sale_price' ]    = $value->farmer_sale_price;
                $data[ $key ][ 'dealer_sale_price' ]    = $value->dealer_sale_price;
                $data[ $key ][ 'retail_sale_price' ]    = $value->retail_sale_price;
                $data[ $key ][ 'total_item' ]           = $total_item;
                $data[ $key ][ 'name' ]                 = $value->product_code;
                $data[ $key ][ 'customer_type' ]        = $customer_type;
                $data[ $key ][ 'id' ]                   = $value->id;
            }
        } else {
            $data[0]['name']    = 'No data found';
            $data[0]['id']      = 0;
        }
        echo json_encode( $data );
        return true;
    }

    function open_add_product_form() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }

        $count                  = $this->input->post( 'count', true );
        $params[ 'count' ]      = $count;
        $params[ 'title' ]      = 'Add Product';

        echo $this->load->view( 'pages/common/form_to_modal', $params, true );

    }

    function get_product_instock() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $product_id             = $this->input->post( 'product_id', true );
        $count                  = $this->input->post( 'count', true );
        $qty                    = $this->input->post( 'qty', true );

        //product purchased
        $where_in               = array( 'purchase', 'purchase-return' );
        $purchased              = $this->Product_model->purchased_product( $product_id, $where_in );
        $sale_where_in          = array( 'sale' );
        $saled                  = $this->Product_model->purchased_product( $product_id, $sale_where_in );
        $purchased_qty          = 0;
        $purchased_return_qty   = 0;
        $saled_qty          = 0;
        $saled_return_qty   = 0;
        if( $purchased ) {
            foreach ($purchased as $key => $value) {
                
                if( $value->mode == 'purchase' ) {
                    $purchased_qty += $value->qty;
                }
                if( $value->mode == 'purchase-return' ) {
                    $purchased_return_qty += $value->qty;
                }
            }
        }

        if( $saled ) {
            foreach ($saled as $key => $value) {
                
                if( $value->mode == 'sale' ) {
                    $saled_qty += $value->qty;
                }
                if( $value->mode == 'sale-return' ) {
                    $saled_return_qty += $value->qty;
                }
            }
        }
        $sales_balance          = $saled_qty - $saled_return_qty;
        $purchased_balalnce     = $purchased_qty - $purchased_return_qty;
        $balance_qty            = $purchased_balalnce - $sales_balance;
        $data[ 'balance_qty' ]  = $balance_qty;
        $data[ 'qty' ]          = (int)$qty;
        echo json_encode( $data );
        return true;
    }

    function template( $data ) {
        
        echo $this->load->view( 'layout/template', $data, true );
        return true;
    }

}

?>