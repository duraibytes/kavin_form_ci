
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company_settings extends CI_Controller {
    function __construct() {
       parent::__construct();
       $this->load->model( 'Setting_model' );
       $this->load->model( 'Bank_model' );
    }

    public function index() {
    	$title                      = 'Comapny Settings';
        $anchor_btn                 = '';
        $breadcrum                  = '<li class="breadcrumb-item"> SETTINGS</li><li class="breadcrumb-item"> <a href="'.base_url().'company-settings"> COMPANY SETTINGS </a></li>';
        $company_info 				= $this->Setting_model->get_company_info( $this->session->userdata( 'company_id' ) );

        $params[ 'title' ]          = $title;
        $params[ 'anchor_btn' ]     = $anchor_btn;
        $params[ 'breadcrum' ]      = $breadcrum;
        $params[ 'company_info' ] 	= $company_info;

    	$content 					= $this->load->view( 'pages/settings/index', $params, true );
    	$data[ 'content' ]          = $content;
        $this->template( $data );
        return true;
    }

    function update_company_setting() {
    	if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $tab                                = $this->input->post( 'tab', true );
        if( isset( $tab ) && $tab == 'company' ) {
            $this->form_validation->set_rules( 'company_name', 'Company Name', 'required' );
        }
        $this->form_validation->set_rules( 'company_email', 'Company Email', 'valid_email' );
        $this->form_validation->set_rules( 'company_mobile', 'Company Mobile', 'numeric' );
        $this->form_validation->set_rules( 'company_phone', 'Company Phone', 'numeric' );
        if ($this->form_validation->run() == FALSE)
        {
            $error                              = 1;
            $error_msg                          = validation_errors();
        } else {
        	$file_full_path                     = null;
            if( isset($_FILES['company_logo']['name']) && !empty($_FILES['company_logo']['name']) ) {

                $file                           = 'company_logo'; 
                $temp_file                      = explode('.', $_FILES["$file"]['name']);  
                $config['upload_path']          = 'assets/images/settings';
                $config['allowed_types']        = '*';
                $config['max_size']             = 500000;
                $filetype                       = $_FILES[ "$file" ][ 'type' ];
                $expfiletype                    = explode( '.', $_FILES[ "$file" ][ 'name' ] );  
                $config['file_name']            = strtolower($temp_file[0]).'-'.rand( 101, 999 ) . '.' . $expfiletype[ 1 ];
                $this->upload->initialize( $config );
                $this->load->library( 'upload', $config );
                if ( $this->upload->do_upload( $file ) ) {  
                    $upload_data                    = $this->upload->data(); 
                    $filename                       = $upload_data[ 'file_name' ];  
                    $file_full_path                 = base_url().'assets/images/settings/'.$filename;
                    $filepath                       = $upload_data[ 'full_path' ];
                } else {
                   $error_msg                       = 'Image upload failed!';
                   $error                           = 1;
                }
            }
            if( isset( $file_full_path ) && !empty( $file_full_path ) )  {
            	$ins[ 'image' ] 				   = $file_full_path;	
            }
        	
            
            if( isset( $tab ) && $tab == 'company' ) {

                $ins[ 'company_name' ]              = $this->input->post( 'company_name', true );
                $ins[ 'mobile' ]                    = $this->input->post( 'company_mobile', true );
                $ins[ 'email' ]                     = $this->input->post( 'company_email', true );
                $ins[ 'phone_no' ]                  = $this->input->post( 'company_phone', true );
                $ins[ 'fax' ]                       = $this->input->post( 'company_fax', true );
                $ins[ 'address' ]                   = $this->input->post( 'company_addres', true ); 
                $ins[ 'website' ]                   = $this->input->post( 'website', true ); 
                $ins[ 'seed_license' ]              = $this->input->post( 'seed_license', true ); 
                $ins[ 'pesticides_license' ]        = $this->input->post( 'pesticides_license', true ); 
                $ins[ 'fertilizer_license' ]        = $this->input->post( 'fertilizer_license', true ); 
                $ins[ 'sales_tax_no' ]              = $this->input->post( 'sale_tax_no', true ); 

                $where[ 'id' ]                      = $this->session->userdata( 'company_id' );
                $this->Common_model->update_table( 'owner', $where, $ins );

            } else if( isset( $tab ) && $tab == 'invoice' ) {
                //check if invoice already created particular company
                $invoice_check                      = $this->Setting_model->get_invoice( $this->session->userdata( 'company_id' ) );

                $doc[ 'purchase_inv_no' ]           = $this->input->post( 'purchase_inv_no', true );
                $doc[ 'purchase_payment_no' ]       = $this->input->post( 'purchase_payment_no', true );
                $doc[ 'purchase_return_no' ]        = $this->input->post( 'purchase_return_no', true );
                $doc[ 'purchase_credit_no' ]        = $this->input->post( 'purchase_credit_no', true );
                $doc[ 'purchase_debit_no' ]         = $this->input->post( 'purchase_debit_no', true );
                $doc[ 'dealer_sales_inv_no' ]       = $this->input->post( 'dealer_sales_inv_no', true );
                $doc[ 'dealer_sales_payment_no' ]   = $this->input->post( 'dealer_sales_payment_no', true );
                $doc[ 'dealer_sales_return_no' ]    = $this->input->post( 'dealer_sales_return_no', true );
                $doc[ 'farmer_sales_inv_no' ]       = $this->input->post( 'farmer_sales_inv_no', true );
                $doc[ 'farmer_sales_payment_no' ]   = $this->input->post( 'farmer_sales_payment_no', true );
                $doc[ 'farmer_sales_return_no' ]    = $this->input->post( 'farmer_sales_return_no', true );
                $doc[ 'sale_credit_no' ]            = $this->input->post( 'sale_credit_no', true );
                $doc[ 'sale_debit_no' ]             = $this->input->post( 'sale_debit_no', true );

                if( $invoice_check ) {

                    $where[ 'company_id' ]          = $this->session->userdata( 'company_id' );
                    $this->Common_model->update_table( 'own_doc_number', $where, $doc );

                } else {

                    $doc[ 'created_by' ]            = $this->session->userdata( 'user_id' );
                    $doc[ 'created_at' ]            = date( 'Y-m-d H:i:s' );
                    $doc[ 'company_id' ]            = $this->session->userdata( 'company_id' );

                    $this->Common_model->insert_table( 'own_doc_number', $doc );
                }
                
            } else if( isset( $tab ) && $tab == 'currency' ) {

                $ins[ 'home_currency' ]             = $this->input->post( 'home_currency', true );
                $ins[ 'unit_length' ]               = $this->input->post( 'unit_length', true );
                $ins[ 'unit_weight' ]               = $this->input->post( 'unit_weight', true );

                $where[ 'id' ]                      = $this->session->userdata( 'company_id' );
                $this->Common_model->update_table( 'owner', $where, $ins );

            } else if( isset( $tab ) && $tab == 'terms' ) { 
                $terms                              = $this->input->post( 'pay_terms',  true );
                $days                               = $this->input->post( 'due_days',  true );
                $pay_terms                          = array_combine( $terms, $days );
                $pay_terms                          = array_filter( $pay_terms );
                if( isset( $pay_terms ) && !empty( $pay_terms ) ) {
                    //delete paytem table
                    $where_term[ 'company_id' ]     = $this->session->userdata( 'company_id' );
                    $this->Common_model->delete_table( 'own_payment_term', $where_term );
                    foreach ( $pay_terms as $key => $value ) {
                        $tm[ 'company_id' ]         = $this->session->userdata( 'company_id' );
                        $tm[ 'term_name' ]          = $key;
                        $tm[ 'due_days' ]           = $value;
                        $tm[ 'created_by' ]         = $this->session->userdata( 'user_id' );
                        $tm[ 'created_at' ]         = date( 'Y-m-d H:i:s' );

                        $this->Common_model->insert_table( 'own_payment_term', $tm );
                    }
                }
                
            } else if( isset( $tab ) && $tab == 'price' ) {
                $total_items                        = $this->input->post( 'total_items', true );

                if( $total_items ){
                    //delete paytem table
                    $where_term[ 'company_id' ]     = $this->session->userdata( 'company_id' );
                    $this->Common_model->delete_table( 'own_pricing_schemes', $where_term );
                    for ( $i=0; $i < $total_items; $i++ ) { 
                        $price_name                 = $this->input->post( 'price_name_'.$i );
                        $is_tax                     = $this->input->post( 'is_tax_'.$i );
                        $tax_percent                = $this->input->post( 'tax_percent_'.$i );
                        $tax_currency               = $this->input->post( 'tax_currency_'.$i );
                        if( $price_name && $tax_currency ) {
                            $tx[ 'company_id' ]     = $this->session->userdata( 'company_id' );
                            $tx[ 'pricing_name' ]   = $price_name;
                            $tx[ 'is_tax_included' ]= $is_tax;
                            $tx[ 'tax_percent' ]    = $tax_percent;
                            $tx[ 'currency' ]       = $tax_currency;
                            $tx[ 'created_at' ]     = date( 'Y-m-d H:i:s' );
                            $tx[ 'created_by' ]     = $this->session->userdata( 'user_id' );

                            $this->Common_model->insert_table( 'own_pricing_schemes', $tx );    
                        }
                    }
                }
                
            } else if( isset( $tab ) && $tab == 'category' ) {
                
                $category                           = $this->input->post( 'category', true );
                $category                           = array_filter( $category );
                
                if( $category ) {
                    //delete category table
                    $where_cat[ 'company_id' ]      = $this->session->userdata( 'company_id' );
                    $this->Common_model->delete_table( 'own_categories', $where_cat );
                    foreach ( $category as $ckey => $cvalue ) {

                        $cat[ 'company_id' ]        = $this->session->userdata( 'company_id' );
                        $cat[ 'name' ]              = $cvalue;
                        $cat[ 'created_by' ]        = $this->session->userdata( 'user_id' );
                        $cat[ 'created_at' ]        = date( 'Y-m-d H:i:s' );

                        $this->Common_model->insert_table( 'own_categories', $cat );    
                    }
                }
                
            } else if( isset( $tab ) && $tab == 'year' ) {
                $start_year                         = $this->input->post( 'start_year',  true );
                $end_year                           = $this->input->post( 'end_year',  true );
                $ph_years                           = array_combine( $start_year, $end_year );
                $ph_years                           = array_filter( $ph_years );
                if( isset( $ph_years ) && !empty( $ph_years ) ) {
                    //delete paytem table
                    $where_term[ 'company_id' ]     = $this->session->userdata( 'company_id' );
                    $this->Common_model->delete_table( 'physical_year', $where_term );
                    foreach ( $ph_years as $key => $value ) {

                        $tm[ 'company_id' ]         = $this->session->userdata( 'company_id' );
                        $tm[ 'start_date' ]         = date( 'Y-m-d', strtotime( $key ) );
                        $tm[ 'end_date' ]           = date( 'Y-m-d', strtotime( $value ) );
                        $tm[ 'created_by' ]         = $this->session->userdata( 'user_id' );
                        $tm[ 'created_at' ]         = date( 'Y-m-d H:i:s' );

                        $this->Common_model->insert_table( 'physical_year', $tm );
                        
                    }
                }

            }

        	
            $error_msg                              = 'Company Updated Successfully';
        	$error 								    = 0;

        }
        $data[ 'error' ]                            = $error;
        $data[ 'error_msg' ]                        = $error_msg;
        echo json_encode( $data );
    }

    function get_ajax_own_info() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }

        $type                           = $this->input->post( 'type', true );
        $company_info                   = $this->Setting_model->get_company_info( $this->session->userdata( 'company_id' ) );
            $params[ 'company_info' ]   = $company_info;
        $view                           = '';
        if( isset( $type ) && $type == 'company' ) {

            $view                       = $this->load->view( 'pages/settings/_company_info', $params, true );

        } else if( isset( $type ) && $type == 'invoice' ) {

            $params[ 'doc_info' ]       = $this->Common_model->find( 'own_doc_number', array( 'company_id' => $this->session->userdata( 'company_id' ) ) );
        
            $view                       = $this->load->view( 'pages/settings/_doc_info', $params, true );

        } else if( isset( $type ) && $type == 'currency' ) {

            $view                       = $this->load->view( 'pages/settings/_currency_unit', $params, true );

        } else if( isset( $type ) && $type == 'terms' ) {

            $params[ 'term_info' ]      = $this->Common_model->all( 'own_payment_term', array( 'company_id' => $this->session->userdata( 'company_id' ) ), 'ACTIVE' );
            $view                       = $this->load->view( 'pages/settings/_pay_terms', $params, true );

        } else if( isset( $type ) && $type == 'price' ) {

            $params[ 'tax_info' ]       = $this->Common_model->all( 'own_pricing_schemes', array( 'company_id' => $this->session->userdata( 'company_id' ) ), 'ACTIVE' );
            $view                       = $this->load->view( 'pages/settings/_price_scheme', $params, true );

        } else if( isset( $type ) && $type == 'category' ) {

            $params[ 'cat_info' ]       = $this->Common_model->all( 'own_categories', array( 'company_id' => $this->session->userdata( 'company_id' ) ), 'ACTIVE' );
            $view                       = $this->load->view( 'pages/settings/_category', $params, true );

        } else if( isset( $type ) && $type == 'year' ) {

            $params[ 'year_info' ]      = $this->Common_model->all( 'physical_year', array( 'company_id' => $this->session->userdata( 'company_id' ) ), 'ACTIVE' );
            $view                       = $this->load->view( 'pages/settings/_physical_year', $params, true );

        } else if( isset( $type ) && $type == 'bank' ) {
            $bank_info                  = $this->Bank_model->get_all_bank();
            $params[ 'own_bank' ]       = $this->Bank_model->own_bank_list( $this->session->userdata( 'user_id' ), 'own' );
            $params[ 'bank_info' ]      = $bank_info; 
            
            $view                       = $this->load->view( 'pages/settings/bank/bank_info', $params, true );
        }

        echo $view;
        return true;
    }

    function add_company_bank() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $this->form_validation->set_rules( 'bank', 'Bank', 'required' );
        $this->form_validation->set_rules( 'bank_branch', 'Bank Branch', 'required' );
        $this->form_validation->set_rules( 'ac_no', 'Bank Ac No', 'required' );
        $this->form_validation->set_rules( 'ifsc', 'IFSC', 'required' );
        $view                           = '';
        if ($this->form_validation->run() == FALSE)
        {
            $error                      = 1;
            $error_msg                  = validation_errors();
        } else {
           
            $id                         = $this->input->post( 'id', true );

            $ins[ 'user_id' ]           = $this->session->userdata( 'company_id' );
            $ins[ 'user_type' ]         = 'own';
            $ins[ 'bank_id' ]           = $this->input->post( 'bank', true );
            $ins[ 'branch_id' ]         = $this->input->post( 'bank_branch', true );
            $ins[ 'bank_ac_no' ]        = $this->input->post( 'ac_no', true );
            $ins[ 'ifsc_code' ]         = $this->input->post( 'ifsc', true );

            if( isset( $id ) && !empty( $id ) ) {
                $where[ 'id' ]          = $id;

                $this->Common_model->update_table( 'own_bank_info', $where, $ins );
                $error                  = 0;
                $error_msg              = '<p> Own Bank Updated successfully</p>';
            } else {
                $ins[ 'created_by' ]    = $this->session->userdata( 'user_id' );
                $ins[ 'created_at' ]    = date( 'Y-m-d H:i:s' );

                $this->Common_model->insert_table( 'own_bank_info', $ins );
                $error                  = 0;
                $error_msg              = '<p> Own Bank Addedd successfully</p>';
            }
           
           $params[ 'own_bank' ]        = $this->Bank_model->own_bank_list( $this->session->userdata( 'user_id' ), 'own' );

           $view                        = $this->load->view( 'pages/settings/bank/_bank_list', $params, true );
           $bank_info                   = $this->Bank_model->get_all_bank();
           $params[ 'bank_info' ]       = $bank_info;
           $form                        = $this->load->view( 'pages/settings/bank/form', $params, true );
        }

        $data[ 'error' ]                = $error;
        $data[ 'error_msg' ]            = $error_msg;
        $data[ 'view' ]                 = $view;
        $data[ 'form' ]                 = $form;
        echo json_encode( $data );
    }

    function update_own_company_bank() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $own_bank_id                    = $this->input->post( 'own_bank_id', true );
        $obank_info                     = $this->Common_model->find( 'own_bank_info', array( 'id' => $own_bank_id ) );
        $bank_info                      = $this->Bank_model->get_all_bank();
        $params[ 'own_bank_info' ]      = $obank_info;
        $params[ 'bank_info' ]          = $bank_info;
        $form                           = $this->load->view( 'pages/settings/bank/form', $params, true );
        $data[ 'form' ]                 = $form;
        echo json_encode( $data );
        return true;
    }

    function delete_own_bank() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        } 
        $own_bank_id                    = $this->input->post( 'own_bank_id', true );
        $obank_info                     = $this->Common_model->find( 'own_bank_info', array( 'id' => $own_bank_id ) );
        $where[ 'id' ]                  = $own_bank_id;
        $this->Common_model->delete_table( 'own_bank_info', $where );

        $params[ 'own_bank' ]           = $this->Bank_model->own_bank_list( $this->session->userdata( 'user_id' ), 'own' );

        $view                           = $this->load->view( 'pages/settings/bank/_bank_list', $params, true );
        $bank_info                      = $this->Bank_model->get_all_bank();
        $params[ 'bank_info' ]          = $bank_info;
        $form                           = $this->load->view( 'pages/settings/bank/form', $params, true );

        $data[ 'view' ]                 = $view;
        $data[ 'form' ]                 = $form;
        echo json_encode( $data );
        return false;
    }

    function template( $data ) {
        echo $this->load->view( 'layout/template', $data, true );
        return true;
    }

}