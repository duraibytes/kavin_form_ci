
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
    function __construct() {
       parent::__construct();
       $this->load->model( 'Employee_model' );
       $this->load->helper( 'auth_helper' );
       $this->load->helper( 'email_helper' );

    }

    public function index() {
        $company_info               = $this->Common_model->all( 'owner' );
        $params[ 'company_info' ]   = $company_info;
    	$content                    = $this->load->view( 'auth/login', $params, true );
        $data[ 'content' ]          = $content;
        $this->template( $data );
        return true;
    }

    public function register() {
    	return $this->load->view( 'auth/register' );	
    }

    public function add_register() {
    	$this->form_validation->set_rules( 'user_name', 'User Name', 'required|is_unique[users.fname]' );
    	$this->form_validation->set_rules( 'email', 'Email', 'required|valid_email|is_unique[users.email]' );
    	$this->form_validation->set_rules( 'mobile', 'Mobile', 'required|numeric|is_unique[users.mobile_no]|max_length[10]' );
    	$this->form_validation->set_rules( 'password', 'Password', 'required' );
    	$data 						= array( 
    									'user_name' => $this->input->post( 'user_name', true ),
    									'email' 	=> $this->input->post( 'email', true ),
    									'mobile' 	=> $this->input->post( 'mobile', true ),
    									);
    	if ($this->form_validation->run() == FALSE)
        {
            return $this->load->view( 'auth/register', $data );
        } else {
        	//
        	$password 				= $this->encryption->encrypt( $this->input->post( 'password', true ) );

        	$ins[ 'fname' ] 		= $this->input->post( 'user_name', true );
        	$ins[ 'email' ] 		= $this->input->post( 'email', true );
        	$ins[ 'mobile_no' ] 	= $this->input->post( 'mobile', true );
        	$ins[ 'password' ] 		= $password;
        	$ins[ 'status' ] 		= $this->input->post( 'user_name', true );
        	$ins[ 'created_at' ] 	= $this->input->post( 'user_name', true );
        	$user_id 				= $this->Employee_model->insert_user( $ins );
        	redirect( 'login' );
        }

    }

    public function validate_login() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        

    	$this->form_validation->set_rules( 'user_name', 'User Name', 'required|is_unique[users.fname]' );
    	$this->form_validation->set_rules( 'password', 'Password', 'required' );
        $this->form_validation->set_rules( 'company', 'Company', 'required' );
    	if ($this->form_validation->run() == FALSE)
        {
            $error          		= 1;
            $error_msg      		= validation_errors();
        } else {
        	//
        	$user_name 				= $this->input->post( 'user_name' );
        	$password 				= $this->input->post( 'password' );
        	
        	$return         		= validate_login( $user_name, $password );

            if( $return == 1 ) {
                $error 				= 0;
                $error_msg 			= '<p> Login Success </p>';
            } else if( $return == 2  ) {
            	$error 				= 1;
            	$error_msg 			= '<p> Invalid Credentials </p>';
            } else {
            	$error 				= 1;
            	$error_msg 			= '<p> User already loggedin. </p>';
            } 
        }

        $data[ 'error_msg' ]    	= $error_msg;
        $data[ 'error' ]        	= $error;
        
        echo json_encode( $data );
        return true;

    }

    public function forgot_password() {
        $content                    = $this->load->view( 'auth/forgot_password', '', true );
        $data[ 'content' ]          = $content;
        $this->template( $data );
        return true;
    }

    public function send_reset_password() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $this->form_validation->set_rules( 'email', 'Email', 'required' );
        // $this->form_validation->set_message( 'callback_email_exists', 'The %s is not registered' );
        if ( $this->form_validation->run() == FALSE )
        {
            $error                  = 1;
            $error_msg              = validation_errors();
        } else {
            //
            $email                  = $this->input->post( 'email' );
            
            if( mail_exists( $email ) ) {

                $from_email         = "kavinfarms@info.com";
                $to_email           = $email;
                $token              = date( 'Ymdhis' );
                $reset_token        = base64_encode( $token );
                //Load email library
                $subject            = 'Reset Password kavinfarms';
                $message            = 'The email send using codeigniter library<div>< a href="">Click Here</a> To Reset Password </div>';
                

                //Send mail
                if( send_smtp_mail( 'kavinfarms@info.com', 'duraibytes@gmail.com', $subject, $message ) ) {
                    $error_msg              = '<p> Password reset link sent to email id, please check your inbox </p>';
                    $error                  = 0;
                } else {
                    $error_msg              = '<p> Failed to sen reset link, pls contact administrator or try again sometimes later. </p>';
                    $error                  = 1;
                }
                
                
            } else {
                $error                  = 1;
                $error_msg              = '<p> Email id not registered </p>';
            }
            

            
        }

        $data[ 'error_msg' ]        = $error_msg;
        $data[ 'error' ]            = $error;
        
        echo json_encode( $data );
        return true;
    }

    
    public function logout() {
    	 
    	$this->session->sess_destroy();
        redirect( 'login' );
	    
    }

    public function renew() {

        $myfile             = fopen("./assets/readme.txt", "w") or die("Unable to open file!");
        $txt                = date( 'Y-m-d', strtotime( '+10 day' ) );
        $enc_text           = $this->encryption->encrypt( $txt );

        fwrite($myfile, $enc_text);
        fclose($myfile);

        $data["date"]      = $txt;
        $this->load->view(  "auth/license/license_form", $data );
    
    }

    public function expired() {
        $this->load->view( 'errors/html/error_expired', '', true );
    }

    function template( $data ) {
        echo $this->load->view( 'auth/layout/content', $data, true );
        return true;
    }
}

?>