
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model( 'Setting_model' );
        $this->load->model( 'Bank_model' );
        $this->load->model( 'Employee_model' );
    }

    public function index() {
    	$title                      = 'Employee';
        $anchor_btn                 = '<a href="'.base_url().'employee/add" class="btn btn-sm inventory-bg text-white" title=""> Create Employee </a>';

        $breadcrum                  = '<li class="breadcrumb-item"><a href="javascript:;">Settings</a></li><li class="breadcrumb-item"> <a href="'.base_url().'employee"> Employee </a></li>';

        $employee_all               = $this->Employee_model->get_employee();
        $params[ 'title' ]          = $title;
        $params[ 'anchor_btn' ]     = $anchor_btn;
        $params[ 'breadcrum' ]      = $breadcrum;
        $params[ 'employee_all' ]   = $employee_all;
    	$content 					= $this->load->view( 'pages/employee/index', $params, true );
    	$data[ 'content' ]          = $content;
        $this->template( $data );
        return true;
    }

    public function add() {
        $slug                       = $this->uri->segment( 3 );

    	$title                      = 'Add Employee';
        $breadcrum                  = '<li class="breadcrumb-item"><a href="javascript:;">Settings</a></li><li class="breadcrumb-item"> <a href="'.base_url().'employee"> Employee </a></li><li class="breadcrumb-item"><a href="javascript:;"> Add Employee</a></li>';
        $employee_info              = '';
        $bank                       = '';
        $bank_id                    = '';
        if( isset( $slug ) && !empty( $slug ) ) {
            $title                  = 'Update Employee';
            $breadcrum              = '<li class="breadcrumb-item"><a href="javascript:;">Settings</a></li><li class="breadcrumb-item"> <a href="'.base_url().'employee"> Employee </a></li><li class="breadcrumb-item"><a href="javascript:;"> Update Employee </a></li>';
            $employee_info          = $this->Employee_model->get_employee_by_id('', $slug );
            $bank                   = $this->Bank_model->bank_info( '', $employee_info->bank_id );
            $bank_id                = $bank->parent_id;

        }
        $anchor_btn                 = '';
        $user_type                  = $this->Setting_model->get_all_options( 'user-type' );
        $bank_info                  = $this->Bank_model->get_all_bank();
        $params[ 'bank_info' ]      = $bank_info; 
        $params[ 'title' ]          = $title;
        $params[ 'anchor_btn' ]     = $anchor_btn;
        $params[ 'breadcrum' ]      = $breadcrum;
        $params[ 'user_type' ]      = $user_type;
        $params[ 'employee_info' ]  = $employee_info;
        $params[ 'slug' ]           = $slug;
        $params[ 'bank' ]           = $bank;
        $params[ 'bank_id' ]        = $bank_id;

    	$content 					= $this->load->view( 'pages/employee/add', $params, true );
    	$data[ 'content' ]          = $content;
        $this->template( $data );
        return true;
    }

     public function insert_employee() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $bank                               = $this->input->post( 'bank', true );
        $ac_no                              = $this->input->post( 'ac_no', true );
        $is_login_credential                = $this->input->post( 'is_login_credential', true );
        $id                                 = $this->input->post( 'id', true );
        $info                               = $this->Employee_model->get_employee_by_id( $id );

        $this->form_validation->set_rules( 'first_name', 'First Name', 'required' );  
        $this->form_validation->set_rules( 'last_name', 'Last Name', 'required' );
        $this->form_validation->set_rules( 'dob', 'Date Of Birth', 'required' );
        $this->form_validation->set_rules( 'gender', 'Gender', 'required' );

        $this->form_validation->set_rules( 'date_of_joining', 'Date Of Joining', 'required' );
        $this->form_validation->set_rules( 'salary', 'Salary', 'required|numeric' );
        if( !$id ) {
            
            $this->form_validation->set_rules( 'mobile_no', 'Mobile Number', 'required|numeric|is_unique[employee.mobile]' );
            $this->form_validation->set_rules( 'email', 'Email', 'required|valid_email|is_unique[employee.email]' );
            if( isset( $is_login_credential ) && !empty( $is_login_credential ) ) {
                $this->form_validation->set_rules( 'user_name', 'User Name', 'required|is_unique[users.fname]' );
                $this->form_validation->set_rules( 'user_password', 'User Password', 'required' );
                $this->form_validation->set_rules( 'user_type', 'User Type', 'required' );
            }
            if( ( isset( $bank ) && !empty( $bank ) ) || ( isset( $ac_no ) && !empty( $ac_no ) ) ) {
                $this->form_validation->set_rules( 'ac_no', 'Bank Account No', 'required|numeric|is_unique[company.bank_ac_no]' );
                $this->form_validation->set_rules( 'bank', 'Bank', 'required' );
                $this->form_validation->set_rules( 'bank_branch', 'Bank Branch', 'required' );
                $this->form_validation->set_rules( 'ifsc', 'Bank IFSC', 'required' );
            }
        } else {
            
            if( $this->input->post( 'mobile_no' ) != $info->mobile ) {
                $this->form_validation->set_rules( 'mobile_no', 'Mobile Number', 'required|numeric|is_unique[employee.mobile]' );
            } else {
                $this->form_validation->set_rules( 'mobile_no', 'Mobile Number', 'required|numeric' );
            }
            if( $this->input->post( 'email' ) != $info->email ) {
                $this->form_validation->set_rules( 'email', 'Email', 'required|valid_email|is_unique[employee.email]' );
            } else {
                 $this->form_validation->set_rules( 'email', 'Email', 'required|valid_email' );
            }
            if( isset( $is_login_credential ) && !empty( $is_login_credential ) ) {
                if( $this->input->post( 'user_name' ) != $info->user_name ) {
                    $this->form_validation->set_rules( 'user_name', 'User Name', 'required|is_unique[users.fname]' );
                } else {
                    $this->form_validation->set_rules( 'user_name', 'User Name', 'required' );
                }
                $this->form_validation->set_rules( 'user_type', 'User Type', 'required' );
            }
            if( ( isset( $bank ) && !empty( $bank ) ) || ( isset( $ac_no ) && !empty( $ac_no ) ) ) {
                if( $this->input->post( 'ac_no' ) != $info->bank_ac_no ) {
                    $this->form_validation->set_rules( 'ac_no', 'Bank Account No', 'required|numeric|is_unique[company.bank_ac_no]' );
                }
                $this->form_validation->set_rules( 'bank', 'Bank', 'required' );
                $this->form_validation->set_rules( 'bank_branch', 'Bank Branch', 'required' );
                $this->form_validation->set_rules( 'ifsc', 'Bank IFSC', 'required' );
            }
        }
        
        

        if ( $this->form_validation->run() == FALSE )
        {
            $error                              = 1;
            $error_msg                          = validation_errors();
        } else {
            $profile_image_path                 = null;
            if( isset($_FILES['profile_image']['name']) && !empty($_FILES['profile_image']['name']) ) {

                $file                           = 'profile_image'; 
                $temp_file                      = explode('.', $_FILES["$file"]['name']);  
                $config['upload_path']          = 'assets/images/employee';
                $config['allowed_types']        = '*';
                $config['max_size']             = 500000;
                $filetype                       = $_FILES[ "$file" ][ 'type' ];
                $expfiletype                    = explode( '.', $_FILES[ "$file" ][ 'name' ] );  
                $config['file_name']            = strtolower($temp_file[0]).'-'.rand( 101, 999 ) . '.' . $expfiletype[ 1 ];
                $this->upload->initialize( $config );
                $this->load->library( 'upload', $config );
                if ( $this->upload->do_upload( $file ) ) {  
                    $upload_data                = $this->upload->data(); 
                    $filename                   = $upload_data[ 'file_name' ];  
                    $profile_image_path         = base_url().'assets/images/employee/'.$filename;
                    $filepath                   = $upload_data[ 'full_path' ];
                } else {
                   $error_msg                   = 'Image upload failed!';
                   $error                       = 1;
                }
            }

            $proof_path                         = null;
            if( isset($_FILES['proof']['name']) && !empty($_FILES['proof']['name']) ) {

                $file                           = 'proof'; 
                $temp_file                      = explode('.', $_FILES["$file"]['name']);  
                $config['upload_path']          = 'assets/images/employee';
                $config['allowed_types']        = '*';
                $config['max_size']             = 500000;
                $filetype                       = $_FILES[ "$file" ][ 'type' ];
                $expfiletype                    = explode( '.', $_FILES[ "$file" ][ 'name' ] );  
                $config['file_name']            = strtolower($temp_file[0]).'-'.rand( 101, 999 ) . '.' . $expfiletype[ 1 ];
                $this->upload->initialize( $config );
                $this->load->library( 'upload', $config );
                if ( $this->upload->do_upload( $file ) ) {  
                    $upload_data                = $this->upload->data(); 
                    $filename                   = $upload_data[ 'file_name' ];  
                    $proof_path                 = base_url().'assets/images/employee/'.$filename;
                    $filepath                   = $upload_data[ 'full_path' ];
                } else {
                   $error_msg                   = 'Image upload failed!';
                   $error                       = 1;
                }
            }

            $resume_path                        = null;
            if( isset($_FILES['resume']['name']) && !empty($_FILES['resume']['name']) ) {

                $file                           = 'resume'; 
                $temp_file                      = explode('.', $_FILES["$file"]['name']);  
                $config['upload_path']          = 'assets/images/employee';
                $config['allowed_types']        = '*';
                $config['max_size']             = 500000;
                $filetype                       = $_FILES[ "$file" ][ 'type' ];
                $expfiletype                    = explode( '.', $_FILES[ "$file" ][ 'name' ] );  
                $config['file_name']            = strtolower($temp_file[0]).'-'.rand( 101, 999 ) . '.' . $expfiletype[ 1 ];
                $this->upload->initialize( $config );
                $this->load->library( 'upload', $config );
                if ( $this->upload->do_upload( $file ) ) {  
                    $upload_data                = $this->upload->data(); 
                    $filename                   = $upload_data[ 'file_name' ];  
                    $resume_path                = base_url().'assets/images/employee/'.$filename;
                    $filepath                   = $upload_data[ 'full_path' ];
                } else {
                   $error_msg                   = 'Image upload failed!';
                   $error                       = 1;
                }
            }
            
            $bank_branch                        = $this->input->post( 'bank_branch', true );
            $ifsc                               = $this->input->post( 'ifsc', true );
            $branch_exists                      = $this->Bank_model->check_bank_branch_exists( $bank, $ifsc );
            if( ! $branch_exists ) {
                $ins_branch[ 'parent_id' ]      = $bank;
                $ins_branch[ 'branch' ]         = $bank_branch;
                $ins_branch[ 'ifsc' ]           = $ifsc;
                $ins_branch[ 'created_by' ]     = $this->session->userdata( 'user_id' );
                $ins_branch[ 'slug' ]           = url_title( $bank.' '.$bank_branch, 'dash', true );
                $bank_id                        = $this->Common_model->insert_table( 'bank', $ins_branch ); 
            } else {
                $bank_id                        = $branch_exists->id;
            }
            $name                               = $this->input->post( 'first_name',  true ).' '. $this->input->post( 'last_name', true );
            $dob                                = $this->input->post( 'dob', true );
            $date_of_joining                    = $this->input->post( 'date_of_joining', true );

            $ins[ 'first_name' ]                = $this->input->post( 'first_name',  true );
            $ins[ 'last_name' ]                 = $this->input->post( 'last_name', true );
            $ins[ 'dob' ]                       = date( 'Y-m-d', strtotime( $dob ) );
            $ins[ 'gender' ]                    = $this->input->post( 'gender', true );
            $ins[ 'email' ]                     = $this->input->post( 'email', true );
            $ins[ 'mobile' ]                    = $this->input->post( 'mobile_no', true );
            $ins[ 'date_of_joining' ]           = date( 'Y-m-d', strtotime( $date_of_joining ) );
            $ins[ 'salary_per_month' ]          = $this->input->post( 'salary', true );
            $ins[ 'benefits' ]                  = $this->input->post( 'benefits', true );
            $ins[ 'bank_ac_no' ]                = $this->input->post( 'ac_no', true );
            $ins[ 'bank_id' ]                   = $bank_id;

            if( isset( $profile_image_path ) && !empty( $profile_image_path ) ) {
                $ins[ 'profile_photo' ]         = $profile_image_path;    
            }
            if( isset( $resume_path ) && !empty( $resume_path ) ) {
                $ins[ 'resume' ]                = $resume_path;    
            }
            if( isset( $proof_path ) && !empty( $proof_path ) ) {
                $ins[ 'proof_image' ]           = $proof_path;    
            }

            $ins[ 'address' ]                   = $this->input->post( 'address', true );
            $ins[ 'city' ]                      = $this->input->post( 'city', true );
            $ins[ 'state' ]                     = $this->input->post( 'state', true );
            $ins[ 'country' ]                   = $this->input->post( 'country', true );
            $ins[ 'zip_code' ]                  = $this->input->post( 'zipcode', true );
            $ins[ 'slug' ]                      = base64_encode( $this->input->post( 'mobile_no', true ) );

            if( isset( $id ) && !empty( $id ) ) {
                $where[ 'id' ]                  = $id;
                $this->Common_model->update_table( 'employee', $where, $ins );
                $error_msg                      = 'Employee Updated Successfully'; 
                $employee_id                    = $id;

                if( isset( $is_login_credential ) && !empty( $is_login_credential ) ) {
                    // $password                   = $this->encryption->encrypt( $this->input->post( 'user_password', true ) );
                    $ins_user[ 'fname']         = $this->input->post( 'user_name', true );
                    $ins_user[ 'email' ]        = $this->input->post( 'email', true );
                    $ins_user[ 'mobile_no' ]    = $this->input->post( 'mobile_no', true );
                    // $ins_user[ 'password' ]     = $password;
                    $ins_user[ 'user_type_id' ] = $this->input->post( 'user_type', true );
                    $where_user[ 'employee_id' ]= $id;
                    $user_id                    = $this->Common_model->update_table( 'users', $where_user, $ins_user );
                }
            } else {
                $ins[ 'created_by' ]            = $this->session->userdata( 'user_id' );
                $ins[ 'created_at' ]            = date( 'Y-m-d H:i:s' );
                $ins[ 'owener_id' ]             = $this->session->userdata( 'company_id' );
                
                $employee_id                    = $this->Common_model->insert_table( 'employee', $ins );
                $error_msg                      = 'Employee Added Successfully'; 

                if( isset( $is_login_credential ) && !empty( $is_login_credential ) ) {
                    $password                   = $this->encryption->encrypt( $this->input->post( 'user_password', true ) );
                    $ins_user[ 'fname']         = $this->input->post( 'user_name', true );
                    $ins_user[ 'email' ]        = $this->input->post( 'email', true );
                    $ins_user[ 'mobile_no' ]    = $this->input->post( 'mobile_no', true );
                    $ins_user[ 'password' ]     = $password;
                    $ins_user[ 'employee_id' ]  = $employee_id;
                    $ins_user[ 'user_type_id' ] = $this->input->post( 'user_type', true );
                    $ins_user[ 'created_at' ]   = date( 'Y-m-d H:i:s' );
                    $user_id                    = $this->Common_model->insert_table( 'users', $ins_user );
                }
            }
            

            
            
            $error                              = 0;
        }
        $data[ 'error' ]                        = $error;
        $data[ 'error_msg' ]                    = $error_msg;
        echo json_encode( $data );
    }

    function delete_employee() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $id                         = $this->input->post( 'id', true );
        $where[ 'id' ]              = $id;
        $table                      = 'employee';
        $data[ 'status' ]           = 'INACTIVE';
        $this->Common_model->update_table( $table, $where, $data );
        
        echo 1;
        return true;
    }


    function template( $data ) {
        echo $this->load->view( 'layout/template', $data, true );
        return true;
    }

}

?>