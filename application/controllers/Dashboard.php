
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
    function __construct() {
       parent::__construct();
    }

    public function index() {
        
        $content                            = $this->load->view( 'dashboard/dash_index', '', true );
        $data[ 'content' ]                  = $content;
        $this->template( $data );
        return true;
    }

    public function upcoming_expiry() {
        $content                            = $this->load->view( 'dashboard/upcoming_expiry', '', true );
        $data[ 'content' ]                  = $content;
        $this->template( $data );
        return true;
    }

    public function creditnote_pending() {
        $content                            = $this->load->view( 'dashboard/creditnote_pending', '', true );
        $data[ 'content' ]                  = $content;
        $this->template( $data );
        return true;
    }

    public function purchase_due() {
        $content                            = $this->load->view( 'dashboard/purchase_due', '', true );
        $data[ 'content' ]                  = $content;
        $this->template( $data );
        return true;
    }

    public function sales_due() {
        $content                            = $this->load->view( 'dashboard/sales_due', '', true );
        $data[ 'content' ]                  = $content;
        $this->template( $data );
        return true;
    }

    function template( $data ){
        echo $this->load->view( 'layout/template', $data, true );
        return true;
    }

}

?>