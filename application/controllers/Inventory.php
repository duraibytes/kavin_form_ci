
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory extends CI_Controller {
    function __construct() {
       parent::__construct();
       $this->load->model( 'Stock_model' );
       $this->load->helper( 'common_helper' );
    }

    public function index() {
    	$title                      = 'Stock';
        $anchor_btn                 = '';
        $breadcrum                  = '<li class="breadcrumb-item"> <a href="javascript>">STOCK </a> </li>';
        //get filter dropdown data
        $product_manufacturer       = get_product_columns( 'product_company', 'product' );
        $product_code               = get_product_columns( 'product_code', 'product' );
        $product_category           = get_product_columns( 'product_category', 'product' );
        $product_lot                = get_product_columns( 'lot_no', 'purchase_items' );
        $purchase_invoice           = get_product_columns( 'invoice_no', 'purchase' );           

        $stock                      = $this->Stock_model->purchase_stock_all();
        $params[ 'stock' ]          = $stock;
        $params[ 'title' ]          = $title;
        $params[ 'anchor_btn' ]     = $anchor_btn;
        $params[ 'breadcrum' ]      = $breadcrum;
        $params[ 'product_manufacturer' ]   = $product_manufacturer;
        $params[ 'product_code' ]           = $product_code;
        $params[ 'product_category' ]       = $product_category;
        $params[ 'product_lot' ]            = $product_lot;
        $params[ 'purchase_invoice' ]       = $purchase_invoice;
    	$content 					        = $this->load->view( 'pages/inventory/index', $params, true );
    	$data[ 'content' ]                  = $content;
        $this->template( $data );
        return true;
    }

    function set_filter_data() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }

        $product_company       = $this->input->post( 'product_company', true );
        $product_code          = $this->input->post( 'product_code', true );
        $product_category      = $this->input->post( 'product_category', true );
        $lot_no                = $this->input->post( 'lot_no', true );
        $invoice_no            = $this->input->post( 'invoice_no', true );
        
        if( trim($product_company) ) {
            $arr[ 'product_company' ]    = $product_company;
        } 
        if( trim($product_code) ) {
            $arr[ 'product_code' ]    = $product_code;
        } 
        if( trim($product_category) ) {
            $arr[ 'product_category' ]    = $product_category;
        } 
        if( trim($lot_no) ) {
            $arr[ 'lot_no' ]    = $lot_no;
        } 
        if( trim($invoice_no) ) {
            $arr[ 'invoice_no' ]    = $invoice_no;
        } 

        
        // $arr[ 'sale_invoice_no' ]       = $this->input->post( 'sale_invoice_no', true );
        $this->session->set_userdata( 'stock_filter', $arr );

        echo json_encode( array( 'resutl' => true ) );
        return true;
        
    }

    function clear_stock_filter() {
        if( !$this->input->is_ajax_request() ) {
            echo 'No Direct Access Denied';
            return false;
            exit();
        }
        $this->session->unset_userdata( 'stock_filter' );
        return false;

    }

    function template( $data ) {
        
        echo $this->load->view( 'layout/template', $data, true );
        return true;
    }

}

?>