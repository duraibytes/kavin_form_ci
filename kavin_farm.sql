-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 10, 2021 at 03:16 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kavin_farm`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `id` int(11) NOT NULL,
  `address_type_id` int(11) DEFAULT NULL,
  `address_type` varchar(122) NOT NULL COMMENT 'godown, home, resident',
  `address` text NOT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `zip_code` varchar(12) DEFAULT NULL,
  `country` varchar(122) DEFAULT NULL,
  `country_id` smallint(11) UNSIGNED DEFAULT NULL,
  `email` varchar(55) DEFAULT NULL,
  `mobile_no` varchar(15) DEFAULT NULL,
  `mode_type` varchar(22) NOT NULL COMMENT 'customer, vendor',
  `mode_id` int(11) NOT NULL,
  `is_primary` varchar(5) DEFAULT 'no' COMMENT 'yes,no',
  `status` enum('ACTIVE','INACTIVE','','') NOT NULL DEFAULT 'ACTIVE',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`id`, `address_type_id`, `address_type`, `address`, `city`, `state`, `state_id`, `zip_code`, `country`, `country_id`, `email`, `mobile_no`, `mode_type`, `mode_id`, `is_primary`, `status`, `created_at`, `updated_at`) VALUES
(3, 12, 'Registered Office Address', 'Verti', 'chei', 'Tamil nau', NULL, '893493', 'india', NULL, NULL, NULL, 'company', 2, 'no', 'ACTIVE', '2021-05-14 17:26:52', '2021-05-14 15:26:52'),
(18, 12, 'Registered Office Address', 'stets', 'ee', 'teste', NULL, 'eteteee', 'ere', NULL, 'test@mai.com', '87654334567', 'company', 1, 'no', 'ACTIVE', '2021-05-17 08:33:13', '2021-05-17 06:33:13'),
(25, 16, 'Godown Address', 'bb road', 'ch', 'tn', NULL, '987654', 'jj', NULL, '', '', 'customer', 1, 'no', 'ACTIVE', '2021-05-17 12:49:50', '2021-05-17 10:49:50'),
(26, 14, 'Shop Point Address', 'Boax boxx', 'fhennai', 'anna ', NULL, '389389', 'india', NULL, '', '', 'customer', 1, 'no', 'ACTIVE', '2021-05-17 12:49:50', '2021-05-17 10:49:50'),
(27, 15, 'Sales Point Address', 'Vertinary addres', 'chennai', ' tmi', NULL, '876543', 'india', NULL, '', '', 'customer', 1, 'no', 'ACTIVE', '2021-05-17 12:49:51', '2021-05-17 10:49:51'),
(28, 15, 'Sales Point Address', 'Anna nager roundana', 'chenna', 'tamil nadu', NULL, '600022', 'india', NULL, '', '', 'customer', 3, 'no', 'ACTIVE', '2021-05-17 13:22:31', '2021-05-17 11:22:31'),
(37, 15, 'Sales Point Address', 'Anna nager  Tower 3rd main road', 'chenna', 'tamil nadu', NULL, '600022', 'india', NULL, '', '', 'customer', 4, 'no', 'ACTIVE', '2021-05-17 14:44:04', '2021-05-17 12:44:04'),
(38, 16, 'Godown Address', 'velik', 'kdkjf', 'kjlkj', NULL, 'kjlk', 'lj', NULL, 'testes@mail.com', '8765432', 'customer', 4, 'no', 'ACTIVE', '2021-05-17 14:44:05', '2021-05-17 12:44:05'),
(39, 12, 'Registered Office Address', 'MEDAVAKAM tank road, VEruthnaagar', 'chennai', 'tamil nadu', NULL, '600021', 'india', NULL, 'zeb@test.om', '8765434567', 'company', 3, 'no', 'ACTIVE', '2021-06-07 15:11:17', '2021-06-07 13:11:17'),
(40, 13, 'Marketing Office Addres', 'Vinnwset', 'Anna nagr', 's', NULL, 's', 's', NULL, '', '', 'company', 3, 'no', 'ACTIVE', '2021-06-07 15:11:17', '2021-06-07 13:11:17'),
(41, 12, 'Registered Office Address', 'Rest home adrdr', 'chennai', 'TAMIL NADU', 21, '60002', 'INDIA', 99, '', '', 'company', 4, 'no', 'ACTIVE', '2021-06-07 16:09:55', '2021-06-07 14:09:55');

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `bank_name` varchar(122) DEFAULT NULL,
  `branch` varchar(122) DEFAULT NULL,
  `ifsc` varchar(15) DEFAULT NULL,
  `micr_code` varchar(25) DEFAULT NULL,
  `email` varchar(24) DEFAULT NULL,
  `phone` bigint(44) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE','','') NOT NULL DEFAULT 'ACTIVE',
  `slug` varchar(122) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`id`, `parent_id`, `bank_name`, `branch`, `ifsc`, `micr_code`, `email`, `phone`, `address`, `status`, `slug`, `created_by`, `created_at`, `updated_at`) VALUES
(4, NULL, 'Axis Bank', NULL, NULL, NULL, 'Axismain@bank.com', 2345678, 'chennai', 'ACTIVE', 'axis-bank', 1, '2021-05-07 22:44:03', '2021-05-13 11:07:22'),
(10, 4, NULL, 'Aron', 'KJJ778', '2345678998', 'ARon@jfld.com', 1234567890, 'durari tesetigsng', 'ACTIVE', 'aron-axis-bank', 1, '2021-05-08 00:37:57', '2021-05-07 22:39:38'),
(11, 4, NULL, 'Vetri of india', '09ASDFG678', '456789856780', 'Chdjd@gkf.com', 6473829047, 'Testing of india in culture', 'ACTIVE', 'vetri-of-india-axis-bank', 1, '2021-05-08 00:40:18', '2021-05-07 22:40:38'),
(12, NULL, 'Canara', NULL, NULL, NULL, 'canara@bank.com', 445623120, 'Chennai Trusting', 'ACTIVE', 'canara', 1, '2021-05-13 13:12:05', '2021-05-13 11:15:40'),
(13, NULL, 'Indian', NULL, NULL, NULL, 'ind@ing.com', 12345678998, 'testet', 'ACTIVE', 'indian', 1, '2021-05-13 13:14:53', '2021-05-13 11:14:53'),
(14, 12, NULL, 'Anna nagara', 'CNR90989', '', 'jlkjdfs@gmail.com', 9876676534, 'TESTING OF INDIA', 'ACTIVE', 'anna-nagara-canara', 1, '2021-05-13 13:34:55', '2021-05-13 11:34:55'),
(15, NULL, 'City', NULL, NULL, NULL, NULL, NULL, NULL, 'ACTIVE', NULL, NULL, '2021-05-14 10:27:11', '2021-05-14 08:27:11'),
(16, NULL, 'City Union Bank', NULL, NULL, NULL, NULL, NULL, NULL, 'ACTIVE', NULL, NULL, '2021-05-14 10:28:02', '2021-05-14 08:28:02'),
(18, NULL, 'Bank Of Baroda', NULL, NULL, NULL, NULL, NULL, NULL, 'ACTIVE', NULL, NULL, '2021-05-14 17:23:58', '2021-05-14 15:23:58'),
(19, 18, NULL, 'CMBT', 'BBOCMB9090', NULL, NULL, NULL, NULL, 'ACTIVE', NULL, NULL, '0000-00-00 00:00:00', '2021-05-14 15:26:52'),
(20, 4, NULL, 'Maduravoyal', 'AXIS9090', NULL, NULL, NULL, NULL, 'ACTIVE', NULL, 1, '0000-00-00 00:00:00', '2021-05-17 06:21:32'),
(21, NULL, 'ICICI BANK', NULL, NULL, NULL, NULL, NULL, NULL, 'ACTIVE', NULL, NULL, '2021-05-17 08:34:58', '2021-05-17 06:34:58'),
(22, 21, NULL, 'ANNA NAGGAR', 'ICIC09292929', NULL, NULL, NULL, NULL, 'ACTIVE', NULL, 1, '0000-00-00 00:00:00', '2021-05-17 06:36:03'),
(23, NULL, 'Andhra Bank', NULL, NULL, NULL, NULL, NULL, NULL, 'ACTIVE', NULL, NULL, '2021-05-17 12:16:58', '2021-05-17 10:16:58'),
(24, 13, NULL, 'Redhills', 'IDIB783748378', NULL, NULL, NULL, NULL, 'ACTIVE', NULL, 1, '0000-00-00 00:00:00', '2021-05-17 10:46:15'),
(25, 4, NULL, 'Redhills', 'AXIN9098989', NULL, NULL, NULL, NULL, 'ACTIVE', NULL, 1, '0000-00-00 00:00:00', '2021-05-17 10:47:55'),
(27, 0, NULL, '', '', NULL, NULL, NULL, NULL, 'ACTIVE', NULL, 1, '0000-00-00 00:00:00', '2021-05-17 11:19:13'),
(28, 12, NULL, 'Redhills', 'CNR90778', NULL, NULL, NULL, NULL, 'ACTIVE', NULL, 1, '0000-00-00 00:00:00', '2021-05-17 11:22:31'),
(29, 15, NULL, 'Ezhil Nagar', 'CITUE09034', NULL, NULL, NULL, NULL, 'ACTIVE', NULL, 1, '0000-00-00 00:00:00', '2021-05-17 12:00:04'),
(30, NULL, 'Alaibaab', NULL, NULL, NULL, NULL, NULL, NULL, 'ACTIVE', 'alaibaab', NULL, '2021-05-18 08:34:10', '2021-05-18 06:34:10'),
(31, 12, NULL, 'Canandian', 'CCd899989', NULL, NULL, NULL, NULL, 'ACTIVE', '12-canandian', 1, '0000-00-00 00:00:00', '2021-05-19 10:42:35'),
(32, 15, NULL, 'Redhills', 'IICIIC3493943', NULL, NULL, NULL, NULL, 'ACTIVE', '15-redhills', 1, '0000-00-00 00:00:00', '2021-05-19 10:44:07'),
(35, 4, NULL, 'Edapalayam', 'AXISEPL909090', NULL, NULL, NULL, NULL, 'ACTIVE', 'edapalayam-axis-bank', NULL, '2021-06-05 10:03:57', '2021-06-05 08:03:57'),
(36, 12, NULL, 'Ambur', 'CNRAMN09890', NULL, NULL, NULL, NULL, 'ACTIVE', 'ambur-canara', NULL, '2021-06-05 10:10:33', '2021-06-05 08:10:33'),
(37, 12, NULL, 'CMBT', 'CNRCMBT9009', NULL, NULL, NULL, NULL, 'ACTIVE', 'cmbt-canara', NULL, '2021-06-05 12:16:41', '2021-06-05 10:16:41'),
(43, 4, NULL, 'Redhills', 'IDDFIDIF99034', NULL, NULL, NULL, NULL, 'ACTIVE', '4-redhills', 1, '0000-00-00 00:00:00', '2021-06-07 14:09:55'),
(53, NULL, 'Arokiya Bank', NULL, NULL, NULL, NULL, NULL, NULL, 'ACTIVE', 'arokiya-bank', NULL, '2021-06-10 08:41:31', '2021-06-10 06:41:31'),
(54, 23, NULL, 'Ninest', 'ijks900', NULL, NULL, NULL, NULL, 'ACTIVE', '23-ninest', NULL, '2021-06-10 09:00:17', '2021-06-10 07:00:17'),
(55, NULL, 'Arumbu', NULL, NULL, NULL, NULL, NULL, NULL, 'ACTIVE', 'arumbu', NULL, '2021-06-10 09:03:54', '2021-06-10 07:03:54'),
(56, 23, NULL, 'Melboure', 'ANDFDKm90343', NULL, NULL, NULL, NULL, 'ACTIVE', '23-melboure', NULL, '2021-06-10 09:04:12', '2021-06-10 07:04:12');

-- --------------------------------------------------------

--
-- Table structure for table `codes`
--

CREATE TABLE `codes` (
  `id` int(11) NOT NULL,
  `name` varchar(122) NOT NULL,
  `decription` text DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE','','') NOT NULL DEFAULT 'ACTIVE',
  `slug` varchar(122) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `codes`
--

INSERT INTO `codes` (`id`, `name`, `decription`, `status`, `slug`, `created_at`, `updated_at`) VALUES
(5, 'USER TYPE', 'like admin, user, employee for giving permission', 'ACTIVE', 'user-type', '2021-05-08 04:14:59', '2021-05-08 02:14:59'),
(6, 'PAYMENT METHOD', 'like cash, cheque, dd, credit, etc..', 'ACTIVE', 'payment-method', '2021-05-08 04:16:26', '2021-05-08 02:16:26'),
(8, 'COMPANY ADDRESS TYPE', '', 'ACTIVE', 'company-address-type', '2021-05-14 07:53:42', '2021-05-14 05:53:42'),
(9, 'DEALER ADDRESS TYPE', '', 'ACTIVE', 'dealer-address-type', '2021-05-17 12:37:40', '2021-05-17 10:37:40'),
(10, 'PAYMENT TERM', 'LIKE NET30, NET40, ETC.', 'ACTIVE', 'payment-term', '2021-06-09 10:50:02', '2021-06-09 08:50:46');

-- --------------------------------------------------------

--
-- Table structure for table `code_options`
--

CREATE TABLE `code_options` (
  `id` int(11) NOT NULL,
  `code_id` int(12) NOT NULL,
  `name` varchar(122) NOT NULL,
  `decription` text DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE','','') NOT NULL DEFAULT 'ACTIVE',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `code_options`
--

INSERT INTO `code_options` (`id`, `code_id`, `name`, `decription`, `status`, `created_at`, `updated_at`) VALUES
(4, 5, 'ADMIN', 'Higher level permission', 'ACTIVE', '2021-05-08 04:15:24', '2021-05-08 02:15:24'),
(5, 5, 'USER', 'Lower level permission', 'ACTIVE', '2021-05-08 04:15:53', '2021-05-08 02:15:53'),
(6, 6, 'CASH', '', 'ACTIVE', '2021-05-08 04:16:52', '2021-05-08 02:16:52'),
(7, 6, 'CHEQUE', '', 'ACTIVE', '2021-05-08 04:16:59', '2021-05-08 02:16:59'),
(8, 6, 'DD', '', 'ACTIVE', '2021-05-08 04:17:04', '2021-05-08 02:17:04'),
(9, 6, 'IMPS', '', 'ACTIVE', '2021-05-08 04:17:16', '2021-05-08 02:17:16'),
(10, 6, 'RTGS', '', 'ACTIVE', '2021-05-08 04:17:32', '2021-05-08 02:17:32'),
(12, 8, 'Registered Office Address', '', 'ACTIVE', '2021-05-14 08:13:28', '2021-05-14 06:13:28'),
(13, 8, 'Marketing Office Addres', '', 'ACTIVE', '2021-05-14 08:13:43', '2021-05-14 06:13:43'),
(14, 9, 'Shop Point Address', 'shop point address', 'ACTIVE', '2021-05-17 12:38:08', '2021-05-17 10:38:08'),
(15, 9, 'Sales Point Address', 'sales point address', 'ACTIVE', '2021-05-17 12:38:39', '2021-05-17 10:38:39'),
(16, 9, 'Godown Address', 'Address', 'ACTIVE', '2021-05-17 12:38:59', '2021-05-17 10:38:59'),
(17, 10, 'NET30', '', 'ACTIVE', '2021-06-09 10:50:19', '2021-06-09 08:50:19'),
(18, 10, 'NET45', '', 'ACTIVE', '2021-06-09 10:50:26', '2021-06-09 08:50:26'),
(22, 10, 'NET 60', NULL, 'ACTIVE', '2021-06-09 12:34:21', '2021-06-09 10:34:21'),
(23, 10, 'ADVANCE', 'Advance amount', 'ACTIVE', '2021-06-09 13:09:43', '2021-06-09 11:09:43');

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `id` int(11) NOT NULL,
  `company_name` varchar(122) NOT NULL,
  `company_logo` text DEFAULT NULL,
  `email` varchar(122) NOT NULL,
  `mobile_no` varchar(12) DEFAULT NULL,
  `phone_no` varchar(12) DEFAULT NULL,
  `fax` varchar(15) DEFAULT NULL,
  `seed_license` varchar(255) DEFAULT NULL,
  `pesticides_license` varchar(255) DEFAULT NULL,
  `fertilizer_license` varchar(200) DEFAULT NULL,
  `gst` varchar(122) DEFAULT NULL,
  `cst` varchar(122) DEFAULT NULL,
  `vat` varchar(122) DEFAULT NULL,
  `bank_ac_no` varchar(122) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `gstin_no` varchar(122) DEFAULT NULL,
  `pan_no` varchar(122) DEFAULT NULL,
  `regional_manager_name` varchar(122) DEFAULT NULL,
  `regional_manager_mobile` varchar(122) DEFAULT NULL,
  `regional_manager_email` varchar(122) DEFAULT NULL,
  `area_manager_name` varchar(122) DEFAULT NULL,
  `area_manager_mobile` varchar(122) DEFAULT NULL,
  `area_manager_email` varchar(122) DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE','','') NOT NULL DEFAULT 'ACTIVE',
  `created_at` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `slug` varchar(122) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `company_name`, `company_logo`, `email`, `mobile_no`, `phone_no`, `fax`, `seed_license`, `pesticides_license`, `fertilizer_license`, `gst`, `cst`, `vat`, `bank_ac_no`, `bank_id`, `gstin_no`, `pan_no`, `regional_manager_name`, `regional_manager_mobile`, `regional_manager_email`, `area_manager_name`, `area_manager_mobile`, `area_manager_email`, `status`, `created_at`, `created_by`, `updated_at`, `slug`, `owner_id`) VALUES
(1, 'Lenova', 'http://localhost/kavinfarms/assets/images/company/bill-116.png', '', NULL, NULL, NULL, '3434098', 'LOP090polddf', '', '', '4', '3', '8765432112', 20, 'GSIT898989893', NULL, 'Suresh', '234567890', 'suresh@lenov.in', 'testet', '9551706025', 'teste@mail.com', 'ACTIVE', '2021-05-14 17:20:23', NULL, '2021-05-17 06:21:32', 'lenova', NULL),
(2, 'Shivran', 'http://localhost/kavinfarms/assets/images/companytulips-866.jpg', '', NULL, NULL, NULL, '', '45454545453', '', '', '', '', '098765456789', 19, NULL, NULL, '', '', '', 'Arun', '89393839883', 'arun@shivaran.com', 'ACTIVE', '2021-05-14 17:26:52', NULL, '2021-05-17 06:33:44', 'shivran', NULL),
(3, 'Zebroninc', 'http://localhost/kavinfarms/assets/images/company/payment-321.png', '', NULL, NULL, NULL, 'SGGGL;984568', '', '', '8', '8', '10', '96201230252', 22, 'zin9900987997', 'YLD89LDS', 'Arun', '9551706025', 'arun@gmal.com', 'ganapathi', '9551706026', 'get@gami.comc', 'ACTIVE', '2021-05-17 08:36:03', 1, '2021-06-07 13:11:17', 'zebroninc', NULL),
(4, 'BenzMark', 'http://localhost/kavinfarms/assets/images/company/img-20200808-wa0007-507.jpg', '', NULL, NULL, NULL, 'kllll909900', '', '', '', '', '', '890876654445555', 43, '098765234567', 'JKDJ89909', 'kUMAR', '98765432112', '', '', '', '', 'ACTIVE', '2021-06-07 16:09:55', 1, '2021-06-07 14:09:55', 'benzmark', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `company_poc`
--

CREATE TABLE `company_poc` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `type` varchar(122) DEFAULT NULL COMMENT 'company - C, dealer - D',
  `place` varchar(255) DEFAULT NULL,
  `position` varchar(122) DEFAULT NULL,
  `name` varchar(122) NOT NULL,
  `mobile` varchar(15) DEFAULT NULL,
  `email` varchar(122) DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE','','') NOT NULL DEFAULT 'ACTIVE',
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, 93),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, 355),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, 213),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, 1684),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, 376),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, 244),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, 1264),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, 0),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, 1268),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, 54),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, 374),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, 297),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, 61),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, 43),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, 994),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, 1242),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, 973),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, 880),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, 1246),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, 375),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, 32),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, 501),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, 229),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, 1441),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, 975),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, 591),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, 387),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, 267),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, 0),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, 55),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, 246),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, 673),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, 359),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, 226),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, 257),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, 855),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, 237),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, 1),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, 238),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, 1345),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, 236),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, 235),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, 56),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, 86),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, 61),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, 672),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, 57),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, 269),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, 242),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, 242),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, 682),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, 506),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384, 225),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, 385),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, 53),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, 357),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, 420),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, 45),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, 253),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, 1767),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, 1809),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, 593),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, 20),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, 503),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, 240),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, 291),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, 372),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, 251),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, 500),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, 298),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, 679),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, 358),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, 33),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, 594),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, 689),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, 0),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, 241),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, 220),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, 995),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, 49),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, 233),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, 350),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, 30),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, 299),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, 1473),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, 590),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, 1671),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, 502),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, 224),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, 245),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, 592),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, 509),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, 0),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, 39),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, 504),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, 852),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, 36),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, 354),
(99, 'IN', 'INDIA', 'India', 'IND', 356, 91),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, 62),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, 98),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, 964),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, 353),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, 972),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, 39),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, 1876),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, 81),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, 962),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, 7),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, 254),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, 686),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408, 850),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, 82),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, 965),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, 996),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418, 856),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, 371),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, 961),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, 266),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, 231),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, 218),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, 423),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, 370),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, 352),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, 853),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, 389),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, 261),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, 265),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, 60),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, 960),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, 223),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, 356),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, 692),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, 596),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, 222),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, 230),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, 269),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, 52),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, 691),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, 373),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, 377),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, 976),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, 1664),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, 212),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, 258),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, 95),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, 264),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, 674),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, 977),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, 31),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, 599),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, 687),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, 64),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, 505),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, 227),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, 234),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, 683),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, 672),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, 1670),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, 47),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, 968),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, 92),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, 680),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, 970),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, 507),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, 675),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, 595),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, 51),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, 63),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, 0),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, 48),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, 351),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, 1787),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, 974),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, 262),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, 40),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, 70),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, 250),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, 290),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, 1869),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, 1758),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, 508),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, 1784),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, 684),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, 378),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, 239),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, 966),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, 221),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, 381),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, 248),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, 232),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, 65),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, 421),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, 386),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, 677),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, 252),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, 27),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, 0),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, 34),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, 94),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, 249),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, 597),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, 47),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, 268),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, 46),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, 41),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, 963),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, 886),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, 992),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, 255),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, 66),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, 670),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, 228),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, 690),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, 676),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, 1868),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, 216),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, 90),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, 7370),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, 1649),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, 688),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, 256),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, 380),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, 971),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, 44),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, 1),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, 1),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, 598),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, 998),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, 678),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, 58),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, 84),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, 1284),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, 1340),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, 681),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, 212),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, 967),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, 260),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, 263);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `fname` varchar(122) NOT NULL,
  `lname` varchar(122) DEFAULT NULL,
  `email` varchar(122) DEFAULT NULL,
  `image` text DEFAULT NULL,
  `mobile_no` varchar(15) DEFAULT NULL,
  `alter_mobile_no` varchar(122) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `state` varchar(122) DEFAULT NULL,
  `zipcode` varchar(122) DEFAULT NULL,
  `type` varchar(15) DEFAULT NULL COMMENT 'dealer, farmer, customer',
  `proprietor_name` varchar(122) DEFAULT NULL,
  `proprietor_mobile` varchar(122) DEFAULT NULL,
  `proprietor_email` varchar(122) DEFAULT NULL,
  `seed_license` varchar(122) DEFAULT NULL,
  `pesticides_license` varchar(122) DEFAULT NULL,
  `fertilizer_license` varchar(122) DEFAULT NULL,
  `gst` varchar(55) DEFAULT NULL,
  `cst` varchar(12) DEFAULT NULL,
  `vat` varchar(22) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `bank_ac_no` varchar(122) DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE','','') NOT NULL DEFAULT 'ACTIVE',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `slug` varchar(122) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `fname`, `lname`, `email`, `image`, `mobile_no`, `alter_mobile_no`, `address`, `state`, `zipcode`, `type`, `proprietor_name`, `proprietor_mobile`, `proprietor_email`, `seed_license`, `pesticides_license`, `fertilizer_license`, `gst`, `cst`, `vat`, `bank_id`, `bank_ac_no`, `status`, `created_at`, `updated_at`, `slug`, `owner_id`) VALUES
(2, 'durairaj', NULL, '', NULL, '987654321', '3456789567', 'testet', '', '600001', 'farmer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ACTIVE', '2021-05-17 13:19:13', '2021-05-17 11:19:13', 'durairaj', NULL),
(3, 'Kannan', NULL, NULL, 'http://localhost/kavinfarms/assets/images/customer/purchase-return-657.png', NULL, NULL, NULL, NULL, NULL, 'dealer', 'Kraupan', '3434937497', '', '', '', '', '8', '8', '12', 28, '98765432234567', 'ACTIVE', '2021-05-17 13:22:31', '2021-05-17 11:22:31', 'kannan', 1),
(4, 'Janaki', NULL, NULL, 'http://localhost/kavinfarms/assets/images/customer/bill-144.png', NULL, NULL, NULL, NULL, NULL, 'dealer', 'amuthan', '9000090099', 'auha@mail.com', '', '', '', '12', '12', '2', 29, '567895678', 'ACTIVE', '2021-05-17 14:00:05', '2021-05-17 12:44:04', 'janaki', 1);

-- --------------------------------------------------------

--
-- Table structure for table `deposit_withdraw`
--

CREATE TABLE `deposit_withdraw` (
  `id` int(11) NOT NULL,
  `payment_type` varchar(22) NOT NULL COMMENT 'deposit or withdraw',
  `payment_method` varchar(55) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `bank_ifsc` varchar(12) DEFAULT NULL,
  `bank_ac_no` varchar(122) DEFAULT NULL,
  `cheque_no` bigint(20) DEFAULT NULL COMMENT 'cheque/dd/ imps/neft no',
  `cheque_issued_date` date DEFAULT NULL,
  `cheque_issued_amount` float DEFAULT NULL,
  `total_amount` decimal(12,3) NOT NULL,
  `deposited_by` varchar(55) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  `against_company_id` int(11) DEFAULT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'ACTIVE',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `first_name` varchar(55) NOT NULL,
  `last_name` varchar(55) DEFAULT NULL,
  `dob` date NOT NULL,
  `gender` varchar(15) NOT NULL,
  `email` varchar(55) DEFAULT NULL,
  `mobile` varchar(55) NOT NULL,
  `date_of_joining` date DEFAULT NULL,
  `salary_per_month` float(12,2) DEFAULT NULL,
  `benefits` longtext DEFAULT NULL,
  `bank_ac_no` varchar(55) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `resume` text DEFAULT NULL,
  `profile_photo` text DEFAULT NULL,
  `proof_id` varchar(122) DEFAULT NULL,
  `proof_image` text DEFAULT NULL,
  `address` text DEFAULT NULL,
  `city` varchar(55) DEFAULT NULL,
  `state` varchar(55) DEFAULT NULL,
  `country` varchar(111) DEFAULT NULL,
  `zip_code` varchar(12) DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE','','') NOT NULL DEFAULT 'ACTIVE',
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `slug` text DEFAULT NULL,
  `owener_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `first_name`, `last_name`, `dob`, `gender`, `email`, `mobile`, `date_of_joining`, `salary_per_month`, `benefits`, `bank_ac_no`, `bank_id`, `resume`, `profile_photo`, `proof_id`, `proof_image`, `address`, `city`, `state`, `country`, `zip_code`, `status`, `created_by`, `created_at`, `updated_at`, `slug`, `owener_id`) VALUES
(1, 'Durai', 'Raj', '2020-12-28', 'male', 'duraibytes@gmail.com', '9551706025', '2021-05-05', 12000.00, 'teset', '87623456', 31, 'http://localhost/kavinfarms/assets/images/employee/today_cahnge-259.docx', 'http://localhost/kavinfarms/assets/images/employee/chrysanthemum-749.jpg', NULL, 'http://localhost/kavinfarms/assets/images/employee/koala-630.jpg', 'chennai of indida anna ngar', 'chenna', 'tamil nadu', 'india', '600012', 'ACTIVE', 1, '2021-05-19 12:42:35', '2021-05-19 12:17:52', 'OTU1MTcwNjAyNQ==', 1),
(2, 'VArun', 'kumr', '1970-01-01', 'male', 'teste@talks.com', '8765432234567', '1970-01-01', 452000.00, '', '7654323456789', 32, NULL, 'http://localhost/kavinfarms/assets/images/employee/lighthouse-638.jpg', NULL, NULL, '', '', '', '', '', 'INACTIVE', 1, '2021-05-19 12:44:07', '2021-05-19 11:07:20', 'ODc2NTQzMjIzNDU2Nw==', 1),
(3, 'Magiszh', 'mathi', '1970-01-01', 'female', 'test@ss.com', '3434342222', '1970-01-01', 25000.00, '', '', 27, NULL, NULL, NULL, 'http://localhost/kavinfarms/assets/images/employee/tulips-607.jpg', 'Velitim', 'chenni', 'taml', 'dfdfdsfsf', ' 3439843897', 'ACTIVE', 1, '2021-05-19 12:45:15', '2021-05-19 10:45:15', 'MzQzNDM0MjIyMg==', 1),
(4, 'kdidfdl', 'dflkd', '2021-04-25', 'female', 'durairaj.s@breaktalks.ins', '3434343', '2021-05-05', 678888.00, '', '', 27, NULL, NULL, NULL, NULL, '', '', '', '', '', 'ACTIVE', 1, '2021-05-19 13:16:13', '2021-05-19 11:16:13', 'MzQzNDM0Mw==', 1);

-- --------------------------------------------------------

--
-- Table structure for table `employee_bank_info`
--

CREATE TABLE `employee_bank_info` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `opening_balance` float(15,2) DEFAULT NULL,
  `cheque_range` varchar(15) DEFAULT NULL,
  `is_primary` varchar(5) DEFAULT NULL,
  `status` varchar(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `employee_document`
--

CREATE TABLE `employee_document` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `document_name` varchar(55) NOT NULL,
  `document_number` varchar(55) DEFAULT NULL,
  `document_image` text DEFAULT NULL,
  `status` varchar(22) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `expense`
--

CREATE TABLE `expense` (
  `id` int(11) NOT NULL,
  `code_id` int(11) DEFAULT NULL,
  `expense_name` varchar(55) NOT NULL,
  `description` text DEFAULT NULL,
  `amount` float(12,2) NOT NULL DEFAULT 0.00,
  `payment_method` varchar(14) DEFAULT NULL,
  `payment_method_id` int(11) DEFAULT NULL,
  `payment_status` varchar(55) DEFAULT NULL,
  `status` varchar(44) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `slug` varchar(122) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `expense`
--

INSERT INTO `expense` (`id`, `code_id`, `expense_name`, `description`, `amount`, `payment_method`, `payment_method_id`, `payment_status`, `status`, `created_by`, `created_at`, `updated_at`, `slug`, `owner_id`) VALUES
(1, NULL, 'test', 'derersr', 520.00, NULL, 6, NULL, 'ACTIVE', 1, '2021-06-02 06:32:20', '2021-06-02 04:32:20', NULL, NULL),
(2, NULL, 'Tea', 'verere', 320.00, NULL, 8, NULL, 'ACTIVE', 1, '2021-06-02 06:38:55', '2021-06-02 04:38:55', NULL, 1),
(3, NULL, 'Conversation', 'tesetet', 200.00, NULL, 7, NULL, 'ACTIVE', 1, '2021-06-02 06:42:07', '2021-06-02 04:42:07', 'Mw==', 1),
(4, NULL, 'Veltitnaoc', 'testet', 1200.00, NULL, 7, NULL, 'ACTIVE', 1, '2021-06-02 06:44:18', '2021-06-02 04:44:18', 'NA==', 1);

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `mode` varchar(33) NOT NULL COMMENT 'purchase,sale',
  `mode_id` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `unit` varchar(22) DEFAULT NULL,
  `unit_id` int(11) DEFAULT NULL,
  `unit_price` decimal(10,0) DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `amount` decimal(10,0) DEFAULT NULL,
  `status` varchar(12) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `owner_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

CREATE TABLE `notes` (
  `id` int(11) NOT NULL,
  `note` text NOT NULL,
  `description` text DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `customer_type` varchar(12) DEFAULT NULL,
  `mode` varchar(12) DEFAULT NULL COMMENT 'purchase,sale,expense',
  `mode_id` int(11) DEFAULT NULL,
  `status` varchar(22) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `owner`
--

CREATE TABLE `owner` (
  `id` int(11) NOT NULL,
  `company_name` varchar(122) NOT NULL,
  `image` text DEFAULT NULL,
  `address` text DEFAULT NULL,
  `mobile` varchar(122) DEFAULT NULL,
  `email` varchar(111) DEFAULT NULL,
  `phone_no` varchar(122) DEFAULT NULL,
  `fax` varchar(122) DEFAULT NULL,
  `invoice_initial` varchar(11) DEFAULT NULL,
  `website` text DEFAULT NULL,
  `seed_license` varchar(255) DEFAULT NULL,
  `pesticides_license` varchar(255) DEFAULT NULL,
  `fertilizer_license` varchar(255) DEFAULT NULL,
  `sales_tax_no` varchar(122) DEFAULT NULL,
  `home_currency` varchar(122) DEFAULT NULL,
  `unit_length` varchar(12) DEFAULT NULL,
  `unit_weight` varchar(12) DEFAULT NULL,
  `is_primary` varchar(13) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `owner`
--

INSERT INTO `owner` (`id`, `company_name`, `image`, `address`, `mobile`, `email`, `phone_no`, `fax`, `invoice_initial`, `website`, `seed_license`, `pesticides_license`, `fertilizer_license`, `sales_tax_no`, `home_currency`, `unit_length`, `unit_weight`, `is_primary`) VALUES
(1, 'KAVIN FARMS', 'http://localhost/kavinfarms/assets/images/settings/logo-154.png', 'Theni', '8765432', 'kavinfarms@help.in', '04490990', '0987653456', NULL, 'http://localhost/kavinfarms/company-settings', '98765456', 'OLP876543234567', 'dfdfj8343', '434d', 'Indian', '90', 'KG', 'YES'),
(2, 'HONEY BEE', 'http://localhost/kavinfarms/assets/images/settings/63411420-lrg_dc6356bc-0509-48ea-b238-29b75aba4a02_530x-392.jpg', '', '', '', '', '', NULL, '', '', '', '', '', NULL, NULL, NULL, 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `own_bank_info`
--

CREATE TABLE `own_bank_info` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_type` varchar(55) DEFAULT NULL,
  `bank_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `bank_ac_no` varchar(255) NOT NULL,
  `ifsc_code` varchar(22) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `status` varchar(12) NOT NULL DEFAULT 'ACTIVE',
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `own_bank_info`
--

INSERT INTO `own_bank_info` (`id`, `user_id`, `user_type`, `bank_id`, `branch_id`, `bank_ac_no`, `ifsc_code`, `address`, `status`, `created_by`, `created_at`) VALUES
(1, 1, 'own', 4, 11, '98765432567', '09ASDFG678', NULL, 'ACTIVE', 1, '2021-06-10 07:18:10'),
(6, 1, 'own', 12, 14, '9876543456789', 'CNR90989', NULL, 'ACTIVE', 1, '2021-06-10 11:35:28');

-- --------------------------------------------------------

--
-- Table structure for table `own_categories`
--

CREATE TABLE `own_categories` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` varchar(12) NOT NULL DEFAULT 'ACTIVE',
  `created_by` int(5) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `own_categories`
--

INSERT INTO `own_categories` (`id`, `company_id`, `name`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(5, 1, 'TEste', 'ACTIVE', 1, '2021-05-28 10:01:33', '2021-05-28 08:01:33'),
(6, 1, 'reopj', 'ACTIVE', 1, '2021-05-28 10:01:33', '2021-05-28 08:01:33'),
(7, 1, 'Belig', 'ACTIVE', 1, '2021-05-28 10:01:33', '2021-05-28 08:01:33');

-- --------------------------------------------------------

--
-- Table structure for table `own_doc_number`
--

CREATE TABLE `own_doc_number` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `purchase_inv_no` varchar(122) DEFAULT NULL,
  `purchase_payment_no` varchar(122) DEFAULT NULL,
  `purchase_return_no` varchar(122) DEFAULT NULL,
  `purchase_credit_no` varchar(122) DEFAULT NULL,
  `purchase_debit_no` varchar(122) DEFAULT NULL,
  `dealer_sales_inv_no` varchar(122) DEFAULT NULL,
  `dealer_sales_payment_no` varchar(122) DEFAULT NULL,
  `dealer_sales_return_no` varchar(122) DEFAULT NULL,
  `farmer_sales_inv_no` varchar(122) DEFAULT NULL,
  `farmer_sales_payment_no` varchar(122) DEFAULT NULL,
  `farmer_sales_return_no` varchar(122) DEFAULT NULL,
  `sale_credit_no` varchar(122) DEFAULT NULL,
  `sale_debit_no` varchar(122) DEFAULT NULL,
  `status` varchar(12) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `own_doc_number`
--

INSERT INTO `own_doc_number` (`id`, `company_id`, `purchase_inv_no`, `purchase_payment_no`, `purchase_return_no`, `purchase_credit_no`, `purchase_debit_no`, `dealer_sales_inv_no`, `dealer_sales_payment_no`, `dealer_sales_return_no`, `farmer_sales_inv_no`, `farmer_sales_payment_no`, `farmer_sales_return_no`, `sale_credit_no`, `sale_debit_no`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 1, 'KAV/PUR/20-21/00000', 'KAV/PAY/20-21/00000', 'KAV/PRET/20-21/00000', 'KAV/PCNT/20-21/00000', 'KAV/PDNT/20-21/00000', 'KAV/SALD/20-21/00000', 'KAV/SPAYD/20-21/00000', 'KAV/SRETD/20-21/00000', 'KAV/SALF/20-21/00000', 'KAV/SPAYF/20-21/00000', 'KAV/SRETF/20-21/00000', 'KAV/SCNT/20-21/00000', 'KAV/SDNT/20-21/00000', NULL, 1, '2021-05-28 08:29:00', '2021-05-28 06:29:00'),
(2, 2, 'HBE/PUR/21-22/00000', 'HBE/PAY/21-22/00000', 'HBE/PRET/21-22/00000', 'HBE/PCRN/21-22/00000', 'HBE/PDBN/21-22/00000', 'HBE/DSAL/21-22/00000', 'HBE/DSPAY/21-22/00000', 'HBE/DSRET/21-22/00000', 'HBE/FSAL/21-22/00000', 'HBE/FSPAY/21-22/00000', 'HBE/FSRET/21-22/00000', 'HBE/SCRN/21-22/00000', 'HBE/SDBN/21-22/00000', NULL, 1, '2021-06-10 14:14:50', '2021-06-10 12:14:50');

-- --------------------------------------------------------

--
-- Table structure for table `own_payment_term`
--

CREATE TABLE `own_payment_term` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `term_name` varchar(122) DEFAULT NULL,
  `due_days` int(11) DEFAULT NULL,
  `status` varchar(12) NOT NULL DEFAULT 'ACTIVE',
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `own_payment_term`
--

INSERT INTO `own_payment_term` (`id`, `company_id`, `term_name`, `due_days`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(5, 1, 'Net 45', 45, 'ACTIVE', 1, '2021-05-28 08:49:30', '2021-05-28 06:49:30');

-- --------------------------------------------------------

--
-- Table structure for table `own_pricing_schemes`
--

CREATE TABLE `own_pricing_schemes` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `pricing_name` varchar(122) DEFAULT NULL,
  `is_tax_included` varchar(12) DEFAULT NULL,
  `tax_percent` float DEFAULT NULL,
  `currency` varchar(122) DEFAULT NULL,
  `is_default` varchar(12) DEFAULT NULL,
  `status` varchar(12) NOT NULL DEFAULT 'ACTIVE',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `own_pricing_schemes`
--

INSERT INTO `own_pricing_schemes` (`id`, `company_id`, `pricing_name`, `is_tax_included`, `tax_percent`, `currency`, `is_default`, `status`, `created_at`, `created_by`, `updated_at`) VALUES
(3, 1, 'Dearl Price', 'yes', 12, 'India Rupee', NULL, 'ACTIVE', '2021-05-28 09:33:51', 1, '2021-05-28 07:33:51'),
(4, 1, 'Farmer Price', 'yes', 8, 'Euro Rupee', NULL, 'ACTIVE', '2021-05-28 09:33:51', 1, '2021-05-28 07:33:51'),
(5, 1, 'Farm Price', NULL, 0, 'Indian Rupee', NULL, 'ACTIVE', '2021-05-28 09:33:51', 1, '2021-05-28 07:33:51');

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` int(11) NOT NULL,
  `receipt_no` varchar(55) NOT NULL COMMENT 'payment receipt no',
  `mode` varchar(22) DEFAULT NULL COMMENT 'purchase,sale,expense',
  `mode_type` varchar(22) DEFAULT NULL COMMENT 'add,return, company',
  `mode_type_id` int(11) DEFAULT NULL,
  `invoice_no` varchar(55) DEFAULT NULL,
  `attachment` text DEFAULT NULL,
  `payment_method` varchar(11) DEFAULT NULL,
  `payment_method_id` int(11) DEFAULT NULL,
  `payment_date` date DEFAULT NULL,
  `amount` float(12,2) NOT NULL,
  `bank_charges` decimal(15,3) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `bank_branch_id` int(11) DEFAULT NULL,
  `bank_branch` varchar(122) DEFAULT NULL,
  `ifsc` varchar(22) DEFAULT NULL,
  `bank_ac_no` bigint(20) DEFAULT NULL,
  `cheque_no` varchar(55) DEFAULT NULL COMMENT 'cheque/dd no',
  `cheque_issue_amount` float DEFAULT NULL,
  `issue_date` date DEFAULT NULL,
  `is_advance_booking` varchar(15) DEFAULT NULL,
  `advance_description` text DEFAULT NULL,
  `status` varchar(22) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `owner_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`id`, `receipt_no`, `mode`, `mode_type`, `mode_type_id`, `invoice_no`, `attachment`, `payment_method`, `payment_method_id`, `payment_date`, `amount`, `bank_charges`, `bank_id`, `bank_branch_id`, `bank_branch`, `ifsc`, `bank_ac_no`, `cheque_no`, `cheque_issue_amount`, `issue_date`, `is_advance_booking`, `advance_description`, `status`, `created_by`, `created_at`, `updated_at`, `owner_id`) VALUES
(1, '', 'expense', 'add', 1, '', NULL, NULL, 6, NULL, 520.00, NULL, 4, NULL, NULL, NULL, 123012301220, '', NULL, NULL, NULL, NULL, NULL, 1, '2021-06-02 06:32:20', '2021-06-02 04:32:20', NULL),
(2, '', 'expense', 'add', 2, '', NULL, NULL, 8, NULL, 320.00, NULL, 12, NULL, NULL, NULL, 57552, '123', NULL, NULL, NULL, NULL, NULL, 1, '2021-06-02 06:38:55', '2021-06-02 04:38:55', 1),
(3, '', 'expense', 'add', 3, '', NULL, NULL, 7, NULL, 200.00, NULL, 16, NULL, NULL, NULL, 9876543211234567, '564556', 200, '1970-01-01', NULL, NULL, NULL, 1, '2021-06-02 06:42:07', '2021-06-02 04:42:07', 1),
(4, '', 'expense', 'add', 4, '', NULL, NULL, 7, NULL, 1200.00, NULL, 23, NULL, NULL, NULL, 345678987654, '5454', 200, '2021-06-29', NULL, NULL, NULL, 1, '2021-06-02 06:44:18', '2021-06-02 04:44:18', 1),
(21, 'KAV/PAY/20-21/00001', 'purchase', 'add', 6, 'KAV/PUR/20-21/00005', 'http://localhost/kavinfarms/assets/images/purchase_payment/img-20200808-wa0006-828.jpg', NULL, 6, '2021-06-01', 300.00, '0.000', 12, 42, 'CMBT', 'CNRCMBT9009', 2987654321, '', 0, NULL, 'yes', NULL, 'ACTIVE', 1, '2021-06-05 13:23:01', '2021-06-05 11:25:10', 1),
(22, 'KAV/PAY/20-21/00002', 'purchase', 'add', 6, 'KAV/PUR/20-21/00005', NULL, NULL, 7, '2021-06-03', 700.00, '10.000', 4, 11, 'Vetri of india', '09ASDFG678', 7898765432, '230230', 700, '2021-06-02', 'no', NULL, 'ACTIVE', 1, '2021-06-05 13:24:49', '2021-06-05 11:24:49', 1),
(23, 'KAV/PAY/20-21/00003', 'purchase', 'add', 5, 'KAV/PUR/20-21/00004', NULL, NULL, 6, '2021-06-02', 300.00, '10.000', 12, 37, 'CMBT', 'CNRCMBT9009', 90000080000790, '', 0, NULL, 'yes', NULL, 'ACTIVE', 1, '2021-06-05 13:29:35', '2021-06-05 11:29:35', 1),
(24, 'KAV/PAY/20-21/00004', 'purchase', 'add', 7, 'KAV/PUR/20-21/00006', NULL, NULL, 6, '2021-06-09', 1200.00, '0.000', 12, 37, 'CMBT', 'CNRCMBT9009', 9876567890, '', 0, NULL, 'yes', NULL, 'ACTIVE', 1, '2021-06-05 13:33:46', '2021-06-05 11:33:46', 1),
(25, 'KAV/PAY/20-21/00005', 'purchase', 'add', 7, 'KAV/PUR/20-21/00006', NULL, NULL, 6, '2021-06-01', 300.00, '20.000', 12, 37, 'CMBT', 'CNRCMBT9009', 4200023000, '', 0, NULL, 'no', '', 'ACTIVE', 1, '2021-06-05 14:22:06', '2021-06-10 11:52:45', 1),
(27, 'KAV/PAY/20-21/00006', 'purchase', 'company', 1, '', NULL, NULL, 6, '2021-06-10', 300.00, '1.000', 4, 11, 'Vetri of india', '09ASDFG678', 98765432567, '', 0, NULL, 'yes', 'testing of teseting', 'ACTIVE', 1, '2021-06-10 13:03:42', '2021-06-10 11:24:33', 1);

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE `permission` (
  `id` int(22) NOT NULL,
  `user_type_id` int(11) NOT NULL,
  `user_id` bigint(15) DEFAULT NULL,
  `module` varchar(122) DEFAULT NULL COMMENT 'sale,purchase,etc',
  `module_event` varchar(33) DEFAULT NULL,
  `action` varchar(22) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `physical_year`
--

CREATE TABLE `physical_year` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `status` varchar(12) NOT NULL DEFAULT 'ACTIVE',
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `physical_year`
--

INSERT INTO `physical_year` (`id`, `company_id`, `start_date`, `end_date`, `status`, `created_by`, `created_at`) VALUES
(3, 2, '2021-04-01', '2022-03-31', 'ACTIVE', 1, '2021-05-28 15:55:42'),
(4, 1, '2021-04-01', '2022-03-31', 'ACTIVE', 1, '2021-05-28 12:27:38'),
(5, 1, '2020-04-01', '2021-03-31', 'ACTIVE', 1, '2021-05-28 12:27:38'),
(6, 1, '2019-04-01', '2020-03-31', 'ACTIVE', 1, '2021-05-28 12:27:38');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `product_name` varchar(122) NOT NULL,
  `product_company` varchar(122) DEFAULT NULL,
  `product_code` varchar(55) NOT NULL,
  `product_variety` varchar(155) DEFAULT NULL,
  `product_category` varchar(55) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `description` text NOT NULL,
  `packaging` varchar(15) DEFAULT NULL COMMENT 'uom',
  `image` longtext DEFAULT NULL,
  `unit_id` int(11) DEFAULT NULL,
  `unit` varchar(15) DEFAULT NULL,
  `net_purchase_price` decimal(10,0) DEFAULT NULL,
  `farmer_sale_price` decimal(10,0) DEFAULT NULL,
  `dealer_sale_price` decimal(10,0) DEFAULT NULL,
  `retail_sale_price` decimal(10,0) DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE','','') NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `slug` varchar(122) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `product_name`, `product_company`, `product_code`, `product_variety`, `product_category`, `category_id`, `description`, `packaging`, `image`, `unit_id`, `unit`, `net_purchase_price`, `farmer_sale_price`, `dealer_sale_price`, `retail_sale_price`, `status`, `created_by`, `created_at`, `updated_at`, `slug`, `owner_id`) VALUES
(2, 'Mseed Sand', 'Menauid', 'MKDF9034', NULL, 'Seeds', NULL, 'seed of india', '10 Kg', 'http://localhost/kavinfarms/assets/images/product/accounting-791.png', NULL, NULL, '350', '600', '501', '613', 'ACTIVE', 1, '2021-05-18 05:31:52', '2021-05-18 04:33:18', 'mseed-sand-mkdf9034', 1),
(4, 'Metrical', 'manu', '9090', NULL, 'tste', NULL, 'tset', 'stet', 'http://localhost/kavinfarms/assets/images/product/login-bg3-697.jpg', NULL, NULL, '0', '0', '0', '0', 'ACTIVE', 1, '2021-05-28 10:06:17', '2021-05-28 08:06:17', 'metrical-9090', 1),
(5, 'Meltis', 'Cindia', 'MEL-090-09', NULL, 'Unknown', NULL, 'test', '3kg', NULL, NULL, NULL, '120', '350', '250', '230', 'ACTIVE', 1, '2021-05-31 09:36:53', '2021-05-31 07:36:53', 'meltis-mel-090-09', 1),
(6, 'Opins', 'India', 'IND-09767-3', NULL, 'Bold', NULL, 'teset', '67 km', NULL, NULL, NULL, '450', '480', '460', '490', 'ACTIVE', 1, '2021-05-31 09:37:38', '2021-05-31 07:37:38', 'opins-ind-09767-3', 1),
(7, 'Oliver', 'India', 'OLi9090', NULL, 'Cur', NULL, 'testing ', '3 kg', NULL, NULL, NULL, '890', '1000', '900', '2000', 'ACTIVE', 1, '2021-06-09 12:25:42', '2021-06-09 10:25:42', 'oliver-oli9090', 1),
(8, 'Myce', 'Magenta', 'Myce-99098', NULL, 'Ctet', NULL, 'teset iff', '10 Kg', NULL, NULL, NULL, '290', '300', '300', '305', 'ACTIVE', 1, '2021-06-09 12:29:23', '2021-06-09 10:29:23', 'myce-myce-99098', 1),
(9, 'Shirts', 'Coot', 'CJsid90s', NULL, 'test', NULL, 'stet', '5 kd', NULL, NULL, NULL, '89', '0', '0', '0', 'ACTIVE', 1, '2021-06-09 12:30:38', '2021-06-09 10:30:38', 'shirts-cjsid90s', 1),
(10, 'Ajantha', '4mani', '90l333', NULL, 'test', NULL, 'wec', '5 Kg', NULL, NULL, NULL, '90', '0', '0', '0', 'ACTIVE', 1, '2021-06-09 12:32:32', '2021-06-09 10:32:32', 'ajantha-90l333', 1),
(11, 'Pencil', 'Nataraj', 'Nat-90', NULL, 'teset', NULL, 'vr', '4kg', NULL, NULL, NULL, '900', '30', '20', '60', 'ACTIVE', 1, '2021-06-09 12:36:16', '2021-06-09 10:36:16', 'pencil-nat-90', 1),
(12, 'Saltines', 'Indi', 'MKDL90333', NULL, 'Celidiid', NULL, 'testing of india', '3 Kg', NULL, NULL, NULL, '320', '150', '230', '60', 'ACTIVE', 1, '2021-06-10 14:17:19', '2021-06-10 12:17:19', 'saltines-mkdl90333', 2);

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `images` text NOT NULL,
  `is_primary` varchar(25) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `purchase`
--

CREATE TABLE `purchase` (
  `id` int(11) NOT NULL,
  `invoice_no` varchar(122) NOT NULL COMMENT 'purchase internal ref no',
  `from_company` int(11) NOT NULL,
  `company_invoice_no` varchar(122) NOT NULL,
  `company_invoice_date` date DEFAULT NULL,
  `purchase_date` date DEFAULT NULL,
  `mode_of_payment` varchar(22) NOT NULL,
  `invoice_due_date` date DEFAULT NULL,
  `mode_of_order` varchar(12) DEFAULT NULL,
  `order_booked_by` varchar(22) DEFAULT NULL,
  `order_booked_id` int(11) DEFAULT NULL,
  `tranport_payment` varchar(122) DEFAULT NULL,
  `transport_charge` varchar(122) DEFAULT NULL,
  `name_of_transport` varchar(122) NOT NULL,
  `transport_id` int(11) DEFAULT NULL,
  `payment_status` varchar(22) DEFAULT NULL COMMENT 'paid, topay',
  `invoice_verified_by` int(11) NOT NULL,
  `goods_delivered` varchar(122) DEFAULT NULL,
  `invoice_amount` float(12,3) NOT NULL,
  `discount` float(12,2) DEFAULT NULL,
  `other_charges` float(12,2) DEFAULT NULL,
  `total_amount` float(12,2) NOT NULL,
  `paid_amount` decimal(11,3) DEFAULT 0.000,
  `paid_status` varchar(22) DEFAULT NULL,
  `remarks` text DEFAULT NULL,
  `attachment` text DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL COMMENT 'active, inactive, return, sold',
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `slug` text DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `purchase`
--

INSERT INTO `purchase` (`id`, `invoice_no`, `from_company`, `company_invoice_no`, `company_invoice_date`, `purchase_date`, `mode_of_payment`, `invoice_due_date`, `mode_of_order`, `order_booked_by`, `order_booked_id`, `tranport_payment`, `transport_charge`, `name_of_transport`, `transport_id`, `payment_status`, `invoice_verified_by`, `goods_delivered`, `invoice_amount`, `discount`, `other_charges`, `total_amount`, `paid_amount`, `paid_status`, `remarks`, `attachment`, `status`, `created_by`, `created_at`, `updated_at`, `slug`, `owner_id`) VALUES
(4, 'KAV/PUR/20-21/00003', 2, '0987653456789', NULL, '2021-12-05', '8', '2021-12-05', NULL, NULL, NULL, NULL, 'Paid', 'Durariek test', 4, NULL, 1, NULL, 0.000, NULL, NULL, 2058.00, '10.000', NULL, '', 'http://localhost/kavinfarms/assets/images/purchase/login-bg2-380.jpg', 'ACTIVE', 0, '2021-05-29 10:26:23', '2021-06-05 08:10:33', 'S0FWL1BVUi8yMC0yMS8wMDAwMw==', 1),
(5, 'KAV/PUR/20-21/00004', 3, '90876590000', NULL, '1970-01-01', '9', '2021-05-06', NULL, NULL, NULL, NULL, 'ToPay', 'Vavivu vetl', 4, NULL, 1, NULL, 0.000, NULL, NULL, 3990.00, '300.000', NULL, 'Durairaj Testing', 'http://localhost/kavinfarms/assets/images/purchase/login-bg3-495.jpg', 'ACTIVE', 1, '2021-05-29 13:13:17', '2021-06-05 11:29:35', 'S0FWL1BVUi8yMC0yMS8wMDAwNA==', 1),
(6, 'KAV/PUR/20-21/00005', 1, '56900009000', NULL, '2021-11-09', '10', '2021-10-07', NULL, NULL, NULL, NULL, 'Paid', 'teste', 4, NULL, 1, NULL, 0.000, NULL, NULL, 5488.00, '1000.000', NULL, '', 'http://localhost/kavinfarms/assets/images/purchase/46b081bd65a02a0795fb169396f6b77b~2-863.jpg', 'ACTIVE', 1, '2021-05-29 13:16:15', '2021-06-05 11:25:10', 'S0FWL1BVUi8yMC0yMS8wMDAwNQ==', 1),
(7, 'KAV/PUR/20-21/00006', 1, '87654321134567', NULL, '2021-02-03', '8', '2021-08-28', NULL, NULL, NULL, NULL, 'Paid', 'Durariek test', 1, NULL, 1, NULL, 0.000, NULL, NULL, 7364.90, '1500.000', NULL, 'Durian TEsting iof netsetet', 'http://localhost/kavinfarms/assets/images/purchase/46b081bd65a02a0795fb169396f6b77b~2-876.jpg', 'ACTIVE', 1, '2021-05-29 13:28:42', '2021-06-10 11:52:45', 'S0FWL1BVUi8yMC0yMS8wMDAwNg==', 1),
(9, 'KAV/PUR/20-21/00007', 1, '562012320', NULL, '2021-06-17', '6', '2021-07-10', NULL, NULL, NULL, NULL, 'ToPay', 'Durariek Verlt', 1, NULL, 1, NULL, 0.000, NULL, NULL, 1008.00, '0.000', NULL, '', NULL, 'ACTIVE', 1, '2021-06-06 07:19:40', '2021-06-06 05:19:40', 'S0FWL1BVUi8yMC0yMS8wMDAwNw==', 1),
(10, 'KAV/PUR/20-21/00008', 1, '454', NULL, '2021-06-02', '6', '2021-06-16', NULL, NULL, NULL, NULL, 'Paid', 'Durariek test', 1, NULL, 1, NULL, 0.000, NULL, NULL, 7242.00, '0.000', NULL, '', NULL, 'ACTIVE', 1, '2021-06-06 07:22:47', '2021-06-06 05:22:47', 'S0FWL1BVUi8yMC0yMS8wMDAwOA==', 1),
(11, 'KAV/PUR/20-21/00009', 2, '55566565656', NULL, '2021-06-01', '22', '2021-06-26', NULL, NULL, NULL, NULL, 'ToPay', 'dURAIRAJRA', 9, NULL, 1, NULL, 0.000, NULL, NULL, 16412.50, '0.000', NULL, '', NULL, 'ACTIVE', 1, '2021-06-09 12:36:34', '2021-06-09 10:36:34', 'S0FWL1BVUi8yMC0yMS8wMDAwOQ==', 1),
(12, 'KAV/PUR/20-21/00010', 1, '987654325', NULL, '2021-06-01', '17', '2021-07-01', NULL, NULL, NULL, 'ToPay', '120', 'Complerx City;', 1, NULL, 1, NULL, 0.000, NULL, NULL, 3048.30, '0.000', NULL, '', NULL, 'ACTIVE', 1, '2021-06-09 15:54:00', '2021-06-09 13:54:00', 'S0FWL1BVUi8yMC0yMS8wMDAxMA==', 1),
(13, 'KAV/PUR/20-21/00011', 4, '8502361', NULL, '2021-06-01', '17', '2021-07-01', NULL, NULL, NULL, 'ToPay', '120', 'Arun', 9, NULL, 1, NULL, 0.000, NULL, NULL, 4735.00, '0.000', NULL, '', NULL, 'ACTIVE', 1, '2021-06-10 04:34:03', '2021-06-10 02:34:03', 'S0FWL1BVUi8yMC0yMS8wMDAxMQ==', 1),
(14, 'HBE/PUR/21-22/00001', 1, '45000020', NULL, '2021-06-01', '17', '2021-07-01', NULL, NULL, NULL, 'ToPay', '230', 'testeing', 1, NULL, 1, NULL, 0.000, NULL, NULL, 550.00, '0.000', NULL, '', NULL, 'ACTIVE', 1, '2021-06-10 14:17:30', '2021-06-10 12:17:30', 'SEJFL1BVUi8yMS0yMi8wMDAwMQ==', 2);

-- --------------------------------------------------------

--
-- Table structure for table `purchase_document`
--

CREATE TABLE `purchase_document` (
  `id` int(11) NOT NULL,
  `purchase_id` int(11) NOT NULL,
  `document_name` varchar(33) DEFAULT NULL,
  `document_file` text DEFAULT NULL,
  `status` varchar(33) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_items`
--

CREATE TABLE `purchase_items` (
  `id` int(11) NOT NULL,
  `purchase_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `lot_no` varchar(222) DEFAULT NULL,
  `unit_id` int(11) DEFAULT NULL,
  `net_rate` float(12,3) NOT NULL,
  `qty` int(11) NOT NULL,
  `date_of_packed` date DEFAULT NULL,
  `date_of_expiry` date DEFAULT NULL,
  `date_of_tested` date DEFAULT NULL,
  `discount` decimal(10,0) DEFAULT NULL,
  `total_amount` float(12,2) NOT NULL,
  `status` varchar(55) DEFAULT NULL COMMENT 'active, inactive, return, sold',
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `purchase_items`
--

INSERT INTO `purchase_items` (`id`, `purchase_id`, `product_id`, `lot_no`, `unit_id`, `net_rate`, `qty`, `date_of_packed`, `date_of_expiry`, `date_of_tested`, `discount`, `total_amount`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(5, 5, 2, '7000', NULL, 350.000, 12, '2021-11-05', '2021-05-06', '1970-01-01', '5', 3990.00, 'ACTIVE', 1, '2021-05-29 13:13:17', '2021-05-29 11:13:17'),
(20, 6, 2, '790087', NULL, 350.000, 14, '2021-03-05', '2021-04-06', '2021-12-05', '0', 4900.00, 'ACTIVE', 1, '2021-05-31 06:39:34', '2021-05-31 04:39:34'),
(21, 6, 4, '89009', NULL, 120.000, 5, '2021-05-03', '2021-07-10', '2021-05-20', '2', 588.00, 'ACTIVE', 1, '2021-05-31 06:39:34', '2021-05-31 04:39:34'),
(25, 7, 2, '454353', NULL, 350.000, 10, '2021-10-05', '2021-05-06', '1970-01-01', '0', 3500.00, 'ACTIVE', 1, '2021-06-06 06:58:10', '2021-06-06 04:58:10'),
(26, 7, 4, '12000', NULL, 430.000, 6, '2021-05-04', '2021-06-05', '2021-05-28', '2', 2528.40, 'ACTIVE', 1, '2021-06-06 06:58:10', '2021-06-06 04:58:10'),
(27, 7, 6, '89230', NULL, 450.000, 3, '2021-05-03', '2021-06-05', '2021-05-26', '1', 1336.50, 'ACTIVE', 1, '2021-06-06 06:58:10', '2021-06-06 04:58:10'),
(30, 9, 2, '8908', NULL, 350.000, 3, '2021-01-06', '2021-07-03', '2021-06-15', '4', 1008.00, 'ACTIVE', 1, '2021-06-06 07:21:25', '2021-06-06 05:21:25'),
(39, 4, 2, '', NULL, 350.000, 6, '1970-01-01', '2021-04-06', '1970-01-01', '2', 2058.00, 'ACTIVE', 1, '2021-06-06 07:27:22', '2021-06-06 05:27:22'),
(40, 10, 2, '9098', NULL, 350.000, 18, '1970-01-01', '2021-07-03', '2021-06-01', '6', 5922.00, 'ACTIVE', 1, '2021-06-06 07:29:32', '2021-06-06 05:29:32'),
(41, 10, 5, '789', NULL, 120.000, 11, '1970-01-01', '2021-06-09', '2021-05-31', '0', 1320.00, 'ACTIVE', 1, '2021-06-06 07:29:32', '2021-06-06 05:29:32'),
(42, 11, 7, '90889', NULL, 890.000, 5, '1970-01-01', '2021-07-10', '2021-06-24', '3', 4316.50, 'ACTIVE', 1, '2021-06-09 12:36:34', '2021-06-09 10:36:34'),
(43, 11, 11, '450230', NULL, 900.000, 14, '1970-01-01', '2021-07-09', '2021-06-10', '4', 12096.00, 'ACTIVE', 1, '2021-06-09 12:36:34', '2021-06-09 10:36:34'),
(44, 12, 5, '909090', NULL, 120.000, 14, '1970-01-01', '2021-07-09', '2021-06-08', '4', 1612.80, 'ACTIVE', 1, '2021-06-09 15:54:00', '2021-06-09 13:54:00'),
(45, 12, 8, '12000230', NULL, 290.000, 5, '1970-01-01', '2021-07-10', '2021-06-24', '1', 1435.50, 'ACTIVE', 1, '2021-06-09 15:54:00', '2021-06-09 13:54:00'),
(46, 13, 2, '09909', NULL, 350.000, 5, '1970-01-01', '2021-07-02', '2021-06-08', '2', 1715.00, 'ACTIVE', 1, '2021-06-10 04:34:03', '2021-06-10 02:34:03'),
(47, 13, 8, '7890', NULL, 290.000, 10, '1970-01-01', '2021-07-10', NULL, '0', 2900.00, 'ACTIVE', 1, '2021-06-10 04:34:03', '2021-06-10 02:34:03'),
(48, 14, 12, '230', NULL, 320.000, 1, '1970-01-01', '2021-06-18', NULL, '0', 320.00, 'ACTIVE', 1, '2021-06-10 14:17:30', '2021-06-10 12:17:30');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_return`
--

CREATE TABLE `purchase_return` (
  `id` int(11) NOT NULL,
  `purchase_id` int(11) NOT NULL,
  `purchase_invoice_no` varchar(55) NOT NULL,
  `return_no` varchar(55) NOT NULL,
  `return_to` int(11) NOT NULL COMMENT 'vendor/company id',
  `return_date` date NOT NULL,
  `transport_charge` varchar(22) DEFAULT NULL,
  `transport_name` varchar(122) NOT NULL,
  `transport_id` int(11) DEFAULT NULL,
  `reason` text NOT NULL,
  `payment_status` varchar(25) DEFAULT NULL,
  `total_qty` int(11) DEFAULT NULL,
  `total_amount` float(12,2) NOT NULL,
  `gst` decimal(10,0) DEFAULT NULL,
  `cst` decimal(10,0) DEFAULT NULL,
  `vat` decimal(10,0) DEFAULT NULL,
  `remarks` text DEFAULT NULL,
  `attachment` text DEFAULT NULL,
  `status` varchar(22) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `slug` varchar(122) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `purchase_return`
--

INSERT INTO `purchase_return` (`id`, `purchase_id`, `purchase_invoice_no`, `return_no`, `return_to`, `return_date`, `transport_charge`, `transport_name`, `transport_id`, `reason`, `payment_status`, `total_qty`, `total_amount`, `gst`, `cst`, `vat`, `remarks`, `attachment`, `status`, `created_by`, `created_at`, `updated_at`, `slug`, `owner_id`) VALUES
(3, 7, 'KAV/PUR/20-21/00006', 'KAV/PRET/20-21/00002', 2, '2021-05-31', 'Paid', 'durariar', 1, 'sddfdsdfdf', NULL, 10, 7364.90, NULL, NULL, NULL, '', NULL, 'ACTIVE', 1, '2021-05-31 13:20:18', '2021-05-31 11:20:18', 'S0FWL1BSRVQvMjAtMjEvMDAwMDI=', 1),
(5, 4, 'KAV/PUR/20-21/00003', 'KAV/PRET/20-21/00006', 2, '2021-05-31', 'ToPay', 'dfsdf', 1, 'sddfdsdfdf', NULL, 5, 1732.50, NULL, NULL, NULL, 'tes in stermailra ramear', NULL, 'ACTIVE', 1, '2021-05-31 13:26:11', '2021-05-31 15:18:15', 'S0FWL1BSRVQvMjAtMjEvMDAwMDM=', 1),
(6, 7, 'KAV/PUR/20-21/00006', 'KAV/PRET/20-21/00005', 1, '2021-05-31', 'ToPay', 'durariar', 4, 'tset', NULL, 3, 1029.00, NULL, NULL, NULL, 'Tested of India', NULL, 'ACTIVE', 1, '2021-05-31 17:01:08', '2021-05-31 15:17:29', 'S0FWL1BSRVQvMjAtMjEvMDAwMDQ=', 1),
(7, 4, 'KAV/PUR/20-21/00003', 'KAV/PRET/20-21/00007', 2, '2021-06-02', 'Paid', 'group', 4, 'sddfdsdfdf', NULL, 1, 343.00, NULL, NULL, NULL, 'friendly of india', 'http://localhost/kavinfarms/assets/images/purchase_return/46b081bd65a02a0795fb169396f6b77b~2-718.jpg', 'ACTIVE', 1, '2021-06-02 04:41:01', '2021-06-02 02:41:35', 'S0FWL1BSRVQvMjAtMjEvMDAwMDY=', 1),
(8, 7, 'KAV/PUR/20-21/00006', 'KAV/PRET/20-21/00008', 1, '2021-06-03', 'Paid', 'Annamalai transport', 1, 'Teste of return', NULL, 1, 425.70, NULL, NULL, NULL, '', 'http://localhost/kavinfarms/assets/images/purchase_return/46b081bd65a02a0795fb169396f6b77b~2-789.jpg', 'ACTIVE', 1, '2021-06-03 07:07:44', '2021-06-03 05:07:44', 'S0FWL1BSRVQvMjAtMjEvMDAwMDg=', 1);

-- --------------------------------------------------------

--
-- Table structure for table `purchase_return_items`
--

CREATE TABLE `purchase_return_items` (
  `id` int(11) NOT NULL,
  `purchase_return_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `lot_no` bigint(20) DEFAULT NULL,
  `unit` varchar(13) DEFAULT NULL,
  `unit_id` int(11) DEFAULT NULL,
  `net_rate` decimal(10,0) DEFAULT NULL,
  `date_of_expiry` date NOT NULL,
  `date_of_packed` date DEFAULT NULL,
  `date_of_tested` date DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `discount` decimal(10,0) DEFAULT NULL,
  `total_amount` float(12,3) NOT NULL,
  `status` varchar(33) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `purchase_return_items`
--

INSERT INTO `purchase_return_items` (`id`, `purchase_return_id`, `product_id`, `lot_no`, `unit`, `unit_id`, `net_rate`, `date_of_expiry`, `date_of_packed`, `date_of_tested`, `qty`, `discount`, `total_amount`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(3, 3, 2, 454353, NULL, NULL, '350', '2021-05-06', '2021-10-05', '1970-01-01', 10, '0', 3500.000, 'ACTIVE', 1, '2021-05-31 13:20:18', '2021-05-31 11:20:18'),
(6, 6, 2, 454353, NULL, NULL, '350', '2021-05-06', '2021-05-10', '2021-08-12', 3, '2', 1029.000, 'ACTIVE', 1, '2021-05-31 17:17:29', '2021-05-31 15:17:29'),
(7, 5, 2, 123, NULL, NULL, '350', '2021-04-06', '1970-01-01', '1970-01-01', 5, '1', 1732.500, 'ACTIVE', 1, '2021-05-31 17:18:15', '2021-05-31 15:18:15'),
(9, 7, 2, 8000, NULL, NULL, '350', '2021-04-06', '1970-01-01', '1970-01-01', 1, '2', 343.000, 'ACTIVE', 1, '2021-06-02 04:41:35', '2021-06-02 02:41:35'),
(10, 8, 4, 12000, NULL, NULL, '430', '2021-06-05', '2021-05-04', '2021-05-28', 1, '1', 425.700, 'ACTIVE', 1, '2021-06-03 07:07:44', '2021-06-03 05:07:44');

-- --------------------------------------------------------

--
-- Table structure for table `sale`
--

CREATE TABLE `sale` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer_type` varchar(12) DEFAULT NULL,
  `count_no` varchar(15) DEFAULT NULL,
  `invoice_no` varchar(55) NOT NULL,
  `invoice_date` date NOT NULL,
  `payment_terms` varchar(22) DEFAULT NULL,
  `payment_term_id` int(11) DEFAULT NULL,
  `invoice_due_date` date DEFAULT NULL,
  `mode_of_order` varchar(12) DEFAULT NULL,
  `order_booked_by` varchar(25) DEFAULT NULL,
  `order_booked_id` int(11) DEFAULT NULL,
  `mode_of_transport` varchar(255) DEFAULT NULL,
  `name_of_transport` varchar(122) DEFAULT NULL,
  `transport_id` int(11) DEFAULT NULL,
  `payment_status` varchar(15) DEFAULT NULL COMMENT 'paid, topay',
  `payment_due_date` date DEFAULT NULL,
  `gst` decimal(10,0) DEFAULT NULL,
  `cst` decimal(10,0) DEFAULT NULL,
  `vat` decimal(10,0) DEFAULT NULL,
  `total_amount` float(12,2) NOT NULL,
  `remarks` text DEFAULT NULL,
  `invoice_verified_by` int(11) NOT NULL,
  `status` varchar(122) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `slug` varchar(122) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sales_rep`
--

CREATE TABLE `sales_rep` (
  `id` int(11) NOT NULL,
  `name` varchar(122) NOT NULL,
  `email` varchar(122) DEFAULT NULL,
  `mobile_no` varchar(122) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `status` varchar(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sales_rep`
--

INSERT INTO `sales_rep` (`id`, `name`, `email`, `mobile_no`, `address`, `company_id`, `status`, `created_at`, `created_by`, `owner_id`) VALUES
(1, 'babu', 'babu@mail.com', '', 'durai testing of india', NULL, NULL, '2021-05-13 15:49:17', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sale_document`
--

CREATE TABLE `sale_document` (
  `id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `document_name` varchar(33) DEFAULT NULL,
  `document_file` text DEFAULT NULL,
  `status` varchar(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sale_items`
--

CREATE TABLE `sale_items` (
  `id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `lot_no` bigint(55) DEFAULT NULL,
  `qty` int(11) NOT NULL,
  `unit` varchar(16) DEFAULT NULL,
  `unit_id` int(11) DEFAULT NULL,
  `net_rate` decimal(10,0) DEFAULT NULL,
  `discount` decimal(10,0) DEFAULT NULL,
  `total_amount` float(12,2) NOT NULL,
  `date_of_expiry` date DEFAULT NULL,
  `status` varchar(122) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sale_return`
--

CREATE TABLE `sale_return` (
  `id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `count_no` varchar(15) DEFAULT NULL,
  `return_number` varchar(55) NOT NULL,
  `return_date` date NOT NULL,
  `from_customer_id` int(11) NOT NULL,
  `invoice_no` varchar(55) DEFAULT NULL,
  `mode_of_transport` varchar(55) DEFAULT NULL,
  `name_of_transport` varchar(55) DEFAULT NULL,
  `transport_id` int(11) DEFAULT NULL,
  `payment_status` varchar(15) DEFAULT NULL COMMENT 'paid, topay',
  `reason` text NOT NULL,
  `remarks` text DEFAULT NULL,
  `verified_by` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `total_amount` float(12,2) NOT NULL,
  `status` varchar(15) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `slug` varchar(122) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sale_return_items`
--

CREATE TABLE `sale_return_items` (
  `id` int(11) NOT NULL,
  `sale_return_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `lot_no` bigint(20) DEFAULT NULL,
  `unit` varchar(55) DEFAULT NULL,
  `unit_id` int(11) DEFAULT NULL,
  `net_rate` decimal(12,2) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `total_amount` decimal(12,2) DEFAULT NULL,
  `date_of_expiry` date DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `id` int(11) NOT NULL,
  `state` varchar(122) NOT NULL,
  `country_id` int(11) NOT NULL,
  `status` varchar(11) NOT NULL DEFAULT 'ACTIVE'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`id`, `state`, `country_id`, `status`) VALUES
(1, 'ANDHRA PRADESH', 99, 'ACTIVE'),
(2, 'ASSAM', 99, 'ACTIVE'),
(3, 'ARUNACHAL PRADESH', 99, 'ACTIVE'),
(4, 'BIHAR', 99, 'ACTIVE'),
(5, 'GUJRAT', 99, 'ACTIVE'),
(6, 'HARYANA', 99, 'ACTIVE'),
(7, 'HIMACHAL PRADESH', 99, 'ACTIVE'),
(8, 'JAMMU & KASHMIR', 99, 'ACTIVE'),
(9, 'KARNATAKA', 99, 'ACTIVE'),
(10, 'KERALA', 99, 'ACTIVE'),
(11, 'MADHYA PRADESH', 99, 'ACTIVE'),
(12, 'MAHARASHTRA', 99, 'ACTIVE'),
(13, 'MANIPUR', 99, 'ACTIVE'),
(14, 'MEGHALAYA', 99, 'ACTIVE'),
(15, 'MIZORAM', 99, 'ACTIVE'),
(16, 'NAGALAND', 99, 'ACTIVE'),
(17, 'ORISSA', 99, 'ACTIVE'),
(18, 'PUNJAB', 99, 'ACTIVE'),
(19, 'RAJASTHAN', 99, 'ACTIVE'),
(20, 'SIKKIM', 99, 'ACTIVE'),
(21, 'TAMIL NADU', 99, 'ACTIVE'),
(22, 'TRIPURA', 99, 'ACTIVE'),
(23, 'UTTAR PRADESH', 99, 'ACTIVE'),
(24, 'WEST BENGAL', 99, 'ACTIVE'),
(25, 'DELHI', 99, 'ACTIVE'),
(26, 'GOA', 99, 'ACTIVE'),
(27, 'PONDICHERY', 99, 'ACTIVE'),
(28, 'LAKSHDWEEP', 99, 'ACTIVE'),
(29, 'DAMAN & DIU', 99, 'ACTIVE'),
(30, 'DADRA & NAGAR', 99, 'ACTIVE'),
(31, 'CHANDIGARH', 99, 'ACTIVE'),
(32, 'ANDAMAN & NICOBAR', 99, 'ACTIVE'),
(33, 'UTTARANCHAL', 99, 'ACTIVE'),
(34, 'JHARKHAND', 99, 'ACTIVE'),
(35, 'CHATTISGARH', 99, 'ACTIVE');

-- --------------------------------------------------------

--
-- Table structure for table `transport`
--

CREATE TABLE `transport` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(122) DEFAULT NULL,
  `email` varchar(55) DEFAULT NULL,
  `mobile_no` varchar(55) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE','','') NOT NULL DEFAULT 'ACTIVE',
  `slug` varchar(122) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `owner_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transport`
--

INSERT INTO `transport` (`id`, `name`, `type`, `email`, `mobile_no`, `address`, `status`, `slug`, `created_at`, `updated_at`, `owner_id`) VALUES
(1, 'annamalirter', NULL, 'jsfjdW@skjdf.comc', '6543223456', 'chennai , annama ipuramr', 'ACTIVE', 'annamalirter', '2021-05-08 22:50:29', '2021-05-08 20:55:32', NULL),
(4, 'Vetrivale', NULL, 'vv@kldjfdj.om', '23456789', 'chennai', 'ACTIVE', 'vetrivale', '2021-05-08 22:57:50', '2021-05-08 20:57:50', NULL),
(6, 'Merlin', NULL, 'merlin@gmail.com', '4343434', '', 'ACTIVE', 'merlin', '2021-06-09 11:33:02', '2021-06-09 09:33:02', NULL),
(7, 'KP Parcel', NULL, 'kpp@parcel.com', '', '', 'ACTIVE', 'kp-parcel', '2021-06-09 11:34:55', '2021-06-09 09:34:55', NULL),
(8, 'Varun Chennai', NULL, 'Carerund@mail.com', '9876543224567', '', 'ACTIVE', 'varun-chennai', '2021-06-09 11:36:25', '2021-06-09 09:36:25', NULL),
(9, 'GOPI CHENNIDU', NULL, 'CHEK@MAIL.COM', '', '', 'ACTIVE', 'gopi-chennidu', '2021-06-09 12:34:46', '2021-06-09 10:34:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(25) DEFAULT NULL,
  `email` varchar(55) DEFAULT NULL,
  `mobile_no` varchar(55) DEFAULT NULL,
  `password` text DEFAULT NULL,
  `image` text DEFAULT NULL,
  `user_type` varchar(122) DEFAULT NULL,
  `user_type_id` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `last_logged_in` datetime DEFAULT NULL,
  `is_logged_in` varchar(5) NOT NULL DEFAULT 'NO' COMMENT 'YES, NO',
  `ip_address` varchar(55) DEFAULT NULL,
  `reset_token` text DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE','','','') NOT NULL DEFAULT 'ACTIVE',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fname`, `lname`, `email`, `mobile_no`, `password`, `image`, `user_type`, `user_type_id`, `employee_id`, `last_logged_in`, `is_logged_in`, `ip_address`, `reset_token`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Kavin Admin', NULL, 'admin@farm.com', '9551706025', '6037129fce27b4f42d706895871a0b4cf61582ae5d1f7dfc6350ee59004481a0db091c4de4437d4e364dfe7fb70325672d8ab2eab3e9ab1a20d8c7c051adc3d9TBQw3/VN9iVOyhSD6sQuTomB6fujB8+EsazVNyta68I=', NULL, NULL, NULL, NULL, NULL, 'NO', NULL, NULL, '', '0000-00-00 00:00:00', '2021-05-18 09:22:53'),
(2, 'ljdfkjfdj', NULL, 'duraibytes@gmail.com1', '9551706026', '7bb4fe09205de0b70405637034a1f2a62e7add8cb6507236611635400448c95e94148d56f36cb967f28b5f1d123cbcb35dc4fb0083bcf8070a09e50319b36e30QxbOYaeMUY8q5M2eR8jGGD7yYCNWj9W9B/0DYxNWhn0=', NULL, NULL, NULL, NULL, NULL, 'NO', NULL, NULL, '', '0000-00-00 00:00:00', '2021-05-06 20:05:07'),
(3, 'durai', NULL, 'duraibytes@gmail.com', '9551706025', '79bcd24fee401c48f9527a22cc84222b362e0061a7f632ba8d20e9eb67da16298c510d8ad365d057cdfa90cb87e282873acc3580bc914aad03865ab7fde68bcaMbREHYD4cTCuWdo/khgOVKnDZXQ/GWXtLAKcSt2/+lk=', NULL, NULL, 4, 1, NULL, 'NO', NULL, NULL, 'ACTIVE', '2021-05-19 12:42:35', '2021-05-19 10:42:35'),
(4, 'Magizh', NULL, 'test@ss.com', '3434342222', '64588b777eb589563c2d8f27c039b625a9b2336d614354c408fdbbe693b44b754441e7b021e4134f7467781f073a5708f4a8d4c2696c6d6631aac073a75fd1edsijwDo0We/kiPZt0voFyffxfdOTNAg7QwN1y0XWWW+4=', NULL, NULL, 5, 3, NULL, 'NO', NULL, NULL, 'ACTIVE', '2021-05-19 12:45:15', '2021-05-19 10:45:15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `codes`
--
ALTER TABLE `codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `code_options`
--
ALTER TABLE `code_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_options` (`code_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_bank` (`bank_id`);

--
-- Indexes for table `company_poc`
--
ALTER TABLE `company_poc`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_comp_pos` (`company_id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_bak_aus` (`bank_id`);

--
-- Indexes for table `deposit_withdraw`
--
ALTER TABLE `deposit_withdraw`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_bank_emp` (`bank_id`);

--
-- Indexes for table `employee_bank_info`
--
ALTER TABLE `employee_bank_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_document`
--
ALTER TABLE `employee_document`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expense`
--
ALTER TABLE `expense`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owner`
--
ALTER TABLE `owner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `own_bank_info`
--
ALTER TABLE `own_bank_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `own_categories`
--
ALTER TABLE `own_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `own_doc_number`
--
ALTER TABLE `own_doc_number`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `own_payment_term`
--
ALTER TABLE `own_payment_term`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `own_pricing_schemes`
--
ALTER TABLE `own_pricing_schemes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission`
--
ALTER TABLE `permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `physical_year`
--
ALTER TABLE `physical_year`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_unit_id` (`unit_id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase`
--
ALTER TABLE `purchase`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_transport_id` (`transport_id`),
  ADD KEY `fk_sales_rep` (`order_booked_id`),
  ADD KEY `fk_invoice_verified` (`invoice_verified_by`);

--
-- Indexes for table `purchase_document`
--
ALTER TABLE `purchase_document`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_items`
--
ALTER TABLE `purchase_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_purchse_id` (`purchase_id`),
  ADD KEY `fk_product_id` (`product_id`);

--
-- Indexes for table `purchase_return`
--
ALTER TABLE `purchase_return`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_return_items`
--
ALTER TABLE `purchase_return_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_return_id` (`purchase_return_id`),
  ADD KEY `fk_reutn_product` (`product_id`);

--
-- Indexes for table `sale`
--
ALTER TABLE `sale`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_customer` (`customer_id`);

--
-- Indexes for table `sales_rep`
--
ALTER TABLE `sales_rep`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sale_document`
--
ALTER TABLE `sale_document`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sale_items`
--
ALTER TABLE `sale_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sale_id` (`sale_id`),
  ADD KEY `fk_sale_product` (`product_id`);

--
-- Indexes for table `sale_return`
--
ALTER TABLE `sale_return`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sr_sale` (`sale_id`),
  ADD KEY `fk_sl_customr` (`from_customer_id`),
  ADD KEY `fk_sl_transport` (`transport_id`);

--
-- Indexes for table `sale_return_items`
--
ALTER TABLE `sale_return_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sl_return_id` (`sale_return_id`),
  ADD KEY `fk_sl_return_pro` (`product_id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transport`
--
ALTER TABLE `transport`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `codes`
--
ALTER TABLE `codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `code_options`
--
ALTER TABLE `code_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `company_poc`
--
ALTER TABLE `company_poc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `deposit_withdraw`
--
ALTER TABLE `deposit_withdraw`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `employee_bank_info`
--
ALTER TABLE `employee_bank_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `employee_document`
--
ALTER TABLE `employee_document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `expense`
--
ALTER TABLE `expense`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `inventory`
--
ALTER TABLE `inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notes`
--
ALTER TABLE `notes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `owner`
--
ALTER TABLE `owner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `own_bank_info`
--
ALTER TABLE `own_bank_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `own_categories`
--
ALTER TABLE `own_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `own_doc_number`
--
ALTER TABLE `own_doc_number`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `own_payment_term`
--
ALTER TABLE `own_payment_term`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `own_pricing_schemes`
--
ALTER TABLE `own_pricing_schemes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `permission`
--
ALTER TABLE `permission`
  MODIFY `id` int(22) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `physical_year`
--
ALTER TABLE `physical_year`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `purchase`
--
ALTER TABLE `purchase`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `purchase_document`
--
ALTER TABLE `purchase_document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `purchase_items`
--
ALTER TABLE `purchase_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `purchase_return`
--
ALTER TABLE `purchase_return`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `purchase_return_items`
--
ALTER TABLE `purchase_return_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `sale`
--
ALTER TABLE `sale`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sales_rep`
--
ALTER TABLE `sales_rep`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sale_items`
--
ALTER TABLE `sale_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sale_return`
--
ALTER TABLE `sale_return`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sale_return_items`
--
ALTER TABLE `sale_return_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `transport`
--
ALTER TABLE `transport`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `code_options`
--
ALTER TABLE `code_options`
  ADD CONSTRAINT `fk_options` FOREIGN KEY (`code_id`) REFERENCES `codes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `company`
--
ALTER TABLE `company`
  ADD CONSTRAINT `fk_bank` FOREIGN KEY (`bank_id`) REFERENCES `bank` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `company_poc`
--
ALTER TABLE `company_poc`
  ADD CONSTRAINT `fk_comp_pos` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `customer`
--
ALTER TABLE `customer`
  ADD CONSTRAINT `fk_bak_aus` FOREIGN KEY (`bank_id`) REFERENCES `bank` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `employee`
--
ALTER TABLE `employee`
  ADD CONSTRAINT `fk_bank_emp` FOREIGN KEY (`bank_id`) REFERENCES `bank` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `fk_unit_id` FOREIGN KEY (`unit_id`) REFERENCES `code_options` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `purchase`
--
ALTER TABLE `purchase`
  ADD CONSTRAINT `fk_invoice_verified` FOREIGN KEY (`invoice_verified_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_sales_rep` FOREIGN KEY (`order_booked_id`) REFERENCES `code_options` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_transport_id` FOREIGN KEY (`transport_id`) REFERENCES `transport` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `purchase_return_items`
--
ALTER TABLE `purchase_return_items`
  ADD CONSTRAINT `fk_return_id` FOREIGN KEY (`purchase_return_id`) REFERENCES `purchase_return` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_reutn_product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sale`
--
ALTER TABLE `sale`
  ADD CONSTRAINT `fk_customer` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sale_items`
--
ALTER TABLE `sale_items`
  ADD CONSTRAINT `fk_sale_id` FOREIGN KEY (`sale_id`) REFERENCES `sale` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_sale_product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sale_return`
--
ALTER TABLE `sale_return`
  ADD CONSTRAINT `fk_sl_customr` FOREIGN KEY (`from_customer_id`) REFERENCES `customer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_sl_transport` FOREIGN KEY (`transport_id`) REFERENCES `transport` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_sr_sale` FOREIGN KEY (`sale_id`) REFERENCES `sale` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sale_return_items`
--
ALTER TABLE `sale_return_items`
  ADD CONSTRAINT `fk_sl_return_id` FOREIGN KEY (`sale_return_id`) REFERENCES `sale_return` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_sl_return_pro` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
